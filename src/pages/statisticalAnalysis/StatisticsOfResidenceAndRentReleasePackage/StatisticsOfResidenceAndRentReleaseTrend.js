/*
 * @Author: 程家兴
 * @Date: 2019-05-06 13:50:01 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-06-25 15:45:39
 * @file 进驻退租统计-趋势图组件
 */
import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';

export default class Trend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
    render() {
        let dataArray1 = this.props.data && this.props.data.length > 0 && this.props.data.slice(0, 12);
        let dataArray2 = this.props.data && this.props.data.length > 0 && this.props.data.slice(12, 24);
        let dataArray3 = this.props.data && this.props.data.length > 0 && this.props.data.slice(24, 36);
        let xAxisData = [];
        let yAxisData1 = [];
        let yAxisData2 = [];
        let yAxisData3 = [];

        for (let i = 0; i < dataArray1.length; i++) {
            xAxisData.push(dataArray1 && dataArray1.length > 0 && dataArray1[i].monthId);
            yAxisData1.push(dataArray1 && dataArray1.length > 0 && dataArray1[i].companyCnt ? dataArray1[i].companyCnt : 0);
            yAxisData2.push(dataArray2 && dataArray2.length > 0 && dataArray2[i].companyCnt ? dataArray2[i].companyCnt : 0);
            yAxisData3.push(dataArray3 && dataArray3.length > 0 && dataArray3[i].companyCnt ? dataArray3[i].companyCnt : 0);
        }

        const option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['流失企业', '退租企业', '入驻企业'],
                right: '1%'
            },
            grid: {
                left: '4%',
                right: '5%',
                bottom: '13%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: xAxisData
            },
            yAxis: {
                type: 'value',
                axisLine: {
                    show: false
                }
            },
            dataZoom: [
                {
                    show: true,
                    type: 'slider',
                    realtime: true,
                    bottom: 0,
                    height: 25
                },
                {
                    type: 'inside'
                }
            ],
            series: [{
                name: '流失企业',
                data: yAxisData1,
                type: 'line'
            }, {
                name: '退租企业',
                data: yAxisData2,
                type: 'line'
            }, {
                name: '入驻企业',
                data: yAxisData3,
                type: 'line'
            }]
        };

        return (
            <div>
                <span style={{ fontWeight: 'bold', color: 'black' }}>企业进退租分析</span>
                <ReactEcharts option={option}></ReactEcharts>
            </div>
        )
    }
}