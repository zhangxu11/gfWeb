import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: '国房资产管理系统',
          title: '国房资产管理系统',
          href: '/',
          blankTarget: false,
        }
      ]}
      copyright={
        <Fragment>
          北京国信达数据技术有限公司
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
