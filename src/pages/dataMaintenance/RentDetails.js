import React, { Component } from 'react';

import { Row, Col, Checkbox, Modal, Spin } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';

const BgElement = Element.BgElement;
const CheckboxGroup = Checkbox.Group;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class EnergyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leaseId: '',
      address: "-",
      allIdle: "-",
      allowEndDate: "-",
      allowStartDate: "-",
      approveDate: "-",
      approveNumber: "-",
      approveOrg: "-",
      approveOrgTxt: "-",
      approvePerson: "-",
      buildId: "-",
      buildingCode: "-",
      buildingName: "-",
      checkPerson: "-",
      communityId: "-",
      communityName: "-",
      fillPerson: "-",
      frontageHouse: "-",
      haveApprove: "-",
      haveChecked: "-",
      houseId: "-",
      houseIdle: "-",
      houseIdleTxt: "-",
      houseLeaseBuildingName: "-",
      houseLeaseDPrice: 0,
      houseLeaseFloor: "-",
      houseLeaseRoomNumber: "-",
      houseName: "-",
      issuingAuthority: "-",
      issuingAuthorityTxt: "-",
      leaseEndDate: "-",
      leaseGarea: "-",
      leaseHarea: 0,
      leaseId: 0,
      leaseMonth: 0,
      leaseStartDate: "-",
      leaseType: "-",
      leaseTypeTxt: "-",
      leaseUse: "-",
      locationCode: "-",
      locationId: "-",
      locationNumber: "-",
      oldNature: "-",
      oldNatureTxt: "-",
      orgName: "-",
      partyA: "-",
      partyB: "-",
      rentAll: 0,
      rentCur: 0,
      rentHq: "-",
      rentMac: "-",
      rentOrg: "-",
      rentOrgType: "-",
      rentOrgTypeTxt: "-",
      showLessee: "-",
      showRent: "-",
      storeCamp: "-",
      whereCamp: "-",
      whereCampTxt: "-",
      loading: true,
      fileList: [],
    };
  }
  componentWillMount() {
    const leaseId = this.props.location.query.leaseId;
    this.setState({ leaseId });
    this.getFeeMaintainById(leaseId);
  }
  getFeeMaintainById(leaseId) {
    axios.post(Api.queryOne, { leaseId }).then(res => {
      console.log(res);
      
        this.setState({
          ...res.data,
          loading: false
        });
      
    });
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;
    const optionsWithDisabled = [
      { label: 'Apple', value: 'Apple' },
      { label: 'Pear', value: 'Pear' },
      { label: 'Orange', value: 'Orange', disabled: false },
    ];
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div style={{
          marginBottom: 50
        }}>
          <Spin spinning={this.state.loading}>
            <Row className={styles.w1120}>
              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>基本信息</p>
                      <p>BASIC INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div>
                  <BannerAnim prefixCls='buildingDetailsBannerItem'>
                    {
                      this.state.fileList.map((item, index) => {
                        return (
                          <Element
                            key={index}
                          >
                            <BgElement
                              key="bg"
                              style={{
                                backgroundImage: `url("http://gisams-hitech.cindata.cn/res_file/${item.filePath}")`,
                                backgroundPosition: 'center',
                                height: 260
                              }}
                            />
                          </Element>
                        );
                      })
                    }
                  </BannerAnim>
                </div>
                <div className={styles.br1}>
                  <div>
                    <span>划转单位：{this.state.orgName}</span>
                    &emsp;
               <span>小区名称： {this.state.communityName}</span>
                    &emsp;
               <span>楼栋名称：{this.state.buildingName}</span>
                    &emsp;
               <span>房间名称：{this.state.houseName}</span>
                  </div>
                </div>
              </div>

              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>租赁信息</p>
                      <p>RENT INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <div>
                    <span>出租方（甲方）：{this.state.partyA}</span>
                    &emsp;
               <span>承租方（乙方）：{this.state.partyB}</span>
                    &emsp;
               <span>坐落详细地址：{this.state.address}</span>
                  </div>
                </div>
              </div>

              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>租赁面积</p>
                      <p>RENT AREA</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <span>房屋：{this.state.leaseHarea}</span>
                  &emsp;&emsp;
                <span>场地：{this.state.leaseGarea}</span>
                </div>
              </div>

              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className='contentHeader'>
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>租金</p>
                      <p>RENT</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <span>总额：{this.state.rentAll}</span>
                  &emsp;&emsp;
                <span>当前年租金：{this.state.rentCur}</span>
                  &emsp;&emsp;
                <span>单价(单位:元/平方米/天)：{this.state.houseLeaseDPrice}</span>
                </div>
              </div>

              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>房屋信息</p>
                      <p>HOUSE INFORMATION</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className='contentHeader'>
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>租赁期限</p>
                      <p>TERM OF LEASE</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <span>开始日期：{this.state.leaseStartDate}</span>
                  &emsp;&emsp;
                <span>结束日期：{this.state.leaseEndDate}</span>
                  &emsp;&emsp;
                <span>租赁月数：{this.state.leaseMonth}</span>
                </div>
              </div>
              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className='contentHeader'>
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>租赁许可期限</p>
                      <p>LEASE LICENSE PERIOD</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <span>开始日期：{this.state.allowStartDate}</span>
                  &emsp;&emsp;
                <span>结束日期：{this.state.allowEndDate}</span>
                </div>
              </div>
            </Row>
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default EnergyDetails;
