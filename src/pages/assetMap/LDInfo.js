import React, { Component } from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Spin,
  Select,
  Upload
} from 'antd';
import axios from 'axios';
import Api from '../../services/apis';
import moment from 'moment';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from "./style.less";

const { confirm } = Modal;
const { RangePicker } = DatePicker;
const { Step } = Steps;
const FormItem = Form.Item;
const { Option } = Select;

class LDInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      buildInfo: {},
      editModalVisible: false,
      fileList: [],
      fileIds: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== undefined) {
      this.setState({
        data: nextProps.data
      });
    };
    if (nextProps.data !== this.props.data) {
      this.getBuildInfo(nextProps.data.buildId);
    }
  }

  componentWillMount() {
    if (this.props.data !== undefined) {
      this.setState({
        data: this.props.data
      });
      this.getBuildInfo(this.props.data.buildId);
    };
  }


  handleMenuClick = (url) => {
    var win = window.open(url, '_blank');
    win.focus();
  }
  getBuildInfo = (buildId) => {
    if (!buildId) return;
    axios.post(Api.amGetBuildInfo, { buildingCode: buildId })
      .then((resp) => {
        const { data } = resp;
        const { images } = data;
        const fileList = images ? images.map(image => ({
          uid: image.imageId,
          id: image.imageId,
          url: `http://gisams-hitech.cindata.cn/res_file${image.filePath}`,
          name: image.imageId,
          status: 'done'
        })) : [];

        this.props.form.setFieldsValue({
          buildingCode: data.buildingCode,
          buildingName2: data.buildingName2,
          houseCount: data.houseCount,
          rentAvg: data.rentAvg,
          buildingStructure: data.buildingStructure,
          buildingArea: data.buildingArea,
          rentArea: data.rentArea,
          totalFloor: data.totalFloor,
          location: data.location,
          province: data.province,
          city: data.city,
          area: data.area,
          principal: data.principal,
        });
        this.setState({
          buildInfo: data,
          fileList
        });
      })
  }
  editBuilding = () => {
    const that = this;
    this.props.form.validateFields((err, data) => {
      this.setState({ loading: true })
      if (!err) {
        axios.post(Api.amUpdateBuildInfo, {
          projectCode: that.state.projectCode,
          buildingCode: data.buildingCode,
          buildingName2: data.buildingName2,
          houseCount: data.houseCount,
          rentAvg: data.rentAvg,
          buildingStructure: data.buildingStructure,
          buildingArea: data.buildingArea,
          rentArea: data.rentArea,
          totalFloor: data.totalFloor,
          location: data.location,
          province: data.province,
          city: data.city,
          area: data.area,
          principal: data.principal,
        })
          .then((resp) => {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification.success({ message: data.msg });
              that.setModalVisible(false)
            } else if (data.code == 0) {
              notification.destroy();
              notification.error({ message: data.msg });
            }
          })
      }
      that.setState({ loading: false })
    })
  };
  setModalVisible = (show) => {
    this.setState({
      editModalVisible: show
    })
  }
  handleChange = (info) => {
    this.setState({
      fileList: info.fileList
    })
    if (info.file && info.file.response && info.file.response.status && info.file.response.status === 'success') {
      notification.destroy();
      notification.success({ message: info.file.response.message });
      this.setState({ fileList: info.fileList })
      const { fileIds } = this.state;
      fileIds.push(info.file.response);
      this.setState({ fileIds });

    } else if (info.file && info.file.response && info.file.response.status && info.file.response.status === 'error') {
      this.setState({ fileList: this.state.regionFileList })
      notification.destroy();
      notification.error({ message: info.file.response.message });
    }
  }
  handleRemove = (info) => {
    axios.get(Api.amDeletePic, {
      params: {
        imageId: info.id
      }
    }).then((resp) => {
      notification.success({ message: resp.data.message });
    });
  }
  render() {
    const { buildInfo } = this.state;
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;
    return (
      <div>
        <div className={styles.roomInfo}>
          <div className={styles.filterItem} style={{ fontSize: '14px', paddingTop: '20px' }}>
            {/* <div className={styles.filterHeader}> */}
            <a className={styles.option} href="javascrip:;" onClick={() => this.setModalVisible(true)}>编辑</a>
            {/* </div> */}
            <a href="javascrip:;"
              onClick={this.handleMenuClick.bind(this, `/fe/assetMap/buildingDetails?buildingCode=${this.props.data.buildId}`)}>
              {buildInfo.buildingName2 ? buildInfo.buildingName2 : '-'}
            </a>
            &emsp;

          {buildInfo.buildingArea ? buildInfo.buildingArea : 0}㎡
        </div>
          <div className={styles.filterItem}>
            <div className={styles.filterHeader}>楼栋图片
          </div>
            <div>
              {
                this.state.fileList && this.state.fileList[0] && this.state.fileList[0].url
                  ?
                  <img style={{ width: 220, height: 140, marginLeft: 40 }} src={this.state.fileList[0].url} />
                  :
                  '暂无图片'
              }
            </div>
          </div>
          <div className={styles.filterItem}>
            <div className={styles.filterInner}>
              <span title=''>划转单位：{buildInfo.transferUnit ? buildInfo.transferUnit : '-'}</span>
              <span title=''>地址：{buildInfo.location ? buildInfo.location : '-'}</span>
            </div>
          </div>
        </div>
        <Modal
          title="编辑楼栋"
          visible={this.state.editModalVisible}
          width={830}
          onOk={this.editBuilding}
          onCancel={() => this.setModalVisible(false)}
        >
          <Row gutter={24}>
            <Col span={8}>
              <FormItem label="楼栋编号：">
                {getFieldDecorator('buildingCode', {
                  rules: [{ required: true, message: '请输入楼栋编号！' },
                  { max: 20, message: '文字内容超过20字' }]
                })(<Input placeholder="请输入楼栋编号" disabled={this.state.modalType == "edit"} />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="楼栋名称：">
                {getFieldDecorator('buildingName2', {
                  rules: [{ required: true, message: '请输入楼栋名称！' },
                  { max: 50, message: '文字内容超过50字' }]
                })(<Input placeholder="请输入楼栋名称" />)}
              </FormItem>
            </Col>
            <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
              <FormItem label="房屋数量：">
                {getFieldDecorator('houseCount', {
                  rules: [{ pattern: /^\d*$/, message: '请输入数值！' }]
                })(<Input disabled />)}
              </FormItem>
            </Col>
            <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
              <FormItem label="在租均价：">
                {getFieldDecorator('rentAvg', {
                  rules: [{ pattern: /^[0-9]{1,10}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前10位后两位' }]
                })(<Input disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="建筑结构：">
                {getFieldDecorator('buildingStructure')(
                  <Select placeholder="请选择建筑结构" style={{ width: "100%" }}>
                    <Option value="砖木结构">砖木结构</Option>
                    <Option value="混合结构">混合结构</Option>
                    <Option value="钢筋混凝土结构">钢筋混凝土结构</Option>
                    <Option value="钢结构">钢结构</Option>
                  </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="建筑面积：">
                {getFieldDecorator('buildingArea', {
                  rules: [{ required: true, message: '请输入建筑面积！' },
                  { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                })(<Input placeholder="请输入建筑面积" />)}
              </FormItem>
            </Col>
            <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
              <FormItem label="在租面积：">
                {getFieldDecorator('rentArea', {
                  rules: [{ pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                })(<Input disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="总层数：">
                {getFieldDecorator('totalFloor', {
                  rules: [{ required: true, message: '请输入总层数！' },
                  { pattern: /^[1-9]$|^[1-4]\d$|50/g, message: '层数范围为1-50' }]
                })(<Input placeholder="请输入总层数" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="地理位置：">
                {getFieldDecorator('location', {
                  rules: [{ required: true, message: '请输入地理位置！' }]
                })(<Input placeholder="请输入地理位置" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="所在省份：">
                {getFieldDecorator('province', {
                  rules: [{ required: true, message: '请选择所在省份！' }]
                })(<Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="请选择所在省份"
                  optionFilterProp="children"
                  onChange={(e) => this.getAreaList('2', e)}
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  disabled={this.state.modalType == "edit"}
                >{this.state.province && this.state.province.length > 0 && this.state.province.map(item => <Option key={item.areaId} value={item.name}>{item.name}</Option>)}
                </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="所在市：" style={{ textAlign: 'left' }}>
                {getFieldDecorator('city', {
                  rules: [{ required: true, message: '请选择所在市！' }]
                })(<Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="请选择所在市"
                  optionFilterProp="children"
                  onChange={(e) => this.getAreaList('3', e)}
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  disabled={this.state.modalType == "edit"}
                >{
                    this.state.city && this.state.city.length > 0 && this.state.city.map(item =>
                      <Option key={item.areaId} value={item.name}>{item.name}</Option>
                    )
                  }
                </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="所在区：">
                {getFieldDecorator('area', {
                  rules: [{ required: true, message: '请选择所在区！' }]
                })(<Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="请选择所在区"
                  optionFilterProp="children"
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  disabled={this.state.modalType == "edit"}
                >{
                    this.state.area && this.state.area.length > 0 && this.state.area.map(item =>
                      <Option key={item.areaId} value={item.name}>{item.name}</Option>
                    )
                  }
                </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="负责人：">
                {getFieldDecorator('principal', {
                  rules: [{ required: true, message: '请输入负责人！' },
                  { max: 50, message: '文字内容超过50字' }]
                })(<Input placeholder="请输入负责人" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="楼栋图片：">
                <Upload
                  action={Api.amUploadPic}
                  listType="text"
                  accept='.jpg,.jpeg,.png,.gif'
                  fileList={this.state.fileList}
                  onChange={this.handleChange}
                  onRemove={this.handleRemove}
                  multiple
                  data={{ addressId: this.props.data.buildId }}
                >
                  {/* {<div>
                          <Icon type="plus" />
                          <div>点击上传图片</div>
                        </div>} */}
                  <Button icon="cloud-upload" type="primary" className="evfilterbtn">上传</Button>
                </Upload>
              </FormItem>
            </Col>
          </Row>
        </Modal>
      </div>

    );
  }
}
const WrappedApp = Form.create()(LDInfo);
export default WrappedApp;
