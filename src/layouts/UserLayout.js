import React, { Fragment } from 'react';
import { formatMessage } from 'umi/locale';
import Link from 'umi/link';
import { notification, Icon, Tooltip } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';
import styles from './UserLayout.less';
import logo from '../assets/loginlogo.png';
import Browser from '@/components/Browser';
const b = new Browser();
if (b.browser.toLowerCase() !== 'chrome') {
  const openNotification = () => {
    const args = {
      message: b.browser.toLowerCase() === 'ie' ? '⚠警告️⚠：本产品使用最新技术制作，使用IE会导致部分功能失效' : '⚠警告️⚠',
      description: <div>
        <div>
          <p style={{
            fontSize: 16,
            fontWeight: 'bold'
          }}>
            当前系统信息:
          </p>
          <p>
            浏览器：{b.browser} {b.version}<br />
            操作系统：{b.os} {b.osVersion}<br />
            系统语言：{b.language} <br />
          </p>

          <Tooltip placement="bottom" title='点击下载Chrome离线安装包'>
            <a className='notification' target='_blank' href="https://www.google.com/chrome/?system=true&standalone=1">推荐使用Chrome浏览器</a>
          </Tooltip>获得最佳使用体验。
        </div>
      </div>,
      duration: b.browser.toLowerCase() === 'ie' ? 0 : 10,
      icon: <Icon type="warning" style={{ color: 'red' }} />
    };
    notification.open(args);
  };
  openNotification();
};
const links = [
  {
    key: 'help',
    title: formatMessage({ id: 'layout.user.link.help' }),
    href: '',
  },
  {
    key: 'privacy',
    title: formatMessage({ id: 'layout.user.link.privacy' }),
    href: '',
  },
  {
    key: 'terms',
    title: formatMessage({ id: 'layout.user.link.terms' }),
    href: '',
  },
];

const copyright = (
  <Fragment>
    北京国信达数据技术有限公司
    <p style={{marginTop: '10px'}}>
      网站备案/许可证号：黑ICP备16003638号-4
    </p>
  </Fragment>
);

class UserLayout extends React.PureComponent {
  componentDidMount() {
    console.clear();
    console.log('%c asdjgfr', 'opacity: 0;color:rgba(0,0,0,0);');
  }
  render() {
    const { children } = this.props;
    return (
      // @TODO <DocumentTitle title={this.getPageTitle()}>
      <div className={styles.container} style={{
        backgroundImage: `url(${require('../assets/loginbg.jpg')})`
      }}>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
              <Link to="/">
                <img alt="logo" className={styles.logo} src={logo} />
                <span className={styles.title}></span>
              </Link>
            </div>
          </div>
          {children}
        </div>
        <GlobalFooter links={links} copyright={copyright} />
      </div>
    );
  }
}

export default UserLayout;
