/* 
* @Author: 程家兴  
* @Date: 2019-05-08 16:12:16  
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-29 14:24:49
* @File: 招商分析
*/
import React, { Component } from 'react';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Pie from './InvestmentAnalysisPackage/InvestmentAnalysisPie'
import InvestmentAnalysisBar from './InvestmentAnalysisPackage/InvestmentAnalysisBar'
import { Col, Row } from 'antd';
import styles from './InvestmentAnalysisPackage/style.less'

@connect(({ user, project, activities, chart, loading }) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))
export default class InvestmentAnalysis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pieData1: [],
            pieData2: []
        }
    }
    componentDidMount() {
        Promise.all([
            axios.post(Api.getEnterprise),
            axios.post(Api.getHyType)

        ]).then(dataList => {
            this.setState({
                pieData1: dataList[0].data.list,
                pieData2: dataList[1].data.list
            });
        })
    }

    render() {
        const { currentUserLoading } = this.props;
        return (
            <PageHeaderWrapper
                loading={currentUserLoading}
            >
                <div className="mainContainer">
                    <div className='contentHeader'>
                        <div className="contentHeaderTitle">
                            <p>招商分析</p>
                            <p>INVESTMENT ANALYSIS</p>
                        </div>
                    </div>
                </div>
                <Row type="flex" justify="space-around" style={{ marginBottom: 40 }}>
                    <Col span={10} className={styles.moduleTitle2}>
                        <Pie
                            key={this.state.pieData1}
                            title="招商客户分析"
                            centerTitle="企业分布"
                            data={this.state.pieData1} />
                    </Col>
                    <Col span={10} className={styles.moduleTitle2}>
                        <Pie key={this.state.pieData2}
                            title="招商行业分析"
                            centerTitle="行业分布"
                            data={this.state.pieData2} />
                    </Col>
                </Row>
                <Row type="flex" justify="space-around" style={{ marginBottom: 40, height: 320 }}>
                    <Col span={22}>
                        <InvestmentAnalysisBar />
                    </Col>
                </Row>
            </PageHeaderWrapper>
        );
    }
}