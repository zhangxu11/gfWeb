
//项目管理
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Tabs,
  Spin,
  Tooltip,
  Select
} from 'antd';

import router from 'umi/router';
import Link from 'umi/link';
import { formatTreeData } from '../../components/commonAips';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { func } from 'prop-types';
import styles from './style.less'

const { TextArea } = Input;
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
const { RangePicker } = DatePicker;
const Step = Steps.Step;
const Option = Select.Option;
const FormItem = Form.Item;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class ServiceTrajectory extends React.Component {
  constructor(props) {
    super(props);
    // const token = sessionStorage.getItem('tokenData') ? sessionStorage.getItem('tokenData') : '';
    this.state = {
      name: '',
      tranUnits: '',
      pageSize: 5,
      current: 1,
      tableData: [],
      modalVisible: false,
      selectdIds: [],
      ownerStatus: [],
      ownerType: [],
      signStatus: [],
      assetStatus: [],
      submitFlag: '',
      // 受理人搜索框
      receiver: '',
      // 日期搜索框
      serviceDate: '',
      // 问题分类数据
      questionTypeData: []
    };
  }
  //获取表格数据
  sendData = () => {
    let that = this;
    this.setState({ loading: true })
    axios.post(Api.getServiceLogList, {
      currentPage: that.state.current,
      receiver: that.state.receiver,
      serviceDate: that.state.serviceDate
    })
      .then(function (resp) {
        const data = resp.data;
        if (data.list.length == 0) {
          notification.destroy();
          notification['warning']({
            message: '暂无数据',
          });
        }

        that.setState({
          tableData: data.list,
          pageSize: data.pageSize,
          current: data.currentPage,
          total: data.totalSize
        })
        that.setState({ loading: false })
      })
  };

  componentWillMount() {
    const that = this;
    that.sendData();
    axios.post(Api.getReasonType, {
      remark: '1'
    })
      .then(function (resp) {
        const { data } = resp;
        that.setState({
          questionTypeData: data
        })
      })
  }

  //查询
  query = () => {
    this.setState({ current: 1 }, () => {
      this.sendData();
    })
  };

  //获取项目
  getProject = (id) => {
    this.setState({ loading: true })
    this.setState({ logId: id })
    const that = this;
    axios.post(Api.getDetailServiceLog, { logId: id })
      .then(function (resp) {
        const { data } = resp;
        that.props.form.setFieldsValue({
          roomNum: data.roomNum,
          repairStatus: data.repairStatus,
          content: data.content,
          reportUser: data.reportUser,
          receiver: data.receiver,
          remark: data.remark,
          serviceDate: data.serviceDate,
          renterName: data.renterName,
          question: data.question,
          dealDate: moment(data.dealDate),
          undertake: data.undertake,
          dealResult: data.dealResult,
          questionType: data.questionType,
        });
        that.setModalVisible(true, 'edit', id);
        that.setState({ loading: false });
      })
  };


  //编辑,新建项目(点弹框确定)
  editProject = () => {
    let that = this;
    this.setState({ loading: true })
    if (this.state.submitFlag == 'add') {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          axios.post(Api.createServiceLog, {
            roomNum: values.roomNum,
            serviceDate: values.serviceDate,
            content: values.content,
            reportUser: values.reportUser,
            remark: values.remark,
            repairStatus: values.repairStatus,
            receiver: values.receiver,
            renterName: values.renterName,
            question: values.question,
            dealDate: moment(values.dealDate).format("YYYY-MM-DD"),
            undertake: values.undertake,
            dealResult: values.dealResult,
            questionType: values.questionType,
          })
            .then(function (resp) {
              const { data } = resp;
              if (data.flag == 1) {
                notification.destroy();
                notification['success']({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else if (data.flag == 0) {
                notification.destroy();
                notification['error']({ message: data.msg });
              }
              that.setState({ loading: false })
            })
        }
      })
    } else {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          axios.post(Api.updateServiceLog, {
            remark: values.remark,
            repairStatus: values.repairStatus,
            logId: this.state.logId,
            renterName: values.renterName,
            question: values.question,
            dealDate: moment(values.dealDate).format("YYYY-MM-DD"),
            undertake: values.undertake,
            dealResult: values.dealResult,
            questionType: values.questionType,
          })
            .then(function (resp) {
              const { data } = resp;
              if (data.flag == 1) {
                notification.destroy();
                notification['success']({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else if (data.flag == 0) {
                notification.destroy();
                notification['error']({ message: data.msg });
              }
              that.setState({ loading: false })
            })
        }
      })
    }

  };

  cancel = () => {
    this.setModalVisible(false)
    this.setState({ loading: false })
  }

  changeName = e => {
    this.setState({ name: e.target.value });
  };

  changeTranUnits = e => {
    this.setState({ tranUnits: e.target.value });
  };

  pageChange = page => {
    this.setState(
      {
        current: page,
      },
      () => {
        this.sendData();
      }
    );
  };

  setModalVisible = (visible, type, id) => {
    let that = this;
    this.setState({ modalVisible: visible, submitFlag: type });
    if (type == 'add' && visible) {
      that.props.form.setFieldsValue({

      })
    }
    if (!visible) {
      that.props.form.setFieldsValue({
        repairStatus: '',
        content: '',
        reportUser: '',
        receiver: '',
        remark: '',
        serviceDate: '',
        roomNum: '',
        renterName: '',
        question: '',
        dealDate: '',
        undertake: '',
        dealResult: '',
        questionType: '',
      })
    }
  };

  changeSelected = (selectedRowKeys, selectedRows) => {
    let selectdIds = [];
    selectedRows && selectedRows.forEach((d, i, arr) => {
      selectdIds.push(d.projectCode);
    });
    this.setState({ selectedArr: selectedRowKeys, selectdIds: selectdIds });
  };

  handleMenuClick = (project) => {
    var win = window.open('/fe/assetManagement/projectInfo?projectCode=' + project.projectCode, '_blank');
    win.focus();
  }

  resetForm = () => {
    this.props.form.setFieldsValue({
      s_name: '',
      s_date: ''
    })
  }
  // 受理人搜索框输入内容
  receiverChange = (e) => {
    this.setState({
      receiver: e.target.value
    })
  }
  // 日期搜索框输入内容
  serviceDateChange = (e) => {
    this.setState({
      serviceDate: e ? moment(e).format('YYYY-MM-DD') : ''
    })
  }
  render() {
    let that = this;
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;

    //表格表头
    const columns = [
      {
        title: '房间号',
        dataIndex: 'roomNum',
        key: 'roomNum',
      },
      {
        title: '日期',
        dataIndex: 'serviceDate',
        key: 'serviceDate',
      },
      {
        title: '内容',
        dataIndex: 'content',
        key: 'content',
      },
      {
        title: '受理人',
        dataIndex: 'receiver',
        key: 'receiver',
      },
      {
        title: '上报人',
        dataIndex: 'reportUser',
        key: 'reportUser',
      },
      {
        title: '修理情况',
        dataIndex: 'repairStatus',
        key: 'repairStatus',
      },
      {
        title: '备注',
        dataIndex: 'remark',
        key: 'remark',
      },
      {
        title: '企业名称',
        dataIndex: 'renterName',
        key: 'renterName',
      },
      {
        title: '问题或意见',
        dataIndex: 'question',
        key: 'question',
      },
      {
        title: '处理时限',
        dataIndex: 'dealDate',
        key: 'dealDate',
      },
      {
        title: '承办',
        dataIndex: 'undertake',
        key: 'undertake',
      },
      {
        title: '处理结果',
        dataIndex: 'dealResult',
        key: 'dealResult',
      },
      {
        title: '问题分类',
        dataIndex: 'questionType',
        key: 'questionType',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <span>
            <a href="javascript:;" onClick={this.getProject.bind(this, item.logId)}>编辑</a>
          </span>
        ),
      },
    ];

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>服务轨迹</p>
              <p>SERVICE TRAJECTORY</p>
            </div>
          </div>
          <Form layout="inline" align='center'>
            <FormItem label="受理人：">
              {
                getFieldDecorator('s_name')(
                  <Input placeholder="请输入受理人" onChange={this.receiverChange} />
                )
              }
            </FormItem>

            <FormItem label="日期：">
              {
                getFieldDecorator('s_date')(
                  <DatePicker style={{ width: "100%" }} onChange={this.serviceDateChange} />
                )
              }
            </FormItem>
            <Row>
              <Col span={24} style={{ textAlign: 'center', marginBottom: '10px' }}>
                <Button
                  type="primary"
                  onClick={this.setModalVisible.bind(this, true, 'add')}
                  htmlType="reset" icon="plus-circle"
                >
                  新增
                </Button>
                &emsp;
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>
                  搜索
                </Button>
                &emsp;
                <Button
                  onClick={this.resetForm.bind(this)}
                  type="primary"
                  htmlType="reset"
                  icon="reload"
                >
                  重置
                </Button>
              </Col>
            </Row>
          </Form>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered={true}
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.tableData}
                rowKey={record => {
                  return record.logId
                }}
                pagination={{
                  pageSize: that.state.pageSize,
                  current: that.state.current,
                  onChange: that.pageChange,
                  total: that.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>
            <Modal
              title={this.state.submitFlag == 'add' ? "新建服务轨迹" : "编辑服务轨迹"}
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.modalVisible}
              onOk={this.editProject}
              onCancel={this.cancel}
              width={830}>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="房间号：">
                    {getFieldDecorator('roomNum', {
                      rules: [
                        { required: true, message: '请输入房间号！' },
                        { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入房间号" disabled={this.state.submitFlag == 'add' ? false : true} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="日期：">
                    {this.state.submitFlag == 'add' ?
                      getFieldDecorator('serviceDate', {
                        rules: [
                          { required: true, message: '请选择日期！' }]
                      })(<DatePicker style={{ width: "100%" }} />)
                      :
                      getFieldDecorator('serviceDate')(<Input placeholder="请输入日期" disabled />)
                    }
                  </FormItem>
                </Col>

                <Col span={8}>
                  <FormItem label="内容：">
                    {getFieldDecorator('content', {
                      rules: [
                        { required: true, message: '请输入内容！' },
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(
                      <Input placeholder="请输入内容" disabled={this.state.submitFlag == 'add' ? false : true} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="受理人：">
                    {getFieldDecorator('reportUser', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入受理人" disabled={this.state.submitFlag == 'add' ? false : true} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="上报人：">
                    {getFieldDecorator('receiver', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入上报人" disabled={this.state.submitFlag == 'add' ? false : true} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="修理情况：">
                    {getFieldDecorator('repairStatus', {
                      rules: [{ message: '请输入修理情况！' }]
                    })(<Select placeholder="请选择修理情况" style={{ width: "100%" }} >
                      <Option value='未修理'>未修理</Option>
                      <Option value='已修理'>已修理</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="备注：">
                    {getFieldDecorator('remark', {
                      rules: [
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入备注" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="企业名称：">
                    {getFieldDecorator('renterName', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入企业名称" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="问题或意见：">
                    {getFieldDecorator('question', {
                      rules: [
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入问题或意见" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="处理时限：">
                    {getFieldDecorator('dealDate')
                      (<DatePicker style={{ width: "100%" }} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="承办：">
                    {getFieldDecorator('undertake', {
                      rules: [
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入承办" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="处理结果：">
                    {getFieldDecorator('dealResult', {
                      rules: [
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入处理结果" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="问题分类：">
                    {getFieldDecorator('questionType')(<Select placeholder="请选择问题分类" >
                      {
                        this.state.questionTypeData.map((item, k) => (
                          <Option value={item.dic_code} key={item.dic_code}>{item.dic_name}</Option>
                        ))
                      }
                    </Select>)}
                  </FormItem>
                </Col>
              </Row>
            </Modal>
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(ServiceTrajectory);
export default WrappedApp;