import React, { Component } from 'react';

import styles from "./style.less";

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        buildingArea: 0,
        buildingCount: 0,
        houseCount: 0,
        landArea: 0,
        projectCount: 0,
        rentingArea: 0,
        waitRentArea: 0,
        xArr: [],
        xArrFee: [],
        yArr: [],
        yArrFee: [],
        cityData: {

        }
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== undefined) {
      this.setState({
        data: nextProps.data
      });
    };

    if (nextProps.cityData.data !== undefined) {
      this.setState({
        cityData: nextProps.cityData.data
      });
    };
  }

  componentWillMount() {
    if (this.props.data !== undefined) {
      this.setState({
        data: this.props.data
      });
    };

    if (this.props.cityData.data !== undefined) {
      this.setState({
        cityData: this.props.cityData.data
      });
    };
  }

  render() {
    return (
      <div className={styles.infoTxt}>
        <h1>
          {function () {
            if (!this.state.cityData) {
              return '全国';
            } else if (this.state.cityData.district) {
              return this.state.cityData.district;
            } else if (this.state.cityData.city) {
              return this.state.cityData.city;
            } else if (this.state.cityData.province) {
              return this.state.cityData.province;
            } else {
              return '-';
            };
          }.bind(this)()}
        </h1>
        <div>
          <span>项目数量：</span>
          <span>{this.state.data.projectCount}&emsp;个</span>
        </div>
        <div>
          <span>楼栋数量：</span>
          <span>{this.state.data.buildingCount}&emsp;栋</span>
        </div>
        <div>
          <span>房屋数量：</span>
          <span>{this.state.data.houseCount}&emsp;套</span>
        </div>
        <div>
          <span>建筑总面积：</span>
          <span>{this.state.data.buildingArea}&emsp;m²</span>
        </div>
        <div>
          <span>土地总面积：</span>
          <span>{this.state.data.landArea}&emsp;m²</span>
        </div>
        <div>
          <span>闲置建筑面积：</span>
          <span>{this.state.data.waitRentArea}&emsp;m²</span>
        </div>
        <div>
          <span>出租建筑面积：</span>
          <span>{this.state.data.rentingArea}&emsp;m²</span>
        </div>
      </div>
    );
  }
}

export default Info;
