import { Row, Col } from 'antd';
import router from 'umi/router';
import { formatNumberWithComma } from '@/utils/utils';
import React, { Component } from 'react';

import styles from "./style.less";

// data: {
// buildingCount: 0, //楼宇数量
// houseCount: 0, // 房屋数量
// totalArea: 0, // 总面积
// totalRentalArea: 0, // 在租面积
// percentageOfRentalArea: '', // 在租面积比例
// totalVacantArea: 0, // 空置面积
// percentageOfVacantArea: '', // 空置面积比例
// avgPrice: '0', // 在租均价
// }
export default class Part1 extends Component {
  jump = (type) => {
    switch (type) {
      case "buildingCount":
        // TODO:跳转到楼栋管理页面
        router.push('/fe/assetManagement/buildingManagement');
        break;
      case "houseCount":
        // TODO:跳转到楼栋管理页面
        router.push('/fe/assetManagement/houseManagement');
        break;
      default:
        break;
    }
  }
  onRentChartClick = (params) => {
    router.push({
      pathname: '/fe/assetManagement/houseManagement',
      state: {
        isRent: params
      }
    });
  }
  render() {
    const { data } = this.props;
    return (
      <React.Fragment>
        {/* <Row>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>资产总量</p>
                <p>TOTAL ARREARS</p>
              </div>
            </div>
          </div>
        </Row> */}
        <Row>
          <Col onClick={() => this.jump('buildingCount')} span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-01-bg.png')})` }}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-01-icon.png')} />楼宇总量</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.buildingCount)}</span><span className={styles.part1ContentBodyUnit}>栋</span></p>
          </Col>
          <Col onClick={() => this.jump('houseCount')} span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-02-bg.png')})` }}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-02-icon.png')} />房屋总量</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.houseCount)}</span><span className={styles.part1ContentBodyUnit}>套</span></p>
          </Col>
          <Col span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-03-bg.png')})` }}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-03-icon.png')} />租赁面积</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.totalArea)}</span><span className={styles.part1ContentBodyUnit}>㎡</span></p>
          </Col>
          {/* <Col span={4} className={styles.part1Content} style={{backgroundImage: `url(${require('../../assets/homepage-04-bg.png')})`}}>
            <p className={styles.part1ContentTitle}><img style={{marginRight: '2px'}} src={require('../../assets/homepage-04-icon.png')} />管理面积</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma((Number(data.building_area) + Number(data.house_area) + Number(data.use_area)).toFixed(2))}</span><span className={styles.part1ContentBodyUnit}>㎡</span></p>
          </Col> */}
          <Col span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-05-bg.png')})` }} onClick={() => this.onRentChartClick('租赁中')}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-05-icon.png')} />在租面积</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.totalRentalArea)}</span><span className={styles.part1ContentBodyUnit}>㎡</span></p>
          </Col>
          <Col span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-06-bg.png')})` }} onClick={() => this.onRentChartClick('待租赁')}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-06-icon.png')} />空置面积</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.totalVacantArea)}</span><span className={styles.part1ContentBodyUnit}>㎡</span></p>
          </Col>
          <Col span={4} className={styles.part1Content} style={{ backgroundImage: `url(${require('../../assets/homepage-07-bg.png')})` }}>
            <p className={styles.part1ContentTitle}><img style={{ marginRight: '2px' }} src={require('../../assets/homepage-07-icon.png')} />在租均价</p>
            <p><span className={styles.part1ContentBodyCount}>{formatNumberWithComma(data.avgPrice)}</span><span className={styles.part1ContentBodyUnit}>元/㎡年</span></p>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}
