//**********************************
//基础功能类-弹出框
//Power by han 2018.07.05
//**********************************
var objInfowindow = {
    pMap:null,//存储地图对象
    init: function (m) { //初始化类对象
        this.pMap = m;
    },
    setMap: function (e) { //设置地图对象
        this.pMap = e;
    },
    getMap: function () { //获取地图对象
        return this.pMap;
    },
    paramTest: function () { //参数验证
        if (this.pMap == null) {
            alert("objInfowindow.js error 'map'");
            console.log("地图未设置");
            return false;
        }
        return true;
    },
    show:function(pt, title, content, width, height){
        if (this.paramTest() == false) return;
        var infoWin = this.pMap.infoWindow;
        infoWin.setTitle(title);
        infoWin.setContent(content);
        infoWin.resize(width, height);
        infoWin.show(this.pMap.toScreen(pt), this.pMap.getInfoWindowAnchor(pt));
    }
}

