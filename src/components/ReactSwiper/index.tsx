import * as React from 'react';
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css';

interface SwiperOptions {
  initialSlide?: number;
  direction?: string;
  speed?: number;
  setWrapperSize?: boolean;
  virtualTranslate?: boolean;
  width?: number;
  height?: number;
  autoHeight?: boolean;
  roundLengths?: boolean;
  nested?: boolean;

  // Autoplay
  autoplay?: number;
  autoplayStopOnLast?: boolean;
  autoplayDisableOnInteraction?: boolean;

  // Progress
  watchSlidesProgress?: boolean;
  watchSlidesVisibility?: boolean;

  // Freemode
  freeMode?: boolean;
  freeModeMomentum?: boolean;
  freeModeMomentumRatio?: number;
  freeModeMomentumVelocityRatio?: number;
  freeModeMomentumBounce?: boolean;
  freeModeMomentumBounceRatio?: number;
  freeModeMinimumVelocity?: number;
  freeModeSticky?: boolean;

  // Effects
  effect?: string;
  fade?: {};
  cube?: {};
  coverflow?: {};
  flip?: {};

  // Parallax
  parallax?: boolean;

  // Slides grid
  spaceBetween?: number;
  slidesPerView?: number | string;
  slidesPerColumn?: number;
  slidesPerColumnFill?: string;
  slidesPerGroup?: number;
  centeredSlides?: boolean;
  slidesOffsetBefore?: number;
  slidesOffsetAfter?: number;

  // Grab Cursor
  grabCursor?: boolean;

  // Touches
  touchEventsTarget?: string;
  touchRatio?: number;
  touchAngle?: number;
  simulateTouch?: boolean;
  shortSwipes?: boolean;
  longSwipes?: boolean;
  longSwipesRatio?: number;
  longSwipesMs?: number;
  followFinger?: boolean;
  onlyExternal?: boolean;
  threshold?: number;
  touchMoveStopPropagation?: boolean;
  iOSEdgeSwipeDetection?: boolean;
  iOSEdgeSwipeThreshold?: number;

  // Touch Resistance
  resistance?: boolean;
  resistanceRatio?: number;

  // Clicks
  preventClicks?: boolean;
  preventClicksPropagation?: boolean;
  slideToClickedSlide?: boolean;

  // Swiping / No swiping
  allowSwipeToPrev?: boolean;
  allowSwipeToNext?: boolean;
  noSwiping?: boolean;
  noSwipingClass?: string;
  swipeHandler?: string | Element;

  // Navigation Controls
  uniqueNavElements?: boolean;

  // Pagination
  pagination?: string | Element;
  paginationType?: string;
  paginationHide?: boolean;
  paginationClickable?: boolean;
  paginationElement?: string;
  paginationBulletRender?(swiper: Swiper, index: number, className: string): void;
  paginationFractionRender?(swiper: Swiper, currentClassName: string, totalClassName: string): void;
  paginationProgressRender?(swiper: Swiper, progressbarClass: string): void;
  paginationCustomRender?(swiper: Swiper, current: number, total: number): void;

  // Navigation Buttons
  nextButton?: string | Element;
  prevButton?: string | Element;

  // Scollbar
  scrollbar?: string | Element | SwiperScrollbarOptions;
  scrollbarHide?: boolean;
  scrollbarDraggable?: boolean;
  scrollbarSnapOnRelease?: boolean;

  // Accessibility
  a11y?: boolean;
  prevSlideMessage?: string;
  nextSlideMessage?: string;
  firstSlideMessage?: string;
  lastSlideMessage?: string;
  paginationBulletMessage?: string;

  // Keyboard / Mousewheel
  keyboardControl?: boolean;
  mousewheelControl?: boolean;
  mousewheelForceToAxis?: boolean;
  mousewheelReleaseOnEdges?: boolean;
  mousewheelInvert?: boolean;
  mousewheelSensitivity?: number;

  // Hash Navigation
  hashnav?: boolean;
  hashnavWatchState?: boolean;
  history?: string;

  // Images
  preloadImages?: boolean;
  updateOnImagesReady?: boolean;
  lazyLoading?: boolean;
  lazyLoadingInPrevNext?: boolean;
  lazyLoadingInPrevNextAmount?: number;
  lazyLoadingOnTransitionStart?: boolean;

  // Loop
  loop?: boolean;
  loopAdditionalSlides?: number;
  loopedSlides?: number;

  zoom?: boolean;

  // Controller
  control?: Swiper;
  controlInverse?: boolean;
  controlBy?: string;

  // Observer
  observer?: boolean;
  observeParents?: boolean;

  // Breakpoints
  breakpoints?: {};

  // Callbacks
  runCallbacksOnInit?: boolean;
  onInit?(swiper: Swiper): void;
  onSlideChangeStart?(swiper: Swiper): void;
  onSlideChangeEnd?(swiper: Swiper): void;
  onSlideNextStart?(swiper: Swiper): void;
  onSlideNextEnd?(swiper: Swiper): void;
  onSlidePrevStart?(swiper: Swiper): void;
  onSlidePrevEnd?(swiper: Swiper): void;
  onTransitionStart?(swiper: Swiper): void;
  onTransitionEnd?(swiper: Swiper): void;
  onTouchStart?(swiper: Swiper, event: Event): void;
  onTouchMove?(swiper: Swiper, event: Event): void;
  onTouchMoveOpposite?(swiper: Swiper, event: Event): void;
  onSliderMove?(swiper: Swiper, event: Event): void;
  onTouchEnd?(swiper: Swiper, event: Event): void;
  onClick?(swiper: Swiper, event: Event): void;
  onTap?(swiper: Swiper, event: Event): void;
  onDoubleTap?(swiper: Swiper, event: Event): void;
  onImagesReady?(swiper: Swiper): void;
  onProgress?(swiper: Swiper, progress: number): void;
  onReachBeginning?(swiper: Swiper): void;
  onReachEnd?(swiper: Swiper): void;
  onDestroy?(swiper: Swiper): void;
  onSetTranslate?(swiper: Swiper, translate: any): void;
  onSetTransition?(swiper: Swiper, transition: any): void;
  onAutoplay?(swiper: Swiper): void;
  onAutoplayStart?(swiper: Swiper): void;
  onAutoplayStop?(swiper: Swiper): void;
  onLazyImageLoad?(swiper: Swiper, slide: any, image: any): void;
  onLazyImageReady?(swiper: Swiper, slide: any, image: any): void;
  onPaginationRendered?(swiper: Swiper, paginationContainer: any): void;

  // Namespace
  slideClass?: string;
  slideActiveClass?: string;
  slideVisibleClass?: string;
  slideDuplicateClass?: string;
  slideNextClass?: string;
  slidePrevClass?: string;
  wrapperClass?: string;
  bulletClass?: string;
  bulletActiveClass?: string;
  paginationHiddenClass?: string;
  paginationCurrentClass?: string;
  paginationTotalClass?: string;
  paginationProgressbarClass?: string;
  buttonDisabledClass?: string;
}

interface IProps {
  children: React.ReactChildren,
  options: SwiperOptions,
  needPagination?: boolean;
  needScrollbar?: boolean;
  needNavigation?: boolean;
  swiperId?: string,
}

export default class ReactSwiper extends React.Component<IProps, any> {
  private swiperContainer: HTMLDivElement;
  private swiperPagination: HTMLDivElement;
  private swiperButtonPrev: HTMLDivElement;
  private swiperButtonNext: HTMLDivElement;
  private swiperScrollbar: HTMLDivElement;
  private swiperInstance: Swiper;

  constructor(props: IProps) {
    super(props);
  }

  public componentDidMount() {
    this.swiperInstance = new Swiper(this.swiperContainer, this.buildOptions());
  }

  public componentDidUpdate(prevProps: Readonly<IProps>) {
    if ((this.props.children as any).length !== (prevProps.children as any).length) this.swiperInstance.update();
  }

  public render() {
    const { 
      swiperId,
      children,
      needScrollbar,
      options,
    } = this.props;
    return (
      <div id={swiperId || null} className='swiper-container' ref={(self) => this.swiperContainer = self}>
        <div className="swiper-wrapper">
          {children.map(child => (<div className="swiper-slide" key={child.key}>{child}</div>))}
        </div>
        {/* 如果需要分页器 */}
        { (children as any).length > options.slidesPerView && <div className="swiper-pagination" ref={(self) => this.swiperPagination = self}/> }
        {/* 如果需要导航按钮 */}
        { (children as any).length > options.slidesPerView && <div className="swiper-button-prev swiper-button-black" style={{height: 18}} ref={(self) => this.swiperButtonPrev = self}/> }
        { (children as any).length > options.slidesPerView && <div className="swiper-button-next swiper-button-black" style={{height: 18}} ref={(self) => this.swiperButtonNext = self}/> }
        {/* 如果需要滚动条 */}
        { needScrollbar && <div className="swiper-scrollbar" ref={(self) => this.swiperScrollbar = self}/> }
      </div>
    )
  }

  /**
   * build swiper options
   *
   * @private
   * @memberof ReactSwiper
   */
  private buildOptions = () => {
    const options = {...this.props.options};
    if (this.props.needPagination) {
      options.pagination = this.swiperPagination;
      options.paginationClickable = true;
      // options.pagination = {
      //   el: this.swiperPagination,
      //   clickable: options.pagination && Object.prototype.hasOwnProperty.call(options.pagination, 'clickable') ? options.pagination.clickable : true,
      // };
    }
    if (this.props.needScrollbar) {
      options.scrollbar = this.swiperScrollbar;
      // options.scrollbar = {
      //   el: this.swiperScrollbar,
      // };
    }
    if (this.props.needNavigation) {
      options.prevButton = this.swiperButtonPrev;
      options.nextButton = this.swiperButtonNext;
    //   options.navigation = {
    //     nextEl: this.swiperButtonPrev,
    //     prevEl: this.swiperButtonNext,
    //   };
    }
    return options;
  }
}