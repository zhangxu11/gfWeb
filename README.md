# 大连国房

#### 项目介绍

大连国房前端部分

#### 软件架构

采用前后端分离开发

###### 前端架构:

1. 前端框架采用[`React`](https://reactjs.org/)
2. 路由管理使用基于[`react-router`](https://reacttraining.com/react-router/)的[`UmiJS`](https://umijs.org/zh/)
3. JavaScript 状态容器使用基于[`Redux`](https://redux.js.org/)的[`DvaJS`](https://dvajs.com/)
4. ~~图表使用[`Echarts`](http://echarts.baidu.com/)~~基于[`AntV`](https://antv.alipay.com/zh-cn/index.html)的[`BizCharts`](https://alibaba.github.io/BizCharts/)
5. 动画组件使用[`Ant Motion`](https://ant-motion.gitee.io/)
6. 网络请求使用[`Axios`](https://github.com/axios/axios)
7. `UI`框架使用[`Ant Design`](https://ant.design/index-cn)
8. 包管理工具使用[`Yarn`](https://yarnpkg.com/)
9. `Cookie`管理使用[`js-cookie`](https://github.com/js-cookie/js-cookie)


###### 后端架构:

1. xxx

#### 使用说明
1. 安装[nodeJs LTS版](https://nodejs.org)
2. 启动项目
```bash
$ cd 项目目录
$ npm install       # 速度慢使用[cnpm](https://npm.taobao.org/)
$ npm start         # 访问 http://localhost:8000
```
3. 打包
```bash
$ npm run build
```



#### 目录结构
```bash
├── config                   # umi 配置，包含路由，构建等配置
├── mock                     # 本地模拟数据
├── public
│   └── favicon.png          # Favicon
├── src
│   ├── assets               # 本地静态资源
│   ├── components           # 业务通用组件
│   ├── e2e                  # 集成测试用例
│   ├── layouts              # 通用布局
│   ├── models               # 全局 dva model
│   ├── pages                # 业务页面入口和常用模板
│   ├── services             # 后台接口服务
│   ├── utils                # 工具库
│   ├── locales              # 国际化资源
│   ├── global.less          # 全局样式
│   └── global.js            # 全局 JS
├── tests                    # 测试工具
├── README.md
└── package.json
```

#### 模板

```bash
- Dashboard
  - 分析页
  - 监控页
  - 工作台
- 表单页
  - 基础表单页
  - 分步表单页
  - 高级表单页
- 列表页
  - 查询表格
  - 标准列表
  - 卡片列表
  - 搜索列表（项目/应用/文章）
- 详情页
  - 基础详情页
  - 高级详情页
- 用户
  - 用户中心页
  - 用户设置页
- 结果
  - 成功页
  - 失败页
- 异常
  - 403 无权限
  - 404 找不到
  - 500 服务器出错
- 帐户
  - 登录
  - 注册
  - 注册成功
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request.

[^最后修改]: liunian