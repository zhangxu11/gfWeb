import React, { Component, PureComponent } from 'react';

import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import {
  Table,
  Col,
  Card,
  Row,
  Icon,
  Input,
  Spin,
  Button,
  Modal,
  message,
  DatePicker,
  Tag
} from 'antd';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import ReactEcharts from 'echarts-for-react';
import axios from 'axios';
import classNames from 'classnames';
import moment from 'moment';
import Api from '../../services/apis';
import router from 'umi/router';
import TimeSelectorWithMonthYear from '@/components/TimeSelector';

import styles from "./style.less";

const { RangePicker } = DatePicker;
const { Search } = Input;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
export default class Part3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartLoading: false,
      startDate: moment().startOf('year'), // 开始时间
      endDate: moment().endOf('year'), // 结束时间
      xAxisData: [], // x轴坐标
      seriesData: [], // x轴数据
    };
  }

  componentDidMount() {
    this.getData(this.state.startDate, this.state.endDate);
  }

  getData = (startDate, endDate) => {
    const startDateYear = moment().startOf('year');
    const endDateYear = moment().endOf('year');
    let checkYear = false;
    if (startDate.isSame(startDateYear, 'day') && endDate.isSame(endDateYear, 'day')) checkYear = true;
    axios.post(Api.getEnterpriseStatistics, {
      startDate: startDate.format("YYYY-MM-DD"),
      endDate: endDate.format("YYYY-MM-DD"),
      byDay: !checkYear,
    }).then((res) => {
      if (res.data.code === '200') {
        const data = res.data.result;
        const xAxisData = this.buildXAxisData(data);
        const seriesData = this.buildSeriesData(data);
        this.setState({ xAxisData, seriesData });
      }
    })
  }

  buildXAxisData = (data) => {
    return data.map(d => d.dateAlias);
  }

  buildSeriesData = (data) => {
    let a = 0;
    return data.map(d => {
      return {
        name: d.dateAlias,
        value: d.renterNum
      };
    })
  }

  dateOnChange = (startDate, endDate) => {
    this.getData(startDate, endDate);
    this.setState({
      startDate,
      endDate,
    });
  }

  onChartClick(param) {
    const type = param.name.substring(param.name.length - 1, param.name.length);
    const month = param.name.substring(0, param.name.length - 1);
    if (type === '月') {
      router.push({
        pathname: '/fe/dataMaintenance/renterManagementForMonth',
        state: {
          month: `2019-${month}`
        }
      });
    }
  }
  render() {
    let onEvents = {
      'click': this.onChartClick.bind(this)
    }
    const { xAxisData, seriesData } = this.state;
    const option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        }
      },
      title: {
        show: true,
        text: '企业增量概况',
        textStyle: {
          color: 'rgba(0, 0, 0, 0.427450980392157)',
          fontSize: 14,
          fontWeight: 400,
          fontStyle: 'normal',
        },
      },
      xAxis: [
        {
          type: 'category',
          data: xAxisData,
          axisPointer: {
            type: 'shadow'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '企业增量',
          axisLabel: {
            formatter: '{value} 家'
          }
        },
      ],
      series: [
        {
          name: '增长量',
          type: 'bar',
          barWidth: 18,
          itemStyle: {
            color: '#FF7476'
          },
          data: seriesData
        },
      ]
    };
    return (
      <Spin spinning={this.state.chartLoading}>
        <Row>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>入驻企业概况</p>
                <p>COMPANY PROFILE</p>
              </div>
            </div>
          </div>
        </Row>
        <Row className={classNames(styles.part3Content)}>
          <ReactEcharts option={option} notMerge={true} lazyUpdate={true} style={{ height: 295 }} onEvents={onEvents} />
          <div style={{ position: 'absolute', top: 0, right: 32 }}>
            <TimeSelectorWithMonthYear startDate={this.state.startDate} endDate={this.state.endDate} onChange={this.dateOnChange} />
          </div>
        </Row>
      </Spin>
    );
  }
}
