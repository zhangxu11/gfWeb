import React, { Component } from 'react';

import { Skeleton, Card, Icon, Row, Col, message, BackTop, Button, Modal, Input, Form, Select, notification, Spin } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import QueueAnim from 'rc-queue-anim';
import styles from "./style.less";
import router from 'umi/router'
import axios from 'axios';
import Api from '../../services/apis'

const { Meta } = Card;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class OwnershipStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      modalVisible: false,
      buildingData: [],
      docLevelDis: false,
      editType: '',
      hasCard: '',
      changeDocLevelDis: '',
      docOwn: '',
    }
  }

  componentDidMount() {
    const that = this;
    this.setState({
      loading: false,
      projectName: this.props.location.query.projectName,
      projectCode: this.props.location.query.projectCode
    }, () => {
      //获取楼栋权属概况
      that.getBuildingList();
    });
  }

  //获取楼栋权属概况
  getBuildingList = () => {
    const that = this;
    that.setState({ loading: true });
    axios.get(Api.omGetBuildingList, {
      params: {
        projectName: that.state.projectName
      }
    })
      .then(function (resp) {
        const { data } = resp;
        if (data.length === 0) {
          notification.destroy();
          notification['warning']({ message: '暂无数据！' })
        }

        that.setState({
          buildingData: data
        })

        that.setState({ loading: false });
      })
  }

  setModalVisible(modalVisible) {
    this.setState({
      modalVisible
    });
    if (!modalVisible) {
      this.setState({
        editType: '',
        hasCard: '',
        changeDocLevelDis: '',
        docOwn: ''
      });
    }
  }

  openModal(item, transferState) {
    this.setModalVisible(true);

    if (item.docLevel) {
      this.setState({ docLevelDis: true })
    } else {
      this.setState({ docLevelDis: false })
    }

    this.props.form.setFieldsValue({
      fzr: item.fzr,
      docLevel: item.docLevel ? item.docLevel : '',
      transferState: transferState,
      docType: item.docType,
      docName: item.docName,
    });
  }

  changeDocLevelDis(changeDocLevelDis) {
    this.setState({
      changeDocLevelDis
    });
  }
  edit(item) {
    switch (item.docLevel) {
      case '楼栋':
        this.setState({
          editType: 'building'
        });
        break;

      case null:
        this.setState({
          editType: 'null'
        });
        break;
    }
    if (!item.canEdit) {
      notification.destroy();
      notification['warning']({ message: '不能编辑！' })
      return false;
    };

    this.setState({ item: item }, () => {
      this.openModal(item, '未过户');
    })
  }
  changeDocOwn(docOwn) {
    this.setState({ docOwn });
    let transferState = '';
    if (docOwn === '国房经营公司') {
      transferState = '已过户';
    } else{
      transferState = '未过户';
    };
    this.props.form.setFieldsValue({
      transferState,
    });
  }
  //编辑、标记有证
  editBuilding(e) {

    const item = this.state.item;
    const that = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        that.setState({ loading: true });
        axios.get(Api.omEditOwnerInfo, {
          params: {
            projectCode: that.state.projectCode,
            transferUnit: item.transferUnit,
            docName: (that.state.projectName ? that.state.projectName : '') + (that.state.item.name ? that.state.item.name : '') + (values.docType ? values.docType : ''),
            fzr: values.fzr,
            code: item.code,
            docType: values.docType,
            docLevel: values.docLevel,
            transferState: values.transferState,
            buildingCode: item.code,
            sourceType: 'b',
            cardBelong: values.docOwn,
            isHaveCard: values.hasCard,
            canHandle: values.canBanli,
            docId: item.docId,
          }
        })
          .then(function (resp) {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification['success']({ message: data.msg })
              that.setModalVisible(false);
              that.getBuildingList();
            } else {
              notification.destroy();
              notification['warning']({ message: data.msg })
            }
            that.setState({ loading: false });
          })
      }
    })

    e.stopPropagation();
  }

  changeBuildingCard(hasCard) {
    this.setState({
      hasCard
    });
  }

  clickItem(item) {
    router.push({
      pathname: '/fe/ownershipManagement/ownershipStatusHouse',
      query: {
        buildingCode: item.code,
        tradeId: item.tradeId,
        projectCode: this.state.projectCode,
        buildingName: item.name,
        docLevel: item.docLevel,
        transferUnit: item.transferUnit
      }
    })
  }

  changeName = (e) => {
    this.setState({ projectName: e.target.value });
  }

  render() {
    const { currentUserLoading, form: { getFieldDecorator } } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>楼栋权属概况</p>
              <p>OWNERSHIP STATUS</p>
            </div>
          </div>

          <Form>
            <Row style={{ textAlign: 'center' }}>
              <span style={{ display: 'inline-block' }}>
                <span className="label">项目名称</span>
                <Input
                  value={this.state.projectName}
                  data-inp="zdbh"
                  onChange={this.changeName}
                  placeholder="请输入项目名称"
                />&emsp;
                  <Button icon="search" type="primary" className="evfilterbtn" onClick={this.getBuildingList}>
                  搜索
                  </Button>
              </span>
            </Row>
          </Form>

          <Spin spinning={this.state.loading}>
            <div className={`clearfix ${styles.cardBox}`}>
              <QueueAnim>
                {this.state.buildingData.map((item, index) => {
                  return (
                    <Card
                      key={index}
                      className={styles.cardItem}
                      loading={this.state.loading}
                      actions={
                        [
                          <a onClick={this.edit.bind(this, item)}><Icon type="edit" />&nbsp;编辑
                        </a>
                        ]
                      }
                      title={item.name ? item.name : '-'}
                      extra={item.fzr ? `负责人：${item.fzr}` : `负责人：-`}
                      style={{
                        borderColor: item.nextDealTag == '有证' ? '#52c41a' : '#e7e7e7',
                        boxShadow: item.nextDealTag == '有证' ? '0 0 10px #52c41a' : '0 0 10px #e7e7e7'
                      }}
                    >
                      <h1>权属状态：<span>{item.nextDealTag ? item.nextDealTag : ''}</span></h1>
                      <Row className={styles.cardMain}
                        onClick={this.clickItem.bind(this, item)}>
                        <Col span={12}>
                          <span className='label'>权证类型：</span>
                          {item.docType ? item.docType : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>权证性质：</span>
                          {item.docLevel ? item.docLevel : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>过户状态：</span>
                          {item.transferState ? item.transferState : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>用途：</span>
                          {item.use ? item.use : '-'}
                        </Col>
                        <Col span={12} title={item.startTime}>
                          <span className='label'>开始时间：</span>
                          {item.startTime ? item.startTime : '-'}
                        </Col>
                        <Col span={12} title={item.endTime}>
                          <span className='label'>结束时间：</span>
                          {item.endTime ? item.endTime : '-'}
                        </Col>
                      </Row>
                    </Card>
                  );
                })}
              </QueueAnim>
            </div>
          </Spin>
        </div>
        <Modal
          title='编辑'
          wrapClassName="vertical-center-modal"
          visible={this.state.modalVisible}
          onOk={this.editBuilding.bind(this)}
          onCancel={this.setModalVisible.bind(this, false)}
          width={830}
        >
          <Row gutter={24}>
            {this.state.editType === 'building' ?
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="权证性质：">
                    {getFieldDecorator('docLevel', {
                      rules: [{ required: true, message: '请选择权证性质！' }]
                    })(<Select placeholder="请选择权证性质" style={{ width: "100%" }} disabled={this.state.docLevelDis}>
                      <Option value='楼栋'>楼栋</Option>
                      <Option value='房屋'>房屋</Option>
                    </Select>)}
                  </FormItem>
                </Col>

                <Col span={8}>
                  <FormItem label="是否有证：">
                    {getFieldDecorator('hasCard', {
                      rules: [{ required: true, message: '请选择是否有证！' }]
                    })(<Select placeholder="请选择是否有证" style={{ width: "100%" }} onChange={this.changeBuildingCard.bind(this)}>
                      <Option value='是'>是</Option>
                      <Option value='否'>否</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                {this.state.hasCard === '是' ?
                  [
                    <Col span={8} key='0'>
                      <FormItem label="权证类型：">
                        {getFieldDecorator('docType', {
                          rules: [{ required: true, message: '请选择权证类型！' }]
                        })(<Select placeholder="请选择权证类型" style={{ width: "100%" }}>
                          <Option value='使用权证'>使用权证</Option>
                          <Option value='产权证'>产权证</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='1'>
                      <FormItem label="权证归属：">
                        {getFieldDecorator('docOwn', {
                          rules: [{ required: true, message: '请选择权证归属：！' }]
                        })(<Select placeholder="请选择权证归属：" style={{ width: "100%" }} onChange={this.changeDocOwn.bind(this)}>
                          <Option value={this.state.item.transferUnit}>{this.state.item.transferUnit}</Option>
                          <Option value='国房经营公司'>国房经营公司</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='2'>
                      <FormItem label="过户状态：">
                        {getFieldDecorator('transferState', {
                          rules: [{ required: true, message: '请输入过户状态！' }]
                        })(<Select placeholder="请选择过户状态" style={{ width: "100%" }} disabled>
                          <Option value='已过户'>已过户</Option>
                          <Option value='未过户'>未过户</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='3'>
                      <FormItem label="代办人：">
                        {getFieldDecorator('fzr', {
                          rules: [{ required: true, message: '请输入代办人！' }]
                        })(<Input placeholder="输入代办人" />)}
                      </FormItem>
                    </Col>
                  ] : null}

                {this.state.hasCard === '否' ?
                  <Col span={8}>
                    <FormItem label="是否可办理：">
                      {getFieldDecorator('canBanli', {
                        rules: [{ required: true, message: '请选择是否可办理！' }]
                      })(<Select placeholder="请选择办理状态" style={{ width: "100%" }}>
                        <Option value='可办理'>可办理</Option>
                        <Option value='不可办理'>不可办理</Option>
                      </Select>)}
                    </FormItem>
                  </Col> : null}
              </Row>
              : null}

            {this.state.editType === 'null' ?
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="权证性质：">
                    {getFieldDecorator('docLevel', {
                      rules: [{ required: true, message: '请选择权证性质！' }]
                    })(<Select placeholder="请选择权证性质" style={{ width: "100%" }} disabled={this.state.docLevelDis} onChange={this.changeDocLevelDis.bind(this)}>
                      <Option value='楼栋'>楼栋</Option>
                      <Option value='房屋'>房屋</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                {this.state.changeDocLevelDis === '楼栋' ?
                  <Col span={8}>
                    <FormItem label="是否有证：">
                      {getFieldDecorator('hasCard', {
                        rules: [{ required: true, message: '请选择是否有证！' }]
                      })(<Select placeholder="请选择是否有证" style={{ width: "100%" }} onChange={this.changeBuildingCard.bind(this)}>
                        <Option value='是'>是</Option>
                        <Option value='否'>否</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  : null}

                {this.state.hasCard === '是' ?
                  [
                    <Col span={8} key='0'>
                      <FormItem label="权证类型：">
                        {getFieldDecorator('docType', {
                          rules: [{ required: true, message: '请选择权证类型！' }]
                        })(<Select placeholder="请选择权证类型" style={{ width: "100%" }}>
                          <Option value='使用权证'>使用权证</Option>
                          <Option value='产权证'>产权证</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='1'>
                      <FormItem label="权证归属：">
                        {getFieldDecorator('docOwn', {
                          rules: [{ required: true, message: '请选择权证归属：！' }]
                        })(<Select placeholder="请选择权证归属：" style={{ width: "100%" }} onChange={this.changeDocOwn.bind(this)}>
                          <Option value={this.state.item.transferUnit}>{this.state.item.transferUnit}</Option>
                          <Option value='国房经营公司'>国房经营公司</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='2'>
                      <FormItem label="过户状态：">
                        {getFieldDecorator('transferState', {
                          rules: [{ required: true, message: '请输入过户状态！' }]
                        })(<Select placeholder="请选择过户状态" style={{ width: "100%" }} disabled>
                          <Option value='已过户'>已过户</Option>
                          <Option value='未过户'>未过户</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='3'>
                      <FormItem label="代办人：">
                        {getFieldDecorator('fzr', {
                          rules: [{ required: true, message: '请输入代办人！' }]
                        })(<Input placeholder="输入代办人" />)}
                      </FormItem>
                    </Col>
                  ] :
                  null
                }
                {this.state.hasCard === '否' ?
                  <Col span={8}>
                    <FormItem label="是否可办理：">
                      {getFieldDecorator('canBanli', {
                        rules: [{ required: true, message: '请选择是否可办理！' }]
                      })(<Select placeholder="请选择办理状态" style={{ width: "100%" }}>
                        <Option value='可办理'>可办理</Option>
                        <Option value='不可办理'>不可办理</Option>
                      </Select>)}
                    </FormItem>
                  </Col> : null}
              </Row>
              : null}

          </Row>
        </Modal>
        <BackTop />
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(OwnershipStatus);
export default WrappedApp;
