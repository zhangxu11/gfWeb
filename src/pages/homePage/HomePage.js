import React, { Component } from 'react';
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import Part1 from './part1';
import Part2 from './part2';
import Part3 from './part3';
import Part4 from './part4';
import Part5 from './part5';

import styles from "./style.less";


class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assetData: { // 资产统计
        buildingCount: 0, //楼宇数量
        houseCount: 0, // 房屋数量
        totalArea: 0, // 总面积
        totalRentalArea: 0, // 在租面积
        percentageOfRentalArea: '', // 在租面积比例
        totalVacantArea: 0, // 空置面积
        percentageOfVacantArea: '', // 空置面积比例
        avgPrice: '0', // 在租均价
      },
      totalRenter: 0, // 租户总量
      industryList: [], // 租户行业表
    }
  }
  componentDidMount() {
    Promise.all([
      axios.post(Api.getAssetStatistics), // 获取园区楼和房信息
      axios.post(Api.getRenterStatistics), // 获取租户信息
    ]).then(resList => {
      let assetData = [];
      let totalRenter = 0;
      let industryList = [];
      if (resList[0].data.code = '200') {
        const data = resList[0].data.result;
        assetData = {
          buildingCount: data.buildingStatisticsModel.buildingCount, //楼宇数量
          houseCount: data.houseStatisticsModel.houseCount, // 房屋数量
          totalArea: data.houseStatisticsModel.totalArea, // 总面积
          totalRentalArea: data.houseStatisticsModel.totalRentalArea, // 在租面积
          percentageOfRentalArea: data.houseStatisticsModel.percentageOfRentalArea, // 在租面积比例
          totalVacantArea: data.houseStatisticsModel.totalVacantArea, // 空置面积
          percentageOfVacantArea: data.houseStatisticsModel.percentageOfVacantArea, // 空置面积比例
          avgPrice: data.houseStatisticsModel.avgPrice, // 在租均价
        };
      }
      if (resList[1].data.code = '200') {
        const data = resList[1].data.result;
        totalRenter = data.totalRenter;
        industryList = data.industryList;
      }
      this.setState({
        assetData,
        totalRenter,
        industryList,
      });
    });
  }

  render() {
    return (
      <div className={styles.homePage} >
        <Part1 data={this.state.assetData} />
        <Part2
          key = {Math.random()}
          leftData={this.state.assetData}
          rightData={{ totalRenter: this.state.totalRenter, industryList: this.state.industryList, }} />
        <Part3 />
        <Part4 />
        <Part5 />
      </div>
    );
  }
}

export default HomePage;
