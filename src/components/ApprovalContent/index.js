import { Steps } from 'antd';

/**
 * 审批进度组件
 * 
 * @export
 * @param {{approvalList: {contractDate: string}[], onClickCallback: (approval: {contractDate: string},canApproval: number, index: number)=> void, isShow: boolean}} {approvalList, onClick, isShow}
 * @returns
 */
export function ApprovalContent({approvalList, onClickCallback, isShow}) {
  console.log(4444444, isShow);
  const canApproval = sessionStorage.getItem('canApproval');
  return (
    <Steps direction="vertical" size="small" style={{width: 404}}>
      {
        approvalList.map((approval, index) => {
          return <Steps.Step key={index} status={approval.contractDate ? 'process': 'wait' }  description={
            <div style={{display: 'flex', justifyContent: 'space-around' }}>
              <span>******部门</span>
              <span style={{display: 'inline-block', minWidth: '72px'}}>{approval.contractDate}</span>
              <span>**部长</span>
              {approval.contractDate ? 
                <a style={{display: 'inline-block', minWidth: '42px',color: '#006600'}}>已审批</a> : 
                (Number(canApproval) === (index +1) && !isShow ? <a style={{display: 'inline-block', minWidth: '42px'}} onClick={() => onClickCallback(approval, Number(canApproval), index)}>审批</a> : <a style={{display: 'inline-block', minWidth: '42px',color: '#0066FF'}}>未审批</a>)
              }
            </div>} />
        })
      }
    </Steps>
  )
}