//**********************************
//基础功能类-几何对象
//Power by han 2018.07.05
//**********************************
var objGeometry = {
    pWkid: 0,//存储坐标系代码
    init: function (w) { //初始化类对象
        this.pWkid = w;
    },
    setWkid: function (e) { //设置坐标系代码
        this.pWkid = e;
    },
    getWkid: function () { //获取坐标系代码
        return this.pWkid;
    },
    paramTest: function () { //参数验证
        if (this.pWkid == 0) {
            alert("objGeometry error 'wkid'");
            console.log("坐标系代码未设置");
            return false;
        }
        return true;
    },
    createPoint: function (x, y) { //创建几何点对象
        if (this.paramTest() == false) return null;
        return new esri.geometry.Point(x, y, new esri.SpatialReference({wkid: this.pWkid}));
    },
    createPolygon: function (pointList) { //创建几何面对象
        if (this.paramTest() == false) return null;
        var gon = new esri.geometry.Polygon(new esri.SpatialReference({wkid: this.pWkid}));
        gon.addRing(pointList);
        return gon;
    },
    createPolygonByExtent(extent) { //根据地图外接矩形（包络线）生成几何面对象
        if (this.paramTest() == false) return null;
        var x1 = extent.xmin;
        var x2 = extent.xmax;
        var y1 = extent.ymin;
        var y2 = extent.ymax;
        var p1 = this.createPoint(x1, y1);
        var p2 = this.createPoint(x1, y2);
        var p3 = this.createPoint(x2, y2);
        var p4 = this.createPoint(x2, y1);
        return this.createPolygon([p1, p2, p3, p4, p1]);
    },
    createExtentByPoints(pts){//根据传入的点集，返回外接矩形
        if (this.paramTest() == false) return null;
        var minx = 0;
        var maxx = 0;
        var miny = 0;
        var maxy = 0;
        for (var i = 0; i < pts.length; i++) {
            if (minx == 0 || maxx == 0 || miny == 0 || maxy == 0) {
                minx = pts[i].x;
                maxx = pts[i].x;
                miny = pts[i].y;
                maxy = pts[i].y;
                continue;
            }
            if (minx > pts[i].x) {
                minx = pts[i].x;
            }
            if (maxx < pts[i].x) {
                maxx = pts[i].x;
            }
            if (miny > pts[i].y) {
                miny = pts[i].y;
            }
            if (maxy < pts[i].y) {
                maxy = pts[i].y;
            }
        }
        var extent = new esri.geometry.Extent(minx, miny, maxx, maxy, new esri.SpatialReference({wkid: this.pWkid}));
        extent = extent.expand(1.2);
        return extent;
    }
}

