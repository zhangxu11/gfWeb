import * as React from 'react';
import {Table, Form, Select, Input, Button, Col, Row, Modal, Icon, Popver, Steps, DatePicker, notification } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import axios from 'axios'
import Api from '../../services/apis'

const FormItem = Form.Item;
const Option = Select.Option;
const Step = Steps.Step;

@connect (({user, loading}) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))

class RecordBorrow extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            modalVisible2: false,
            tableData: [],
            current: 1,
            pageSize: 5,
            docIds:'',
            stepData: [
                {
                title: '申请借档',
                },
                {
                title: '部门领导审核',
                },
                {
                title: '经办部门审核',
                },
                {
                title: '经办人',
                },
                {
                title: '完成'
                }
            ],
        }
    }

    sendData = () => {
        const that = this;
        const searchValues = this.props.form.getFieldsValue();
        axios.post(Api.rmGetDocList,{
            docName: searchValues.s_name,
            docStatus: searchValues.s_state,
            docType: searchValues.s_type,
            currentPage: this.state.current
        })
        .then((function(resp){
            const data = resp.data;
            if(data.list.length === 0){
                notification.destroy();
                notification['warning']({message:'暂无数据'})
            }else{
                data.list.forEach((d,i) => {
                    d.key = i;
                    d.doc_status2 = d.doc_status === '0' ? "未借出" : parseInt(d.doc_status) < 4 ? '审批中' : "已借出";
                    switch (d.doc_type){
                        case "0":
                            d.doc_type = "核实情况报告";
                        break;
                        case "1":
                            d.doc_type = "内部文档";
                        break;
                        case "2":
                            d.doc_type = "划转通知";
                        break;
                        case "3":
                            d.doc_type = "权属文件";
                        break;
                    }
                });
            }

            that.setState({
                tableData: data.list,
                pageSize: data.pageSize,
                current: data.currentPage,
                totalSize: data.totalSize
            })
        }))
    }

    componentWillMount(){
        this.sendData();
    }

    query = (e) => {
        e.preventDefault();
        /* this.props.form.validateFields((err,values)=>{
            if(!err){
                console.log('value:',values)
            }
        }) */
        this.setState({current:1},()=>{
            this.sendData();
        })
    }

    pageChange = (page) => {
        this.setState({current:page},()=>{
            this.sendData();
        })
    }


    resetForm = () => {
        this.props.form.resetFields();
    }

    setModalVisible = (visible,id) => {
        this.setState({
            modalVisible:visible,
            docIds:id,
        })

        this.props.form.setFieldsValue({
            borrower: '',
            reason: '',
            startTime: null,
            endTime: null
        });
    }
    
    setModalVisible2 = (visible) => {
        this.setState({modalVisible2:visible})
    }

    borrow = () => {
        const that = this;
        this.props.form.validateFields((err,values)=>{
            if(!err){
                if(values.startTime>values.endTime){
                    notification.destroy();
                    notification['warning']({message:'开始时间不能晚于截止时间！'})
                    return;
                }
                axios.post(Api.rmBorrow,{
                    docIds: that.state.docIds,
                    borrowPerson: values.borrower,
                    borrowCause : values.reason,
                    borrowStart: values.startTime.format('YYYY-MM-DD'),
                    borrowEnd: values.endTime.format('YYYY-MM-DD')
                })
                .then((function(resp){
                    const data = resp.data;
                    if(data.flag == '1'){
                        notification.destroy();
                        notification['success']({message:data.msg})
                        that.sendData()
                    }else{
                        notification.destroy();
                        notification['warning']({message:data.msg})
                    }
                    that.setModalVisible(false);
                }))
            }
        })
    }

    openModal = (id) => {

        //根据ID查询档案信息
        this.getDocById(id);
        //获取档案借款记录
        this.getRecord(id);
    }

    //根据ID查询档案信息
    getDocById = (id) => {
        const that = this;
        axios.post(Api.rmGetDocById,{
            docId:id
        })
        .then(function(resp){
            const data = resp.data;
            that.setModalVisible2(true);
            switch (data.doc_type){
                case "0":
                    data.doc_type = "核实情况报告";
                break;
                case "1":
                    data.doc_type = "内部文档";
                break;
                case "2":
                    data.doc_type = "划转通知";
                break;
                case "3":
                    data.doc_type = "权属文件";
                break;
            }
            that.setState({
                m_number: data.ref_code,
                m_name: data.doc_name,
                m_room: data.housedesc,
                m_type: data.doc_type,
                m_state: parseInt(data.doc_status),
            })
        })
    }

    //获取审批记录
    getRecord = (id) => {
        const that = this;
        axios.post(Api.rmApprovalOne,{
            docId: id,
            currentPage: that.state.m_current
        })
        .then(function(resp){
            const { data } = resp;
            if(data){
                const stepData = that.state.stepData;
                stepData[0].description = data.borrowUser;
                stepData[0].time = data.borrowDate ? data.borrowDate.substring(0,10) : '';
                stepData[1].description = data.auditingDept;
                stepData[1].time = data.auditingDate ?data.auditingDate.substring(0,10) : '';
                stepData[2].description = data.chargeDept;
                stepData[2].time = data.chargeDeptDate ? data.chargeDeptDate.substring(0,10) : '';
                stepData[3].description = data.chargePerson;
                stepData[3].time = data.chargePersonDate ? data.chargePersonDate.substring(0,10) : '';

                that.setState({
                    stepData: stepData
                })
            }
        })
    }


    render () {
        const { form : { getFieldDecorator }, currentUserLoading } = this.props;

        const columns = [
            {
                title: '档案名称',
                key: 'doc_name',
                dataIndex: 'doc_name',
            },
            {
                title: '档案位置',
                key: 'cabinetdesc',
                dataIndex: 'cabinetdesc',
            },
            {
                title: '档案柜所在房间',
                key: 'housedesc',
                dataIndex: 'housedesc',
            },
            {
                title: '档案状态',
                key: 'doc_status2',
                dataIndex: 'doc_status2',
            },
            {
                title: '档案类型',
                key: 'doc_type',
                dataIndex: 'doc_type',
            },
            {
                title: '操作',
                key: 'action',
                render: (text, record) => {
                    return record.doc_status == '0' ? (
                        <span>
                            <a href="javascript:;" onClick={()=>this.setModalVisible(true,record.doc_id)}>
                                借取
                            </a>
                        </span>
                    ) : (
                        <span>
                            <a href="javascript:;" onClick={()=>this.openModal(record.doc_id)}>
                                查看
                            </a>
                        </span>
                    )
                },
            },
        ]

        return (
            <PageHeaderWrapper loading={currentUserLoading}>
                <div className='mainContainer'>
                    <div className='contentHeader'>
                        <div className='contentHeaderTitle'>
                            <p>档案借取</p>
                            <p>RECORD BORROW</p>
                        </div>
                    </div>

                    <div className='pom'>
                        <div className='mrb15'>
                            <Form layout='inline' align='center' onSubmit={this.query}>
                                <FormItem label="档案名称：">
                                    {
                                        getFieldDecorator('s_name')(
                                            <Input placeholder="请输入档案名称"/>
                                        )
                                    }
                                </FormItem>
                                <FormItem label='档案状态：'>
                                    {
                                        getFieldDecorator('s_state')(
                                            <Select placeholder="请选择档案状态">
                                                <Option value="0">未借出</Option>
                                                <Option value="1">已借出</Option>
                                            </Select>
                                        )
                                    }
                                </FormItem>
                                <FormItem label='档案类型：'>
                                    {
                                        getFieldDecorator('s_type')(
                                            <Select placeholder="请选择档案类型">
                                                <Option value="0">核实情况报告</Option>
                                                <Option value="1">内部文档</Option>
                                                <Option value="2">划转通知</Option>
                                                <Option value="3">权属文件</Option>
                                            </Select>
                                        )
                                    }
                                </FormItem>
                                <Row gutter={24} style={{ textAlign: 'center'}}>
                                    <Button icon='search' type='primary' htmlType='submit'>搜索</Button>&emsp;
                                    <Button icon="reload" type="primary" htmlType='button' onClick={this.resetForm}>重置</Button>
                                </Row>
                            </Form>
                        </div>

                        <Table 
                            bordered={true}
                            scroll={{ x:true }}
                            columns={columns}
                            dataSource={this.state.tableData}
                            pagination={{
                                pageSize:this.state.pageSize,
                                current:this.state.current,
                                total:this.state.totalSize,
                                onChange:this.pageChange,
                                showTotal: total => `共 ${total} 项`
                            }}/>
                    </div>

                    <Modal
                        title="档案借取"
                        centered
                        visible={this.state.modalVisible}
                        onOk={this.borrow}
                        onCancel={() => this.setModalVisible(false)}
                        width={800}
                    >
                        <div>
                                <Row gutter={24}>
                                    <Col span={8}>
                                        <FormItem label='借取人：'>
                                        {getFieldDecorator('borrower',{
                                            rules:[{required:true,message:"请输入借取人！"}]
                                        })(<Input placeholder="请输入借取人"/>)}
                                        </FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='事由：'>
                                        {getFieldDecorator('reason',{
                                            rules:[{required:true,message:"请输入事由！"}]
                                        })(<Input placeholder='请输入事由'/>)}
                                        </FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='开始时间：'>
                                        {getFieldDecorator('startTime',{
                                            rules:[{required:true,message:"请选择借取开始时间！"}]
                                        })(<DatePicker  format="YYYY-MM-DD" placeholder='请选择借取开始时间' style={{width:'100%'}}/>)}
                                        </FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='截止时间：'>
                                        {getFieldDecorator('endTime',{
                                            rules:[{required:true,message:"请选择借取截止时间！"}]
                                        })(<DatePicker  format="YYYY-MM-DD" placeholder='请选择借取截止时间' style={{width:'100%'}}/>)}
                                        </FormItem>
                                    </Col>
                                </Row>  
                        </div>
                    </Modal>

                    <Modal
                        title="借取审批"
                        centered
                        visible={this.state.modalVisible2}
                        width={800}
                        onCancel = {() => this.setModalVisible2(false)}
                        onOk = {() => this.setModalVisible2(false)}
                    >
                        <div>
                            <Steps size="small" current={this.state.m_state}>
                                {this.state.stepData.map((item, index) => {
                                return (
                                    <Step
                                    description={[
                                        <div key={index}>
                                        <p>{item.description}</p>
                                        <p>{item.time}</p>
                                        </div>,
                                    ]}
                                    title={item.title}
                                    key={index}
                                    />
                                );
                                })}
                            </Steps>
                            <Form layout='inline' style={{marginTop:'25px'}}>
                                <Row>
                                    <Col span={8}>
                                        <FormItem label='档案编号：'>{this.state.m_number}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='档案名称：'>{this.state.m_name}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='所在房间：'>{this.state.m_room}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='档案类型：'>{this.state.m_type}</FormItem>
                                    </Col>
                                </Row>  
                            </Form>
                        </div>
                    </Modal>
                
                </div>
            </PageHeaderWrapper>
        )
    }
}

const WrappedApp = Form.create()(RecordBorrow);

export default WrappedApp;
