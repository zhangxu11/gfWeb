// 项目管理
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Spin,
  Select,
  Upload
} from 'antd';
import Link from 'umi/link';
import axios from 'axios';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';

import moment from 'moment';
import Api from '../../services/apis';
import 'moment/locale/zh-cn';

const { confirm } = Modal;
const { RangePicker } = DatePicker;
const { Step } = Steps;
const FormItem = Form.Item;
const { Option } = Select;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class buildingManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectCode: 'PJ20190125', // 写死了现在
      activeBuildingCode: null, // 编辑时候的楼栋code
      searchParams: {
        buildingName2: null, // 楼栋名称
        buildingCode: null, // 楼栋编号
        rentAreaStart: null, // 在租面积起
        rentAreaEnd: null, // 在租面积止
        buildingAreaStart: null, // 建筑面积起
        buildingAreaEnd: null, // 建筑面积止
        rentAvgStart: null, // 在租均价起
        rentAvgEnd: null, // 在租均价止
      },
      fileList: [], // 编辑时候的图片
      fileIds: [], // 编辑时候的图片id
      // name: '',
      // tranUnits: '',
      pageSize: 5,
      current: 1,
      tableData: [],
      modalVisible: false,
      selectdIds: [],
      loading: false,
      modalVisible: false,
      province: [],
      city: [],
      area: [],
      ownerStatus: [],
      ownerType: [],
      signStatus: [],
      assetStatus: [],
      communitys: [],
      hookBuilds: [],

      showDownload: false,
      downloadName: '',
      downloadLink: '',
      startDownload: false,

      more: false,
    };
  }

  componentWillMount() {
    // 项目编码
    this.setState({
      projectName: this.props.location.query.projectName,
      communityName: this.props.location.query.communityName,
      manageType: this.props.location.query.manageType,
      transferUnit: this.props.location.query.transferUnit,
    }, () => {
      this.sendData();
    });

    // 获取系统参数
    axios.all([this.getSysDic("OWNER_SHIP_STATUS"), this.getSysDic("OWNER_SHIP_TYPE"), this.getSysDic("SIGN_STATUS"), this.getSysDic("ASSET_STATUS")])
      .then(axios.spread((r1, r2, r3, r4) => {
        this.setState({
          ownerStatus: r1.data, // 权属权属状态
          ownerType: r2.data,  // 权属形式
          signStatus: r3.data,  // 签署协议状态
          assetStatus: r4.data,  // 资产状态
        })
      }))

    this.getAreaList('1');
  }

  // 获取省、市、区
  getAreaList = (level, areaId) => {
    const that = this;
    const param = { level };
    if (level !== '1') {
      param.id = areaId;
    }
    that.setState({ loading: true })
    return axios.post(Api.amGetAreaList, param)
      .then((resp) => {
        const { data } = resp;
        if (level === '1') {
          that.setState({ province: data })
        } else if (level === '2') {
          that.setState({ city: data })
          that.props.form.setFieldsValue({
            city: '',
            area: ''
          })
        } else if (level === '3') {
          that.setState({ area: data })
          that.props.form.setFieldsValue({
            area: ''
          })
        }

        that.setState({ loading: false })
      })
  }

  // 获取挂接楼栋下拉
  getHookBuildData = (communityId, buildingCode) => {
    const that = this;
    axios.post(Api.amGetHookBuildData, {
      communityId,
      buildingCode
    })
      .then((resp) => {
        const { data } = resp;
        that.setState({ hookBuilds: data })
      })
  }

  // /获取系统参数
  getSysDic = (type) => axios.post(Api.amGetSysDic, { key: type })

  // 获取表格数据
  sendData = () => {
    // let that = this;
    this.setState({ loading: true });
    const params = {
      ...this.state.searchParams,
      currentPage: this.state.current,
      projectCode: this.state.projectCode
    };
    axios.post(Api.amGetBuildList, params).then((resp) => {
      const { data } = resp;
      if (data.list.length == 0) {
        notification.destroy();
        notification.warning({
          message: '暂无数据',
        });
      }

      data.list.forEach((d, i) => {
        d.key = i;
        this.state.ownerType.forEach(item => {
          if (item.dicCode == d.ownershipType) {
            d.ownershipTypeText = item.dicName
          }
        })
      });

      this.setState({
        tableData: data.list,
        pageSize: data.pageSize,
        current: data.currentPage,
        total: data.totalSize,
        loading: false
      })
    })
  };

  // 查询
  query = () => {
    if (this.state.searchParams.rentAreaStart && this.state.searchParams.rentAreaEnd) {
      if (this.state.searchParams.rentAreaEnd > this.state.searchParams.rentAreaStart) {
        this.setState({ current: 1 }, () => {
          this.sendData();
        });
      } else {
        notification['warning']({ message: '请输入正确的面积范围' });
      }
    } else {
      this.setState({ current: 1 }, () => {
        this.sendData();
      });
    }

  };

  // 导出
  export = () => {
    this.setState({
      startDownload: false
    });
    axios.post(Api.amExportBuild, {
      ...this.state.searchParams
    },
      { responseType: 'arraybuffer' }
    ).then((res) => {
      notification.destroy();
      notification.success({ message: '开始下载！' })

      const blob = new Blob([res.data], { type: "application/vnd.ms-excel" });
      const [dLink, name] = [
        window.URL.createObjectURL(blob),
        `楼栋列表${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.xls`
      ];
      const link = document.createElement('a');
      link.href = dLink;
      link.download = name;
      link.click();
      this.setState({
        startDownload: true,
        downloadLink: dLink,
        downloadName: name,
        showDownload: true
      });
    }).catch((err) => {
      notification.destroy();
      notification.warning({ message: `下载失败！错误信息：${err}` })
    });
  };

  showDownload(showDownload) {
    this.setState({ showDownload });
  }

  changeState = (key, value) => {
    const searchParams = { ...this.state.searchParams };
    searchParams[key] = value;
    this.setState({ searchParams });
  }

  resetSearchParams = () => {
    this.setState({
      searchParams: {
        buildingName2: null, // 楼栋名称
        buildingCode: null, // 楼栋编号
        rentAreaStart: null, // 在租面积起
        rentAreaEnd: null, // 在租面积止
        buildingAreaStart: null, // 建筑面积起
        buildingAreaEnd: null, // 建筑面积止
        rentAvgStart: null, // 在租均价起
        rentAvgEnd: null, // 在租均价止
      },
      pageSize: 5,
      current: 1,
    });
  }

  pageChange = page => {
    this.setState({ current: page }, () => {
      this.sendData();
    });
  };

  setAddProjectModalVisible = (visible) => {
    if (visible) this.setState({ addProjectModalVisible: visible });
    else {
      this.setState({
        addProjectModalVisible: visible,
        activeBuildingCode: null,
        fileList: [],
        fileIds: [],
      });
    }
  }

  handleMenuClick = (building) => {
    const win = window.open(`/fe/assetMap/buildingDetails?buildingCode=${building.buildingCode}`, '_blank');
    win.focus();
  }

  // 获取楼栋详情
  getBuildInfo = (item) => {
    this.setState({ loading: true })
    axios.post(Api.amGetBuildInfo, { buildingCode: item.buildingCode })
      .then((resp) => {
        const { data } = resp;
        const { images } = data;
        const fileList = images ? images.map(image => ({
          uid: image.imageId,
          id: image.imageId,
          url: `http://gisams-hitech.cindata.cn/res_file${image.filePath}`,
          name: image.imageId,
          status: 'done'
        })) : [];

        this.props.form.setFieldsValue({
          buildingCode: data.buildingCode,
          buildingName2: data.buildingName2,
          houseCount: data.houseCount,
          rentAvg: data.rentAvg,
          buildingStructure: data.buildingStructure,
          buildingArea: data.buildingArea,
          rentArea: data.rentArea,
          totalFloor: data.totalFloor,
          location: data.location,
          province: data.province,
          city: data.city,
          area: data.area,
          principal: data.principal,
        });

        this.setState({
          activeBuildingCode: data.buildingCode,
          communityId: data.communityId,
          communityName: data.communityName,
          modalType: 'edit',
          projectCode: data.projectCode,
          fileList,
          regionFileList: fileList
        });

        // 赋值省市区联动
        this.getAreaList(2, data.province)
          .then(() => {
            this.props.form.setFieldsValue({ city: data.city })

            this.getAreaList(3, data.city)
              .then(() => {
                this.props.form.setFieldsValue({ area: data.area })
              })
          })

        this.setAddProjectModalVisible(true);
        this.setState({ loading: false })
      })
  }

  // 编辑项目(点弹框确定)
  editBuilding = () => {
    const that = this;
    this.props.form.validateFields((err, data) => {
      this.setState({ loading: true })
      if (!err) {
        axios.post(Api.amUpdateBuildInfo, {
          projectCode: that.state.projectCode,
          buildingCode: data.buildingCode,
          buildingName2: data.buildingName2,
          houseCount: data.houseCount,
          rentAvg: data.rentAvg,
          buildingStructure: data.buildingStructure,
          buildingArea: data.buildingArea,
          rentArea: data.rentArea,
          totalFloor: data.totalFloor,
          location: data.location,
          province: data.province,
          city: data.city,
          area: data.area,
          principal: data.principal,
        })
          .then((resp) => {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification.success({ message: data.msg });
              that.sendData();
              that.setAddProjectModalVisible(false);
            } else if (data.code == 0) {
              notification.destroy();
              notification.error({ message: data.msg });
            }
          })
      }
      that.setState({ loading: false })
    })
  };

  // 新增、编辑
  modalSubmit = () => {
    if (this.state.modalType == 'edit') {
      // 编辑
      this.editBuilding();
    } else {
      // 新建
      this.addBuilding();
    }
  }

  // 取消
  cancel = () => {
    this.setAddProjectModalVisible(false);
    this.setState({ loading: false });
  }

  addBuilding = () => {
    const that = this;
    that.setState({ loading: true })
    this.props.form.validateFields((err, data) => {
      if (!err) {
        axios.post(Api.amAddBuildInfo, {
          projectCode: that.state.projectCode,
          buildingCode: data.buildingCode,
          buildingName2: data.buildingName2,
          houseCount: data.houseCount,
          rentAvg: data.rentAvg,
          buildingStructure: data.buildingStructure,
          buildingArea: data.buildingArea,
          rentArea: data.rentArea,
          totalFloor: data.totalFloor,
          location: data.location,
          province: data.province,
          city: data.city,
          area: data.area,
          principal: data.principal,
        }).then((resp) => {
          const { data } = resp;
          if (data.code == 1) {
            notification.destroy();
            notification.success({ message: data.msg });
            that.sendData();
            that.setAddProjectModalVisible(false);
          } else if (data.code == 0) {
            notification.destroy();
            notification.error({ message: data.msg });
          }
          that.setState({ loading: false });
        })
      }
    })
  }

  // 删除楼栋
  deleteBuilding = (id) => {
    const that = this;
    confirm({
      title: "删除确认",
      content: '确认删除该楼栋？',
      onOk() {
        that.setState({ loading: true })
        axios.post(Api.amDelBuildInfo, { buildingCode: id })
          .then((resp) => {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification.success({ message: '删除成功' });
              that.setState({
                current: '1'
              })
              that.sendData();
            } else {
              notification.destroy();
              notification.warning({ message: data.msg });
            }
            that.setState({ loading: false })
          })
      }
    })
  }

  openAddModal = () => {
    const myDate = new Date();
    const date = myDate.toLocaleDateString().replace(/\//g, '-');
    this.setState({ addProjectModalVisible: true }, () => {
      this.setState({ modalType: 'new' })
      this.props.form.setFieldsValue({
        buildingCode: '',
        buildingName2: '',
        houseCount: '',
        rentAvg: '',
        buildingStructure: '',
        buildingArea: '',
        rentArea: '',
        totalFloor: '',
        location: '',
        province: '',
        city: '',
        area: '',
        principal: '',
      })
    })
  }

  openEditModal = (item) => {
    // 获取楼栋详情
    this.getBuildInfo(item);
  }

  handleChange = (info) => {
    this.setState({
      fileList: info.fileList
    })
    if (info.file && info.file.response && info.file.response.status && info.file.response.status === 'success') {
      notification.destroy();
      notification.success({ message: info.file.response.message });
      this.setState({ fileList: info.fileList })
      const { fileIds } = this.state;
      fileIds.push(info.file.response);
      this.setState({ fileIds });

    } else if (info.file && info.file.response && info.file.response.status && info.file.response.status === 'error') {
      this.setState({ fileList: this.state.regionFileList })
      notification.destroy();
      notification.error({ message: info.file.response.message });
    }
  }
  handleRemove = (info) => {
    axios.get(Api.amDeletePic, {
      params: {
        imageId: info.id
      }
    }).then((resp) => {
      notification.success({ message: resp.data.message });
    });
  }
  beforeUpload = (file) => {
    if (file.size > 10485760) {
      notification.error({ message: '图片上传不能超过10M' });
      return false;
    } else {
      const { fileList } = this.state;
      fileList.push(file)
      this.setState({
        fileList
      })
    }
  }
  render() {
    const that = this;
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;
    // 表格表头
    const columns = [
      {
        title: '楼栋名称',
        dataIndex: 'buildingName2',
        key: 'buildingName2',
        render: (text, item, index) => (
          <a href="javascrip:;" onClick={this.handleMenuClick.bind(this, item)}>
            {text}
          </a>
        ),
      },
      {
        title: '楼栋编号',
        dataIndex: 'buildingCode',
        key: 'buildingCode',
      },
      {
        title: '在租均价',
        dataIndex: 'rentAvg',
        key: 'rentAvg',
      },
      {
        title: '建筑面积',
        dataIndex: 'buildingArea',
        key: 'buildingArea',
      },
      {
        title: '在租面积',
        dataIndex: 'rentArea',
        key: 'rentArea',
      },
      {
        title: '房屋数量',
        dataIndex: 'houseCount',
        key: 'houseCount',
        render: (text, item, index) =>
          (
            <Link
              key={index}
              target='_blank'
              to={{
                pathname: '/fe/assetManagement/houseManagement',
                search: `?buildingCode=${item.buildingCode}&projectCode=${this.state.projectCode}&projectName=${this.state.projectName}&manageType=${this.state.manageType}&transferUnit=${this.state.transferUnit}&obtainWay=${item.obtainWay}&ownershipType=${item.ownershipType}&buildingName2=${item.buildingName2}`
              }}
            >{text}
            </Link>
          )
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <span>
            <a href="javascript:;" onClick={() => this.getBuildInfo(item)}>编辑</a>&emsp;
            <a href="javascript:;" onClick={() => this.deleteBuilding(item.buildingCode)}>删除</a>
          </span>
        ),
      },
    ];

    if (this.props.location.state !== undefined && this.props.location.state.fromHomepage) {
      columns.unshift({
        title: '项目名称',
        dataIndex: 'project_name',
        key: 'project_name',
      });
    }


    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >

        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>楼栋管理</p>
              <p>BUILDING MANAGEMENT</p>
            </div>
          </div>
          <div className='mrb15'>
            <Form layout="inline" align='center'>
              <FormItem>
                <span className="label">楼栋名称</span>
                <Input
                  value={this.state.searchParams.buildingName2}
                  data-inp="zdbh"
                  onChange={(e) => this.changeState('buildingName2', e.target.value)}
                  placeholder="请输入楼栋名称"
                />
              </FormItem>

              <FormItem>
                <span className="label">楼栋编号</span>
                <Input
                  value={this.state.searchParams.buildingCode}
                  data-inp="zdbh"
                  onChange={(e) => this.changeState('buildingCode', e.target.value)}
                  placeholder="请输入楼栋编号"
                />
              </FormItem>
              <FormItem>
                <span className="label" style={{ width: 'auto' }}>在租面积：</span>
                <Input
                  style={{ width: 120, textAlign: 'center' }}
                  placeholder="下限面积"
                  value={this.state.searchParams.rentAreaStart}
                  onChange={(e) => this.changeState('rentAreaStart', e.target.value)}
                />
                ~
                <Input
                  style={{ width: 120, textAlign: 'center' }}
                  placeholder="上限面积"
                  value={this.state.searchParams.rentAreaEnd}
                  onChange={(e) => this.changeState('rentAreaEnd', e.target.value)}
                />
              </FormItem>
              {
                this.state.more ? (
                  <React.Fragment>
                    <FormItem>
                      <span className="label" style={{ width: 'auto' }}>建筑面积：</span>
                      <Input
                        style={{ width: 120, textAlign: 'center' }}
                        placeholder="下限面积"
                        value={this.state.searchParams.buildingAreaStart}
                        onChange={(e) => this.changeState('buildingAreaStart', e.target.value)}
                      />
                      ~
                      <Input
                        style={{ width: 120, textAlign: 'center' }}
                        placeholder="上限面积"
                        value={this.state.searchParams.buildingAreaEnd}
                        onChange={(e) => this.changeState('buildingAreaEnd', e.target.value)}
                      />
                    </FormItem>
                    <FormItem>
                      <span className="label" style={{ width: 'auto' }}>在租均价：</span>
                      <Input
                        style={{ width: 120, textAlign: 'center' }}
                        placeholder="下限价格"
                        value={this.state.searchParams.rentAvgStart}
                        onChange={(e) => this.changeState('rentAvgStart', e.target.value)}
                      />
                      ~
                      <Input
                        style={{ width: 120, textAlign: 'center' }}
                        placeholder="上限价格"
                        value={this.state.searchParams.rentAvgEnd}
                        onChange={(e) => this.changeState('rentAvgEnd', e.target.value)}
                      />
                    </FormItem>
                  </React.Fragment>
                ) : ''
              }

              <Row gutter={24} style={{ textAlign: 'center' }}>
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>搜索</Button>
                &emsp;
                <Button type="primary" htmlType="reset" className="evfilterbtn" onClick={() => { this.setState({ more: !this.state.more }) }}>更多条件</Button>
                &emsp;
                <Button type="primary" htmlType="reset" className="evfilterbtn" onClick={this.resetSearchParams}>重置</Button>
                &emsp;
                <Button icon="plus-circle" onClick={this.openAddModal} type="primary" htmlType="reset" className="evfilterbtn">新增楼栋</Button>
                &emsp;
                <Button icon='profile' ghost onClick={this.export} type="primary" htmlType="reset" className="evfilterbtn">生成报表</Button>
              </Row>
            </Form>
          </div>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.tableData}
                pagination={{
                  pageSize: that.state.pageSize,
                  current: that.state.current,
                  onChange: that.pageChange,
                  total: that.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>

            <Modal
              title={this.state.modalType == 'edit' ? "编辑楼栋" : "新增楼栋"}
              visible={this.state.addProjectModalVisible}
              width={830}
              onOk={this.modalSubmit}
              onCancel={this.cancel}
            >
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="楼栋编号：">
                    {getFieldDecorator('buildingCode', {
                      rules: [{ required: true, message: '请输入楼栋编号！' },
                      { max: 20, message: '文字内容超过20字' }]
                    })(<Input placeholder="请输入楼栋编号" disabled={this.state.modalType == "edit"} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="楼栋名称：">
                    {getFieldDecorator('buildingName2', {
                      rules: [{ required: true, message: '请输入楼栋名称！' },
                      { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入楼栋名称" />)}
                  </FormItem>
                </Col>
                <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
                  <FormItem label="房屋数量：">
                    {getFieldDecorator('houseCount', {
                      rules: [{ pattern: /^\d*$/, message: '请输入数值！' },
                      { pattern: /^(?:[1-9]\d{0,3}|0|10000|[1-4]\d{4})$/, message: '房屋数量范围为1-10000的正整数' }]
                    })(<Input disabled />)}
                  </FormItem>
                </Col>
                <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
                  <FormItem label="在租均价：">
                    {getFieldDecorator('rentAvg', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="建筑结构：">
                    {getFieldDecorator('buildingStructure')(
                      <Select placeholder="请选择建筑结构" style={{ width: "100%" }}>
                        <Option value="砖木结构">砖木结构</Option>
                        <Option value="混合结构">混合结构</Option>
                        <Option value="钢筋混凝土结构">钢筋混凝土结构</Option>
                        <Option value="钢结构">钢结构</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="建筑面积：">
                    {getFieldDecorator('buildingArea', {
                      rules: [{ required: true, message: '请输入建筑面积！' },
                      { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入建筑面积" />)}
                  </FormItem>
                </Col>
                <Col span={8} style={{ display: this.state.modalType == "new" ? 'none' : 'inline-block' }}>
                  <FormItem label="在租面积：">
                    {getFieldDecorator('rentArea', {
                      rules: [{ pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="总层数：">
                    {getFieldDecorator('totalFloor', {
                      rules: [{ required: true, message: '请输入总层数！' },
                      { pattern: /^[1-9]$|^[1-4]\d$|50/g, message: '层数范围为1-50' }]
                    })(<Input placeholder="请输入总层数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="地理位置：">
                    {getFieldDecorator('location', {
                      rules: [{ required: true, message: '请输入地理位置！' }]
                    })(<Input placeholder="请输入地理位置" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="所在省份：">
                    {getFieldDecorator('province', {
                      rules: [{ required: true, message: '请选择所在省份！' }]
                    })(<Select
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="请选择所在省份"
                      optionFilterProp="children"
                      onChange={(e) => this.getAreaList('2', e)}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      disabled={this.state.modalType == "edit"}
                    >{this.state.province.map(item => <Option key={item.areaId} value={item.name}>{item.name}</Option>)}
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="所在市：" style={{ textAlign: 'left' }}>
                    {getFieldDecorator('city', {
                      rules: [{ required: true, message: '请选择所在市！' }]
                    })(<Select
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="请选择所在市"
                      optionFilterProp="children"
                      onChange={(e) => this.getAreaList('3', e)}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      disabled={this.state.modalType == "edit"}
                    >{
                        this.state.city.map(item =>
                          <Option key={item.areaId} value={item.name}>{item.name}</Option>
                        )
                      }
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="所在区：">
                    {getFieldDecorator('area', {
                      rules: [{ required: true, message: '请选择所在区！' }]
                    })(<Select
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="请选择所在区"
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      disabled={this.state.modalType == "edit"}
                    >{
                        this.state.area.map(item =>
                          <Option key={item.areaId} value={item.name}>{item.name}</Option>
                        )
                      }
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="负责人：">
                    {getFieldDecorator('principal', {
                      rules: [{ required: true, message: '请输入负责人！' },
                      { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入负责人" />)}
                  </FormItem>
                </Col>
                {
                  this.state.modalType === 'edit' ? (
                    <Col span={8}>
                      <FormItem label="楼栋图片：">
                        <Upload
                          action={Api.amUploadPic}
                          listType="text"
                          accept='.jpg,.jpeg,.png,.gif'
                          fileList={this.state.fileList}
                          onChange={this.handleChange}
                          onRemove={this.handleRemove}
                          beforeUpload={this.beforeUpload}
                          multiple
                          data={{ addressId: this.state.activeBuildingCode }}
                        >
                          {/* {<div>
                          <Icon type="plus" />
                          <div>点击上传图片</div>
                        </div>} */}
                          <Button icon="cloud-upload" type="primary" className="evfilterbtn">上传</Button>
                        </Upload>
                      </FormItem>
                    </Col>
                  ) : null
                }
              </Row>
            </Modal>


            <Modal
              title="开始下载"
              visible={this.state.showDownload}
              width={400}
              wrapClassName="vertical-center-modal"
              onOk={this.showDownload.bind(this, false, 'ok')}
              onCancel={this.showDownload.bind(this, false, 'cancel')}
              style={{
                textAlign: 'center'
              }}
            >
              {this.state.downloadName !== '' ?
                <div>
                  <p>开始自动下载...</p>
                  <p><a download={this.state.downloadName} href={this.state.downloadLink}>下载没有开始，点击此链接手动下载。</a></p>
                </div>
                :
                <div>
                  <p>文件生成中，请稍后。</p>
                  <p><Icon type="loading" /></p>
                </div>
              }
            </Modal>

          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(buildingManagement);
export default WrappedApp;
