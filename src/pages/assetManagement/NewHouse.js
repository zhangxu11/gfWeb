//项目管理
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Select,
  Upload
} from 'antd';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';
import styles from "./style.less";

const { TextArea } = Input;
const confirm = Modal.confirm;
const Option = Select.Option;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const Step = Steps.Step;
import moment from 'moment';
import 'moment/locale/zh-cn';


@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))


class newHouse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectCode: 'PJ20190125',
      choicedIndex: 0,
      buildingData: [],
      modalVisible: false,
      buildingInfo: new Object,
      province: [],
      city: [],
      area: [],
      ownerStatus: [],
      ownerType: [],
      signStatus: [],
      assetStatus: [],
      communitys: [],
      selectdIds: [],
      hookBuilds: [],
      rooms: [],
      cabinets: [],
      boxs: [],
      bags: [],

    };
  }


  componentWillMount() {

    const that = this;
    this.setState({
      buildingCode: this.props.location.state.buildingCode,
      buildingName2: this.props.location.state.buildingName2,
      projectName: this.props.location.state.projectName,
      ownershipType: this.props.location.state.ownershipType,
      transferUnit: that.props.location.state.transferUnit,
      obtainWay: that.props.location.state.obtainWay,
    })

    console.log(this.props.location)

    //获取房间下拉
    this.getRoomList();

    this.getAreaList('1');
  }


  componentDidMount() {
    const that = this;
    //获取系统参数
    axios.all([this.getSysDic("OWNER_SHIP_STATUS"), this.getSysDic("OWNER_SHIP_TYPE"), this.getSysDic("SIGN_STATUS"), this.getSysDic("ASSET_STATUS")])
      .then(axios.spread(function (r1, r2, r3, r4) {
        that.setState({
          ownerStatus: r1.data, //权属权属状态
          ownerType: r2.data,  //权属形式
          signStatus: r3.data,  //签署协议状态
          assetStatus: r4.data,  //资产状态
        }, () => {
          that.props.form.setFieldsValue({
            update_date: moment().format('YYYY-MM-DD HH:mm:ss')
            //   ownershipStatus: '40',
            //   manageType: that.props.location.state.manageType,
            //   transferUnit: that.props.location.state.transferUnit,
            //   obtainWay: that.props.location.state.obtainWay,
            //   ownershipType:that.props.location.state.ownershipType,
          })
        })
      }))
  }


  //获取房间下拉
  getRoomList = () => {
    const that = this;
    axios.post(Api.rmGetPositionList, {})
      .then(function (resp) {
        that.setState({
          rooms: resp.data
        })
      })
  }

  ///获取系统参数
  getSysDic = (type) => {
    return axios.post(Api.amGetSysDic, { key: type })
  }

  //获取省、市、区
  getAreaList = (level, areaId) => {
    const that = this;
    const param = { level: level };
    if (level !== '1') {
      param.id = areaId;
    }
    that.setState({ loading: true })
    return axios.post(Api.amGetAreaList, param)
      .then(function (resp) {
        const { data } = resp;
        if (level === '1') {
          that.setState({ province: data })
        } else if (level === '2') {
          that.setState({ city: data })
          // that.props.form.setFieldsValue({
          //   city: '',
          //   area: ''
          // })
        } else if (level === '3') {
          that.setState({ area: data })
          // that.props.form.setFieldsValue({
          //   area: ''
          // })
        }

        that.setState({ loading: false })
      })
  }

  //根据房间获取柜子/根据柜子过去档案盒
  getPositionList = (type, value) => {
    const that = this;
    return axios.post(Api.rmGetPositionListByParentList, {
      parentId: value
    })
      .then(function (resp) {
        if (type == "cabinets") {
          that.setState({
            cabinets: resp.data
          })
        } else {
          that.setState({
            boxs: resp.data
          })
        }
      })
  }

  //根据档案盒获取档案袋
  getPackageList = (value) => {
    const that = this;
    return axios.post(Api.rmGetPackageList, {
      positionId: value
    })
      .then(function (resp) {
        that.setState({
          bags: resp.data
        })
      })
  }

  addHouse = () => {
    let that = this;
    this.props.form.validateFields((err, values) => {

      this.setState({ loading: true })
      if (!err) {
        let equipmentList = [];
        const forNum = Math.max(values.equipmentName.length, values.equipmentValue.length, values.equipmentModel.length, values.equipmentStatus.length);

        for (let i = 0; i < forNum; i++) {
          var arr = {
            equipmentName: values.equipmentName[i],
            equipmentValue: values.equipmentValue[i],
            equipmentModel: values.equipmentModel[i],
            equipmentStatus: values.equipmentStatus[i],
          };

          equipmentList.push(arr);
        }

        axios.post(Api.amAddHouseInfo, {
          projectCode: 'PJ20190125',
          houseCode: values.houseCode,
          houseName2: values.houseName2,
          buildingArea: Number(values.buildingArea),
          useArea: Number(values.useArea),
          heatingArea: Number(values.heatingArea),
          prerent_price: Number(values.prerent_price),
          isRent: values.isRent,
          renovationCondition: values.renovationCondition,
          update_date: values.update_date,
          update_user: values.update_user,
          notes: values.notes,

          // use: values.use,
          // unitNumber: values.unitNumber,
          // houseNumber: values.houseNumber,
          // buildingStructure: values.buildingStructure,
          // buildYear: values.buildYear,
          // province:values.province,
          // city: values.city,
          // area: values.area,
          // location: values.location,
          // currentFloor:values.currentFloor,
          // isRent: values.isRent,
          // renovationCondition: values.renovationCondition,
          // ownershipType: values.ownershipType,
          transferUnit: that.state.transferUnit,
          houseName: '',
          // obtainWay:values.obtainWay,
          // manageType:values.manageType,
          buildingCode: that.state.buildingCode,
          buildingName2: that.state.buildingName2,
          projectName: that.state.projectName,
          // assetStatus:values.assetStatusm,
          // ownershipStatus:values.ownershipStatus,
          // keyPositionId:values.keyPositionId,
          equipmentList: equipmentList,
          fee: {
            waterFee: values.waterFee,
            gasFee: values.gasFee,
            electricFee: values.electricFee,
            propertyFee: values.propertyFee,
            heatingFee: values.heatingFee,
          }
        }).then(function (resp) {
          const { data } = resp;
          if (data.code == 1) {
            notification.destroy();
            notification['success']({ message: data.msg });
            that.goBack();
          } else {
            notification.destroy();
            notification['warning']({ message: data.msg });
          }
          that.setState({ loading: false });
        })
      }
    })
  }

  goBack = () => {
    router.goBack()
  }


  render() {

    let that = this;
    const { form: { getFieldDecorator, getFieldValue }, currentUserLoading } = this.props;

    let id = 0;
    getFieldDecorator('keys', { initialValue: [0] });
    const keys = getFieldValue('keys');
    const remove = (k) => {
      const { form } = this.props;
      const keys = form.getFieldValue('keys');
      if (keys.length === 1) {
        return;
      }

      form.setFieldsValue({
        keys: keys.filter((key, i) => i !== k),
      });
    }

    const add = () => {
      const { form } = this.props;
      const keys = form.getFieldValue('keys');
      const nextKeys = keys.concat(++id);
      console.log(keys)
      form.setFieldsValue({
        keys: nextKeys,
      });
    }


    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>新增房屋</p>
              <p>HOUSE ADD</p>
            </div>
          </div>


          <div className={styles.formTitle}>基本信息</div>
          <div className={styles.formContent}>
            <Row gutter={24}>
              <Col span={8}>
                <FormItem label="房屋编号：">
                  {getFieldDecorator('houseCode', {
                    rules: [{ required: true, message: '请输入房屋编号！' },
                    { max: 20, message: '长度不能超过20位' }]
                  })(<Input placeholder="请输入房屋编号" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="房屋名称：">
                  {getFieldDecorator('houseName2', {
                    rules: [{ required: true, message: '请输入房屋名称！' },
                    { max: 50, message: '长度不能超过50位' }]
                  })(<Input placeholder="请输入房屋名称" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="建筑面积：">
                  {getFieldDecorator('buildingArea', {
                    rules: [{ required: true, message: '请输入建筑面积！' },
                    { pattern: /^[0-9]{1,4}([.][0-9]{1,2})?$/, message: '面积应小于10000，限制小数点后两位' }]
                  })(<Input placeholder="请输入建筑面积" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="使用面积：">
                  {getFieldDecorator('useArea', {
                    rules: [{ required: true, message: '请输入使用面积！' },
                    { pattern: /^[0-9]{1,4}([.][0-9]{1,2})?$/, message: '面积应小于10000，限制小数点后两位' }]
                  })(<Input placeholder="请输入使用面积" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="供热面积：">
                  {getFieldDecorator('heatingArea', {
                    rules: [{ required: true, message: '请输入供热面积！' },
                    { pattern: /^[0-9]{1,4}([.][0-9]{1,2})?$/, message: '面积应小于10000，限制小数点后两位' }]
                  })(<Input placeholder="请输入供热面积" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="预租单价：">
                  {getFieldDecorator('prerent_price', {
                    rules: [{ required: true, message: '请输入预租单价！' },
                    { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入预租单价" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="租赁状态：">
                  {getFieldDecorator('isRent', {
                    rules: [{ required: true, message: '请选择租赁状态！' }]
                  })(<Select placeholder="请选择租赁状态" style={{ width: '100%' }}>
                    <Option value='租赁中'>租赁中</Option>
                    <Option value='待租赁'>待租赁</Option>
                  </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="装修情况：">
                  {getFieldDecorator('renovationCondition', {
                    rules: [{ required: true, message: '请输入装修情况！' }]
                  })(<Select placeholder="请选择装修情况" style={{ width: '100%' }}>
                    <Option value='精装'>精装</Option>
                    <Option value='简装'>简装</Option>
                    <Option value='毛坯'>毛坯</Option>
                  </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="操作时间：">
                  {getFieldDecorator('update_date', {
                    rules: [{ required: true }]
                  })(<Input disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="操作人:">
                  {getFieldDecorator('update_user', {
                    rules: [{ required: true, message: '请输入操作人！' }
                      , { max: 50, message: '文字内容超过50字' }]
                  })(<Input placeholder="请输入操作人" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="备注：">
                  {getFieldDecorator('notes', {
                    rules: [{
                      max: 200, message: '输入内容超过200字'
                    }]
                  })(<TextArea rows={4} placeholder="请输入备注" />)}
                </FormItem>
              </Col>

              {/* <Col span={8}>
                    <FormItem label="用途：">
                      {getFieldDecorator('use',{
                          rules:[{required:true,message:'请选择用途！'}]
                      })(
                      <Select placeholder="请选择用途" style={{width:"100%"}}>
                        <Option value='保障性住房'>保障性住房</Option>
                        <Option value='公房'>公房</Option>
                        <Option value='学校宿舍'>学校宿舍</Option>
                        <Option value='普通住宅'>普通住宅</Option>
                        <Option value='临建房'>临建房</Option>
                        <Option value='车库'>车库</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="权属形式：">
                      {getFieldDecorator('ownershipType')(<Select placeholder="请择权属形式" style={{width:"100%"}} disabled>
                        {
                          this.state.ownerType.map(item => 
                            <Option key={item.dicCode} value={item.dicCode}>{item.dicName}</Option>)
                        }
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="权属状态：">
                      {getFieldDecorator('ownershipStatus')(
                      <Select placeholder="请选择权属状态" disabled style={{width:"100%"}}>
                        {
                          this.state.ownerStatus.map((item,i) => 
                            <Option key={item.dicCode} value={item.dicCode}>{item.dicName}</Option>)
                        }
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="管理方式：">
                      {getFieldDecorator('manageType')(
                      <Select placeholder="请选择管理方式" style={{width:"100%"}} disabled>
                        <Option value='自管'>自管</Option>
                        <Option value='托管'>托管</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="单元号：">
                      {getFieldDecorator('unitNumber',{
                        rules:[{required:true,message:'请输入单元号！'},
                        {pattern:/^\d*$/,message:'请输入数值！'}]
                      })(<Input placeholder="请输入单元号"/>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="户号：">
                      {getFieldDecorator('houseNumber',{
                        rules:[{required:true,message:'请输入户号！'}]
                      })(<Input placeholder="请输入户号"/>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="划转单位：">
                      {getFieldDecorator('transferUnit')(<Input placeholder="请输入划转单位" disabled/>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="获取方式：">
                      {getFieldDecorator('obtainWay')(<Select placeholder="请选择获取方式" style={{width:"100%"}} disabled>
                        <Option value='划转'>划转</Option>
                        <Option value='收购'>收购</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑结构：">
                      {getFieldDecorator('buildingStructure')(<Select placeholder="请选择建筑结构" style={{width:"100%"}}>
                      <Option value="砖木结构">砖木结构</Option>
                      <Option value="混合结构">混合结构</Option>
                      <Option value="钢筋混凝土结构">钢筋混凝土结构</Option>
                      <Option value="钢结构">钢结构</Option>
                    </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑年份：">
                      {getFieldDecorator('buildYear',{
                        rules:[
                        {pattern:/^[1,2]\d{3}$/,message:'请输入四位年份！'}]
                      })(<Input placeholder="请输入建筑年份"/>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                      <FormItem label="所在省份：">
                        {getFieldDecorator('province',{
                          rules:[{required:true,message:'请选择所在省份！'}]
                        })(<Select
                            showSearch style={{width:'100%'}}
                            placeholder="请选择所在省份"
                            optionFilterProp="children"
                            onChange={(e)=>this.getAreaList('2',e)}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                          >{
                            this.state.province.map(item=>
                              <Option key={item.areaId} value={item.name}>{item.name}</Option>
                              )
                            }
                        </Select>)}
                      </FormItem>
                    </Col>
                    <Col span={8}>
                      <FormItem label="所在市：" style={{textAlign:'left'}}>
                        {getFieldDecorator('city',{
                          rules:[{required:true,message:'请选择所在市！'}]
                        })(<Select
                          showSearch style={{width:'100%'}}
                          placeholder="请选择所在市"
                          optionFilterProp="children"
                          onChange={(e)=>this.getAreaList('3',e)}
                          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >{
                          this.state.city.map(item=>
                            <Option key={item.areaId} value={item.name}>{item.name}</Option>
                            )
                          }
                      </Select>)}
                      </FormItem>
                    </Col>
                    <Col span={8}>
                      <FormItem label="所在区：">
                        {getFieldDecorator('area',{
                          rules:[{required:true,message:'请选择所在区！'}]
                        })(<Select
                          showSearch style={{width:'100%'}}
                          placeholder="请选择所在区"
                          optionFilterProp="children"
                          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >{
                          this.state.area.map(item=>
                            <Option key={item.areaId} value={item.name}>{item.name}</Option>
                            )
                          }
                      </Select>)}
                      </FormItem>
                    </Col>
                    <Col span={8}>
                      <FormItem label="详细地址：">
                        {getFieldDecorator('location',{
                          rules:[{required:true,message:'请输入详细地址！'}]
                        })(<Input placeholder="请输入详细地址"/>)}
                      </FormItem>
                    </Col>
                    <Col span={8}>
                      <FormItem label="所在层：">
                        {getFieldDecorator('currentFloor',{
                          rules:[{required:true,message:'请输入所在层！'},
                          {pattern:/^\d*$/,message:'请输入数值！'}]
                        })(<Input placeholder="请输入所在层"/>)}
                      </FormItem>
                    </Col> */}


            </Row>
          </div>

          <div className={styles.formTitle}>
            设备信息
                  <Button type="primary" icon="plus" size="small" onClick={add} style={{ float: 'right' }}>添加</Button>
          </div>
          <div className={styles.formContent}>
            {
              keys.map((k, index) => (
                <Row gutter={24} key={index}>
                  <Col span={8}>
                    <FormItem label="设备名称：">
                      {getFieldDecorator(`equipmentName[${index}]`, {
                        rules: [
                          { max: 50, message: '文字内容超过50字' }]
                      })(<Input placeholder="请输入设备名称" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="设备价值：">
                      {getFieldDecorator(`equipmentValue[${index}]`, {
                        rules: [{ pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '设备价值应小于1千万，限制小数点后两位' }
                        ]
                      })(<Input placeholder="请输入设备价值" />)}
                    </FormItem>
                  </Col>
                  <Col span={7}>
                    <FormItem label="设备型号：">
                      {getFieldDecorator(`equipmentModel[${index}]`, {
                        rules: [
                          { max: 50, message: '文字内容超过50字' }]
                      })(<Input placeholder="请输入设备型号" />)}
                    </FormItem>
                  </Col>
                  <Col span={7}>
                    <FormItem label="设备状态：">
                      {getFieldDecorator(`equipmentStatus[${index}]`)(<Select placeholder="请选择设备状态" style={{ width: '100%' }}>
                        <Option value='正常'>正常</Option>
                        <Option value='损坏'>损坏</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  {keys.length > 1 && index > 0 ? (
                    <Icon style={{ height: '75px', lineHeight: '120px' }}
                      className="dynamic-delete-button"
                      type="minus-circle-o"
                      disabled={keys.length === 1}
                      onClick={() => remove(index)}
                    />
                  ) : null}
                </Row>
              ))
            }

          </div>

          {/* <div className={styles.formTitle}>钥匙信息</div>
                  <div className={styles.formContent}>
                    <Row gutter={24}>
                      <Col span={8}>
                          <FormItem label='所在房间：'>
                              {getFieldDecorator('m_room')(<Select style={{width:"100%"}} placeholder="请选择所在房间" onChange={(value)=>this.getPositionList("cabinets",value)}>
                                  {this.state.rooms.map((item,index)=>
                                      <Option key={index} value={item.positionId}>{item.positionDesc}</Option>
                                  )}
                              </Select>)}
                          </FormItem>
                      </Col>
                      <Col span={8}>
                          <FormItem label='所在档案柜：'>
                              {getFieldDecorator('m_cabinet')(<Select style={{width:"100%"}}placeholder="请选择所在档案柜" onChange={(value)=>this.getPositionList('boxs',value)}>
                                  {this.state.cabinets.map((item,index)=>
                                      <Option key={index} value={item.positionId}>{item.positionDesc}</Option>
                                  )}
                              </Select>)}
                          </FormItem>
                      </Col>
                      <Col span={8}>
                        <FormItem label='所在档案盒：'>
                            {getFieldDecorator('m_box')(<Select style={{width:"100%"}}placeholder="请选择所档案盒" onChange={(value)=>this.getPackageList(value)}>
                                {this.state.boxs.map((item,index)=>
                                    <Option key={index} value={item.positionId}>{item.positionDesc}</Option>
                                )}
                            </Select>)}
                        </FormItem>
                      </Col>
                      <Col span={8}>
                        <FormItem label='所在档案袋：'>
                            {getFieldDecorator('keyPositionId')(<Select style={{width:"100%"}}placeholder="请选择所档案袋">
                                {this.state.bags.map((item,index)=>
                                    <Option key={index} value={item.packageId}>{item.packageName}</Option>
                                )}
                            </Select>)}
                        </FormItem>
                      </Col>
                    </Row>
                  </div> */}

          <div className={styles.formTitle}>费用信息</div>
          <div className={styles.formContent}>
            <Row gutter={24}>
              <Col span={8}>
                <FormItem label="水费：">
                  {getFieldDecorator('waterFee', {
                    rules: [
                      { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入水费" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="电费：">
                  {getFieldDecorator('electricFee', {
                    rules: [
                      { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入电费" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="燃气费：">
                  {getFieldDecorator('gasFee', {
                    rules: [
                      { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入燃气费" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="供暖费：">
                  {getFieldDecorator('heatingFee', {
                    rules: [
                      { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入供暖费" />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="物业费：">
                  {getFieldDecorator('propertyFee', {
                    rules: [
                      { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                  })(<Input placeholder="请输入物业费" />)}
                </FormItem>
              </Col>
            </Row>
            <Row style={{ textAlign: 'center' }}>
              <Button icon="check-circle"
                onClick={this.addHouse}
                type="primary"
                htmlType="reset"
                className="evfilterbtn"
              >
                提交
                    </Button>
              &emsp;
                    <Button icon='arrow-left' ghost
                onClick={this.goBack}
                type="primary"
                htmlType="reset"
                className="evfilterbtn"
              >
                返回
                    </Button>
            </Row>
          </div>

        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(newHouse);
export default WrappedApp;
