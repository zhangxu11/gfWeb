import React, { Component } from 'react';
import { Input, Col, Select, Button, Tooltip, Tag, Pagination, Checkbox, Radio, Icon, Row, Badge, message } from 'antd';

import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import IframeComm from "../../components/IframeComm";
import QueueAnim from 'rc-queue-anim';

import Info from "./Info";
import Chart from "./Chart";

import styles from "./style.less";
import ico_house from "../../assets/ico_house.png";
import { relative } from 'path';

const InputGroup = Input.Group;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const RadioGroup = Radio.Group;


class ImageContrast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchVal: '',
      sendMapData: '',
      showTotalResult: true,
      current: 1,
    };
  }

  searchChange(e) {
    this.setState({searchVal:e.target.value})
  };


  searchTotal = () => {
    axios.get(Api.icBuildingCount).then((res) => {
      this.setState({
        totalResult: res.data,
        showTotalResult: true,
        resultDatasCount: 1
      });
    })
  }

  componentWillMount() {
    this.searchTotal();
  }

  onReceiveMessage(data) {
    
  }

  clickSearch = (flag) => {
    this.setState({ current: 1 }, ()=> {
      this.search(flag);
    })
  }

  search = (flag) => {
    const that = this;
    let searchContent = that.state.searchVal;
    if (flag == 'total') {
      searchContent = '';
      that.setState({searchVal:''})
    }
    axios.get(Api.icQueryBuilding, {
      params:{
        currentPage: that.state.current,
        searchContent: searchContent
      }
    }).then((res) => {
      this.setState({
        resultDatas: res.data.list,
        showTotalResult: false,
        resultDatasCount: res.data.totalSize,
      });
    })
  }

  showTotal(total) {
    return <span>共<span style={{ color: '#e22534' }}>{total}</span>条结果</span>;
  }
  
  changePage(page, size) {
    this.setState({ current: page }, () => {
      this.search();
    })
  }

  returnTotal = () => {
    this.setState({
      showTotalResult: true,
      resultDatasCount: 1
    });
  }

  toMap = (buildingId) => {
    this.setState({
      sendMapData: {
        buildingId: buildingId,
        type: 'buildingId'
      }
    })
  }

  toBuildingDetail = (buildingCode) => {
    var win = window.open(`/fe/assetMap/buildingDetails?buildingCode=${buildingCode}`, '_blank');
    win.focus();
  }

  render() {
    return (
      <div className={styles.mapBox}>
        <Row gutter={24} className={styles.ImgContrast}>
          <div className={styles.leftDiv}>
            <div className={styles.searchBox}>
              <InputGroup compact>
                <Select
                  className={styles.select}
                  defaultValue="1"
                  // onChange={this.changeLeft.bind(this)}
                >
                  <Option value="1">自有房产</Option>
                </Select>

                <Input className={styles.search} placeholder='请输入内容' onChange={this.searchChange.bind(this)} value={this.state.searchVal} />
                
                <Tooltip placement="bottom" title='搜索'>
                  <Button type="primary" icon="search" onClick={this.clickSearch.bind(this)} />
                </Tooltip>
              </InputGroup>
            </div>

            <div className={styles.searchResult}>
              <QueueAnim component='ul' className={styles.mandatory}>
                {
                  this.state.showTotalResult && this.state.totalResult ?
                    /* 统计 */
                    <li className={styles.searchResultItem}
                      style={{
                        position: 'sticky',
                        top: 0,
                        zIndex: 99
                      }}
                    >
                      <div className={styles.totalResult}>
                        <img src={ico_house} />
                        <div onClick={()=>this.clickSearch('total')}>
                          <div className={styles.item}>共<span className={styles.bold}>{this.state.totalResult.buildcount}</span>个楼栋</div>
                          <div className={styles.item}>共<span className={styles.bold}>{this.state.totalResult.housecount}</span>个房产</div>
                          <div className={styles.item}>总面积：<span className={styles.bold}>{this.state.totalResult.area}</span>平方米</div>
                        </div>
                      </div>
                    </li>
                    :
                    /* 列表 */
                    
                      (this.state.resultDatas && this.state.resultDatas.length > 0 ?
                        this.state.resultDatas.map((item, index) => {
                          return (
                            <li key={index} className={styles.searchResultItem}
                              style={{
                                position: 'sticky',
                                top: 0,
                                zIndex: 99
                              }}
                            >
                              <div className={styles.result}>
                                <a href="javascript:;" className={styles.results_add_h4} onClick={()=>this.toMap(item.buildingId)}>{item.buildingName}</a>&nbsp;
                                <a href="javascript:;" style={{ color: '#06C;' }} onClick={()=>this.toBuildingDetail(item.buildingCode)}>详情</a>
                                <br />
                                <span style={{ lineHeight: '24px' }} title={item.address}>{item.address}</span>
                              </div>
                              <img src={`http://gisams-hitech.cindata.cn/res_file${item.imgUrl[0]}`} className={styles.listImg} />
                              <div style={{ clear: 'both' }}></div>
                            </li>
                          )
                        })
                        : null)
                  }
                
              
              
              </QueueAnim>
              {
                this.state.showMapDiv ? null :
                  <div style={{postion:'relative',height:'45px'}}>
                      <Pagination size="small" total={this.state.resultDatasCount} current={this.state.current} showTotal={this.showTotal.bind(this)} className='pd10' style={{
                        padding: 10
                      }}
                      onChange={this.changePage.bind(this)}
                    />
                    <a href='javascript:;' onClick={this.returnTotal} className={styles.return} style={{display:this.state.showTotalResult?'none':'inline'}}>返回</a>
                </div>
              }

            </div>
          </div>
          {Number(sessionStorage.getItem('isMap')) === 1 ?
            <div className={styles.rightDiv} style={{ width: 'calc(100% - 410px)'}}>
              <IframeComm
                attributes={{
                  src: "/gis/mapImage.html",
                  style: {
                    width: '100%',
                    height: '100%',
                    top: '0px',
                  }
                }}
                postMessageData={this.state.sendMapData}
                handleReceiveMessage={this.onReceiveMessage.bind(this)}
              />
            </div>
            :
            null
          }
          </Row>
        </div>
    );
  }
}

export default ImageContrast;
