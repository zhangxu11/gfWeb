import { Card, Col, Progress, } from 'antd';
import * as React from 'react';

/**
 * 退租原因组件
 *
 * @export
 * @param {{ reason: string, value: string}} props
 * @returns
 */
export default function RentReasonCard(props: { reason: string, value: string}) {
  const { reason, value } = props;
  const percent = value ? Number(value.replace('%', '')) : 0;
  return (
    <Col span={4}>
      <Card style={{ width: 205 }} title={reason} headStyle={{border: 0}} bordered={false}>
        <div style={{float: 'left'}}>
          <p style={{fontSize: 12, color: '#a4a4a4'}}>占比率</p>
          <p style={{fontSize: 28, color: '#484848'}}>{percent}%</p>
        </div>
        <Progress style={{float: 'right'}} type="circle" showInfo={false} percent={percent} width={64} strokeColor="#ff6c74" strokeWidth={20} />
      </Card>
    </Col>
  );
}
