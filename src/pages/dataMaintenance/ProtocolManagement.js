// 项目管理
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Tabs,
  Spin,
  Tooltip,
  Select,
  Upload,
  Popover
} from 'antd';

import router from 'umi/router';
import Link from 'umi/link';
import { formatTreeData } from '../../components/commonAips';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import styles from './style.less'
import ico_map from "../../assets/ico_map.png";
import ico_nodata from "../../assets/ico_nodata.png";
import IframeComm from '../../components/IframeComm';
import { ApprovalContent } from '@/components/ApprovalContent';

const { confirm } = Modal;
const { TabPane } = Tabs;
const { RangePicker } = DatePicker;
const { Step } = Steps;
const { Option } = Select;
const FormItem = Form.Item;
const { TextArea } = Input;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent'],
}))
class ProtocolManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchParmas: { // 搜索参数
        rentName: null, // 企业名称
        startDate: null, // 开始时间
        endDate: null, // 结束时间
        // status: null, // 协议状态
        // type: null, // 协议类型
        yearRent: null, // 当年租金
      },
      currentPage: 1, // 当前页数
      total: 0, // 总页数
      loading: false,
      pageSize: 5, // 最大页数
      showCreateModel: false,
      showMapModal: false,
      data: [],
      orgTreeData: [], // 树
      houseList: [],
      mapHouse: null,
      sendMapData: {},
      renterList: [],
      selectedArr: [], // 多选
      startDownload: false, // 开始下载
      downloadLink: '', // 下载连接
      downloadName: '', // 下载名
      showDownload: false, // 显示下载
      approvalList: [], // 审批进度
      startYearRent: null,
      endYearRent: null,
      isApproval: false
    };
  }

  componentDidMount() {
    const userName = sessionStorage.getItem('currentUser');
    this.searchData();
    axios.get(Api.getIndustryTree, {}).then((resp) => {
      const { data } = resp;
      if (data) {
        const treeData = new Array(data)[0];
        formatTreeData(treeData, 'name', 'orgLevelFormat', 'industryId', 'industryList');
        this.setState({ orgTreeData: treeData });
      }
    });

    axios.post(Api.queryRenter, {})
      .then((resp) => {
        const { data } = resp;
        this.setState({
          renterList: data
        })
      })

    axios.post(Api.getFunListByUserCode, { userCode: userName })
      .then((resp) => {
        const userInfo = resp.data;
        let isApproval = false;
        console.log(userInfo)
        userInfo.map(item => {
          if (item.functionId === 110005) {
            isApproval = true
          }
        })
        this.setState({
          userInfo,
          isApproval
        })
      })
  }

  renterChange = (value, option) => {
    // 获取房屋信息
    axios.post(Api.queryRenterInfo, {
      renterId: value
    })
      .then((resp) => {
        const { data } = resp;
        this.props.form.setFieldsValue({
          person: data.linkman ? data.linkman : '-',
          cardNumber: data.card_num ? data.card_num : '-',
          industry: data.hy_type_str ? data.hy_type_str : '-',
          contact: data.contact_way ? data.contact_way : '-',
          mail: data.mailbox ? data.mailbox : '-',
          mainContent: data.main_business ? data.main_business : '-',
          investNumber: data.investment_amount ? data.investment_amount : '-',
          personSize: data.people_num ? data.people_num : '-',
          renterStatus: data.renter_situation ? data.renter_situation : '-',
          hConpermany: data.high ? data.high : '-',
          foundDate: data.established_time ? data.established_time.substr(0, 10) : '-',
          rentIncome: data.full_income ? data.full_income : '-',
          rType: data.zc_type ? data.zc_type : '-',
          income: data.income ? data.income : '-',
          company: data.intro ? data.intro : '-'
        });
      })
  }

  // 查询楼栋数据
  getHouseByParams = (buildingId) => {
    if (buildingId) {
      axios
        .get(Api.queryHouseByParams, {
          params: {
            buildingId,
          },
        })
        .then(res => {
          if (res.data === null) {
            notification.destroy();
            notification.warning({ message: '暂无数据！' });
          } else {
            const data = res.data.houses && res.data.houses.map(item => ({ houseCode: item.houseCode, isRent: item.isRent }))
            this.setState({
              sendMapData: {
                type: 'clickedLD',
                data,
                buildingId,
                floors: res.data.floors
              }
            });
          }
        });
    }
  }

  onReceiveMessage = (data) => {
    if (data.data.type === 'clickedLD') {
      this.getHouseByParams(data.data.build_id);
    }

    let flag = false;
    if (data.data.type === 'clickedLDR') {
      const info = data.data.attribute;
      this.state.houseList && this.state.houseList.map(item => {
        if (item.houseCode === data.data.house_id) {
          flag = true;
        }
      })
      if (flag) {
        notification.destroy();
        notification.warning({ message: '该房间已选择，请重新选择！' })
        this.setState({ mapHouse: null });
        return;
      }
      const par = {
        houseCode: data.data.house_id,
        houseName2: info.house_name2,
        buildingName2: info.building_name2,
        useArea: 1, // info.use_area,
        rentPrice: info.prerent_price,
        renovationCondition: info.renovation_condition,
        buildingArea: info.building_area, // 建筑面积
        heatingArea: info.heating_area, // 供热面积
        useArea: info.use_area, // 使用面积
      };
      this.setState({ mapHouse: par }, () => {
        notification.destroy();
        notification.success({ message: `已选择房间${par.houseName2}` });
      });
    }
  }

  changeMapModal = (visible) => {
    this.setState({ showMapModal: visible });
  }

  // 删除选择的房屋
  delHouse = (index) => {
    const { houseList } = this.state;
    houseList.splice(index, 1);
    this.setState({ houseList }, () => {
      this.computRent();
    });
  }

  setHouseToModal = () => {
    if (this.state.mapHouse && this.state.houseList) {
      const { houseList } = this.state;
      houseList.push(this.state.mapHouse);
      this.setState({ houseList }, () => {
        this.computRent();
      });
    }
    this.changeMapModal(false)
  }

  computRent = () => {
    let rentMoney = 0;
    let rentTotalMoney = 0;
    const zlArea = 0;
    const chArea = 0;
    let useArea = 0;
    let gnArea = 0;
    let jzArea = 0;
    this.state.houseList.forEach(item => {
      if (item.rentPrice) {
        rentMoney += Number(item.rentPrice);
        if (item.useArea) rentTotalMoney += Number(item.useArea) * Number(item.rentPrice);
      }
      if (item.buildingArea) jzArea += Number(item.buildingArea);
      if (item.useArea) useArea += Number(item.useArea);
      if (item.heatingArea) gnArea += Number(item.heatingArea);
    })
    rentMoney = rentMoney ? rentMoney / this.state.houseList.length : '';
    this.props.form.setFieldsValue({
      rentMoney,
      rentTotalMoney: rentTotalMoney || '',
      jzArea,
      gnArea,
      useArea,
      zlArea: jzArea,
      chArea: jzArea,
    });
  }

  // changeSelected = (selectedRowKeys, selectedRows) => {
  //   const selectdIds = [];
  //   selectedRows &&
  //     selectedRows.forEach((d, i, arr) => {
  //       selectdIds.push(d.projectCode);
  //     });

  //   this.setState({ selectedArr: selectedRowKeys, selectdIds });
  // };

  changeParmas = (key, value) => {
    const searchParmas = { ... this.state.searchParmas };
    searchParmas[key] = value;
    this.setState({ searchParmas });
  }

  resetSearch = () => {
    this.setState({
      searchParmas: { // 搜索参数
        rentName: '', // 企业名称
        startDate: null, // 开始时间
        endDate: null, // 结束时间
        // status: null, // 协议状态
        // type: null, // 协议类型
      },
      currentPage: 1, // 当前页面
      selectedArr: [], // 多选
      startDownload: false, // 开始下载
      downloadLink: '', // 下载连接
      downloadName: '', // 下载名
      showDownload: false, // 显示下载
      startYearRent: null,
      endYearRent: null,
    }, () => {
      this.searchData();
    });
  }

  getApproval = (contractId) => {
    axios.post(Api.getApproval, {
      contractId,
    }).then(res => {
      // const approvalList = [
      //   {}, {}, {}, {}
      // ];
      // res.data.items.forEach(approval => {
      //   if (approval.contractStatus !== '9') return;
      //   approvalList[Number(approval.contractPerple) - 1] = {
      //     contractDate: moment(approval.contractDate).format("YYYY-MM-DD")
      //   };
      // });
      // this.setState({ approvalList, currentContractId: contractId });
    });
  }

  approvalClick = (e) => {
    let that = this;
    confirm({
      title: '是否确定通过审批?',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        axios.post(Api.disorderApproval, {
          contractId: e.contract_id,
          node: '1',
        }).then((resp) => {
          that.searchData();
        })
      },
      onCancel() {
        console.log('Cancel');
      },
    });

  }

  /**
   * 提交协议
   *
   * @param {{contract_id: number}} item
   * 
   * @memberof ProtocolManagement
   */
  commitApproval = (item) => {
    axios.post(Api.commitApproval, {
      contractId: item.contract_id
    }).then(res => {
      this.searchData();
    });
  }

  /**
   * 删除协议
   *
   * @param {{contract_id: number}} item
   * @memberof ProtocolManagement
   */
  delApproval = (item) => {
    axios.post(Api.delApproval, {
      contractId: item.contract_id
    }).then(res => {
      this.setState({ currentPage: 1 }, () => {
        this.searchData();
      });
    });
  }

  query = () => {
    this.setState({ currentPage: 1 }, () => {
      this.searchData();
    });
  };

  searchData = () => {
    this.setState({ loading: true });
    axios.post(Api.queryProtocol, {
      rentName: this.state.searchParmas.rentName,
      offset: this.state.currentPage,
    }).then(res => {
      const {
        currentPage,
        endSize,
        totalSize,
        pageSize,
        list,
      } = res.data;
      list.forEach(item => {
        if (item.rent_area && item.rent_price) item.sum_count = (item.rent_area * item.rent_price).toFixed(2);
        else item.sum_count = null;
      });
      this.setState({
        currentPage,
        total: totalSize,
        loading: false,
        pageSize,
        data: list,
      });
    })
  }

  create = () => {
    const houseCodes = this.state.houseList && this.state.houseList.map(item => item.houseCode);
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios.post(Api.contractadd, {
          houseCodeList: houseCodes.join(','),
          renterId: values.rentName
        })
          .then((resp) => {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification.success({ message: data.msg });
              this.searchData();
              window.location.reload();
              this.setModalVisible(false);
            } else if (data.code === 0) {
              notification.destroy();
              notification.error({ message: data.msg });
            }
            this.setState({ loading: false })
          })
      }
    })
  }

  cancel = () => {
    this.setState({
      houseList: []
    })
    this.props.form.setFieldsValue({
      rentName: '',
      person: '',
      cardNumber: '',
      industry: '',
      contact: '',
      mail: '',
      mainContent: '',
      investNumber: '',
      personSize: '',
      renterStatus: '',
      hConpermany: '',
      foundDate: '',
      rentIncome: '',
      rType: '',
      income: '',
      company: ''
    });
    this.setModalVisible(false)
  }
  setModalVisible = (show) => {
    this.setState({
      houseList: []
    })
    this.props.form.setFieldsValue({
      rentName: '',
      person: '',
      cardNumber: '',
      industry: '',
      contact: '',
      mail: '',
      mainContent: '',
      investNumber: '',
      personSize: '',
      renterStatus: '',
      hConpermany: '',
      foundDate: '',
      rentIncome: '',
      rType: '',
      income: '',
      company: ''
    });
    this.setState({ showCreateModel: show });
  }

  pageChange = (page) => {
    this.setState({
      currentPage: page,
      selectedArr: [], // 多选
      startDownload: false, // 开始下载
      downloadLink: '', // 下载连接
      downloadName: '', // 下载名
      showDownload: false, // 显示下载
    },
      () => {
        this.searchData();
      }
    );
  };

  export = () => {
    // if (this.state.selectdIds.length !== 0) {
    // const param = this.state.selectdIds.join(',');
    this.setState({ startDownload: false });
    axios.post(Api.exportProtocol, {
      rentName: this.state.searchParmas.rentName,
      offset: this.state.currentPage,
    },
      {
        responseType: 'arraybuffer'
      }).then((res) => {
        notification.destroy();
        notification.success({ message: '开始下载！' })

        const blob = new Blob([res.data], { type: "application/vnd.ms-excel" });
        const [dLink, name] = [
          window.URL.createObjectURL(blob),
          `协议列表${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.xls`
        ];
        const link = document.createElement('a');
        link.href = dLink;
        link.download = name;
        link.click();
        this.setState({
          startDownload: true,
          downloadLink: dLink,
          downloadName: name,
          showDownload: true
        });
      }).catch((err) => {
        notification.destroy();
        notification.warning({ message: `下载失败！错误信息：${err}` })
      });
    // } else {
    //   notification.destroy();
    //   notification.warning({ message: '请选择表格中的项后进行导出！' })
    // }
  };

  disorderApproval = (approval, canApproval, index) => {
    if (approval.contractDate || (canApproval !== (index + 1))) return;
    axios.post(Api.disorderApproval, {
      contractId: this.state.currentContractId,
      node: canApproval,
    }).then((resp) => {
      this.getApproval(this.state.currentContractId);
      this.searchData();
    })

  }
  changeState = (key, value) => {
    this.setState({
      [key]: value
    });
  }

  render() {
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;
    const canApproval = sessionStorage.getItem('canApproval');
    const canCommit = sessionStorage.getItem('canCommit');

    const columns = [
      {
        title: '企业名称',
        dataIndex: 'renter_name',
        key: 'renter_name',
        render: (text, item, index) => (
          <Link target='_blank' to={{
            pathname: '/fe/dataMaintenance/protocolDetails',
            search: `?contractId=${item.contract_id}`
          }}>{text}</Link>
        ),
      },
      {
        title: '楼栋名称',  // 空着
        dataIndex: 'building_name2',
        key: 'building_name2',
      },
      {
        title: '房屋名称',
        dataIndex: 'house_name2',
        key: 'house_name2',
      },
      {
        title: '出租面积',
        dataIndex: 'rent_area',
        key: 'rent_area',
      },
      {
        title: '租金单价（元/㎡年）',
        dataIndex: 'rent_price',
        key: 'rent_price',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <span>
            {canCommit && item.commit_flag !== '1' ? <a style={{ display: 'inline-block', marginRight: '5px' }} href="javascript:;" onClick={() => this.commitApproval(item)}>提交</a> : null}
            {canCommit && item.commit_flag !== '1' ? <a style={{ display: 'inline-block', marginRight: '5px' }} href="javascript:;" onClick={() => this.delApproval(item)}>删除</a> : null}
            {canApproval && item.commit_flag === '1' && this.state.isApproval ? <a href="javascript:;" onClick={() => this.approvalClick(item)}>审批</a> : null}
          </span>
        ),
      },
    ];

    // const rowSelection = {
    //   selectedRowKeys: this.state.selectedArr,
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.changeSelected(selectedRowKeys, selectedRows);
    //   },
    // };

    return (
      <PageHeaderWrapper loading={currentUserLoading}>
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>协议管理</p>
              <p>PROTOCOL MANAGEMENT</p>
            </div>
          </div>

          <Form layout="inline" align='center'>
            <FormItem>
              <span className="label">企业名称：</span>
              <Input
                value={this.state.searchParmas.rentName}
                data-inp="zdbh"
                onChange={(e) => this.changeParmas('rentName', e.target.value)}
                placeholder="请输入企业名称"
              />
            </FormItem>

            <Row>
              <Col span={24} style={{ textAlign: 'center', marginBottom: '10px' }}>
                <Button icon="plus-circle" type="primary" className="evfilterbtn" onClick={() => this.setModalVisible(true)}>
                  新增
                </Button>
                &emsp;
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>
                  搜索
                </Button>
                &emsp;
                <Button
                  onClick={this.resetSearch}
                  type="primary"
                  htmlType="reset"
                  icon="reload"
                >
                  重置
                </Button>
                &emsp;
                <Button
                  icon='profile'
                  ghost
                  onClick={this.export}
                  type="primary"
                  htmlType="reset"
                  className="evfilterbtn"
                >
                  生成报表
                </Button>
              </Col>
            </Row>
          </Form>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.data}
                // rowKey={record => {
                //   return record.renterId
                // }}
                pagination={{
                  pageSize: this.state.pageSize,
                  current: this.state.currentPage,
                  onChange: this.pageChange,
                  total: this.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>
          </Spin>

          <Modal
            title="新建协议"
            wrapClassName="vertical-center-modal"
            className='modalAlign'
            visible={this.state.showCreateModel}
            onOk={this.create}
            onCancel={this.cancel}
            width={830}
          >
            <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>房屋基本信息</Row>
            <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
            <Row gutter={24} style={{ marginBottom: '10px' }}>
              <Col span={24} style={{ textAlign: 'center', borderBottom: "1px dashed #eee" }}>
                <Button type="primary" style={{ margin: '13px 0' }} onClick={() => this.changeMapModal(true)}>请选择房屋&nbsp;&nbsp;<img src={ico_map} width={19} style={{ float: 'right' }} /></Button>
              </Col>
              <Col span={24} style={{ maxHeight: '225px', overflowY: "auto" }}>
                {
                  !this.state.houseList || this.state.houseList.length === 0 ?
                    <div className={styles.noData}><img src={ico_nodata} width={169} /><br />暂无数据</div>
                    :
                    this.state.houseList.map((item, index) => (
                      <Row gutter={24} className={styles.mapHouse} key={item.houseCode}>
                        <span className={styles.dot} />
                        <span span={4}>房屋名称：{item.houseName2 ? item.houseName2 : '-'}</span>
                        <span span={4}>楼栋名称：{item.buildingName2 ? item.buildingName2 : '-'}</span>
                        <span span={4}>房屋面积：{item.buildingArea ? `${item.buildingArea}㎡` : '-'}</span>
                        <span span={4}>租金单价：{item.rentPrice ? `${item.rentPrice}元/㎡/年` : '-'}</span>
                        <span span={4}>装修情况：{item.renovationCondition ? item.renovationCondition : '-'}</span>
                        <span span={4}>
                          <Tooltip title="删除">
                            <a><Icon type="delete" onClick={() => this.delHouse(index)} theme="twoTone" twoToneColor="#E22534" /></a>
                          </Tooltip>
                        </span>
                      </Row>
                    ))
                }
              </Col>
            </Row>
            <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>企业基本信息</Row>
            <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
            <Row gutter={24}>
              <Col span={8} id="selectPosition">
                <FormItem label="企业名称：">
                  {getFieldDecorator('rentName', {
                    rules: [{
                      required: true, message: '请选择企业名称'
                    }]
                  })(<Select
                    onChange={this.renterChange}
                    placeholder="请选择企业名称"
                    style={{ width: "100%" }}
                    getPopupContainer={() => document.getElementById('selectPosition')}
                    showSearch
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }>
                    {
                      this.state.renterList && this.state.renterList.map((item, index) => <Option value={item.id} key={index}>{item.renter_name}</Option>)
                    }
                  </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="联系人：">
                  {getFieldDecorator('person')(<Input placeholder="请输入联系人" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="证件号码：">
                  {getFieldDecorator('cardNumber')(<Input placeholder="请输入证件号码" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="行业分类：">
                  {getFieldDecorator('industry', {
                    rules: [{ message: '请输入行业分类！' }]
                  })(<Select placeholder="请选择行业分类" style={{ width: "100%" }} disabled>
                    <Option value='自管'>自管</Option>
                    <Option value='托管'>托管</Option>
                  </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="联系方式：">
                  {getFieldDecorator('contact')(
                    <Input placeholder="请输入联系方式" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="邮箱：">
                  {getFieldDecorator('mail')(<Input placeholder="请输入邮箱" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="主营业务：">
                  {getFieldDecorator('mainContent')(<Input placeholder="请输入主营业务" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="投资额：">
                  {getFieldDecorator('investNumber')(<Input placeholder="请输入投资额" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="人数：">
                  {getFieldDecorator('personSize')(
                    <Select placeholder="请选择人员规模" style={{ width: "100%" }} disabled>
                      <Option value='100人以下'>100人以下</Option>
                      <Option value='100-400人'>100-400人</Option>
                      <Option value='400人以上'>400人以上</Option>
                    </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="企业情况：">
                  {getFieldDecorator('renterStatus')(
                    <Select placeholder="请选择联系方式" style={{ width: "100%" }} disabled>
                      <Option value='上市'>上市</Option>
                      <Option value='三板'>三板</Option>
                      <Option value='区域挂牌'>区域挂牌</Option>
                    </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="高企：">
                  {getFieldDecorator('hConpermany')(
                    <Select placeholder="请选择是否高企" style={{ width: "100%" }} disabled>
                      <Option value='是'>是</Option>
                      <Option value='否'>否</Option>
                    </Select>)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="成立时间：">
                  {getFieldDecorator('foundDate')(<Input placeholder="请输入成立时间" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="当年全口径税收：">
                  {getFieldDecorator('rentIncome')(<Input placeholder="请输入当年全口径税收" disabled />)}
                </FormItem>
              </Col>

              <Col span={8}>
                <FormItem label="注册类型：">
                  {getFieldDecorator('rType')(<Input placeholder="请输入注册类型" disabled />)}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem label="当年收入：">
                  {getFieldDecorator('income')(<Input placeholder="请输入当年收入" disabled />)}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="企业简介：">
                  {getFieldDecorator('company')(<TextArea rows={4} placeholder="请输入企业简介" disabled />)}
                </FormItem>
              </Col>
            </Row>
          </Modal>


          <Modal
            title='地图选房'
            wrapClassName="vertical-center-modal"
            className='modalAlign'
            visible={this.state.showMapModal}
            onOk={this.setHouseToModal}
            onCancel={() => this.changeMapModal(false)}
            width={830}
          >
            <div>
              <IframeComm
                attributes={{
                  src: '/gis/mapHouse.html',
                  style: {
                    width: '100%',
                    height: '450px',
                  },
                }}
                postMessageData={this.state.sendMapData}
                handleReceiveMessage={this.onReceiveMessage}
              />
            </div>
          </Modal>
        </div>
      </PageHeaderWrapper>
    );
  }
}
const WrappedApp = Form.create()(ProtocolManagement);
export default WrappedApp;
