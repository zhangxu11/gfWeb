import React from 'react';
import RenderAuthorized from '@/components/Authorized';
import { getAuthority } from '@/utils/authority';
import Redirect from 'umi/redirect';
import router from 'umi/router';

const Authority = getAuthority();

const Authorized = RenderAuthorized(Authority);

export default ({ children }) => {
  //判断是否登录
  if (!sessionStorage.getItem('currentUser')) {
    router.push({
      pathname: '/fe/user/login'
    });
    return null;
  } else {
    return (
      <Authorized authority={children.props.route.authority} noMatch={<Redirect to="/fe/user/login" />}>
        {children}
      </Authorized>
    );
  };

};
