import React, { Component } from 'react';

import { Row, Col, Button, Icon, Modal, Steps, message } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';

const BgElement = Element.BgElement;

const nopic = require('../../assets/nopic.png');
const Step = Steps.Step;
@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class HouseDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doc: 1,
      step: {
        doc: {},
        itemList: []
      },
      stepCount: 0,
      companyData: {},
      feesData: [
        {
          title: '水费',
          money: '-',
          icon: 'dashboard'
        },
        {
          title: '电费',
          money: '-',
          icon: 'thunderbolt'
        },
        {
          title: '供暖费',
          money: '-',
          icon: 'sliders'
        },
        {
          title: '物业费',
          money: '-',
          icon: 'money-collect'
        },
        {
          title: '燃气费',
          money: '-',
          icon: 'fire'
        },
      ],
      modalVisible: false,
      data: {
        houseName2: '-',
        houseCode: '-',
        buildingArea: '-',
        isRent: '-',//资产状态
        use: '-',
        receivedDate: '-',
        ownershipType: '-',//权属形式
        ownershipStatus: '-',//权属状态
        docPositionId: '-',//档案柜编号
        docPosition: '-',//档案柜位置
        netValue: '-',//净值
        assessmentValue: '-',//评估值
        assessmentDate: '-',//评估时间
        equipmentList: [],//钥匙位置
        packageName: '-',//袋名称
        boxPositionName: '-',//盒位置名称
        housePositionName: '-',//房间位置名称
        cabinetPositionName: '-',//房间位置名称
        houseUrl: null,
        fileList: []
      }
    };
  }

  componentWillMount() {
    const that = this;
    axios.get(Api.getHouseMessage, {
      params: {
        houseCode: this.props.location.query.houseCode ? this.props.location.query.houseCode : this.props.location.state.houseCode
      }
    }).then((res) => {
      let feesData = this.state.feesData;
      feesData[0].money = res.data.fee.waterFee;
      feesData[1].money = res.data.fee.electricFee;
      feesData[2].money = res.data.fee.heatingFee;
      feesData[3].money = res.data.fee.propertyFee;
      feesData[4].money = res.data.fee.gasFee;

      const images = res.data.imageData.concat(res.data.imageCode);
      res.data.images = images && Array.isArray(images) ? images : [];
      that.setState({
        data: res.data,
        feesData
      });

      axios.get(Api.getHouseOwnership, {
        params: {
          houseCode: res.data.houseCode
        }
      }).then((res) => {
        if (Number(res.data.doc) === 0) {
          that.setState({
            doc: Number(res.data.doc)
          });
        } else {
          that.setState({
            step: res.data,
            stepCount: res.data.itemList.length
          });
        };
      });
    })
  }
  changeList(index) {
    let headerData = this.state.headerData;
    headerData[this.state.choicedIndex].choiced = false;
    headerData[index].choiced = true;
    this.setState({
      headerData,
      choicedIndex: index
    });
  }
  setModalVisible(modalVisible) {
    if (this.state.doc === 0) {
      message.warn('暂无数据！')
    } else {
      this.setState({ modalVisible });
    }
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className={styles.cdMain}>
          <Row className={styles.w1120}>
            {/* <div className={styles.cdHeader}>
            {this.state.houseName2}
          </div> */}
            {/* <Row> */}
            {/* <div className={`${styles.cdHeaderLeft} pull-left`}>
              <BannerAnim prefixCls='buildingDetailsBannerItem' autoPlay>
              {
                  this.state.data.images && this.state.data.images.map((item, index) =>
                      <Element
                        key={index}
                      >
                        <BgElement
                          key="bg"
                          className={styles.bannerItemPic}
                          style={{
                            backgroundImage: `url(http://gisams-hitech.cindata.cn/res_file${item.filePath})`
                          }}
                        />
                      </Element>
                    )
                }
              </BannerAnim>
            </div> */}
            {/* <div className={`${styles.cdHeaderRight} pull-left`}> */}
            {/* <h1 className={styles.basicInfo}>
                基本信息
              </h1> */}
            {
              //  this.state.data.projectCode == 'PJ008' ?

              //       ['会议室', '贵宾室', '报告厅', '办公室', '餐厅'].indexOf(this.state.data.houseAction) > -1 ?

              //       <Row>
              //         <Col span={12}>
              //           <span className={styles.label}>划转单位：</span>
              //           {this.state.data.transferUnit ? this.state.data.transferUnit : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>详细地址：</span>
              //           {this.state.data.ownershipType ? this.state.data.ownershipType : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>建筑名称：</span>
              //           {this.state.data.buildingName2 ? this.state.data.buildingName2 : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>楼层：</span>
              //           {this.state.data.currentFloor ? this.state.data.currentFloor : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>房号：</span>
              //           {this.state.data.houseName ? this.state.data.houseName : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>住房用途：</span>
              //           {this.state.data.use ? this.state.data.use : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>使用面积：</span>
              //           {this.state.data.useArea ? this.state.data.useArea : '-'}
              //         </Col>
              //         <Col span={12}>
              //           <span className={styles.label}>是否可用：</span>
              //           {this.state.data.houseIsable ? this.state.data.houseIsable : '-'}
              //         </Col>

              //           <Col span={12}>
              //             <span className={styles.label}>可容纳人数：</span>
              //               {`${this.state.data.housevenuetypehx}(回形) ${this.state.data.housevenuetypekz}(课桌) ${this.state.data.housevenuetypejy}(剧院)`}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>挑高：</span>
              //             {this.state.data.housevenuethigh ? this.state.data.housevenuethigh : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>舞台面积：</span>
              //             {this.state.data.housevenuedancearea ? this.state.data.housevenuedancearea : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>自然光：</span>
              //             {this.state.data.housevenueisnatural ? this.state.data.housevenueisnatural : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>显示器：</span>
              //             {this.state.data.housevenueisvideo ? this.state.data.housevenueisvideo : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>LED屏：</span>
              //             {this.state.data.housevenueisled ? this.state.data.housevenueisled : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>投影仪：</span>
              //             {this.state.data.housevenueisprojector ? this.state.data.housevenueisprojector : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>是否有柱子：</span>
              //             {this.state.data.housevenueispillars ? this.state.data.housevenueispillars : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>会场配置：</span>
              //             {this.state.data.housevenueconfig ? this.state.data.housevenueconfig : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>备注：</span>
              //             {this.state.data.housevenuenote ? this.state.data.housevenuenote : '-'}
              //           </Col>
              //         </Row>
              //         : 
              //         <Row>
              //           <Col span={12}>
              //             <span className={styles.label}>划转单位：</span>
              //             {this.state.data.transferUnit ? this.state.data.transferUnit : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>详细地址：</span>
              //             {this.state.data.ownershipType ? this.state.data.ownershipType : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>建筑名称：</span>
              //             {this.state.data.buildingName2 ? this.state.data.buildingName2 : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>楼层：</span>
              //             {this.state.data.currentFloor ? this.state.data.currentFloor : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>房号：</span>
              //             {this.state.data.houseName ? this.state.data.houseName : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>住房用途：</span>
              //             {this.state.data.use ? this.state.data.use : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>使用面积：</span>
              //             {this.state.data.useArea ? this.state.data.useArea : '-'}
              //           </Col>
              //           <Col span={12}>
              //             <span className={styles.label}>是否可用：</span>
              //             {this.state.data.houseIsable ? this.state.data.houseIsable : '-'}
              //           </Col>
              //       </Row>
              //   : 
              //   <Row>
              //     <Col span={12}>
              //       <span className={styles.label}>房屋名称：</span>
              //       {this.state.data.houseName2}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>房屋编号：</span>
              //       {this.state.data.houseCode}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>建筑面积：</span>
              //       {this.state.data.buildingArea}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>资产状态：</span>
              //       {this.state.data.isRent}
              //     </Col>
              //     {/* <Col span={12}>
              //       <span className={styles.label}>用途：</span>
              //       {this.state.data.use}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>接管日期：</span>
              //       {this.state.data.receivedDate}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>权属形式：</span>
              //       {this.state.data.ownershipType}
              //     </Col>
              //     <Col span={12}>
              //       <span className={styles.label}>权属状态：</span>
              //       {this.state.data.ownershipStatus}
              //     </Col> */}
              //   </Row>
            }
            {/* </div> */}
            {/* </Row> */}

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="contentHeaderTitle">
                    <p>房屋展示</p>
                    <p>HOUSE SHOW</p>
                  </div>
                </div>
              </div>
              <div className={styles.br2} style={{ height: '237px' }}>
                <IframeComm
                  attributes={{
                    src: this.state.data.houseUrl,
                    style: {
                      width: '100%',
                      height: '237px',
                      top: '106px',
                      left: '0px',
                      position: "absolute",
                    }
                  }}
                  postMessageData={this.state.postMsg}
                />
                {/* <BannerAnim prefixCls='buildingDetailsBannerItem' autoPlay> */}
                {/* <Element>
                  <BgElement key="bg" className={styles.bannerItemPic} style={{backgroundImage: this.state.data.houseUrl }}/>
                </Element> */}
                {/* {
                    this.state.data.images && this.state.data.images.map((item, index) =>
                        <Element
                          key={index}
                        >
                          <BgElement
                            key="bg"
                            className={styles.bannerItemPic}
                            style={{
                              backgroundImage: `url(http://gisams-hitech.cindata.cn/res_file${item.filePath})`
                            }}
                          />
                        </Element>
                      )
                  } */}
                {/* </BannerAnim> */}
              </div>
            </div>

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="contentHeaderTitle">
                    <p>房屋信息</p>
                    <p>HOUSE INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.br2} style={{ height: 'auto' }}>
                <Row>
                  <Col className={styles.col8Overflow} title={this.state.data.houseCode} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>房屋编号：</span>{this.state.data.houseCode ? this.state.data.houseCode : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.houseName2} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>房屋名称：</span>{this.state.data.houseName2 ? this.state.data.houseName2 : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.buildingArea} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>建筑面积：</span>{this.state.data.buildingArea ? this.state.data.buildingArea : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.useArea} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>使用面积：</span>{this.state.data.useArea ? this.state.data.useArea : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.heatingArea} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>供热面积：</span>{this.state.data.heatingArea ? this.state.data.heatingArea : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.prerent_price} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>预租单价：</span>{this.state.data.prerent_price ? this.state.data.prerent_price : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.isRent} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>租赁状态：</span>{this.state.data.isRent ? this.state.data.isRent : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.renovationCondition} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>装修情况：</span>{this.state.data.renovationCondition ? this.state.data.renovationCondition : '-'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.data.renovationCondition} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>附件：</span>
                    <span>
                      {this.state.data.fileList && this.state.data.fileList.map((item, index) => (
                        <div style={{ display: 'inline-block' }}>
                          {item.uploadName}
                        </div>
                      ))}
                    </span>
                  </Col>
                </Row>
              </div>
            </div>

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="contentHeaderTitle">
                    <p>设备信息</p>
                    <p>DEVICE INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.br2} style={{ height: 'auto', }}>
                {this.state.data.equipmentList.map((item, index) => (
                  <Row key={index}>
                    <Col className={styles.col8Overflow} title={item.equipmentName} style={{ textAlign: 'left' }} span={8}>
                      <span style={{ width: '140px' }} className='label'>设备名称：</span>{item.equipmentName}
                    </Col>
                    <Col className={styles.col8Overflow} title={item.equipmentModel} style={{ textAlign: 'left' }} span={8}>
                      <span style={{ width: '140px' }} className='label'>设备型号：</span>{item.equipmentModel}
                    </Col>
                    <Col className={styles.col8Overflow} title={item.equipmentValue} style={{ textAlign: 'left' }} span={8}>
                      <span style={{ width: '140px' }} className='label'>设备价值：</span>{item.equipmentValue}
                    </Col>
                    <Col className={styles.col8Overflow} title={item.equipmentStatus} style={{ textAlign: 'left' }} span={8}>
                      <span style={{ width: '140px' }} className='label'>设备状况：</span>{item.equipmentStatus}
                    </Col>
                  </Row>
                ))}
              </div>
            </div>

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>费用信息</p>
                    <p>ARREARS INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.fees} style={{ background: '#fff' }}>
                {this.state.feesData.map((item, index) => {
                  return (
                    <div key={index} className={styles.feesItem}>
                      <p>
                        <Icon type={item.icon} theme="filled"></Icon>
                      </p>
                      <p>
                        {item.money}元
                </p>
                      <p>
                        {item.title}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>


            {/* <div className={`pull-left ${styles.w1120}`}>
            <div className='mainContainer'>
              <div className="contentHeader">
                <div className="contentHeaderTitle">
                  <p>企业信息</p>
                  <p>COMPANY INFORMATION</p>
                </div>
              </div>
            </div>
            <div className={styles.br1} style={{
              height: 'auto'
            }}>
              <div>
                <span>企业名称：{this.state.data.companyName?this.state.data.companyName:'-'}</span>
                &emsp;&emsp;
                <span>主营业务：{this.state.data.mainBusiness?this.state.data.mainBusiness:'-'}</span>
                          &emsp;&emsp;
                <span>注册资金（万元）：{this.state.data.registeredCapital?this.state.data.registeredCapital+' 万元':'-'}</span>&emsp;&emsp;
                <span>人数：{this.state.data.hunmenCount?this.state.data.hunmenCount:'-'}</span>&emsp;&emsp;
                <span>联系人：{this.state.data.legalPerson?this.state.data.legalPerson:'-'}</span>&emsp;&emsp;
                <span>电话：{this.state.data.phoneNumber?this.state.data.phoneNumber:'-'}</span>
              </div>
            </div>
          </div> */}

            {/* {
            this.state.data.projectCode !== 'PJ008' ?
              <div>
                <Row className={styles.w1120}>
                  <div className={`pull-left ${styles.w560}`}>
                    <div className='mainContainer'>
                      <div className="contentHeader">
                        <div className="contentHeaderTitle">
                          <p>账面信息</p>
                          <p>BOOK INFORMATION</p>
                        </div>
                      </div>
                    </div>
                    <div className={styles.br1}>
                      <span>净值：
                    {this.state.data.netValue ? this.state.data.netValue : '- '}元</span>
                      &emsp;&emsp;
                  <span>评估值：
                    {this.state.data.assessmentValue ? this.state.data.assessmentValue : '- '}元</span>
                      &emsp;&emsp;
                  <span>评估时间：
                    {this.state.data.assessmentDate}</span>
                    </div>
                  </div>

                  <div className={`pull-left ${styles.w560}`}>
                    <div className='mainContainer'>
                      <div className='contentHeader'>
                        <div className={`contentHeaderTitle ${styles.small}`}>
                          <p>钥匙信息</p>
                          <p>KEY INFORMATION</p>
                        </div>
                      </div>
                    </div>
                    <div className={styles.br1}>
                      <span>钥匙袋：
                    {this.state.data.packageName}</span>
                      &emsp;&emsp;
                  <span>盒位置：
                    {this.state.data.boxPositionName}</span>
                      &emsp;&emsp;
                  <span>房间位置：
                    {this.state.data.housePositionName}</span>
                      &emsp;&emsp;
                  <span>柜位置：
                    {this.state.data.cabinetPositionName}</span>
                    </div>
                  </div>
                  <div className={`pull-left ${styles.w1120}`}>
                    <div className='mainContainer'>
                      <div className="contentHeader">
                        <div className="contentHeaderTitle">
                          <p>设备信息</p>
                          <p>DEVICE INFORMATION</p>
                        </div>
                      </div>
                    </div>
                    <div className={styles.br1} style={{
                      height: 'auto'
                    }}>
                      {this.state.data.equipmentList.map((item, index) => (
                        <div key={index}>
                          <span>设备名称：{item.equipmentName}</span>
                          &emsp;&emsp;
                          <span>设备型号：{item.equipmentModel}</span>
                                    &emsp;&emsp;
                          <span>设备价值：{item.equipmentValue}元</span>
                        </div>
                      ))}
                    </div>
                  </div>
                </Row>

                <div className='mainContainer'>
                  <div className='contentHeader'>
                    <div className="contentHeaderTitle">
                      <p>欠费情况</p>
                      <p>ARREARS</p>
                    </div>
                  </div>
                </div>
                <div className={styles.fees}>
                  {this.state.feesData.map((item, index) => {
                    return (
                      <div key={index} className={styles.feesItem}>
                        <p>
                          <Icon type={item.icon} theme="filled"></Icon>
                        </p>
                        <p>
                          {item.money}元
                    </p>
                        <p>
                          {item.title}
                        </p>
                      </div>
                    );
                  })}

                </div>
              </div>
              : null
          } */}
          </Row>
        </div>
        <Modal
          title="权属详情"
          centered
          visible={this.state.modalVisible}
          onOk={this.setModalVisible.bind(this, false)}
          onCancel={this.setModalVisible.bind(this, false)}
        >
          <Row className='mrb15'>
            <Col span={12}>证件类型：{this.state.step.doc.docType ? this.state.step.doc.docType : '-'}</Col>
            <Col span={12}>负责人：{this.state.step.itemList[0] ? this.state.step.itemList[0].userCode : '-'}</Col>
          </Row>
          <Steps direction="vertical" current={this.state.stepCount}>
            <Step title="无证" description={this.state.step.itemList[0] ? this.state.step.itemList[0].operateDate : ''} />
            <Step title="办理中" description={this.state.step.itemList[1] ? this.state.step.itemList[1].operateDate : ''} />
            <Step title="办理完成" description={this.state.step.itemList[2] ? this.state.step.itemList[2].operateDate : ''} />
            <Step title="过户中" description={this.state.step.itemList[3] ? this.state.step.itemList[3].operateDate : ''} />
            <Step title="有证" description={this.state.step.itemList[4] ? this.state.step.itemList[4].operateDate : ''} />
          </Steps>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default HouseDetails;
