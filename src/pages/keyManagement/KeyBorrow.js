import * as React from 'react';
import { Table, Form, Select, Input, Button, Col, Row, Modal, Icon, Popver, Steps, notification, DatePicker } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import axios from 'axios'
import Api from '../../services/apis'

const FormItem = Form.Item;
const Option = Select.Option;
const Step = Steps.Step;

@connect(({ user, loading }) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))

class RecordBorrow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            modalVisible2: false,
            tableData: [],
            current: 1,
            pageSize: 5,
            keyId: '',
            stepData: [
                {
                    title: '申请借档',
                },
                {
                    title: '部门领导审核',
                },
                {
                    title: '经办部门审核',
                },
                {
                    title: '经办人',
                },
                {
                    title: '完成'
                }
            ],
        }
    }

    sendData = () => {
        const that = this;
        const searchValues = this.props.form.getFieldsValue();
        axios.post(Api.kmGetKeyList, {
            keyName: searchValues.s_name,
            keyStatus: searchValues.s_state,
            currentPage: this.state.current
        })
            .then((function (resp) {
                const data = resp.data;
                if (data.list.length === 0) {
                    notification.destroy();
                    notification['warning']({ message: '暂无数据' })
                } else {
                    data.list.forEach((d, i) => {
                        d.key = i;
                        d.key_status2 = d.key_status === '0' ? "未借出" : parseInt(d.key_status) < 4 ? '审批中' : "已借出";
                    });
                }

                that.setState({
                    tableData: data.list,
                    pageSize: data.pageSize,
                    current: data.currentPage,
                    totalSize: data.totalSize
                })
            }))
    }

    componentWillMount() {
        this.sendData();
    }

    query = (e) => {
        e.preventDefault();
        this.setState({ current: 1 }, () => {
            this.sendData();
        })
    }

    pageChange = (page) => {
        this.setState({ current: page }, () => {
            this.sendData();
        })
    }


    resetForm = () => {
        this.props.form.resetFields();
    }

    setModalVisible = (visible, id) => {
        console.log(id)
        this.setState({
            modalVisible: visible,
            packageId: id
        })
        this.props.form.setFieldsValue({
            borrower: "",
            reason: "",
            startTime: null,
            endTime: null
        })
    }

    setModalVisible2 = (visible) => {
        this.setState({ modalVisible2: visible })
    }

    borrow = () => {
        const that = this;
        this.props.form.validateFields((err, values) => {
            // if (!err) {
            if (values.startTime > values.endTime) {
                notification.destroy();
                notification['warning']({ message: '开始时间不能晚于截止时间！' })
                return;
            }
            axios.post(Api.kmBorrow, {
                // keyIds: that.state.keyId,
                borrowPerson: values.borrower,
                borrowCause: values.reason,
                borrowStart: values.startTime.format('YYYY-MM-DD'),
                borrowEnd: values.endTime.format('YYYY-MM-DD')
            })
                .then((function (resp) {
                    const data = resp.data;
                    if (data.flag == '1') {
                        notification.destroy();
                        notification['success']({ message: data.msg })
                        that.sendData()
                    } else {
                        notification.destroy();
                        notification['warning']({ message: data.msg })
                    }
                    that.setModalVisible(false);
                }))
            // }
        })
    }

    openModal = (id, type, status) => {
        //根据ID查询档案信息
        this.getKeyById(id);
        //获取借款记录
        this.getRecord(id);
    }

    //根据ID查询信息
    getKeyById = (id) => {
        const that = this;
        axios.post(Api.kmGetKeyById, {
            packageId: id
        })
            .then(function (resp) {
                const data = resp.data;
                that.setModalVisible2(true);
                that.setState({
                    m_number: data.key_code,
                    m_name: data.key_name,
                    m_num: data.key_num,
                    m_room: data.housedesc,
                    m_bag: data.package_name,
                    m_keySpecie: data.speciesNames,
                    m_state: parseInt(data.key_status)
                })
            })
    }

    //获取审批记录
    getRecord = (id) => {
        const that = this;
        axios.post(Api.kmApprovalOne, {
            packageId: id,
            currentPage: that.state.m_current
        })
            .then(function (resp) {
                const { data } = resp;
                if (data) {
                    const stepData = that.state.stepData;
                    stepData[0].description = data.borrowUser;
                    stepData[0].time = data.borrowDate ? data.borrowDate.substring(0, 10) : '';
                    stepData[1].description = data.auditingDept;
                    stepData[1].time = data.auditingDate ? data.auditingDate.substring(0, 10) : '';
                    stepData[2].description = data.chargeDept;
                    stepData[2].time = data.chargeDeptDate ? data.chargeDeptDate.substring(0, 10) : '';
                    stepData[3].description = data.chargePerson;
                    stepData[3].time = data.chargePersonDate ? data.chargePersonDate.substring(0, 10) : '';

                    that.setState({
                        stepData: stepData
                    })
                }
            })
    }

    render() {
        const { form: { getFieldDecorator }, currentUserLoading } = this.props;

        const columns = [
            {
                title: '钥匙名称',
                dataIndex: 'key_name',
                key: 'key_name',
            },
            {
                title: '钥匙位置',
                dataIndex: 'cabinetdesc',
                key: 'cabinetdesc',
            },
            {
                title: '抽屉所在房间',
                dataIndex: 'housedesc',
                key: 'housedesc'
            },
            {
                title: '钥匙状态',
                dataIndex: 'key_status2',
                key: 'key_status2',
            },
            {
                title: '钥匙数量',
                dataIndex: 'key_num',
                key: 'key_num',
            },
            {
                title: '操作',
                key: 'action',
                render: (text, record) => {
                    return record.key_status == '0' ? (
                        <span>
                            <a href="javascript:;" onClick={() => {
                                this.setModalVisible(true, record.package_id)
                            }}>
                                借取
                            </a>
                        </span>
                    ) : (
                            <span>
                                <a href="javascript:;" onClick={() => this.openModal(record.package_id)}>
                                    查看
                            </a>
                            </span>
                        )
                },
            },
        ];


        return (
            <PageHeaderWrapper loading={currentUserLoading}>
                <div className='mainContainer'>
                    <div className='contentHeader'>
                        <div className='contentHeaderTitle'>
                            <p>钥匙借取</p>
                            <p>KEY BORROW</p>
                        </div>
                    </div>

                    <div className='pom'>
                        <div className='mrb15'>
                            <Form layout='inline' align='center' onSubmit={this.query}>
                                <FormItem label="钥匙名称：">
                                    {
                                        getFieldDecorator('s_name')(
                                            <Input placeholder="请输入钥匙名称" />
                                        )
                                    }
                                </FormItem>
                                <FormItem label='钥匙状态：'>
                                    {
                                        getFieldDecorator('s_state')(
                                            <Select placeholder="请选择钥匙状态">
                                                <Option value="0">未借出</Option>
                                                <Option value="1">已借出</Option>
                                            </Select>
                                        )
                                    }
                                </FormItem>
                                <Row gutter={24} style={{ textAlign: 'center' }}>
                                    <Button icon='search' type='primary' htmlType='submit'>搜索</Button>&emsp;
                                    <Button icon="reload" type="primary" htmlType='button' onClick={this.resetForm}>重置</Button>
                                </Row>
                            </Form>
                        </div>

                        <Table
                            bordered={true}
                            scroll={{ x: true }}
                            columns={columns}
                            dataSource={this.state.tableData}
                            pagination={{
                                pageSize: this.state.pageSize,
                                current: this.state.current,
                                total: this.state.totalSize,
                                onChange: this.pageChange,
                                showTotal: total => `共 ${total} 项`
                            }} />
                    </div>

                    <Modal
                        title="钥匙借取"
                        centered
                        visible={this.state.modalVisible}
                        onOk={this.borrow}
                        onCancel={() => this.setModalVisible(false)}
                        width={800}
                    >
                        <div>
                            <Row gutter={24}>
                                <Col span={8}>
                                    <FormItem label='借取人：'>
                                        {getFieldDecorator('borrower', {
                                            rules: [{ required: true, message: "请输入借取人！" }]
                                        })(<Input placeholder="请输入借取人" />)}
                                    </FormItem>
                                </Col>
                                <Col span={8}>
                                    <FormItem label='事由：'>
                                        {getFieldDecorator('reason', {
                                            rules: [{ required: true, message: "请输入事由！" }]
                                        })(<Input placeholder='请输入事由' />)}
                                    </FormItem>
                                </Col>
                                <Col span={8}>
                                    <FormItem label='开始时间：'>
                                        {getFieldDecorator('startTime', {
                                            rules: [{ required: true, message: "请选择借取开始时间！" }]
                                        })(<DatePicker format="YYYY-MM-DD" placeholder='请选择借取开始时间' style={{ width: '100%' }} />)}
                                    </FormItem>
                                </Col>
                                <Col span={8}>
                                    <FormItem label='截止时间：'>
                                        {getFieldDecorator('endTime', {
                                            rules: [{ required: true, message: "请选择借取截止时间！" }]
                                        })(<DatePicker format="YYYY-MM-DD" placeholder='请选择借取截止时间' style={{ width: '100%' }} />)}
                                    </FormItem>
                                </Col>
                            </Row>
                        </div>
                    </Modal>

                    <Modal
                        title="借取审批"
                        centered
                        visible={this.state.modalVisible2}
                        onCancel={() => this.setModalVisible2(false)}
                        width={800}
                        onOk={() => this.setModalVisible2(false)}
                    >
                        <div>
                            <Steps size="small" current={this.state.m_state}>
                                {this.state.stepData.map((item, index) => {
                                    return (
                                        <Step
                                            description={[
                                                <div key={index}>
                                                    <p>{item.description}</p>
                                                    <p>{item.time}</p>
                                                </div>,
                                            ]}
                                            title={item.title}
                                            key={index}
                                        />
                                    );
                                })}
                            </Steps>
                            <Form layout='inline' style={{ marginTop: '25px' }}>
                                <Row>
                                    <Col span={8}>
                                        <FormItem label='钥匙编号：'>{this.state.m_number}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='钥匙名称：'>{this.state.m_number}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='钥匙位置：'>{this.state.m_bag}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='所在房间：'>{this.state.m_room}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='钥匙数量：'>{this.state.m_num}</FormItem>
                                    </Col>
                                    <Col span={8}>
                                        <FormItem label='附属物品：'>{this.state.m_keySpecie}</FormItem>
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                    </Modal>

                </div>
            </PageHeaderWrapper>
        )
    }
}

const WrappedApp = Form.create()(RecordBorrow);

export default WrappedApp;
