/*
 * Created by hanzhiqiang
 * 用于定位要素位置。
 */

var bufferDis = 0;    // 缓冲分析的半径
var poiType = "";     // 缓冲分析类型
var cityID;           // 城市标识，可以是城市ID，城市名称
var AddressID;      // 小区ID
var bufferGeometry;   // 临时变量, 缓冲分析的polygon geometry
var data = [];
var index = 0;         // 全局变量，记录data数据结构的索引
var zdPriceMeasureRet = false;  // 宗地价格测算，查询返回值，如果zdPriceMeasureRet=false；没有数据，就创建点面，否则不做事情

var sqlType1 = false;
var sqlType2 = false;
var sqlTypeJT = -1; //交通
var sqlTypeJY = -1; //教育
var sqlTypeYL = -1; //医疗
var sqlTypeSY = -1;// 商业
var sqlTypeHJ = -1;// 环境
var sqlTypeWT = -1;// 文体

//查询poi的总数，往前台返
function showAllPOInums(geometry) {
    sqlType1 = false;
    sqlType2 = false;
    sqlTypeJT = 0; //交通
    sqlTypeJY = 0; //教育
    sqlTypeYL = 0; //医疗
    sqlTypeSY = 0;// 商业
    sqlTypeHJ = 0;// 环境
    sqlTypeWT = 0;// 文体
    var sqlType1 = "";
    sqlType1 += " TAG_NAME ='中学' or TAG_NAME='小学' or ";
    sqlType1 += " TAG_NAME ='三级甲等医院' or ";
    sqlType1 += " TAG_NAME='会展中心' or TAG_NAME='购物中心' or TAG_NAME='综合市场' or TAG_NAME='超级市场' or TAG_NAME='普通商场' or TAG_NAME='商场' or TAG_NAME='特色商业街' or ";
    sqlType1 += " TAG_NAME='公园' or TAG_NAME='公园广场' or TAG_NAME='水上活动中心' or TAG_NAME='植物园' or TAG_NAME='动物园' or TAG_NAME='游乐场' or ";
    sqlType1 += " TAG_NAME='剧院' or TAG_NAME='博物馆' or TAG_NAME='美术馆' or TAG_NAME='图书馆' or TAG_NAME='科技馆' or TAG_NAME='体育馆' or TAG_NAME='文化宫' or TAG_NAME='科教文化场所'";
    QueryBySQL(config.urlPOI + config.urlPOITarge_p, sqlType1, showAllPOInumsCallback1, queryError, geometry);
    var sqlType2 = "1=1";
    QueryBySQL(config.urlPOI + config.urlPOITraffic_p, sqlType2, showAllPOInumsCallback2, queryError, geometry);
}


function showAllPOInumsCallback1(res) {


    for (var i = 0; i < res.features.length; i++) {
        var attribute = res.features[i].attributes;
        if (attribute.TAG_NAME == "中学" || attribute.TAG_NAME == "小学") {//教育
            sqlTypeJY +=1;
        }
        if (attribute.TAG_NAME == "三级甲等医院") {//医疗
            sqlTypeYL+=1;
        }
        if (attribute.TAG_NAME == "会展中心" || attribute.TAG_NAME == "购物中心" || attribute.TAG_NAME == "综合市场" || attribute.TAG_NAME == "超级市场" || attribute.TAG_NAME == "普通商场" || attribute.TAG_NAME == "商场" || attribute.TAG_NAME == "特色商业街") {//商业
            sqlTypeSY+=1;
        }
        if (attribute.TAG_NAME == "公园" || attribute.TAG_NAME == "公园广场" || attribute.TAG_NAME == "水上活动中心" || attribute.TAG_NAME == "植物园" || attribute.TAG_NAME == "动物园" || attribute.TAG_NAME == "游乐场") {
            sqlTypeHJ+=1;
        }
        if (attribute.TAG_NAME == "剧院" || attribute.TAG_NAME == "博物馆" || attribute.TAG_NAME == "美术馆" || attribute.TAG_NAME == "图书馆" || attribute.TAG_NAME == "科技馆" || attribute.TAG_NAME == "体育馆" || attribute.TAG_NAME == "文化宫" || attribute.TAG_NAME == "科教文化场所") {//文体
            sqlTypeWT+=1;
        }
    }

    sqlType1 = true;

    if (sqlType2 == true) {
        //发消息
        //alert(sqlTypeJT.toString() + "," + sqlTypeJY.toString()+ "," + sqlTypeYL.toString()+ "," + sqlTypeSY.toString()+ "," + sqlTypeHJ.toString()+ "," + sqlTypeWT.toString());
        var poiNums = [];
        poiNums.push(sqlTypeJT);
        poiNums.push(sqlTypeJY);
        poiNums.push(sqlTypeYL);
        poiNums.push(sqlTypeSY);
        poiNums.push(sqlTypeHJ);
        poiNums.push(sqlTypeWT);
        var msg = {};
        msg.type = "poiNums";
        msg.nums = poiNums;
        sendMessage(msg);
    }
}

function showAllPOInumsCallback2(res) {
    if (res.features.length >0) {
        sqlTypeJT = res.features.length;
    }

    sqlType2 = true;

    if (sqlType1 == true) {
        //发消息
       //alert(sqlTypeJT.toString() + "," + sqlTypeJY.toString()+ "," + sqlTypeYL.toString()+ "," + sqlTypeSY.toString()+ "," + sqlTypeHJ.toString()+ "," + sqlTypeWT.toString());
        var poiNums = [];
        poiNums.push(sqlTypeJT);
        poiNums.push(sqlTypeJY);
        poiNums.push(sqlTypeYL);
        poiNums.push(sqlTypeSY);
        poiNums.push(sqlTypeHJ);
        poiNums.push(sqlTypeWT);
        var msg = {};
        msg.type = "poiNums";
        msg.nums = poiNums;
        sendMessage(msg);

    }
}


function getPolygonStyles(classifyLevel) {
    var fontColor = new dojo.Color([255, 255, 255]);
    var color2 = new dojo.Color([255, 255, 255]);

    if (priceType == PriceType.Business) {   // 商业
        if (classifyLevel == "1") {
            fontColor = new dojo.Color([168, 0, 0, 0.55]);
            color2 = new dojo.Color([168, 0, 0, 0.25]);
        }
        else if (classifyLevel == "2") {
            fontColor = new dojo.Color([230, 0, 0, 0.55]);
            color2 = new dojo.Color([230, 0, 0, 0.25]);
        }
        else if (classifyLevel == "3") {
            fontColor = new dojo.Color([255, 0, 0, 0.55]);
            color2 = new dojo.Color([255, 0, 0, 0.25]);
        }
        else if (classifyLevel == "4") {
            fontColor = new dojo.Color([255, 127, 127, 0.55]);
            color2 = new dojo.Color([255, 127, 127, 0.25]);
        }
        else {
            fontColor = new dojo.Color([255, 190, 190, 0.55]);
            color2 = new dojo.Color([255, 190, 190, 0.25]);
        }
    }
    else if (priceType == PriceType.Resident) {   // 住宅
        if (classifyLevel == "1") {
            fontColor = new dojo.Color([76, 115, 0, 0.55]);
            color2 = new dojo.Color([76, 115, 0, 0.25]);
        }
        else if (classifyLevel == "2") {
            fontColor = new dojo.Color([112, 168, 0, 0.55]);
            color2 = new dojo.Color([112, 168, 0, 0.25]);
        }
        else if (classifyLevel == "3") {
            fontColor = new dojo.Color([152, 230, 0, 0.55]);
            color2 = new dojo.Color([152, 230, 0, 0.25]);
        }
        else if (classifyLevel == "4") {
            fontColor = new dojo.Color([170, 255, 0, 0.55]);
            color2 = new dojo.Color([170, 255, 0, 0.25]);
        }
        else {
            fontColor = new dojo.Color([209, 255, 115, 0.55]);
            color2 = new dojo.Color([209, 255, 115, 0.25]);
        }
    }
    else if (priceType == PriceType.Industry) {   // 工业
        if (classifyLevel == "1") {
            fontColor = new dojo.Color([0, 0, 255, 0.55]);
            color2 = new dojo.Color([0, 0, 255, 0.25]);
        }
        else if (classifyLevel == "2") {
            fontColor = new dojo.Color([0, 77, 168, 0.55]);
            color2 = new dojo.Color([0, 77, 168, 0.25]);
        }
        else if (classifyLevel == "3") {
            fontColor = new dojo.Color([0, 92, 230, 0.55]);
            color2 = new dojo.Color([0, 92, 230, 0.25]);
        }
        else if (classifyLevel == "4") {
            fontColor = new dojo.Color([0, 112, 255, 0.55]);
            color2 = new dojo.Color([0, 112, 255, 0.25]);
        }
        else {
            fontColor = new dojo.Color([115, 178, 255, 0.55]);
            color2 = new dojo.Color([115, 178, 255, 0.25]);
        }
    }

    var PolygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID,
        new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
            new dojo.Color(fontColor), 2), new dojo.Color(color2)
    );
    return PolygonSymbol;
}

// 周边配套，缓冲分析
function showCommunityPointBuffer(BufferDistance, pt, type) {
    bufferDis = BufferDistance;
    poiType = type;
    hightliteLayer.clear();
    var src = "img/map.png";
    var attribute = [];
    attribute.layerID = "ptBuffer";
    attribute.x = pt.x;
    attribute.y = pt.y;

    //map.centerAndZoom(pt, config.showBufferLevel);
    addPopo(hightliteLayerPoint, pt.x, pt.y, src, 15, 15, attribute, "", "9pt", -70, -4, 0);
    getGeometryBuffer(config.urlGeometryServer, pt, bufferDis, showBuffer);
}

function showBuffer(res) {
    var url = config.urlPOI + config.urlPOI_p;
    //var sql = "LABEL like '%小学%'or LABEL like '%中学%' or LABEL like '%幼儿园%' or LABEL like '%公园%'";
    // 小学symbol_id=111,中学112，幼儿园110 公园351 宾馆酒店469  电影院416 公交站57
    // 综合医院305 306 三级甲等307 专科医院319 331 口腔医院 325 传染病医院320 肿瘤医院330 妇科医院322
    // 脑科医院326 耳鼻喉医院321 精神病医院324 骨科医院323 卫生院308 胸科医院327 眼科医院328
    // 购物: 购物中心161 商场159 普通商场160
    // 特色商业街196
    // var sql = "SYMBOL_ID=111 or SYMBOL_ID=112 or SYMBOL_ID=110 or SYMBOL_ID=351 ";
    // sql += " or SYMBOL_ID=469 or SYMBOL_ID=416 or SYMBOL_ID=57 ";
    // sql += " or SYMBOL_ID=305 or SYMBOL_ID=306 or SYMBOL_ID=307 or SYMBOL_ID=319 or SYMBOL_ID=331 or SYMBOL_ID=325 ";
    // sql += " or SYMBOL_ID=320 or SYMBOL_ID=330 or SYMBOL_ID=322 or SYMBOL_ID=321 or SYMBOL_ID=324 ";
    // sql += " or SYMBOL_ID=323 or SYMBOL_ID=308 or SYMBOL_ID=327 or SYMBOL_ID=328 ";
    // sql += " or SYMBOL_ID=161 or SYMBOL_ID=159 or SYMBOL_ID=160 ";
    // sql += " or SYMBOL_ID=196 ";
    var sql = "";

    if (poiType == "BusStation")
      sql = "SYMBOL_ID=57";
    else if (poiType == "Education")
      sql = " SYMBOL_ID=111 or SYMBOL_ID=112 or SYMBOL_ID=110 ";
    else if (poiType == "Park")
      sql = " SYMBOL_ID=351 ";
    else if (poiType == "Dinner")
      sql = " SYMBOL_ID=469 ";
    else if (poiType == "Cinema")
      sql = " SYMBOL_ID=416 ";
    else if (poiType == "Hospital") {
      sql = "SYMBOL_ID=305 or SYMBOL_ID=306 or SYMBOL_ID=307 or SYMBOL_ID=319 or SYMBOL_ID=331 or SYMBOL_ID=325 ";
      sql += " or SYMBOL_ID=320 or SYMBOL_ID=330 or SYMBOL_ID=322 or SYMBOL_ID=321 or SYMBOL_ID=324 ";
      sql += " or SYMBOL_ID=323 or SYMBOL_ID=308 or SYMBOL_ID=327 or SYMBOL_ID=328";
    }
    else if (poiType == "Shopping")
      sql = "SYMBOL_ID=161 or SYMBOL_ID=159 or SYMBOL_ID=160";
    else if (poiType == "Business")
      sql = "SYMBOL_ID=196";

    for (var i = 0; i < res.length; i++) {
        var graphic = new esri.Graphic(res[i], getBufferPolygonStyle());
        hightliteLayer.add(graphic);
        map.setExtent(res[i].getExtent().expand(1.2));
        var geometry = res[i];
        data = [];
        index = 0;
        queryArroundPOI(url, sql, geometry);
    }
}

function queryArroundPOI(url, sql, geometry) {
	bufferGeometry = geometry;
  QueryBySQL(url, sql, queryArroundPOICallback, queryError, geometry);
}

function queryArroundPOICallback(res) {
	var content = "";
	var resData={};
    for (var i = 0, len = res.features.length; i < len; i++) {
        var geo = res.features[i].geometry;

        var src = "img/point_4.png";
        var attribute = res.features[i].attributes;
        attribute.layerID = "POI";

        // 计算中心点到每个POI点的直线距离
	    var distance = calcDistance(bufferGeometry, geo);
      var tag_id;
	    tag_id = attribute.SYMBOL_ID;

	    var typemsg = {};
	    var msg = {};
	    var childmsg = {};
	    if (tag_id == 110 || tag_id == 111 || tag_id == 112) {  // 幼儿园，小学，中学
		    typemsg.type = "Education";
		    src = configImagePath.img_POI_eduction;
      }
      else if (tag_id == 351) {    // 公园
        typemsg.type = "Park";
		    src = configImagePath.img_POI_park;
	    }
	    else if (tag_id == 469) {   // 餐饮
        typemsg.type = "Dinner";
		    src = configImagePath.img_POI_dinner;
	    }
	    else if (tag_id == 416) {     // 电影院
        typemsg.type = "Cinema";
		    src = configImagePath.img_POI_ciname;
	    }
	    else if (tag_id == 57) {    // 公交站
        typemsg.type = "BusStation";
		    src = configImagePath.img_POI_bus;
	    }
	    else if (tag_id == 305 || tag_id == 306 || tag_id == 307 || tag_id == 308 || tag_id == 319 || tag_id == 320
            || tag_id == 321 || tag_id == 322 || tag_id == 323 || tag_id == 324 || tag_id == 325 || tag_id == 326
            || tag_id ==327 || tag_id == 328 || tag_id == 331  || tag_id == 330) {    // 医院
        typemsg.type = "Hospital";
		    src = configImagePath.img_POI_medical;
	    }
	    else if (tag_id == 159  || tag_id == 160 || tag_id == 161) {    // 购物
        typemsg.type = "Shopping";
		    src = configImagePath.img_POI_shooping;
	    }
      else if (tag_id == 196) {    // 特色商业街
        typemsg.type = "Business";
        src = configImagePath.img_POI_business;
      }
	    content = "";

	    msg.id = attribute.POI_ID;
	    msg.index = String(i + 1);
	    msg.poiName = attribute.LABEL;
	    msg.address = attribute.ADDRESS;
	    msg.distance = distance;
	    msg.x = geo.x;
	    msg.y = geo.y;

      // if(!(resData[typemsg.type] instanceof Array)){
      //   resData[typemsg.type]=[];
      // };
      //
      // resData[typemsg.type].push({
      //   id : attribute.POI_ID,
      //   index : String(i + 1),
      //   poiName : attribute.LABEL,
      //   address : attribute.ADDRESS,
      //   distance : distance,
      //   x : geo.x,
      //   y : geo.y
      // })
      childmsg.key = String(index);
	    childmsg.popName = msg;
	    data.push(childmsg);
	    index++;

	    attribute.distance = distance;
	    attribute.type = attribute.LABEL;
      addPopo(graphicLayerPoint, geo.x, geo.y, src, 25, 25, attribute, content, "9pt", 10, -4, configColor.Black);

	    // 给标注点添加编号，编号最大为3位数，最小为1位数
	    var offsetX, offsetY;

	    switch (String(index).length) {
		    case 1:
			    offsetX = -4;
			    offsetY = 0;
			    break;
		    case 2:
			    offsetX = -7;
			    offsetY = 0;
			    break;
		    case 3:
			    offsetX = -10;
			    offsetY = 0;
			    break;
		    default:
			    offsetX = -10;
			    offsetY = 0;
			    break;
	    }
	    addPopo(graphicLayerPoint, geo.x, geo.y, "", 12, 12, attribute, String(index), "10pt", offsetX, offsetY, 1);
    }
    window.parent.postMessage(data, '*');
}

// 查询POI并显示
function queryPOIAndShow(poiID) {
  var sql = "POI_ID='" + poiID + "'";
  QueryBySQL(config.urlPOI + config.urlPOI_p, sql, queryPOIAndShowCallback, queryError, null);
}

function queryPOIAndShowCallback(res) {
  for (var i = 0, len = res.features.length; i < len; i++) {
    var geo = res.features[i].geometry;

    var src = "img/point_4.png";
    var attribute = res.features[i].attributes;
    attribute.layerID = "POI";
    attribute.name = attribute.LABEL;
    attribute.address = attribute.ADDRESS;

    addPopo(graphicLayerPoint, geo.x, geo.y, "", 25, 25, attribute, "", "9pt", 10, -4, configColor.Black);
  }
}

// 计算距离
function calcDistance(startPoint, endPoint) {
	var x2 = startPoint.getExtent().getCenter().x;
	var y2 = startPoint.getExtent().getCenter().y;

	// 结果点坐标
	var x1 = endPoint.x;
	var y1 = endPoint.y;

	// 两点间距离公式:
	var distance = Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	distance = distance.toFixed(0);
	return distance;
}



