//项目权属管理
import * as React from 'react';
import {
  Form,
  Select,
  Input,
  Button,
  Col,
  Row,
  Checkbox,
  Tabs,
  DatePicker,
  Modal
} from 'antd';
import axios from 'axios';
import Api from '../../services/apis';
import moment from 'moment';

import Link from 'umi/link';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './style.less';

const FormItem = Form.Item;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const confirm = Modal.confirm;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))

class EnergyEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: '1'
    };
  }
  changeStep(step) {
    this.setState({
      step
    });
  }
  cancelEdit(){
    confirm({
      title: '确定取消编辑？',
      content: '信息将不会保存。',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        window.close();
      }
    });
  }
  tabClick(step) {
    this.setState({
      step
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      currentUserLoading,
    } = this.props;
    const plainOptions = ['Apple', 'Pear', 'Orange'];
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <Form layout='inline' align='left' onSubmit={this.query}>
            <Tabs
              defaultActiveKey="1"
              activeKey={this.state.step}
              onTabClick={this.tabClick.bind(this)}
            >
              <TabPane tab="第一步 基本信息" key="1">
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>基本信息</p>
                    <p>BASIC INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="所属单位：">
                      中国电能成套设备有限公司
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="地址：">
                      中国电能成套设备有限公司
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑名称：">
                      中国电能成套设备有限公司
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="楼层：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入楼层" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>租赁信息</p>
                    <p>RENT INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_state')(
                          <Select placeholder="请选择坐落">
                            <Option key='0' initialvalue="0">未借出</Option>
                            <Option key='1' initialvalue="1">已借出</Option>
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>租赁面积</p>
                    <p>RENT AREA</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <FormItem label="租赁用途明细：">
                      {
                        getFieldDecorator('s_name')(
                          <TextArea style={{
                            width: 1020
                          }} placeholder="请输入租赁用途明细" autosize={{ minRows: 2, maxRows: 6 }} />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>租金</p>
                    <p>RENT</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>房屋空余情况</p>
                    <p>HOUSING VACANCY</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_state')(
                          <Select placeholder="请选择坐落">
                            <Option key='0' initialvalue="0">未借出</Option>
                            <Option key='1' initialvalue="1">已借出</Option>
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_state')(
                          <Select placeholder="请选择坐落">
                            <Option key='0' initialvalue="0">未借出</Option>
                            <Option key='1' initialvalue="1">已借出</Option>
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    {
                      getFieldDecorator('s_state')(
                        <CheckboxGroup options={plainOptions} initialvalue={['Apple']} />
                      )
                    }
                  </Col>
                </Row>
              </TabPane>

              <TabPane tab="第二步 项目批复信息" key='2'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>项目信息</p>
                    <p>PROJECT INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <FormItem label="项目批准单位：">
                    {
                      getFieldDecorator('s_name')(
                        <Input placeholder="请输入项目批准单位" />
                      )
                    }
                  </FormItem>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>项目文号和日期</p>
                    <p>PROJECT BUMBER AND DATE</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='缴费时间：'
                    >
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                      //onChange={this.changeDate.bind(this)}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>租赁期限</p>
                    <p>TERM OF LEASE</p>
                  </div>
                </div>
                <Row>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='缴费时间：'
                    >
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                      //onChange={this.changeDate.bind(this)}
                      />
                    </FormItem>
                  </Col>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='缴费时间：'
                    >
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                      //onChange={this.changeDate.bind(this)}
                      />
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>租赁许可期限</p>
                    <p>LEASE LICENSE PERIOD</p>
                  </div>
                </div>
                <Row>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='缴费时间：'
                    >
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                      //onChange={this.changeDate.bind(this)}
                      />
                    </FormItem>
                  </Col>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='缴费时间：'
                    >
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                      //onChange={this.changeDate.bind(this)}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>其他信息</p>
                    <p>OTHER INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_state')(
                          <Select placeholder="请选择坐落">
                            <Option key='0' initialvalue="0">未借出</Option>
                            <Option key='1' initialvalue="1">已借出</Option>
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="房间号：">
                      {
                        getFieldDecorator('s_name')(
                          <Input placeholder="请输入房间号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    {
                      getFieldDecorator('s_state')(
                        <CheckboxGroup options={plainOptions} initialvalue={['Apple']} />
                      )
                    }
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
            <div className={styles.energyEditFooter}>
              <Button type='primary' disabled={this.state.step === '1' ? true : false} onClick={this.changeStep.bind(this, '1')}>上一步</Button>
              &emsp;
              <Button type='primary' disabled={this.state.step !== '1' ? true : false} onClick={this.changeStep.bind(this, '2')}>下一步</Button>
              &emsp;
              <Button type='primary'>保存</Button>
              &emsp;
              <Button type='primary' onClick={this.cancelEdit.bind(this)}>取消</Button>
            </div>
          </Form>
        </div>
      </PageHeaderWrapper >
    );
  }
}

const WrappedApp = Form.create()(EnergyEdit);

export default WrappedApp;
