//**********************************
//基础功能类-查询
//Power by han 2018.07.05
//**********************************
var objQuery = {
    pWkid: 0,//存储坐标系代码
    pMap: null,//存储地图对象
    init: function (w, m) { //初始化类对象
        this.pWkid = w;
        this.pMap = m;
    },
    setWkid: function (e) { //设置坐标系代码
        this.pWkid = e;
    },
    getWkid: function () { //获取坐标系代码
        return this.pWkid;
    },
    setMap: function (e) { //设置地图对象
        this.pMap = e;
    },
    getMap: function () { //获取地图对象
        return this.pMap;
    },
    paramTest: function () { //参数验证
        if (this.pWkid == 0) {
            alert("objQuery error 'wkid'");
            console.log("坐标系代码未设置");
            return false;
        }
        if (this.pMap == null) {
            alert("objQuery error 'map'");
            console.log("地图未设置");
            return false;
        }
        return true;
    },
    sqlQuery: function (url, sql, callback, errback, geometry) { //SQL查询
        if (this.paramTest() == false) return;
        var query = new esri.tasks.Query();
        if (geometry && geometry != null) {
            query.geometry = geometry;
            query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_INTERSECTS; //包含全部查询
        }
        query.where = sql;
        query.outSpatialReference = new esri.SpatialReference({"wkid": this.pWkid});
        query.returnGeometry = true;
        query.outFields = ["*"];
        var queryTask = new esri.tasks.QueryTask(url);
        var e = this.errorQuery;
        if (errback && errback != null) {
            e = errback;
        }
        queryTask.execute(query, callback, e);
    },
    spatialQuery: function (url, geometry, slayers, callback, errback) {
        if (this.paramTest() == false) return;
        require(["esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters"],
            function (IdentifyTask, IdentifyParameters) {
                var identifyTask = new IdentifyTask(url);
                var identifyParams = new IdentifyParameters();
                identifyParams.tolerance = 10;
                identifyParams.returnGeometry = true;
                identifyParams.layerIds = slayers;
                identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                identifyParams.width = objQuery.pMap.width;
                identifyParams.height = objQuery.pMap.height;
                identifyParams.mapExtent = objQuery.pMap.extent;
                identifyParams.geometry = geometry;
                var e = this.errorQuery;
                if (errback && errback != null) {
                    e = errback;
                }
                identifyTask.execute(identifyParams, callback, e);
            });
    },
    //查询返回失败调用函数
    errorQuery: function (e) {
        console.log("查询返回错误：");
        console.log(e);
    }
}

