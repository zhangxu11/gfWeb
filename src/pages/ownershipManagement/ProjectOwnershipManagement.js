//项目权属管理
import * as React from 'react';
import {
  Table,
  Form,
  Select,
  Input,
  Button,
  Col,
  Row,
  Progress,
  Tree,
  Card,
  Icon,
  notification,
  Popover,
  Spin
} from 'antd';
import router from 'umi/router';
import Link from 'umi/link';
import { connect } from 'dva';
import axios from 'axios'
import Api from '../../services/apis'
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

const FormItem = Form.Item;
const Option = Select.Option;
const { Meta } = Card;


@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class ProjectOwnershipManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedKeys: [],
      loading: true,
      pageSize: 5,
      current: 1,
      tableData: []
    };
  }

  onSelect(selectedKeys, info) {
    console.log('onSelect', info);
    this.setState({ selectedKeys });
  }

  componentWillMount(){
      this.sendData();
  }

  sendData = () => {
      const that = this;
      const searchValues = this.props.form.getFieldsValue();
      that.setState({loading:true});

      axios.post(Api.ownership,{
          projectName: searchValues.name,
          transferUnit: searchValues.company,
          currentPage: this.state.current
      })
      .then((function(resp){
          const data = resp.data;
          if(data.list.length === 0){
              notification.destroy();
              notification['warning']({message:'暂无数据'})
          }else{
              data.list.forEach((d,i) => {
                  d.key = i;
              });
          }

          that.setState({
              tableData: data.list,
              pageSize: data.pageSize,
              current: data.currentPage,
              totalSize: data.totalSize
          })
          
          that.setState({loading:false});
      }))
  }


  handleSubmit = e => {
    e.preventDefault();
    this.setState({current:1},()=>{
        this.sendData();
    })
  };

  pageChange = (page) => {
      this.setState({current:page},()=>{
          this.sendData();
      })
  }

  onEdit() {
    this.setState({
      canEdit: !this.state.canEdit,
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      currentUserLoading,
    } = this.props;

    const columns = [
      {
        title: '权属项目名称',
        dataIndex: 'projectName',
        key: 'projectName',
        render: (text, record) => (
          <Link to={{
            pathname: '/fe/ownershipManagement/warrantManagement',
            query: {
              name: record.projectName
            }
          }}>{text? text : '-'}</Link>
        ),
      },
      {
        title: '划转单位',
        dataIndex: 'transferUnit',
        key: 'transferUnit',
      },
      {
        title: '权属进展',
        dataIndex: 'osPlan',
        key: 'osPlan',
        render: percent => (
          <Progress status={percent >= 100 ? 'success' : 'active'} percent={parseInt(percent)} size="small" />
        ),
      },
      {
        title: '接管日期',
        dataIndex: 'receiveDate',
        key: 'receiveDate',
        render: (text, record) => (
          <span>{text ? text.substring(0, 10) : '-'}</span>
        )
      },
      {
        title: '工作开始日期',
        dataIndex: 'workStartDate',
        key: 'workStartDate',
        render: (text, record) => (
          <span>{text ? text.substring(0, 10) : '-'}</span>
        )
      },
      {
        title: '完成日期',
        key: 'doneDate',
        dataIndex: 'doneDate',
        render: (text, record) => (
          <span>{text ? text.substring(0, 10) : '-'}</span>
        )
      },
      {
        title: '操作人',
        key: 'acceptUser',
        dataIndex: 'acceptUser',
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => (
          <Link
           target='_blank'
           to={{
            pathname: '/fe/ownershipManagement/ownershipStatus',
            search: `?projectName=${record.projectName}&projectCode=${record.projectCode}`
          }}>
            详情
          </Link>       
        ),
      },
    ];

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>项目权属管理</p>
              <p>PROJECT OWNERSHIP MANAGEMENT</p>
            </div>
          </div>
          <div className="pom">
            <div className='mrb15'>
              <Form layout="inline" align='center' onSubmit={this.handleSubmit}>
                  <FormItem label="项目名称：">
                    {getFieldDecorator('name')(<Input placeholder="请输入项目名称" />)}
                  </FormItem>
                  <FormItem label="划转单位：">
                    {getFieldDecorator('company')(
                      <Input placeholder="请输入项目名称" />
                    )}
                  </FormItem>
                  <Row gutter={24} style={{ textAlign: 'center'}}>
                    <Button icon="search" type="primary" htmlType="submit">
                      搜索
                    </Button>
                  </Row>
              </Form>
            </div>
            
            <Spin spinning={this.state.loading}>
            <Table
              bordered={true}
              scroll={{ x: true }}
              columns={columns}
              dataSource={this.state.tableData}
              pagination={{
                  pageSize:this.state.pageSize,
                  current:this.state.current,
                  total:this.state.totalSize,
                  onChange:this.pageChange,
                  showTotal: total => `共 ${total} 项`
              }} />
            </Spin>
          </div>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(ProjectOwnershipManagement);
export default WrappedApp;
