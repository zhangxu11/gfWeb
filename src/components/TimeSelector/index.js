import React from 'react';
import moment from 'moment';
import { Tag, DatePicker } from 'antd';

/**
 * 带年月选择器的时间选择器
 *
 * @export
 * @param {{startDate: Monent, endDate: Monent, onChange: (startDate: Monent, endDate: Monent) => void}} props
 * @returns
 */
export default function TimeSelectorWithMonthYear({ startDate, endDate, onChange }) {
  const startDateMonth = moment().startOf('month');
  const endDateMonth = moment().endOf('month');
  const startDateYear = moment().startOf('year');
  const endDateYear = moment().endOf('year');
  let checkYear = false;
  let checkMonth = false;
  if (startDate.isSame(startDateMonth, 'day') && endDate.isSame(endDateMonth, 'day')) checkMonth = true;
  if (startDate.isSame(startDateYear, 'day') && endDate.isSame(endDateYear, 'day')) checkYear = true;
  return (
    <div>
      <Tag.CheckableTag checked={checkMonth} onChange={() => onChange(startDateMonth, endDateMonth)}>本月</Tag.CheckableTag>
      <Tag.CheckableTag checked={checkYear} onChange={() => onChange(startDateYear, endDateYear)}>全年</Tag.CheckableTag>
      <DatePicker.RangePicker onChange={(dates) => onChange(dates[0], dates[1])} value={[startDate, endDate]} />
    </div>
  );
}
