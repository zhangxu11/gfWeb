import React, { Component } from 'react';

import { Skeleton, Card, Icon, Row, Col, message, BackTop, Button, Modal, Input, Form, Select, notification, Spin, Checkbox } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import QueueAnim from 'rc-queue-anim';
import styles from "./style.less";
import router from 'umi/router'
import axios from 'axios';
import Api from '../../services/apis'

const { Meta } = Card;
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class OwnershipStatusHouse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      modalVisible: false,
      houseData: [],
      docLevelDis: false,
      checked: [],
      showChecked: false
    }
  }
  componentDidMount() {

    this.setState({
      loading: false,
      buildingCode: this.props.location.query.buildingCode,
      tradeId: this.props.location.query.tradeId,
      projectCode: this.props.location.query.projectCode,
      buildingName: this.props.location.query.buildingName,
      docLevel: this.props.location.query.docLevel,
      transferUnit: this.props.location.query.transferUnit
    }, () => {
      this.getHouseList();
    });
  }

  //获取房屋权属概况
  getHouseList = () => {
    const that = this;
    that.setState({ loading: true });
    axios.get(Api.omGetDataList, {
      params: {
        buildingCode: that.state.buildingCode,
        tradeId: that.state.tradeId
      }
    })
      .then(function (resp) {
        const { data } = resp;
        if (data.length === 0) {
          notification.destroy();
          notification['warning']({ message: '暂无数据！' })
        }
        let checked = [];
        for (let i = 0, len = data.length; i < len; i++) {
          checked.push({ selected: false, item: data[i] });
        };
        that.setState({
          houseData: data,
          checked
        })
        that.setState({ loading: false });
      })
  }

  setModalVisible(modalVisible) {
    this.setState({
      modalVisible
    });
  }

  openModal(item, transferState, type) {
    this.setModalVisible(true);

    if (item.docLevel === '楼栋') {
      this.setState({ docLevelDis: true })
    } else {
      this.setState({ docLevelDis: false })
    }

    this.props.form.setFieldsValue({
      fzr: item.fzr,
      docLevel: item.docLevel ? item.docLevel : '',
      transferState: transferState,
      docType: item.docType,
      docName: item.docName
    })
  }

  changeDocLevelDis(changeDocLevelDis) {
    this.setState({
      changeDocLevelDis
    });
  }

  edit(item, editFlag) {
    this.props.form.resetFields();
    switch (item.docLevel) {
      case '房屋':
      case '楼栋':
        this.setState({
          editType: 'building'
        });
        break;

      case null:
        this.setState({
          editType: 'null'
        });
        break;
    }
    if (this.state.docLevel == '楼栋') {
      notification.destroy();
      notification['warning']({ message: '不能编辑！' })
      return false;
    };

    this.setState({ item: item, editFlag: editFlag, changeDocLevelDis: '', hasCard: '' }, () => {
      this.openModal(item, '未过户');
    })
  }

  changeDocOwn(docOwn) {
    this.setState({ docOwn });
    let transferState = '';
    if (docOwn === '国房经营公司') {
      transferState = '已过户';
    } else {
      transferState = '未过户';
    };
    this.props.form.setFieldsValue({
      transferState,
    });
  }
  editHouse = (codes) => {
    const item = this.state.item;
    const that = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        that.setState({ loading: true });
        let params = {
          projectCode: that.state.projectCode,
          transferUnit: item.transferUnit,
          docName: (that.state.buildingName ? that.state.buildingName : '') + (that.state.item.name ? that.state.item.name : '') + (values.docType ? values.docType : ''),
          fzr: values.fzr,
          code: item.code,
          docType: values.docType,
          docLevel: values.docLevel,
          transferState: values.transferState,
          buildingCode: that.state.buildingCode,
          sourceType: 'h',
          cardBelong: values.docOwn,
          isHaveCard: values.hasCard,
          canHandle: values.canBanli,
          docId: item.docId,
          tradeId: item.tradeId,
        }

        //批量
        if (that.state.editFlag == 'muli') {
          const checks = that.state.checked;
          console.log(checks)
          let ids = [];
          let docNames = [];
          let docIds = [];
          let tradeIds = [];
          checks.forEach((v, i, arr) => {
            if (v.selected) {
              ids.push(v.item.code ? v.item.code : '');
              docNames.push((that.state.buildingName ? that.state.buildingName : '') + (v.item.name ? v.item.name : '') + (values.docType ? values.docType : ''));
              docIds.push(v.item.docId ? v.item.docId : '');
              tradeIds.push(v.item.tradeId ? v.item.tradeId : '');
            }
          })
          params.code = ids.join(',');
          params.docName = docNames.join(',');
          params.docId = docIds.join(',');
          params.tradeId = tradeIds.join(',');
        }
        axios.get(Api.omEditOwnerInfo, {
          params: params
        })
          .then(function (resp) {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification['success']({ message: data.msg })
              that.setModalVisible(false);
              that.getHouseList();
            } else {
              notification.destroy();
              notification['warning']({ message: data.msg })
            }
            that.setState({ loading: false });
          })
      }
    })
  }


  goBack = () => {
    router.goBack();
  }
  changeChe(val, index) {
    let checked = this.state.checked;
    let showChecked = false;
    checked[index].selected = !checked[index].selected;
    for (let i = 0, len = checked.length; i < len; i++) {
      if (checked[i].selected) {
        showChecked = true;
      };
    };
    this.setState({
      checked,
      showChecked
    });
  }
  checkAll(e) {
    let checked = this.state.checked;
    let tmp = [];
    for (let i = 0, len = checked.length; i < len; i++) {
      tmp.push({ selected: e.target.checked, item: checked[i].item });
    };
    this.setState({
      checked: tmp,
      showChecked: e.target.checked,
    });
  }

  changeBuildingCard(hasCard) {
    this.setState({
      hasCard
    });
  }

  //批量提交
  muliSubmit = () => {
    if (this.state.docLevel == '楼栋') {
      notification.destroy();
      notification['warning']({ message: '权证性质为楼栋，不支持批量提交操作！' });
      return;
    }

    this.edit({ docLevel: this.state.docLevel }, 'muli');
  }
  render() {
    const { currentUserLoading, form: { getFieldDecorator } } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>房屋权属概况</p>
              <p>OWNERSHIP STATUS</p>
            </div>
          </div>

          <div className='clearfix'>
            <Button type='primary' className='pull-right' onClick={this.goBack}>返回</Button>

            <div
              className='pull-right'
              style={{
                whiteSpace: 'nowrap',
                margin: '0 10px'
              }}
            >
              <Checkbox onChange={this.checkAll.bind(this)}>全选</Checkbox>
              <Button type='primary' disabled={!this.state.showChecked} onClick={this.muliSubmit}>批量提交</Button>
            </div>

          </div>
          <Spin spinning={this.state.loading}>
            <div className={`clearfix ${styles.cardBox}`}>
              <QueueAnim>
                {this.state.houseData.map((item, index) => {
                  return (
                    <Card
                      key={index}
                      className={styles.cardItem}
                      loading={this.state.loading}
                      actions={
                        [
                          <a onClick={this.edit.bind(this, item, 'single')}><Icon type="edit" />&nbsp;编辑
                          </a>
                        ]
                      }
                      title={[item.name ? item.name : '-',
                      <Checkbox key={index} className={styles.checkTit} onChange={this.changeChe.bind(this, item, index)} checked={this.state.checked[index].selected} style={{
                        display: this.state.showChecked ? 'block' : 'none'
                      }} />]}
                      extra={item.fzr ? `负责人：${item.fzr}` : `负责人：-`}
                      style={{
                        borderColor: item.nextDealTag == '有证' ? '#52c41a' : '#e7e7e7',
                        boxShadow: item.nextDealTag == '有证' ? '0 0 10px #52c41a' : '0 0 10px #e7e7e7'
                      }}
                    >
                      <h1>权属状态：<span>{item.nextDealTag ? item.nextDealTag : ''}</span></h1>
                      <Row className={styles.cardMain}>
                        <Col span={12}>
                          <span className='label'>权证类型：</span>
                          {item.docType ? item.docType : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>权证性质：</span>
                          {item.docLevel ? item.docLevel : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>过户状态：</span>
                          {item.transferState ? item.transferState : '-'}
                        </Col>
                        <Col span={12}>
                          <span className='label'>用途：</span>
                          {item.use ? item.use : '-'}
                        </Col>
                        <Col span={12} title={item.startTime}>
                          <span className='label'>开始时间：</span>
                          {item.startTime ? item.startTime : '-'}
                        </Col>
                        <Col span={12} title={item.endTime}>
                          <span className='label'>结束时间：</span>
                          {item.endTime ? item.endTime : '-'}
                        </Col>
                      </Row>
                    </Card>
                  );
                })}
              </QueueAnim>
            </div>
          </Spin>
        </div>
        <Modal
          title='编辑'
          wrapClassName="vertical-center-modal"
          visible={this.state.modalVisible}
          onOk={this.editHouse}
          onCancel={this.setModalVisible.bind(this, false)}
          width={830}
        >
          <Row gutter={24}>
            {this.state.editType === 'building' ?
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="权证性质：">
                    {getFieldDecorator('docLevel', {
                      rules: [{ required: true, message: '请选择权证性质！' }]
                    })(<Select placeholder="请选择权证性质" style={{ width: "100%" }} disabled={this.state.docLevelDis} disabled>
                      <Option value='楼栋'>楼栋</Option>
                      <Option value='房屋'>房屋</Option>
                    </Select>)}
                  </FormItem>
                </Col>

                <Col span={8}>
                  <FormItem label="是否有证：">
                    {getFieldDecorator('hasCard', {
                      rules: [{ required: true, message: '请选择是否有证！' }]
                    })(<Select placeholder="请选择是否有证" style={{ width: "100%" }} onChange={this.changeBuildingCard.bind(this)}>
                      <Option value='是'>是</Option>
                      <Option value='否'>否</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                {this.state.hasCard === '是' ?
                  [
                    <Col span={8} key='0'>
                      <FormItem label="权证类型：">
                        {getFieldDecorator('docType', {
                          rules: [{ required: true, message: '请选择权证类型！' }]
                        })(<Select placeholder="请选择权证类型" style={{ width: "100%" }}>
                          <Option value='使用权证'>使用权证</Option>
                          <Option value='产权证'>产权证</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='1'>
                      <FormItem label="权证归属：">
                        {getFieldDecorator('docOwn', {
                          rules: [{ required: true, message: '请选择权证归属：！' }]
                        })(<Select placeholder="请选择权证归属：" style={{ width: "100%" }} onChange={this.changeDocOwn.bind(this)}>
                          <Option value={this.state.transferUnit}>{this.state.transferUnit}</Option>
                          <Option value='国房经营公司'>国房经营公司</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='2'>
                      <FormItem label="过户状态：">
                        {getFieldDecorator('transferState', {
                          rules: [{ required: true, message: '请输入过户状态！' }]
                        })(<Select placeholder="请选择过户状态" style={{ width: "100%" }} disabled>
                          <Option value='已过户'>已过户</Option>
                          <Option value='未过户'>未过户</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='3'>
                      <FormItem label="代办人：">
                        {getFieldDecorator('fzr', {
                          rules: [{ required: true, message: '请输入代办人！' }]
                        })(<Input placeholder="输入代办人" />)}
                      </FormItem>
                    </Col>
                  ] : null}

                {this.state.hasCard === '否' ?
                  <Col span={8}>
                    <FormItem label="是否可办理：">
                      {getFieldDecorator('canBanli', {
                        rules: [{ required: true, message: '请选择是否可办理！' }]
                      })(<Select placeholder="请选择办理状态" style={{ width: "100%" }}>
                        <Option value='可办理'>可办理</Option>
                        <Option value='不可办理'>不可办理</Option>
                      </Select>)}
                    </FormItem>
                  </Col> : null}
              </Row>
              : null}

            {this.state.editType === 'null' ?
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="权证性质：">
                    {getFieldDecorator('docLevel', {
                      rules: [{ required: true, message: '请选择权证性质！' }]
                    })(<Select placeholder="请选择权证性质" style={{ width: "100%" }} disabled={this.state.docLevelDis} onChange={this.changeDocLevelDis.bind(this)}>
                      <Option value='楼栋'>楼栋</Option>
                      <Option value='房屋'>房屋</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                {this.state.changeDocLevelDis === '楼栋' ?
                  <Col span={8}>
                    <FormItem label="是否有证：">
                      {getFieldDecorator('hasCard', {
                        rules: [{ required: true, message: '请选择是否有证！' }]
                      })(<Select placeholder="请选择是否有证" style={{ width: "100%" }} onChange={this.changeBuildingCard.bind(this)}>
                        <Option value='是'>是</Option>
                        <Option value='否'>否</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  : null}

                {this.state.hasCard === '是' ?
                  [
                    <Col span={8} key='0'>
                      <FormItem label="权证类型：">
                        {getFieldDecorator('docType', {
                          rules: [{ required: true, message: '请选择权证类型！' }]
                        })(<Select placeholder="请选择权证类型" style={{ width: "100%" }}>
                          <Option value='使用权证'>使用权证</Option>
                          <Option value='产权证'>产权证</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='1'>
                      <FormItem label="权证归属：">
                        {getFieldDecorator('docOwn', {
                          rules: [{ required: true, message: '请选择权证归属：！' }]
                        })(<Select placeholder="请选择权证归属：" style={{ width: "100%" }} onChange={this.changeDocOwn.bind(this)}>
                          <Option value={this.state.transferUnit}>{this.state.transferUnit}</Option>
                          <Option value='国房经营公司'>国房经营公司</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='2'>
                      <FormItem label="过户状态：">
                        {getFieldDecorator('transferState', {
                          rules: [{ required: true, message: '请输入过户状态！' }]
                        })(<Select placeholder="请选择过户状态" style={{ width: "100%" }} disabled>
                          <Option value='已过户'>已过户</Option>
                          <Option value='未过户'>未过户</Option>
                        </Select>)}
                      </FormItem>
                    </Col>,
                    <Col span={8} key='3'>
                      <FormItem label="代办人：">
                        {getFieldDecorator('fzr', {
                          rules: [{ required: true, message: '请输入代办人！' }]
                        })(<Input placeholder="输入代办人" />)}
                      </FormItem>
                    </Col>
                  ] :
                  null
                }
                {this.state.hasCard === '否' ?
                  <Col span={8}>
                    <FormItem label="是否可办理：">
                      {getFieldDecorator('canBanli', {
                        rules: [{ required: true, message: '请选择是否可办理！' }]
                      })(<Select placeholder="请选择办理状态" style={{ width: "100%" }}>
                        <Option value='可办理'>可办理</Option>
                        <Option value='不可办理'>不可办理</Option>
                      </Select>)}
                    </FormItem>
                  </Col> : null}
              </Row>
              : null}

          </Row>
        </Modal>
        <BackTop />
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(OwnershipStatusHouse);
export default WrappedApp;
