import React, { Component } from 'react';

import { Row, Col, Checkbox, Modal, Spin } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import ReactEcharts from 'echarts-for-react';

const BgElement = Element.BgElement;
const CheckboxGroup = Checkbox.Group;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class RenterDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renterId: '',
      renterName: '-',
      linkman: '-',
      cardNum: '-',
      hy_type: '-',
      contactWay: '-',
      mailbox: '-',
      mainBusiness: '-',
      investmentAmount: '-',
      peopleNum: '-',
      renterSituation: '-',
      high: '-',
      establishedTime: '-',
      fullIncome: '-',
      income: '-',
      zcType: '-',
      registrationAuthority: '-',
      registeredCapital: '-',
      registeredAddress: '-',
      intro: '-',

      openingBank: '-',
      accountNumber: '-',
      i_contactWay: '-',
      i_nsrsbh: '-',
      address: '-',
      receiveDate: '-',

      shxydm: '-',
      b_nsrsbh: '-',
      registrationNumber: '-',
      orgNum: '-',
      updateTime: '-',

      bachelorNum: '-',
      masterNumber: '-',
      doctorNumber: '-',
      abroadNumber: '-',
      personnelNumber: '-',

      loading: true,
      chartLoading: false,
      fileList: [],
      xAxis: [],
      yAxis: [],
      picurl: ''
    };
  }
  componentWillMount() {
    const renterId = this.props.location.query.id;
    this.setState({ renterId });
    this.getFeeMaintainById(renterId);
  }

  getFeeMaintainById(renterId) {
    axios.post(Api.getRenterDetailInfoById2, { renterId: renterId }).then(res => {
      const xAxis = this.buildXAxis(res.data.incomeChart);
      const yAxis = this.buildYAxis(res.data.incomeChart);

      this.setState({
        ...res.data,
        xAxis,
        yAxis,
        loading: false
      });
    });
  }

  buildXAxis = (data) => {
    if (!data) return [];
    return data.map(d => d.year);
  }

  buildYAxis = (data) => {
    if (!data) return [];
    return data.map(d => d.fullIncome);
  }

  render() {
    const {
      currentUserLoading,
    } = this.props;
    const optionsWithDisabled = [
      { label: 'Apple', value: 'Apple' },
      { label: 'Pear', value: 'Pear' },
      { label: 'Orange', value: 'Orange', disabled: false },
    ];

    const barData = {
      color: ['#f35245'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      xAxis: [
        {
          type: 'category',
          data: this.state.xAxis,
        }
      ],
      yAxis: [
        {
          type: 'value',
          min: 0,
        }
      ],
      series: [
        {
          name: '数量',
          type: 'bar',
          data: this.state.yAxis,
          barWidth: 30,
        }
      ]
    }
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div style={{
          marginBottom: 50
        }}>
          <Row className={styles.w1120}>
            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>企业展示</p>
                    <p>COMPANY EXHIBITION</p>
                  </div>
                </div>
              </div>
              <div>
                {
                  this.state.picurl
                    ?
                    <img style={{ width: '1122px', height: '300px' }} src={`http://gisams-hitech.cindata.cn/res_file${this.state.picurl}`}></img>
                    :
                    <div style={{ textAlign: 'center', marginBottom: 20 }}>暂无图片</div>
                }
              </div>

            </div>

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>企业信息</p>
                    <p>COMPANY INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.br2}>
                <Row>
                  <Col span={24}>
                    <Col style={{ float: 'left', textAlign: 'left' }} span={10}>
                      <span className='label'>企业名称：</span>{this.state.renterName}
                    </Col>
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.linkman} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>联系人：</span>{this.state.linkman}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.r_contactWay} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>联系方式：</span>{this.state.r_contactWay}
                  </Col>
                  <Col className={styles.col8Overflow} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>证件类型：</span>{'身份证'}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.cardNum} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>证件号：</span>{this.state.cardNum}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.mailbox} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>邮箱：</span>{this.state.mailbox}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.hy_type} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>行业分类：</span>{this.state.hy_type}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.mainBusiness} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>主营业务：</span>{this.state.mainBusiness}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.registeredCapital} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>注册资金：</span>{this.state.registeredCapital}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.investmentAmount} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>投资额：</span>{this.state.investmentAmount}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.peopleNum} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>人员规模：</span>{this.state.peopleNum}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.renterSituation} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>企业情况：</span>{this.state.renterSituation}
                  </Col>
                  <Col className={styles.col8Overflow} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>高企：</span>{this.state.high}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.establishedTime} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>成立时间：</span>{this.state.establishedTime}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.zcType} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>注册类型：</span>{this.state.zcType}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.registrationAuthority} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>登记机关：</span>{this.state.registrationAuthority}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.registeredAddress} style={{ textAlign: 'left' }} span={6}>
                    <span className='label'>注册地址：</span>{this.state.registeredAddress}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.intro} style={{ textAlign: 'left' }} span={24}>
                    <span className='label'>企业简介：</span>{this.state.intro}
                  </Col>
                </Row>
              </div>
            </div>

            <div className={`pull-left ${styles.w560}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className={`renter_contentHeaderTitle ${styles.small}`}>
                    <p>开票信息</p>
                    <p>INVOICE INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.br3}>

                <Row>
                  <span style={{ width: '106px' }} className='label'>开户行：</span>{this.state.openingBank}
                </Row>
                <Row>
                  <span style={{ width: '106px' }} className='label'>账号：</span>{this.state.accountNumber}
                </Row>
                <Row>
                  <span style={{ width: '106px' }} className='label'>纳税人识别号：</span>{this.state.i_nsrsbh}
                </Row>
                <Row>
                  <span style={{ width: '106px' }} className='label'>联系方式：</span>{this.state.i_contactWay}
                </Row>
                <Row>
                  <span style={{ width: '106px' }} className='label'>开票地址：</span>{this.state.address}
                </Row>
              </div>
            </div>

            <div className={`pull-left ${styles.w560}`}>
              <div className='mainContainer'>
                <div className='contentHeader'>
                  <div className={`renter_contentHeaderTitle ${styles.small}`}>
                    <p>工商信息</p>
                    <p>RENT</p>
                  </div>
                </div>
              </div>
              <div className={styles.br3}>

                <Row>
                  <span style={{ width: '150px' }} className='label'>统一社会信用代码：</span>{this.state.shxydm}
                </Row>
                <Row>
                  <span style={{ width: '150px' }} className='label'>注册号：</span>{this.state.registrationNumber}
                </Row>
                <Row>
                  <span style={{ width: '150px' }} className='label'>纳税人识别号：</span>{this.state.b_nsrsbh}
                </Row>
                <Row>
                  <span style={{ width: '150px' }} className='label'>组织机构代码：</span>{this.state.orgNum}
                </Row>
              </div>
            </div>

            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>全口径税收</p>
                    <p>HOUSE INFORMATION</p>
                  </div>
                </div>
                <Spin spinning={this.state.chartLoading}>
                  <div>
                    <ReactEcharts
                      option={barData}
                      notMerge={true}
                      lazyUpdate={true}
                      style={{
                        height: 300
                      }}
                    />
                  </div>
                </Spin>
              </div>
            </div>
            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer' style={{ padding: '0' }}>
                <div className={styles.br1} style={{ boxShadow: '5px 4px 11px #ccc', height: 'auto', overflow: 'auto' }}>
                  <Col span={24}>
                    <Col style={{ float: 'left', fontSize: '16px', fontWeight: 600 }} span={4}>
                      <span style={{ float: 'left', fontSize: '16px', fontWeight: 600 }} className='label'>附件</span>
                    </Col>
                  </Col>
                  {
                    this.state.fileList.map((item, index) => {
                      return (
                        <Col span={4} key={item.fileId}>
                          <a style={{ fontSize: '14px' }} className='label' href={`http://gisams-hitech.cindata.cn/res_file${item.filePath}`} download={item.uploadName}>{item.uploadName}</a>
                        </Col>
                      )
                    })
                  }
                </div>

              </div>
            </div>

          </Row>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default RenterDetails;
