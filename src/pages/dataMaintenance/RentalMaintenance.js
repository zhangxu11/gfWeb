// 项目权属管理
import * as React from 'react';
import {
  Table,
  Form,
  Select,
  Input,
  Button,
  Col,
  Row,
  Modal,
  Checkbox,
  notification,
  DatePicker,
  Spin,
  Upload,
  message,
  Icon, 
} from 'antd';
import axios from 'axios';
import Api from '@/services/apis';
import moment from 'moment';

import Link from 'umi/link';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './style.less';

const FormItem = Form.Item;
const {Option} = Select;
const CheckboxGroup = Checkbox.Group;
const {RangePicker} = DatePicker;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class RentalMaintenance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      defCheck: [],
      leaseDate: [],
      sTransferUnit: [],// 划转单位下拉
      sCommunitiesByTrUnit: [],// 小区信息
      sBuildsByProjCode: [],// 栋号
      sHousesByBuildingCode: [],// 房号
      m_leaseDate: [],
      m_allowDate: [],
      "transfer_unit": "",// 划转单位名称
      "community_name": "",// 小区名称
      "building_code": "",// 楼栋代号
      "house_code": "",// 户代号
      "fee_type": "",// 费用类型
      "begin_time": "",// 开始时间
      "end_time": "",// 终止时间
      currentPage: 1,// 当前页
      "pageSize": 10,// 页大小
      address: '',// 地址
      feeType: [],
      tableData: [],
      loading: false,
      current: 1,
      totalSize: 0,
      m_current: 1,
      m_pageSize: 5,
      submitFlag: '',
      rooms: [],
      cabinets: [],
      boxs: [],
      modalLoading: false,
      bags: [],
      keyId: '',
      searchData: {
        orgName: '',
        address: '',
        partyB: '',
        leaseStartDate: '',
        leaseEndDate: '',
        currentPage: 1,
      },
      modalTitle: '',
      feeDate: '',
      statisticDate: '',
      statisticType: [],
      nowItem: '',
      modalSpin: false,
      checkData: {
        allIdle: 0,// 是否整座空余
        storeCamp: 0,// 是否储备房区
        frontageHouse: 0,// 是否临街房屋
        showLessee: 0,// 公示承租人
        showRent: 0,// 公示年租金
        haveChecked: 0, // 是否通过复检
        haveApprove: 0, // 是否通过核查验收
      },
      editData: {
        address: "",
        allIdle: "",
        allowEndDate: "",
        allowStartDate: "",
        approveDate: "",
        approveNumber: "",
        approveOrg: "",
        approveOrgTxt: "",
        approvePerson: "",
        buildId: "",
        buildingCode: "",
        buildingName: "",
        checkPerson: "",
        communityId: "",
        communityName: "",
        fillPerson: "",
        frontageHouse: "",
        haveApprove: "",
        haveChecked: "",
        houseId: "",
        houseIdle: "",
        houseIdleTxt: "",
        houseLeaseBuildingName: "",
        houseLeaseDPrice: "",
        houseLeaseFloor: "",
        houseLeaseRoomNumber: "",
        houseName: "",
        issuingAuthority: "",
        issuingAuthorityTxt: "",
        leaseEndDate: "",
        leaseGarea: "",
        leaseHarea: "",
        leaseId: 6,
        leaseMonth: "",
        leaseStartDate: "",
        leaseType: "",
        leaseTypeTxt: "",
        leaseUse: "",
        locationCode: "",
        locationId: "",
        locationNumber: "",
        oldNature: "",
        oldNatureTxt: "",
        orgName: "0",
        partyA: "",
        partyB: "",
        rentAll: "",
        rentCur: "",
        rentHq: "",
        rentMac: "",
        rentOrg: "",
        rentOrgType: "",
        rentOrgTypeTxt: "",
        showLessee: "",
        showRent: "",
        storeCamp: "",
        whereCamp: "",
        whereCampTxt: "",
      },
      fileList:[],
      fileIds: [],
    };
  }
  
  // 上传相关
  // 图片预览
  handlePreview(file) {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  // 上传
  handleChange(info) {
    const { fileList } = info;
    this.setState({ fileList });
    if (info.file.status === 'done') {
      message.success(`${info.file.name} 上传成功！`);

      const {fileIds} = this.state;
      fileIds.push(info.file.response);

      this.setState({ fileIds });
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败！`);
    }
  }

  // 删除上传
  removePic(info) {
    const p = new Promise((resp, rej) => {
      axios.get(Api.deleteFile, {
        params: {
          fileId: info.response
        }
      }).then((res) => {
        if (Number(res.data.code) === 1 || Number(info.response) === 0) {
          message.success(res.data.msg);
          resp('success');
        } else {
          message.error(res.data.msg);
          rej('error');
        };
      });
    });
    return p;
  }

  // 关闭图片预览
  handlePreviewCancel() {
    this.setState({ previewVisible: false })
  }

  changeDate(type, dates, dateStrings) {
    switch (type) {
      case 'leaseDate':
        this.setState({
          leaseDate: dateStrings
        });
        break;
      case 'm_approveDate':
        this.setState({
          m_approveDate: dateStrings
        });
        break;
      case 'm_leaseDate':
        this.setState({
          m_leaseDate: dateStrings
        });
        break;
      case 'm_allowDate':
        this.setState({
          m_allowDate: dateStrings
        });
        break;
      default: break;
    }
  }

  componentWillMount() {
    this.queryAllUnit();
    this.queryAllCommunity();
    this.getAllStatisticType();
    this.getTableData(this.state.searchData);
  }

  queryAllCommunity() {
    axios.post(Api.queryAllCommunity).then((res) => {
      this.setState({
        sCommunitiesByTrUnit: res.data
      });
    });
  }

  modalSubmit() {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const param = {
          ...this.state.checkData
        };
        for (const key in values) {
          param[key.slice(2)] = values[key];
        };
        param.approveDate = param.approveDate ? moment(param.approveDate).format('YYYY-MM-DD') : '';
        param.leaseStartDate = Array.isArray(values.m_leaseDate) ? moment(values.m_leaseDate[0]).format('YYYY-MM-DD') : '';
        param.leaseEndDate = Array.isArray(values.m_leaseDate) ? moment(values.m_leaseDate[1]).format('YYYY-MM-DD') : '';
        param.allowStartDate = Array.isArray(values.m_allowDate) ? moment(values.m_allowDate[0]).format('YYYY-MM-DD') : '';
        param.allowEndDate = Array.isArray(values.m_allowDate) ? moment(values.m_allowDate[1]).format('YYYY-MM-DD') : '';
        param.houseId = values.m_house_code;
        param.fileIds = this.state.fileIds;

        this.maintainData(param);
      }
    })
  }

  queryOne(leaseId) {
    this.setState({
      modalSpin: true
    });
    axios.post(Api.queryOne, { leaseId }).then(res => {
      const fileList = [];
      let fileIds = [];
      if (res.data.fileId!=null) {
        fileIds = res.data.fileId.split(',');
      };
      for (let i = 0, len = res.data.fileList.length; i < len; i++) {
        fileList.push(
          {
            uid: i,
            name: res.data.fileList[i].uploadName,
            status: 'done',
            url: `http://gisams-hitech.cindata.cn/res_file/${res.data.fileList[i].filePath}`
          }
        );
      }
      this.setState({
        editData: res.data,
        modalSpin: false,
        defCheck: this.formatCheck(res.data),
        fileList,
        fileIds
      });
    });
  }

  setModalVisible(modalVisible, modalType, nowItem) {
    // this.resetForm();
    this.setState({
      modalSpin: false
    });
    
    if (modalType === 'updata') {
      this.queryOne(nowItem.lease_id);
    };
    let modalTitle = '';
    switch (modalType) {
      case 'new':
        modalTitle = '新建费用维护';
        this.setState({ fileIds: [] ,fileList:[]});
        break;
      default:
        modalTitle = '编辑费用维护';
        break;
    };
    this.setState({ modalVisible, modalType, modalTitle, nowItem });
  }

  resetForm() {
    this.props.form.resetFields();
    const searchData = {
      orgName: '',
      address: '',
      partyB: '',
      leaseStartDate: '',
      leaseEndDate: '',
      currentPage: 1,
    };
    this.setState({ searchData });
    this.getTableData(searchData);
  }

  query = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      const data = {};
      data.orgName = values.orgName === undefined ? '' : this.state.sTransferUnit[values.orgName];
      data.address = values.address === undefined ? '' : values.address;
      data.partyB = values.partyB === undefined ? '' : values.partyB;
      data.leaseStartDate = this.state.leaseDate[0] ? this.state.leaseDate[0] : '';
      data.leaseEndDate = this.state.leaseDate[1] ? this.state.leaseDate[1] : '';
      data.currentPage = this.state.currentPage;
      this.setState({
        searchData: data
      });
      this.getTableData(data);
    });
  }

  getTableData(data) {
    this.setState({
      loading: true
    });
    axios.post(Api.queryAll, { "pageSize": 10, ...data }).then((res) => {
      const tableData = res.data.list;
      for (let i = 0, len = tableData.length; i < len; i++) {
        tableData[i].key = i;
      };
      this.setState({
        tableData,
        loading: false,
        totalSize: res.data.totalSize
      });
    });
  }

  pageChange = (page) => {
    this.setState({ current: page }, () => {
      const data = this.state.searchData;
      data.currentPage = page;
      this.getTableData(data);
    })
  }


  queryAllUnit() {
    axios.get(Api.queryAllUnit).then((res) => {
      this.setState({
        sTransferUnit: res.data
      });
    });
  }

  maintainData(data) {
    this.setState({
      modalLoading: true
    });
    const type = this.state.modalType;
    const {nowItem} = this.state;
    data.fileId = data.fileIds.join();

    switch (type) {
      case 'new':
        axios.post(Api.add, data).then((res) => {
          if (Number(res.data.flag) !== 0) {
            message.error(`新建失败，${res.data.msg}`);
          } else {
            message.success(res.data.msg);
            this.setModalVisible(false);
            this.getTableData(this.state.searchData);
          };
          this.setState({
            modalLoading: false
          });
        });
        break;
      case 'updata':
        axios.post(Api.update, { leaseId: nowItem.lease_id, ...data }).then((res) => {
          if (Number(res.data.flag) !== 0) {
            message.error(`更新失败，${res.data.msg}`);
          } else {
            message.success(res.data.msg);
            this.setModalVisible(false);
            this.getTableData(this.state.searchData);
          };
          this.setState({
            modalLoading: false
          });
        });
        break;
    }

  }

  queryBuildingByCommunity(communityId) {
    axios.post(Api.queryBuildingByCommunity, { communityId }).then((res) => {
      this.setState({
        sBuildsByProjCode: res.data
      });
    });
  }

  changeCommunitiesByTrUnit(id) {
    this.queryBuildingByCommunity(id);

    this.setState({
      sBuildsByProjCode: [],// 栋号
      sHousesByBuildingCode: [],// 房号
    });
    let reset = {
      "building_code": "",// 楼栋代号
      "house_code": "",// 户代号
    };;
    if (this.state.modalVisible) {
      reset = {
        "m_building_code": "",// 楼栋代号
        "m_house_code": "",// 户代号
      };
    }
    this.props.form.setFieldsValue(reset);
  }

  queryHouseByBuilding(buildingCode) {
    axios.post(Api.queryHouseByBuilding, { buildingCode }).then((res) => {
      this.setState({
        sHousesByBuildingCode: res.data
      });
    });
  }

  changeBuildsByProjCode(buildingCode) {
    this.queryHouseByBuilding(buildingCode);

    this.setState({
      sHousesByBuildingCode: [],// 房号
    });
    let reset = {
      "house_code": "",// 户代号
    };;
    if (this.state.modalVisible) {
      reset = {
        "m_house_code": "",// 户代号
      };
    }
    this.props.form.setFieldsValue(reset);
  }

  getAllHousesByBuildingCode(building_code) {
    axios.post(Api.getAllHousesByBuildingCode, { building_code }).then((res) => {
      this.setState({
        sHousesByBuildingCode: res.data.data
      });
    });
  }

  getAllStatisticType() {
    axios.get(Api.getAllStatisticType).then((res) => {
      this.setState({
        statisticType: res.data.data
      });
    });
  }

  deleteFeeMaintain(item) {
    Modal.warning({
      title: '确认删除本条数据？',
      content: [<p>注意，删除后不可恢复。</p>, <p>esc关闭此窗口。</p>],
      okText: '确认删除',
      onOk: () => {
        axios.post(Api.del, { leaseId: item.lease_id }).then((res) => {
          if (Number(res.data.flag) !== 0) {
            message.error(res.data.msg);
          } else {
            message.success(res.data.msg);
            this.getTableData(this.state.searchData);
          };
        });
      }
    });
  }

  changeCheck(values) {
    const checkData = {
      allIdle: 0,// 是否整座空余
      storeCamp: 0,// 是否储备房区
      frontageHouse: 0,// 是否临街房屋
      showLessee: 0,// 公示承租人
      showRent: 0,// 公示年租金
      haveChecked: 0, // 是否通过复检
      haveApprove: 0, // 是否通过核查验收
    };
    for (let i = 0, len = values.length; i < len; i++) {
      switch (values[i]) {
        case '是否整座空余':
          checkData.allIdle = 1;
          break;
        case '是否储备房区':
          checkData.storeCamp = 1;
          break;
        case '是否临街房屋':
          checkData.frontageHouse = 1;
          break;
        case '公示承租人':
          checkData.showLessee = 1;
          break;
        case '公示年租金':
          checkData.showRent = 1;
          break;
        case '是否通过复检':
          checkData.haveChecked = 1;
          break;
        case '是否通过核查验收':
          checkData.haveApprove = 1;
          break;
      };
    };
    this.setState({
      checkData,
      defCheck: values
    });
  }

  formatCheck(data) {
    const defCheck = [];
    Number(data.allIdle) === 1 ? defCheck.push('是否整座空余') : void (0);
    Number(data.storeCamp) === 1 ? defCheck.push('是否储备房区') : void (0);
    Number(data.frontageHouse) === 1 ? defCheck.push('是否临街房屋') : void (0);
    Number(data.showLessee) === 1 ? defCheck.push('公示承租人') : void (0);
    Number(data.showRent) === 1 ? defCheck.push('公示年租金') : void (0);
    Number(data.haveChecked) === 1 ? defCheck.push('是否通过复检') : void (0);
    Number(data.haveApprove) === 1 ? defCheck.push('是否通过核查验收') : void (0);
    return defCheck;
  }

  render() {
    const that = this;
    const { getFieldDecorator } = this.props.form;
    const {
      currentUserLoading,
    } = this.props;
    const columns = [
      {
        title: '出租项目详细地址',
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: '公示承租人',
        dataIndex: 'show_lessee',
        key: 'show_lessee',
        render: (text) => (Number(text) === 0 ? '否' : '是')
      },
      {
        title: '出租单位',
        dataIndex: 'rent_org',
        key: 'rent_org',
      },
      {
        title: '租赁用途明细',
        dataIndex: 'lease_use',
        key: 'lease_use',
      },
      {
        title: '总额',
        dataIndex: 'rent_all',
        key: 'rent_all',
      },
      {
        title: '租赁许可年限-开始日期',
        dataIndex: 'allow_start_date',
        key: 'allow_start_date'
      },
      {
        title: '租赁许可年限-结束日期',
        dataIndex: 'allow_end_date',
        key: 'allow_end_date'
      },
      {
        title: '租赁面积 房屋',
        dataIndex: 'lease_harea',
        key: 'lease_harea',
      },
      {
        title: '租金单价',
        dataIndex: 'houseleasedprice',
        key: 'houseleasedprice',
      },
      {
        title: '操作',
        key: 'action',
        fixed: 'right',
        render: (item, record) => (
          <span>
            <Link
              target='_blank'
              to={{
              pathname: '/fe/dataMaintenance/rentDetails',
              search: `?leaseId=${item.lease_id}`
            }}
            >
              <Button type='primary'>详情</Button>
            </Link>
            &emsp;
            <Button type='primary' onClick={this.setModalVisible.bind(this, true, 'updata', item)}>编辑</Button>
            &emsp;
            <Button type='primary' onClick={this.deleteFeeMaintain.bind(this, item)}>删除</Button>
          </span>
        ),
      },
    ];
    const checkboxOptions = ['是否整座空余', '是否储备房区', '是否临街房屋', '公示承租人', '公示年租金', '是否通过复检', '是否通过核查验收'];
    
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div>点击上传文件</div>
      </div>
    );

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>租赁维护</p>
              <p>RENT MAINTENANCE</p>
            </div>
          </div>
          <div className="pom">
            <div className='mrb15'>
              <Form layout='inline' align='left' onSubmit={this.query.bind(this)}>
                <FormItem label="划转单位：">
                  {
                    getFieldDecorator('orgName')(
                      <Select
                        placeholder="请选择划转单位"
                      >
                        {this.state.sTransferUnit.map((item, index) => (
                          <Option value={index} key={index}>{item}</Option>
                        ))}
                      </Select>
                    )
                  }
                </FormItem>
                <FormItem label="详细地址：">
                  {getFieldDecorator('address')(<Input placeholder="请输入详细地址" />)}
                </FormItem>
                <FormItem label="承租方：">
                  {getFieldDecorator('partyB')(<Input placeholder="请输入承租方" />)}
                </FormItem>
                <FormItem
                  label='租赁年限：'
                  className={styles.rangePicker}
                >
                  {
                    getFieldDecorator('leaseDate')(
                      <RangePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                        onChange={this.changeDate.bind(this, 'leaseDate')}
                      />)
                  }
                </FormItem>

                <Row gutter={24} style={{ textAlign: 'center' }}>
                  <Button icon='search' type='primary' htmlType='submit'>搜索</Button>
                  &emsp;
                  <Button icon="plus-circle" type="primary" htmlType='button' onClick={this.setModalVisible.bind(this, true, 'new')}>新建</Button>
                  &emsp;
                  <Button icon="reload" type="primary" htmlType='button' onClick={this.resetForm.bind(this)}>重置</Button>
                </Row>
              </Form>
            </div>
            <Spin spinning={this.state.loading}>
              <Table
                bordered
                scroll={{ x: true }}
                columns={columns}
                dataSource={that.state.tableData}
                pagination={{
                  current: that.state.current,
                  onChange: this.pageChange.bind(this),
                  total: that.state.totalSize,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Spin>
          </div>
          <Modal
            title={this.state.modalTitle}
            destroyOnClose
            centered
            visible={this.state.modalVisible}
            width={830}
            className='modalAlign'
            onCancel={this.setModalVisible.bind(this, false)}
            footer={[
              <Button key="back" onClick={this.setModalVisible.bind(this, false)}>取消</Button>,
              <Button key="submit" type="primary" loading={this.state.modalLoading} onClick={this.modalSubmit.bind(this)}>
                提交
              </Button>,
            ]}
          >
            <Form layout='inline' align='left' onSubmit={this.query.bind(this)}>
              <Spin spinning={this.state.modalSpin}>
                <Row gutter={24}>
                  <Col span={8}>
                    <FormItem label="划转单位：">
                      {
                        getFieldDecorator('m_orgName', {
                          rules: [{ required: true, message: '请选择划转单位!' }],
                          initialValue: this.state.modalType === 'updata' ? this.state.editData.orgName : ''
                        })(
                          <Select
                            placeholder="请选择划转单位"
                          >
                            {this.state.sTransferUnit.map((item, index) => (
                              <Option value={item} key={index}>{item}</Option>
                            ))}
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='小区信息：'>
                      {
                        getFieldDecorator('m_community_name', {
                          rules: [{ required: true, message: '请选择小区信息!' }],
                          initialValue: this.state.modalType === 'updata' ? this.state.editData.communityName : ''
                        })(
                          <Select
                            placeholder="请选择小区信息"
                            onChange={this.changeCommunitiesByTrUnit.bind(this)}
                          >
                            {this.state.sCommunitiesByTrUnit.map((item, index) => (
                              <Option value={item.community_id} key={index}>{item.community_name}</Option>
                            ))}
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='楼栋名称：'>
                      {
                        getFieldDecorator('m_building_code', {
                          rules: [{ required: true, message: '请选择楼栋名称!' }],
                          initialValue: this.state.modalType === 'updata' ? this.state.editData.buildingName : ''
                        })(
                          <Select
                            placeholder="请选择楼栋名称"
                            onChange={this.changeBuildsByProjCode.bind(this)}
                          >
                            {this.state.sBuildsByProjCode.map((item, index) => (
                              <Option value={item.building_code} key={index}>{item.building_name2}</Option>
                            ))}
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='户名：'>
                      {
                        getFieldDecorator('m_house_code', {
                          rules: [{ required: true, message: '请选择户名!' }],
                          initialValue: this.state.modalType === 'updata' ? this.state.editData.houseName : ''
                        })(
                          <Select
                            placeholder="请选择户名"
                          >
                            {this.state.sHousesByBuildingCode.map((item, index) => (
                              <Option value={item.house_code} key={index}>{item.house_name2}</Option>
                            ))}
                          </Select>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='出租单位：'>
                      {getFieldDecorator('m_rentOrg', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.rentOrg : ''
                      })(<Input placeholder="请输入出租单位" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='甲方：'>
                      {getFieldDecorator('m_partyA', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.partyA : ''
                      })(<Input placeholder="请输入甲方" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='乙方：'>
                      {getFieldDecorator('m_partyB', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.partyB : ''
                      })(<Input placeholder="请输入乙方" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='地址：'>
                      {getFieldDecorator('m_address', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.address : ''
                      })(<Input placeholder="请输入地址" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='出租单位性质：'>
                      {getFieldDecorator('m_rentOrgType', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.rentOrgType : ''
                      })(<Select
                        placeholder="请选择出租单位性质"
                      >
                        <Option value='0' key={0}>暂无</Option>
                        <Option value='1' key={1}>政府机关</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='房屋空余情况：'>
                      {getFieldDecorator('m_houseIdle', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.houseIdle : ''
                      })(<Select
                        placeholder="请选择房屋空余情况"
                      >
                        <Option value='0' key={0}>暂无</Option>
                        <Option value='1' key={1}>整体空闲</Option>
                      </Select>)}

                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='房屋竞价招租方式：'>
                      {getFieldDecorator('m_leaseType', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.leaseType : ''
                      })(<Select
                        placeholder="请选择房屋竞价招租方式"
                      >
                        <Option value='0' key={0}>暂无</Option>
                        <Option value='1' key={1}>公告招租</Option>
                      </Select>)}

                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='租赁面积·房屋：'>
                      {getFieldDecorator('m_leaseHarea', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.leaseHarea : ''
                      })(<Input placeholder="请输入租赁面积·房屋" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='租赁面积·场地：'>
                      {getFieldDecorator('m_leaseGarea', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.leaseGarea : ''
                      })(<Input placeholder="请输入租赁面积·场地" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='租赁用途明细：'>
                      {getFieldDecorator('m_leaseUse', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.leaseUse : ''
                      })(<Input placeholder="请输入租赁用途明细" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='总额：'>
                      {getFieldDecorator('m_rentAll', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.rentAll : ''
                      })(<Input placeholder="请输入总额" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='年租金：'>
                      {getFieldDecorator('m_rentCur', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.rentCur : '',
                        rules: [{ required: true, message: '请输入总价!' }, { pattern: /^[0-9]+([.]{1}[0-9]+){0,1}$/, message: '请输入数值！' }],
                      })(<Input placeholder="请输入年租金" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='租金单价：'>
                      {getFieldDecorator('m_houseLeaseDPrice', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.houseLeaseDPrice : ''
                      })(<Input placeholder="请输入租金单价" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='项目批准单位：'>
                      {getFieldDecorator('m_approveOrg', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.approveOrg : ''
                      })(<Input placeholder="请输入项目批准单位" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='项目项目文号：'>
                      {getFieldDecorator('m_approveNumber', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.approveNumber : ''
                      })(<Input placeholder="请输入项目项目文号" />)}
                    </FormItem>
                  </Col>
                  <Col
                    span={8}
                  >
                    <FormItem
                      label='项目日期：'
                    >{getFieldDecorator('m_approveDate', {
                      initialValue: this.state.editData.approveDate ? moment(this.state.editData.approveDate) : '',
                      rules: [{ required: true, message: '请选择项目日期!' }],
                    })(
                      <DatePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                        onChange={this.changeDate.bind(this, 'm_approveDate')}
                      />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='租赁月数：'>
                      {getFieldDecorator('m_leaseMonth', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.leaseMonth : ''
                      })(<Input placeholder="请输入租赁月数" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='发证机关：'>
                      {getFieldDecorator('m_issuingAuthority', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.issuingAuthority : ''
                      })(
                        <Select
                          placeholder="请选择发证机关"
                        >
                          <Option value='政府机关' key={0}>政府机关</Option>
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='项目填表人：'>
                      {getFieldDecorator('m_fillPerson', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.fillPerson : ''
                      })(<Input placeholder="请输入项目填表人" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='项目负责人：'>
                      {getFieldDecorator('m_checkPerson', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.checkPerson : ''
                      })(<Input placeholder="请输入项目负责人" />)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label='房管部门审查人：'>
                      {getFieldDecorator('m_approvePerson', {
                        initialValue: this.state.modalType === 'updata' ? this.state.editData.approvePerson : ''
                      })(<Input placeholder="请输入房管部门审查人" />)}
                    </FormItem>
                  </Col>
                  <Col
                    span={24}
                  >
                    <FormItem
                      label='租赁年限：'
                    >{getFieldDecorator('m_leaseDate', {
                      initialValue: this.state.editData.leaseStartDate && this.state.editData.leaseEndDate ? [moment(this.state.editData.leaseStartDate), moment(this.state.editData.leaseEndDate)] : '',
                      rules: [{ required: true, message: '请选择租赁年限!' }],
                    })(
                      <RangePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                        onChange={this.changeDate.bind(this, 'm_leaseDate')}
                      />)}
                    </FormItem>
                  </Col>
                  <Col
                    span={24}
                  >
                    <FormItem
                      label='租赁许可年限：'
                    >{getFieldDecorator('m_allowDate', {
                      initialValue: this.state.editData.allowStartDate && this.state.editData.allowEndDate ? [moment(this.state.editData.allowStartDate), moment(this.state.editData.allowEndDate)] : '',
                      rules: [{ required: true, message: '请选择租赁许可年限!' }],
                    })(
                      <RangePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment().startOf('month'), moment().endOf('month')] }}
                        onChange={this.changeDate.bind(this, 'm_allowDate')}
                      />)}
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    <FormItem>
                      <CheckboxGroup
                        options={checkboxOptions}
                        value={this.state.defCheck}
                        onChange={this.changeCheck.bind(this)}
                      />
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    <Upload
                      action={Api.hlUploadFile}
                      listType="picture-card"
                      accept='.jpg,.jpeg,.png,.gif'
                      fileList={this.state.fileList}
                      onPreview={this.handlePreview.bind(this)}
                      onChange={this.handleChange.bind(this)}
                      onRemove={this.removePic.bind(this)}
                    >
                      {uploadButton}
                    </Upload>
                    <Modal visible={this.state.previewVisible} footer={null} onCancel={this.handlePreviewCancel.bind(this)}>
                      <img alt="example" style={{ width: '100%' }} src={this.state.previewImage} />
                    </Modal>
                  </Col>
                </Row>
              </Spin>
            </Form>
          </Modal>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(RentalMaintenance);

export default WrappedApp;
