import React, { Component } from 'react';
import { Input, Col, Select, Button, Tooltip, Tag, Pagination, Checkbox, Radio, Icon } from 'antd';

import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import IframeComm from "../../components/IframeComm";
import QueueAnim from 'rc-queue-anim';

import styles from "./style.less";

const InputGroup = Input.Group;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const RadioGroup = Radio.Group;

const noPic = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0NSA3OS4xNjM0OTksIDIwMTgvMDgvMTMtMTY6NDA6MjIgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjkxQkRDRjc5RTdCNjExRTg4NjFCQTZBMDM3QUZDMUNCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjkxQkRDRjdBRTdCNjExRTg4NjFCQTZBMDM3QUZDMUNCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTFCRENGNzdFN0I2MTFFODg2MUJBNkEwMzdBRkMxQ0IiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCRENGNzhFN0I2MTFFODg2MUJBNkEwMzdBRkMxQ0IiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCAC0ALQDAREAAhEBAxEB/8QAeAABAAMBAQEBAAAAAAAAAAAAAAMEBQIGAQgBAQAAAAAAAAAAAAAAAAAAAAAQAAECBgECBQEGBAQHAQAAABEBAgAxQRIiAwQhBVFhgRMjgnGhscHRMpFCoiRishQ08HLSQ3NUFTURAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP0S5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwC35Db9R8vCAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwHO97devbsc0tahULMQGbt75qK+3pVyO/crlC9PsMB2zvPGfiut97wiyB/jAF73w1u+N/UeHUesB9Z3fRt2prZqcq7FRCqon6wFrl8jVx9av2NVWuVG9KqD+UBWXvPCW7FykUn98AXvPCW7FykUn98AXvPCW7FykUn98BbTYx+ldyMVWuajk6hVQGAor3vird8T+o8KesAXvfFW74n9R4U9YAve+Kt3xP6jwp6wHeru3H3bU1t1Ou2KidVT9YC7uc3Xr2bHNLWoVCzRICtp7lxuRtXXra5XO8ehHrAS8zenH0bNq67kx6EHqICPhc1OY3avtWq0FLifuSAtOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcBztaWbERlyqnRFrAec3cblakXZt1q0rNenVfsgJuHuejVYrGOa5VL3/uTpRVWAg43vXPdrYj7Gq59yIqI1Jr1gLfE5PN3PVNWpioipfa1E6QGryW8V5ZvRq9UcjXOBoZwFXfx+1t07Va1lzWqrQ/qqjwMBl8Fmp/KYm1EXX1KKoSXjAa68XtXUN1r4Zz++AnVNP+n2N1IjmNaEtcZJ+UBg8PYzXtVztfuojVDPFYD7qdu3bVtcxrlUo1wRPQoID6m/ku2+02x7nKEtYwKvl0gOtDU/8AqBE6e45EbKqwG1zk/tORjNkzPpAYHD3t0b02ObciIuKdJpAW+b3XVyND9bdatVypkq+CwEXb+fr4zNjXsV94kolAbHE5LOXqfsbrVEutVFWYRF/OAnc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcBxyNK7dW3WiBXoLv8AjwgMDl8XRx3u1e6rtrR0tCdUPisBzrTiJofe53v/APbH7R59ICzo38HTxN2pFV23a0K9UVEI6eiQEvZWod6IqP6N6IUqviiQFnm9r/1O5dtytVERESZgKPN7WzjaXbF2qqlEYgmq+sBR1JqVV9xytQdLUPWA1P8A4SKiqm1VkOidfvgLWjhJxeNuYi3o7qZUgMfgLvTcvssR+y1Q1ZfikBHuc5+967QxxW8J0NZQE3DTc3cqcaza9UkvTpPpdbAOPcvckuTJdilsupWA2+cn9pyMZsmZ9IDz3G269W1H7NabGISxYC6vcuB1/sm9fs/SA7083g7drdacJiK9UQ9P0gNVunWxrm69SNaoUNCIv8IDpzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgPP92//AENv0/5UgJ/b5n/oauv+Ff8AqgIuS3kppcr+Jr1N6F7Wqip1p1WAn7GhXegPRvSVVgNTft1amPfsCN6dVWcBh8ndu7hybdTFt/lb+awEHL4/+n3u0m5WohXzVEWA3eRytWnjLtVEei2hEd+6AauQnJ4mzamu0oqAzEBj9ubsfvc3X+9WOtp1gJ9nD7qhVzG7DNQxfx6wHHHds1crX7nGRjnORpDm/u6eIgONIXunRLkXY4efVYDY7grW8PerkBaEUzVekBkdp0+7yurL2taqqiy8IDbXjaMvgash0TrAef4qf32tAc06esB6RzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAZ/M7Q7kcjZuTZbcA0GSInjARr2Pb1G9V8MZ/1QHx3Ytin51Xwxn/VAXOHwGcXW9qF6uBcvQj8ID5ze3pytjXOVUayiTcR4ygJtXG06WOZq1hvSvVYCvze1s5OxdiKrH9EUBUWArr2FOvyqol0Tr98Be18Rmrju0a29ACq9VVawGevYHdRuIljP+qAL2B3UbiJYz/qgC9gd1G4iWM/6oCfi9oZo2Lsc5drm/sT9qdfVYCTuHA2cpzLVta03eZA6eUBLx+Hq4+t2vWwkFVXq6Amc03Ykis4DO19mTXvTberrXI5GhEP3wGi5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwFXmc/j8ZXNe1XPcFtRfx8ICuzvfHc5UfqVqOHUn+MBo9HI5UbcjgqKizgKnL7nxdL3sVFe/oUavh4rAQN73x3Kt+pzbgVRTKA0WP17WK/WiPa4BUWf6QEHM5vH4xTYhe4BqL1X9ICs3vfHc5UfqVqOHUn+MBd379bND99t+sIpRf3JAV+P3PRydq6manXP8VSnrAWeRt1aWPftCN6VnAUH9749zrdLnIs1VQR/GAscbuPG5KuaiK3Y4YOUEeCwFtzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwHmeY9z+Xuc5St6/cogO+dxF4u5Nd1xajj9vT8oC6zku19mR387lsa4+a/giQFHicTZytqsZ0CFzlgLXM7O/Tqds1uvazq5FmKqkBx2je5u9dIuZt/ll1TqYCDnvc/m7lWj1T0aoSA55OrVqejWPvUJf0AXwgL3HVy9l3lOiOCL5FF/OAr9qQ81qA9F6SpAO6b128x6fy61tRPs6LAT8fsuzZp9x7rXORFY1EM/GAqbeHydW5zEY5zmL0c1FX1SA3+M7Zs46P2a1TY5EuReiqqdJUgJXNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgPMcn/c7f8And+MBc75/vG9P5E/FYD77Ln9lRWp+x1y+aIqp9xgIe3cxnH2OTYh17ERF8hKAvc7ufFXRsZqW92xAeoHrAUe16XbOW1Ubc1hc6nkkBDzP93v/wDI7/MsBfXs27a9dl6IxwVPHrAWuXx2aO2bdbGhqInU9V6p1gMztSHmtQHovSVICPn6na+ZtaqTcrk+x3VIDU4/deIugbcXoiI5OvUdOggM7mc/bt5D36nvZrUIjUVU6J9kBtcNm1OI33UV+xyIrlVeqnr90BO5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcBj7uy8t+7Y9HMDnKqdVqp8ICfuXbeRyeR7mu1ERqNyVSqlfKAs8Liv0cX2djUctQvRSprAUuV2Ryvc7jqiNox35LAQM7JzFXK1qeJMBrcbh6uNqcxjbiC5V6u/SAzuR2bl7N+3Y1WBzlchVSFU+EBqtYqMtVpVEai9ZiA45mh+7j7dbERHPRAqr0ULAUOF2rk6OQmx9jmoi9EVSpT7IC3zu36+UiqLdiftelftSAzHdk5qKqJa5KKi/rAWOL2VWOv3h9v7WNXov2rAajmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwC35Db9R8vCAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gHx+5/j9fCA//2Q==';


class MountMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showList: false,
      resultDatas: [],
      resultDatasCount: 0,
      showFilter: false,
      res: [],
      sendMapData: {
        type: "",
        data: {}
      },
      timer: '',
      showLoading: true,
      searchVal: ''
    };
  }
  searchVal(searchVal) {
    let address = this.state.searchVal;
    if (searchVal) {
      address = searchVal
    };
    axios.get(Api.searchCommunityName, {
      params: {
        address
      }
    }).then((res) => {
      this.setState({
        sendMapData: {
          type: "search",
          data: res
        }
      });
    });
  }

  filter() {
    this.setState({
      showList: false,
      showFilter: !this.state.showFilter
    });
  }
  changeFilter(type, checkedValues) {
    let res = {
      type
    };

    switch (type) {
      case 'qszt':
        this.setState({
          radioValue: checkedValues.target.value
        });
        res.checkedValues = checkedValues.target.value;
        break;
      case 'qsxz':
        this.setState({
          qsValue: checkedValues
        });
        res.checkedValues = checkedValues;
        break;
      case 'zczt':
        this.setState({
          zcValue: checkedValues
        });
        res.checkedValues = checkedValues;
        break;
    };

    this.setState({
      sendMapData: res
    });

  }

  closeInfo() {
    this.setState({
      right: -290
    });
  }
  changeTools(nowType) {
    this.setState({
      nowType
    });
  }
  onReceiveMessage(data) {
    let sendData = {
      projectCode: this.props.location.state.data.projectCode,
      ...data.data
    };
    axios.get(Api.mountCommunity, {
      params: {
        ...sendData
      }
    }).then((res) => {

    });

  }
  clickList() {
    let sendMapData = {};
    sendMapData.type = 'search';
    sendMapData.data = {};
    this.setState({
      sendMapData
    });
  }
  componentWillMount() {
    let sendMapData = {};
    sendMapData.type = 'jump';
    console.log(this.props);

    sendMapData.data = JSON.parse(this.props.location.query.data);
    this.setState({
      sendMapData
    });
  }
  handleSearch(searchContent) {
    if (this.state.timer !== '') {
      clearTimeout(this.state.timer);
    };
    let timer = setTimeout(() => {
      const that = this;
      axios.get(Api.queryAsset, {
        params: {
          searchType: 3,
          searchContent
        }
      }).then((res) => {
        this.setState({
          res: res.data
        });
        if (res.data.length === 0) {
          this.setState({
            showLoading: false
          });
        };
      });
    }, 600);
    this.setState({
      timer
    });
  }
  handleChange(searchVal) {
    this.setState({ searchVal });
    this.searchVal(searchVal);
  }
  clickDraw(type) {
    this.setState({
      sendMapData: {
        type: 'draw',
        event: type
      }
    });
  }
  render() {
    return (
      <div className={styles.mapBox}>
        <div className={styles.searchBox} style={{
          width: 610
        }}>
          <InputGroup compact>
            <Select
              showSearch
              placeholder='请输入内容'
              className={styles.search}
              defaultActiveFirstOption={false}
              showArrow={false}
              filterOption={false}
              onSearch={this.handleSearch.bind(this)}
              onChange={this.handleChange.bind(this)}
              notFoundContent={null}
            >

              {this.state.res.map((item, index) => (
                <Option value={item} key={index}>{item}</Option>
              ))}
            </Select>
            <Tooltip placement="bottom" title='搜索'>
              <Button type="primary" icon="search" onClick={this.searchVal.bind(this)} />
            </Tooltip>
            &nbsp;
            <Tooltip placement="bottom" title='点击选择新的小区地址'>
              <Button type="primary" onClick={this.clickDraw.bind(this, 'draw')}>绘制</Button>
            </Tooltip>
            &nbsp;
            <Tooltip placement="bottom" title='保存选择的小区地址'>
              <Button type="primary" onClick={this.clickDraw.bind(this, 'save')}>保存绘制</Button>
            </Tooltip>
            &nbsp;
            <Tooltip placement="bottom" title='清除选择的小区地址'>
              <Button type="primary" onClick={this.clickDraw.bind(this, 'clear')}>清除绘制</Button>
            </Tooltip>

          </InputGroup>
        </div>
        {Number(sessionStorage.getItem('isMap')) === 1 ?
          <div>
            <IframeComm
              attributes={{
                src: "/gis/mapzcgj.html",
                style: {
                  width: '100%',
                  height: '100%',
                  top: '0px',
                  left: '0px',
                  position: "absolute"
                }
              }}
              postMessageData={this.state.sendMapData}
              handleReceiveMessage={this.onReceiveMessage.bind(this)}
            />
          </div>
          :
          null
        }
      </div>
    );
  }
}

export default MountMap;
