import React, { PureComponent } from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Icon,
  Input,
  Button,
  Form,
  Tooltip,
  Select,
  Upload,
  Pagination,
} from 'antd';
import axios from 'axios';
import { formatTreeData } from '@/components/commonAips';
import Api from '../../services/apis';
import styles from './style.less';
import IframeComm from '../../components/IframeComm';
import ico_map from "../../assets/ico_map.png";
import ico_nodata from "../../assets/ico_nodata.png";

const { TextArea } = Input;
// const {confirm} = Modal;
// const {TabPane} = Tabs;
// const { RangePicker } = DatePicker;
// const {Step} = Steps;
const { Option } = Select;
const FormItem = Form.Item;

class RoomInfo2 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      clientVisible: false,
      hetongModalVisible: false,
      mapModal: false,
      // data: {},
      orgTreeData: [],
      fileIds: [],
      fileList: [],
      houseList: [],
      renterList: [],
      mapHouse: [],
      sendMapData: {},
      // loading: false,
      houseInfo: {},

      current: 1,
      renterPageSize: 2,
      total: 0,
      houseRenterData: [],
      showHouseRenterData: [],
    };
  }

  componentDidMount() {
    // if (this.props.data !== undefined) {
    //   this.setState({
    //     data: this.props.data,
    //   });
    // }

    axios.get(Api.getIndustryTree, {}).then((resp) => {
      const { data } = resp;
      if (data) {
        const treeData = new Array(data)[0];
        formatTreeData(treeData, 'name', 'orgLevelFormat', 'industryId', 'industryList');
        this.setState({ orgTreeData: treeData });
      }
    });

    axios.post(Api.queryRenter, {}).then((resp) => {
      const { data } = resp;
      this.setState({
        renterList: data
      })
    });
  }
  componentWillMount() {
    if (this.props.data !== undefined) {
      this.setState({
        data: this.props.data
      });
      this.getHouseInfo(this.props.data.house_id);
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== undefined) {
      const { data } = this.props;
      // 只有在更改了type floor_id house_id build_id 之后才能获取 节约性能
      if (nextProps.data.type !== data.type || nextProps.data.floor_id !== data.floor_id || nextProps.data.house_id !== data.house_id || nextProps.data.build_id !== data.build_id) {
        this.getHouseInfo(nextProps.data.house_id);
        this.getHouseRenter(nextProps.data.house_id);
      }
    }
  }

  getHouseRenter = (houseId) => {
    if (!houseId) return;
    axios.post(Api.getHouseRenter, {
      houseCode: houseId
    }).then((res) => {
      const total = res.data.result.length;
      const houseRenterData = res.data.result;
      this.setState({ total, houseRenterData, current: 1 }, () => {
        this.pageOnChange(1);
      });
    }).catch(() => {
      notification.destroy();
      notification.warning({ message: '获取房间租客失败！' });
    });
  }

  pageOnChange = (page) => {
    const { renterPageSize, total, houseRenterData } = this.state;
    const start = (page - 1) * renterPageSize > 0 ? (page - 1) * renterPageSize : 0;
    const end = page * renterPageSize >= total ? total : page * renterPageSize;
    const showHouseRenterData = houseRenterData.slice(start, end);
    console.log(showHouseRenterData)
    this.setState({
      current: page,
      showHouseRenterData,
    });
  }

  getHouseInfo = (houseId) => {
    if (!houseId) return;
    axios.get(Api.getHouseMessage, {
      params: {
        houseCode: houseId
      }
    }).then((res) => {
      this.setState({ houseInfo: res.data })
    }).catch(() => {
      notification.destroy();
      notification.warning({ message: '获取房间信息失败！' });
    });
  }

  getHouseByParams = (buildingId) => {
    if (buildingId) {
      axios
        .get(Api.queryHouseByParams, {
          params: {
            buildingId,
          },
        })
        .then(res => {
          if (res.data === null) {
            notification.destroy();
            notification.warning({ message: '暂无数据！' });
          } else {
            const data = res.data.houses && res.data.houses.map(item => ({ houseCode: item.houseCode, isRent: item.isRent }))
            this.setState({
              sendMapData: {
                type: 'clickedLD',
                data,
                buildingId,
                floors: res.data.floors
              }
            });
          }
        });
    }
  }

  onReceiveMessage = (data) => {
    if (data.data.type === 'clickedLD') {
      this.getHouseByParams(data.data.build_id);
    }

    let flag = false;
    if (data.data.type === 'clickedLDR') {
      const info = data.data.attribute;
      this.state.houseList && this.state.houseList.map(item => {
        if (item.houseCode === data.data.house_id) {
          flag = true;
        }
      })
      if (flag) {
        notification.destroy();
        notification.warning({ message: '该房间已选择，请重新选择！' });
        this.setState({ mapHouse: null });
        return;
      }
      const par = {
        houseCode: data.data.house_id,
        houseName2: info.house_name2,
        buildingName2: info.building_name2,
        useArea: 1,// info.use_area,
        rentPrice: 20,// info.itemprerent_price,
        buildingArea: info.building_area,
        renovationCondition: info.renovation_condition,
      }
      this.setState({ mapHouse: par }, () => {
        notification.destroy();
        notification.success({ message: `已选择房间${par.houseName2}` });
      });
    }
  }

  handleChange = (info) => {
    this.setState({ fileList: info.fileList })
    if (info.file.status === 'done') {
      notification.destroy();
      notification.success({ message: '上传成功！' });

      const { fileIds } = this.state;
      fileIds.push(info.file.response);
      this.setState({ fileIds });

    } else if (info.file.status === 'error') {
      notification.destroy();
      notification.error({ message: '上传失败！' });
    }
  }

  // 删除选择的房屋
  delHouse = (index) => {
    const { houseList } = this.state;
    houseList.splice(index, 1);
    this.setState({ houseList }, () => {
      this.computRent();
    });
  }

  setHouseToModal = () => {
    const { houseList, mapHouse } = this.state;
    console.log(mapHouse, houseList)
    if (mapHouse && houseList) {
      houseList.push(mapHouse);
      this.setState({ houseList }, () => {
        this.computRent();
      });
    }
    this.setModalVisible(false, 'mapModal');
  }

  handleRemove = (info) => {
    const p = new Promise((resp, rej) => {
      axios.get(Api.renterdeleteFile, {
        params: {
          fileId: info.id
        }
      }).then((res) => {
        if (Number(res.data.code) === 1 || Number(info.id) === 0) {
          notification.destroy();
          notification.success({ message: res.data.msg });
          const { fileIds } = this.state;
          const index = fileIds.indexOf(info.id);
          fileIds.splice(index, 1);
          this.setState({ fileIds });
          resp('success');
        } else {
          notification.destroy();
          notification.error({ message: res.data.msg });
          rej('error');
        };
      });
    });
    return p;
  }

  handleMenuClick = url => {
    const win = window.open(url, '_blank');
    win.focus();
  };

  setModalVisible = (visible, name) => {
    const { houseInfo } = this.state;
    console.log('rrrrrrrrr', houseInfo)
    if (visible && name === 'hetongModalVisible') {
      this.setState({
        houseList: [{
          houseName2: houseInfo.houseName2,
          houseCode: houseInfo.houseCode,
          buildingName2: houseInfo.buildingName2,
          buildingArea: houseInfo.buildingArea,
          rentPrice: houseInfo.prerent_price,
          renovationCondition: houseInfo.renovationCondition,
        }]
      });
    }
    if (!visible && name === 'hetongModalVisible') this.setState({ houseList: [] });
    this.setState({ [name]: visible });
    if (!visible) {
      const { form } = this.props;
      form.setFieldsValue({
        // 企业
        client_renterName: '',
        client_linkman: '',
        client_cardNum: '',
        client_hyType: '',
        client_contactWay: '',
        client_mailbox: '',
        client_mainBusiness: '',
        client_investmentAmount: '',
        client_peopleNum: '',
        client_renterSituation: '',
        client_high: '',
        client_establishedTime: '',
        client_fullIncome: '',
        client_income: '',
        client_zcType: '',
        client_registrationAuthority: '',
        client_registeredCapital: '',
        client_registeredAddress: '',
        client_intro: '',
        client_openingBank: '',
        client_accountNumber: '',
        client_phoneNumber: '',
        client_i_nsrsbh: '',
        client_address: '',
        client_receiveDate: '',
        client_shxydm: '',
        client_b_nsrsbh: '',
        client_registrationNumber: '',
        client_orgNum: '',
        client_updateTime: '',
        client_bachelorNum: '',
        client_masterNumber: '',
        client_doctorNumber: '',
        client_abroadNumber: '',
        client_personnelNumber: '',
        client_enterpriseType: '',

        // 合同
        hetong_sPerson: '',
        hetong_sDate: '',
        hetong_c_person: '',
        hetong_rentName: '',
        hetong_cardNumber: '',
        hetong_person: '',
        hetong_industry: '',
        hetong_contact: '',
        hetong_mail: '',
        hetong_mainContent: '',
        hetong_investNumber: '',
        hetong_personSize: '',
        hetong_renterStatus: '',
        hetong_hConpermany: '',
        hetong_foundDate: '',
        hetong_rentIncome: '',
        hetong_income: '',
        hetong_rType: '',
        hetong_rAuthority: '',
        hetong_zMoney: '',
        hetong_zAddress: '',
        hetong_company: ''
      });
    }
  };

  // 计算单价、总价并赋值
  computRent = () => {
    let dj = 0; let zj = 0;
    const { houseList } = this.state;
    const { form } = this.props;
    houseList.forEach(item => {
      if (item.rentPrice) {
        dj += Number(item.rentPrice);
        if (item.useArea) {
          zj += Number(item.useArea) * Number(item.rentPrice);
        }
      }
    })
    dj = dj ? dj / houseList.length : '';
    form.setFieldsValue({
      hetong_rentMoney: dj,
      hetong_rentTotalMoney: zj || ''
    })
  }

  editProject = () => {
    const { fileList } = this.state;
    console.log(fileList)
    const { form, data } = this.props;

    // this.setState({ loading: true })
    const { fileIds } = this.state;
    fileList.forEach(item => {
      if (item.id) {
        fileIds.push(item.id)
      }
    });
    let fieldNames = ['client_renterName', 'client_linkman', 'client_cardNum', 'client_hyType', 'client_contactWay',
      'client_mailbox', 'client_mainBusiness', 'client_investmentAmount', 'client_peopleNum', 'client_renterSituation',
      'client_high', 'client_establishedTime', 'client_fullIncome', 'client_income', 'client_zcType', 'client_registrationAuthority',
      'client_registeredCapital', 'client_registeredAddress', 'client_intro', 'client_openingBank', 'client_accountNumber',
      'client_phoneNumber', 'client_i_nsrsbh', 'client_address', 'client_receiveDate', 'client_shxydm', 'client_b_nsrsbh',
      'client_registrationNumber', 'client_orgNum', 'client_updateTime', 'client_bachelorNum', 'client_masterNumber',
      'client_doctorNumber', 'client_abroadNumber', 'client_personnelNumber', 'client_enterpriseType'];
    form.validateFields(fieldNames, (err, values) => {
      if (!err) {
        axios.post(Api.createRenter, {
          houseCode: data.house_id,
          renterName: values.client_renterName,
          linkman: values.client_linkman,
          cardNum: values.client_cardNum,
          hyType: values.client_hyType,
          r_contactWay: values.client_contactWay,
          mailbox: values.client_mailbox,
          mainBusiness: values.client_mainBusiness,
          investmentAmount: values.client_investmentAmount,
          peopleNum: values.client_peopleNum,
          renterSituation: values.client_renterSituation,
          high: values.client_high,
          establishedTime: values.client_establishedTime,
          fullIncome: values.client_fullIncome,
          income: values.client_income,
          zcType: values.client_zcType,
          registrationAuthority: values.client_registrationAuthority,
          registeredCapital: values.client_registeredCapital,
          registeredAddress: values.client_registeredAddress,
          intro: values.client_intro,

          openingBank: values.client_openingBank,
          accountNumber: values.client_accountNumber,
          i_contactWay: values.client_phoneNumber,
          i_nsrsbh: values.client_i_nsrsbh,
          address: values.client_address,
          receiveDate: values.client_receiveDate,
          shxydm: values.client_shxydm,
          b_nsrsbh: values.client_b_nsrsbh,
          registrationNumber: values.client_registrationNumber,
          orgNum: values.client_orgNum,
          updateTime: values.client_updateTime,
          bachelorNum: values.client_bachelorNum,
          masterNumber: values.client_masterNumber,
          doctorNumber: values.client_doctorNumber,
          abroadNumber: values.client_abroadNumber,
          personnelNumber: values.client_personnelNumber,
          enterpriseType: values.client_enterpriseType,
          fileIds: fileIds.join(','),
        })
          .then((resp) => {
            if (resp.data.flag === '1') {
              notification.destroy();
              notification.success({ message: resp.data.msg });
              this.setModalVisible(false, 'clientVisible');
            } else if (resp.data.flag === '0') {
              notification.destroy();
              notification.error({ message: resp.data.msg });
            }
            this.getHouseInfo();
            this.getHouseRenter();
            // this.setState({ loading: false })
          })
      }
    });
  };

  editHeTong = () => {
    const { houseList } = this.state;
    const { form } = this.props;
    // this.setState({ loading: true })
    const houseCodes = []
    for (let i = 0, j = houseList.length; i < j; i++) {
      if (houseList[i].houseCode) {
        houseCodes.push(houseList[i].houseCode)
      }
    }
    // const houseCodes = houseList.map(item => item.houseCode);
    let fieldNames = ['hetong_rentName'];
    console.log(houseCodes)
    form.validateFields(fieldNames, (err, values) => {
      if (!err) {
        axios.post(Api.contractadd, {
          houseCodeList: houseCodes.join(','),
          // update_user: values.hetong_c_person,

          renterId: values.hetong_rentName,
          // linkman: values.hetong_person,
          // card_num: values.hetong_cardNumber,
          // hy_type: values.hetong_industry,
          // contact_way: values.hetong_contact,
          // mailbox: values.hetong_mail,
          // main_business: values.hetong_mainContent,
          // investment_amount: values.hetong_investNumber,
          // people_num: values.hetong_personSize,
          // renter_situation: values.hetong_renterStatus,
          // high: values.hetong_hConpermany,
          // established_time: values.hetong_foundDate,
          // full_income: values.hetong_rentIncome,
          // zc_type: values.hetong_income,
          // income: values.hetong_rType,
          // rAuthority: values.hetong_rAuthority,
          // intro: values.hetong_company,

          // lessee: values.hetong_leaseholder,
          // rentArea: values.hetong_zlArea,
          // mapArea: values.hetong_chArea,
          // useArea: values.hetong_useArea,
          // heatArea: values.hetong_gnArea,
          // buildArea: values.hetong_jzArea,
          // rentPrice: values.hetong_rentMoney,
          // rentTotal: values.hetong_rentTotalMoney,
          // expectedBond: values.hetong_bzPrice,
          // actualBond: values.hetong_realPrice,
          // startDate: values.hetong_s_fontDate,
          // endDate: values.hetong_s_eDate,
          // acceptDate: values.hetong_sDate,
          // acceptUser: values.hetong_sPerson,
          // totalMoney: values.hetong_allMoney,
        }).then((resp) => {
          const { data } = resp;
          if (data.code === '1') {
            notification.destroy();
            notification.success({ message: data.msg });
            this.setModalVisible(false, 'hetongModalVisible')
          } else if (data.code === '0') {
            notification.destroy();
            notification.error({ message: data.msg });
          }
          this.getHouseInfo();
          this.getHouseRenter();
          // this.setState({ loading: false })
        })
      }
    })
  }

  renterChange = (value, option) => {
    // 获取房屋信息
    axios.post(Api.queryRenterInfo, {
      renterId: value
    })
      .then((resp) => {
        const { data } = resp;
        this.props.form.setFieldsValue({
          hetong_person: data.linkman ? data.linkman : '-',
          hetong_cardNumber: data.card_num ? data.card_num : '-',
          hetong_industry: data.hy_type_str ? data.hy_type_str : '-',
          hetong_contact: data.contact_way ? data.contact_way : '-',
          hetong_mail: data.mailbox ? data.mailbox : '-',
          hetong_mainContent: data.main_business ? data.main_business : '-',
          hetong_investNumber: data.investment_amount ? data.investment_amount : '-',
          hetong_personSize: data.people_num ? data.people_num : '-',
          hetong_renterStatus: data.renter_situation ? data.renter_situation : '-',
          hetong_hConpermany: data.high ? data.high : '-',
          hetong_foundDate: data.established_time ? data.established_time.substr(0, 10) : '-',
          hetong_rentIncome: data.full_income ? data.full_income : '-',
          hetong_rType: data.zc_type ? data.zc_type : '-',
          hetong_income: data.income ? data.income : '-',
          hetong_company: data.intro ? data.intro : '-'
        });
      })
  }



  render() {
    const { form: { getFieldDecorator }, data } = this.props;

    const {
      orgTreeData,
      fileList,
      sendMapData,
      houseList,
      renterList,
      houseInfo,
      clientVisible,
      mapModal,
      hetongModalVisible,
      // 分页相关
      current,
      renterPageSize,
      total,
      showHouseRenterData,
    } = this.state;

    return (
      <div className={styles.roomInfo}>
        <div className={styles.filterItem} style={{ fontSize: '14px', paddingTop: '20px' }}>
          <b style={{ fontWeight: 'bold' }}>{houseInfo.houseName2 ? houseInfo.houseName2 : '-'}</b> &emsp;{houseInfo.buildingArea ? houseInfo.buildingArea : 0}/㎡ &emsp; 租金：{houseInfo.prerent_price ? houseInfo.prerent_price : 0}元/㎡日
          <a className={styles.option} href="javascrip:;" onClick={() => this.setModalVisible(true, 'hetongModalVisible')}>新增租赁协议</a>
        </div>
        <div className={styles.filterItem}>
          <div className={styles.filterHeader}>
            摘要
            <a className={styles.option} href="javascrip:;" onClick={this.handleMenuClick.bind(this, `/fe/assetMap/houseDetails?houseCode=${data.house_id}`)}>
              详情
            </a>
          </div>
          <div className={styles.filterInner}>
            <span>租赁状况：{houseInfo.isRent ? houseInfo.isRent : '-'}</span>
            <span>装修状况：{houseInfo.renovationCondition ? houseInfo.renovationCondition : '-'}</span>
          </div>
        </div>
        <div className={styles.filterItem}>
          <div className={styles.filterHeader}>
            意向企业
            <a className={styles.option} href="javascrip:;" onClick={() => this.setModalVisible(true, 'clientVisible')}>
              新增
            </a>
          </div>
          {
            showHouseRenterData.map(renter => (
              <div key={renter.id} className={styles.filterInner}>
                <span title={renter.renterName}>企业名称：{renter.renterName || '-'}</span>
                <span title={renter.linkman}>联系人：{renter.linkman || '-'}</span>
                <span title={renter.contactWay}>联系方式：{renter.contactWay || '-'}</span>
                <span title={renter.industryType}>行业分类：{renter.industryType || '-'}</span>
              </div>
            ))
          }
          {total > renterPageSize ? <Pagination style={{ textAlign: 'right' }} total={total} current={current} pageSize={renterPageSize} onChange={this.pageOnChange} /> : null}
        </div>
        {/* <div className={styles.filterItem}>
          <div className={styles.filterHeader}>
            租赁协议
            <a className={styles.option} href="javascrip:;" onClick={() => this.setModalVisible(true, 'hetongModalVisible')}>
              新增
            </a>
          </div>
          <div className={styles.filterInner}>
            <span title='哈尔滨市大地勘察测绘有限公司'>承租方：{data.projectCount}哈尔滨市大地勘察测绘有限公司</span>
            <span title='170㎡'>租赁面积：{data.projectCount}170㎡</span>
            <span title='2019年2月21日'>开始日期：{data.projectCount}2019年2月21日</span>
            <span title='2021年1月21日'>结束日期：{data.projectCount}2021年1月21日</span>
            <span title='22222元'>合同金额：{data.projectCount}22222元</span>
            <span title='3.0/元㎡日'>租金单价：{data.projectCount}3.0/元㎡日</span>
          </div>
        </div> */}

        <Modal
          title='新建意向企业'
          wrapClassName="vertical-center-modal"
          className="modalAlign"
          visible={clientVisible}
          onOk={this.editProject}
          onCancel={() => this.setModalVisible(false, 'clientVisible')}
          width={830}
        >
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>
            企业基本信息
          </Row>
          <Row style={{ textAlign: 'center' }}>
            <span
              style={{
                height: '1px',
                width: '124px',
                textAlign: 'center',
                borderBottom: '1px solid #E9E9E9',
                display: 'inline-block',
              }}
            />
          </Row>
          <Row gutter={24}>
            <Col span={8}>
              <FormItem label="企业名称：">
                {getFieldDecorator('client_renterName', {
                  rules: [{ required: true, message: '请输入企业名称！' }],
                })(<Input placeholder="请输入企业名称" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="法人：">
                {getFieldDecorator('client_linkman', {
                  rules: [
                    { required: true, whitespace: true, message: '请输入法人！' },
                    { max: 50, message: '文字内容超过50字' }
                  ]
                })(<Input placeholder="请输入法人" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="证件号码：">
                {getFieldDecorator('client_cardNum', {
                  rules: [
                    { pattern: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: '请按照规则输入！' },
                  ]
                })(<Input placeholder="请输入证件号码" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="行业分类：">
                {getFieldDecorator('client_hyType', {
                  rules: [{ required: true, whitespace: true, message: '请输入行业分类！' }]
                })(
                  <TreeSelect
                    treeData={this.state.orgTreeData}
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto', maxWidth: 300, top: 80 }}
                    allowClear
                    showSearch
                    treeDefaultExpandedKeys={['1']}
                    filterTreeNode={(inputValue, option) => {
                      if (option.props.name.indexOf(inputValue) > -1) {
                        return true;
                      }
                    }}
                  />
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="联系方式：">
                {getFieldDecorator('client_contactWay', {
                  rules: [
                    { pattern: /^1[3456789]\d{9}$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入联系方式" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="邮箱：">
                {getFieldDecorator('client_mailbox', {
                  rules: [
                    { pattern: /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/, message: '请输入正确的邮箱格式！' },
                    { max: 50, message: '文字内容超过50字' }]
                })(<Input placeholder="请输入邮箱" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="主营业务：">
                {getFieldDecorator('client_mainBusiness', {
                  rules: [
                    { max: 200, message: '文字内容超过200字' }
                  ]
                })(<Input placeholder="请输入主营业务" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="投资额：">
                {getFieldDecorator('client_investmentAmount', {
                  rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                })(<Input placeholder="请输入投资额" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="人员规模：">
                {getFieldDecorator('client_peopleNum')(
                  <Select placeholder="请选择人员规模" style={{ width: '100%' }}>
                    <Option value="100人以下">100人以下</Option>
                    <Option value="100-400人">100-400人</Option>
                    <Option value="400人以上">400人以上</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="企业情况：">
                {getFieldDecorator('client_renterSituation')(
                  <Select placeholder="请选择企业情况" style={{ width: '100%' }}>
                    <Option value="上市">上市</Option>
                    <Option value="三板">三板</Option>
                    <Option value="区域挂牌">区域挂牌</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="高企：">
                {getFieldDecorator('client_high')(
                  <Select placeholder="请选择是否高企" style={{ width: '100%' }}>
                    <Option value="是">是</Option>
                    <Option value="否">否</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="成立时间：">
                {getFieldDecorator('client_establishedTime')(<DatePicker style={{ width: '100%' }} />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="当年全口径税收：">
                {getFieldDecorator('client_fullIncome', {
                  rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                })(<Input placeholder="请输入土地面积" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="当年收入：">
                {getFieldDecorator('client_income', {
                  rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                })(<Input placeholder="请输入当年收入" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="注册类型：">
                {getFieldDecorator('client_zcType', {
                  rules: [{
                    max: 50, message: '输入内容超过50字'
                  }]
                })(<Input placeholder="请输入注册类型" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="登记机关：">
                {getFieldDecorator('client_registrationAuthority', {
                  rules: [{
                    max: 50, message: '输入内容超过50字'
                  }]
                })(<Input placeholder="请输入登记机关" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="注册资金：">
                {getFieldDecorator('client_registeredCapital', {
                  rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                })(<Input placeholder="请输入注册资金" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="注册地址：">
                {getFieldDecorator('client_registeredAddress', {
                  rules: [{
                    max: 200, message: '输入内容超过200字'
                  }]
                })(<Input placeholder="请输入注册地址" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="企业类型：">
                {getFieldDecorator('client_enterpriseType', {
                  initialValue: '意向企业'
                })(
                  <Select placeholder="请选择企业类型"
                    style={{ width: "100%" }}>
                    <Option value='意向企业'>意向企业</Option>
                    <Option value='在租企业'>在租企业</Option>
                    <Option value='潜在企业'>潜在企业</Option>
                    <Option value='流失企业'>流失企业</Option>
                  </Select>)}
              </FormItem>
            </Col>
            <Col span={16}>
              <FormItem label="附件上传：">
                {getFieldDecorator('client_fujian')(
                  <Upload
                    action={Api.renteruploadFile}
                    fileList={fileList}
                    onChange={this.handleChange}
                    onRemove={this.handleRemove}
                    multiple
                    data={{ renterId: 0 }}
                  >
                    <Button>
                      <Icon type="upload" /> 上传文件
                    </Button>
                    <p className="ant-upload-text">支持扩展名：.rar .zip .doc .docx .pdf .jpg...</p>
                  </Upload>)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="企业简介：">
                {getFieldDecorator('client_intro')(<TextArea rows={4} placeholder="请输入企业简介" />)}
              </FormItem>
            </Col>
          </Row>
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>
            开票信息
          </Row>
          <Row style={{ textAlign: 'center' }}>
            <span
              style={{
                height: '1px',
                width: '124px',
                textAlign: 'center',
                borderBottom: '1px solid #E9E9E9',
                display: 'inline-block',
              }}
            />
          </Row>
          <Row gutter={24}>
            <Col span={8}>
              <FormItem label="开户行：">
                {getFieldDecorator('client_openingBank', {
                  rules: [{
                    max: 100, message: '输入内容超过100字'
                  }]
                })(<Input placeholder="请输入开户行" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="账号（银行卡卡号）：">
                {getFieldDecorator('client_accountNumber', {
                  rules: [{ pattern: /^([1-9]{1})(\d{14}|\d{18})$/, message: '请输入正确的银行卡账号' }]
                })(
                  <Input placeholder="请输入账号（银行卡卡号）" />
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="联系方式：">
                {getFieldDecorator('client_phoneNumber', {
                  rules: [
                    { pattern: /^1[3456789]\d{9}$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入联系方式" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="纳税人识别号：">
                {getFieldDecorator('client_i_nsrsbh', {
                  rules: [
                    { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入纳税人识别号型" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="开票地址：">
                {getFieldDecorator('client_address', {
                  rules: [{
                    max: 100, message: '输入内容超过100字'
                  }]
                })(<Input placeholder="请输入开票地址" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="更新日期：">
                {getFieldDecorator('client_receiveDate')(<DatePicker style={{ width: '100%' }} />)}
              </FormItem>
            </Col>
          </Row>
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>
            工商信息
          </Row>
          <Row style={{ textAlign: 'center' }}>
            <span
              style={{
                height: '1px',
                width: '124px',
                textAlign: 'center',
                borderBottom: '1px solid #E9E9E9',
                display: 'inline-block',
              }}
            />
          </Row>
          <Row gutter={24}>
            <Col span={8}>
              <FormItem label="统一社会信用代码：">
                {getFieldDecorator('client_shxydm', {
                  rules: [
                    { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入统一社会信用代码" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="纳税人识别号：">
                {getFieldDecorator('client_b_nsrsbh', {
                  rules: [
                    { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入纳税人识别号" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="注册号：">
                {getFieldDecorator('client_registrationNumber', {
                  rules: [
                    { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入注册号型" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="组织机构代码：">
                {getFieldDecorator('client_orgNum', {
                  rules: [
                    { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                  ]
                })(<Input placeholder="请输入组织机构代码" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="更新日期：">
                {getFieldDecorator('client_updateTime')(<DatePicker style={{ width: '100%' }} />)}
              </FormItem>
            </Col>
          </Row>
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>
            企业人员信息
          </Row>
          <Row style={{ textAlign: 'center' }}>
            <span
              style={{
                height: '1px',
                width: '124px',
                textAlign: 'center',
                borderBottom: '1px solid #E9E9E9',
                display: 'inline-block',
              }}
            />
          </Row>
          <Row gutter={24}>
            <Col span={8}>
              <FormItem label="学士：">
                {getFieldDecorator('client_bachelorNum', {
                  rules: [{
                    pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                  }]
                })(<Input placeholder="请输入学士" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="硕士：">
                {getFieldDecorator('client_masterNumber', {
                  rules: [{
                    pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                  }]
                })(<Input placeholder="请输入硕士" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="博士：">
                {getFieldDecorator('client_doctorNumber', {
                  rules: [{
                    pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                  }]
                })(<Input placeholder="请输入博士" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="留学人员：">
                {getFieldDecorator('client_abroadNumber', {
                  rules: [{
                    pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                  }]
                })(<Input placeholder="请输入留学人员" />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="外籍人员：">
                {getFieldDecorator('client_personnelNumber', {
                  rules: [{
                    pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                  }]
                })(<Input placeholder="请输入外籍人员" />)}
              </FormItem>
            </Col>
          </Row>
        </Modal>

        <Modal
          title='地图选房'
          wrapClassName="vertical-center-modal"
          className='modalAlign'
          visible={mapModal}
          onOk={this.setHouseToModal}
          onCancel={() => this.setModalVisible(false, 'mapModal')}
          width={830}
        >
          <div>
            <IframeComm
              attributes={{
                src: '/gis/mapHouse.html',
                style: {
                  width: '100%',
                  height: '450px',
                },
              }}
              postMessageData={sendMapData}
              handleReceiveMessage={this.onReceiveMessage}
            />
          </div>
        </Modal>

        <Modal
          title="新建协议"
          wrapClassName="vertical-center-modal"
          className='modalAlign'
          visible={hetongModalVisible}
          onOk={this.editHeTong}
          onCancel={() => this.setModalVisible(false, 'hetongModalVisible')}
          width={830}
        >
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>房屋基本信息</Row>
          <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
          <Row gutter={24} style={{ marginBottom: '10px' }}>
            <Col span={24} style={{ textAlign: 'center', borderBottom: "1px dashed #eee" }}>
              <Button type="primary" style={{ margin: '13px 0' }} onClick={() => this.setModalVisible(true, 'mapModal')}>请选择房屋&nbsp;&nbsp;<img src={ico_map} width={19} style={{ float: 'right' }} /></Button>
            </Col>
            <Col span={24} style={{ maxHeight: '225px', overflowY: "auto" }}>
              {
                !houseList || houseList.length === 0 ?
                  <div className={styles.noData} style={{ textAlign: 'center' }}><img src={ico_nodata} width={169} alt='暂无数据' /><br />暂无数据</div>
                  :
                  houseList.map((item, index) => (
                    <Row gutter={24} className={styles.mapHouse} key={item.houseName2}>
                      <span className={styles.dot} />
                      <span span={4}>房屋名称：{item.houseName2 ? item.houseName2 : '-'}</span>
                      <span span={4}>楼栋名称：{item.buildingName2 ? item.buildingName2 : '-'}</span>
                      <span span={4}>房屋面积：{item.buildingArea ? `${item.buildingArea}㎡` : '-'}</span>
                      <span span={4}>租金单价：{item.rentPrice ? `${item.rentPrice}元/㎡/日` : '-'}</span>
                      <span span={4}>装修情况：{item.renovationCondition ? item.renovationCondition : '-'}</span>
                      <span span={4}>
                        <Tooltip title="删除">
                          <a><Icon type="delete" onClick={() => this.delHouse(index)} theme="twoTone" twoToneColor="#E22534" /></a>
                        </Tooltip>
                      </span>
                    </Row>
                  ))
              }
            </Col>
          </Row>
          <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>企业基本信息</Row>
          <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
          <Row gutter={24}>
            <Col span={8} id="selectPosition">
              <FormItem label="企业名称：">
                {getFieldDecorator('hetong_rentName', {
                  rules: [
                    { required: true, message: '请输入企业名称' }
                  ]
                })(<Select
                  onChange={this.renterChange}
                  placeholder="请选择企业名称"
                  style={{ width: "100%" }}
                  showSearch
                  getPopupContainer={() => document.getElementById('selectPosition')}
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }>
                  {renterList && renterList.map((item) =>
                    <Option key={item.id} value={item.id}>{item.renter_name}</Option>)}</Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="联系人：">
                {getFieldDecorator('hetong_person', {
                  rules: [{ message: '请输入联系人！' }]
                })(<Input placeholder="请输入联系人" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="证件号码：">
                {getFieldDecorator('hetong_cardNumber', {
                  rules: [{ message: '请输入证件号码！' }]
                })(<Input placeholder="请输入证件号码" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="行业分类：">
                {getFieldDecorator('hetong_industry', {
                  rules: [{ message: '请输入行业分类！' }]
                })(<Select placeholder="请选择行业分类" style={{ width: "100%" }} disabled>
                  <Option value='自管'>自管</Option>
                  <Option value='托管'>托管</Option>
                </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="联系方式：">
                {getFieldDecorator('hetong_contact')(
                  <Input placeholder="请输入联系方式" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="邮箱：">
                {getFieldDecorator('hetong_mail')(<Input placeholder="请输入邮箱" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="主营业务：">
                {getFieldDecorator('hetong_mainContent')(<Input placeholder="请输入主营业务" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="投资额：">
                {getFieldDecorator('hetong_investNumber')(<Input placeholder="请输入投资额" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="人数：">
                {getFieldDecorator('hetong_personSize')(<Input placeholder="请输入人数" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="企业情况：">
                {getFieldDecorator('hetong_renterStatus')(
                  <Select placeholder="请选择联系方式" style={{ width: "100%" }} disabled>
                    <Option value='上市'>上市</Option>
                    <Option value='三板'>三板</Option>
                    <Option value='区域挂牌'>区域挂牌</Option>
                  </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="高企：">
                {getFieldDecorator('hetong_hConpermany')(
                  <Select placeholder="请选择是否高企" style={{ width: "100%" }} disabled>
                    <Option value='是'>是</Option>
                    <Option value='否'>否</Option>
                  </Select>)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="成立时间：">
                {getFieldDecorator('hetong_foundDate')(<Input placeholder="请输入成立时间" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="当年全口径税收：">
                {getFieldDecorator('hetong_rentIncome', {
                  rules: [{ message: '请输入土地面积！' }]
                })(<Input placeholder="请输入土地面积" disabled />)}
              </FormItem>
            </Col>

            <Col span={8}>
              <FormItem label="注册类型：">
                {getFieldDecorator('hetong_rType')(<Input placeholder="请输入注册类型" disabled />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="当年收入：">
                {getFieldDecorator('hetong_income')(<Input placeholder="请输入当年收入" disabled />)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="企业简介：">
                {getFieldDecorator('hetong_company')(<TextArea rows={4} placeholder="请输入企业简介" disabled />)}
              </FormItem>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
}

const WrappedApp = Form.create()(RoomInfo2);
export default WrappedApp;
