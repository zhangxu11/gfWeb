import React, { Component } from 'react';
import { Tabs, Icon } from 'antd';
import ReactEcharts from 'echarts-for-react';
import styles from "./style.less";

const TabPane = Tabs.TabPane;
class mapChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      barData: {
        color: ['#f35245'],
        tooltip: {
          trigger: 'axis',
          axisPointer: {            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
          }
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '5%',
          top: '10px',
          containLabel: true
        },
        xAxis: [
          {
            axisLabel: {
              rotate: -30,
              interval: 0
            },
            type: 'category',
            data: [],
          }
        ],
        yAxis: [
          {
            type: 'value'
          }
        ],
        series: [
          {
            name: '数量',
            type: 'bar',
            data: []
          }
        ]
      },
      pieData: {
        tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b} : {c}元 ({d}%)"
        },
        series: [
          {
            name: '费用占比',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            data: [],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      }
    }
  }

  formatLabel(text, index, total) {
    if (index === total - 1) {
      return null;
    }
    return {
      top: true,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== undefined && nextProps.data.yArr.length > 0 && nextProps.data.xArr.length > 0) {
      let barData = JSON.parse(JSON.stringify(this.state.barData));
      let pieData = JSON.parse(JSON.stringify(this.state.pieData));
      barData.xAxis[0].data = nextProps.data.xArr;
      barData.series[0].data = nextProps.data.yArr;
      pieData.series[0].data = [];
      for (let i = 0, len = nextProps.data.xArrFee ? nextProps.data.xArrFee.length : 0; i < len; i++) {
        pieData.series[0].data.push({
          name: nextProps.data.xArrFee[i],
          value: nextProps.data.yArrFee[i]
        });
      };
      this.setState({
        barData,
        pieData
      });
    };
  }

  render() {
    return (
      <div className={styles.chartBox}>
        <h4>资产用途分析</h4>
        <ReactEcharts
          option={this.state.barData}
          style={{
            height: 260
          }}
        />
        <h4>资产分布分析</h4>
        <ReactEcharts
          option={this.state.pieData}
          style={{
            height: 260
          }}
        />
      </div>
    );
  }
}

export default mapChart;
