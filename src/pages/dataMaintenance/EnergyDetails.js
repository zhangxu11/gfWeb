import React, { Component } from 'react';

import { Row, Col, Spin, Modal } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class EnergyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      data: {
        building_code: "-",
        building_name: "-",
        community_name: "-",
        description: "-",
        display_number: "-",
        fee_type: "-",
        houseInfo: {
          area: "-",
          buildYear: "-",
          buildingArea: 0,
          buildingCount: 0,
          buildingName2: "-",
          buildingStructure: "-",
          city: "-",
          currentFloor: 0
        },
        houseCode: "-",
        houseCount: 0,
        houseIsable: "-",
        houseName: "-",
        houseName2: "-",
        houseNumber: "-",
        isRent: "-",
        keyCount: 0,
        keyPositionId: 0,
        location: "-",
        manageType: "-",
        obtainWay: "-",
        province: "-",
        transferUnit: "-",
        unitNumber: "-",
        use: "-"
      },
      loading: true
    };
  }
  componentWillMount() {
    const id = this.props.location.query.id;
    this.setState({ id });
    this.getFeeMaintainById(id);
  }
  getFeeMaintainById(id) {
    axios.post(Api.getFeeMaintainById, { id }).then(res => {
      if (Number(res.data.code) !== 0) {
        Modal.error({
          title: '暂无查询数据！',
          content: '点击确定关闭窗口。',
          onOk: () => {
            window.close();
          }
        });
      } else {
        this.setState({
          data: res.data.data,
          loading: false
        });
      }
    });
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div style={{
          marginBottom: 50
        }}>
          <Spin spinning={this.state.loading}>
            <Row className={styles.w1120}>
              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>基本信息</p>
                      <p>BASIC INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1}>
                  <div>
                    <span>划转单位：{this.state.data.transfer_unit}</span>
                    &emsp;
               <span>小区名称： {this.state.data.community_name}</span>
                    &emsp;
               <span>楼栋名称：{this.state.data.building_name}</span>
                    &emsp;
               <span>房间名称： {this.state.data.house_name}</span>
                  </div>
                </div>
              </div>
              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>房屋信息</p>
                      <p>HOUSE INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1} style={{
                  height: 'auto'
                }}>
                  <span>建筑面积：{this.state.data.houseInfo.area}</span>
                  &emsp;
                  <span>建筑年份：{this.state.data.houseInfo.buildYear}</span>
                  &emsp;
                  <span>建筑面积：{this.state.data.houseInfo.buildingArea}</span>
                  &emsp;
                  <span>楼号：{this.state.data.houseInfo.buildingName2}</span>
                  &emsp;
                  <span>建筑结构：{this.state.data.houseInfo.buildingStructure}</span>
                  &emsp;
                  <span>城市：{this.state.data.houseInfo.city}</span>
                  &emsp;
                  <span>楼层：{this.state.data.houseInfo.currentFloor}</span>
                  &emsp;
                </div>
              </div>
            </Row>
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default EnergyDetails;
