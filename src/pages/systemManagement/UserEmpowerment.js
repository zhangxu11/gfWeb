//评估记录
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  message,
  InputNumber,
  notification
} from 'antd';
import router from 'umi/router';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

const confirm = Modal.confirm;
const FormItem = Form.Item;
@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class userEmpowerment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      name: '',
      modalVisible: false,
      modifyVisible: false,
      newNumber: '',
      newName: '',
      newPassword: '',
      samePassword: '',
      newPhone: '',
      newDepartment: '',
      newArea: '',
      current: 1,
      pageSize: 0,
      tableData: [],
    };
  }

  componentWillMount() {
    const that = this;
    console.log('this.props.location.query;', this.props.location.query);
    //获取表格数据
    this.sendData();
  }

  resetForm() {
    this.setState({
      number: '',
      name: '',
    });
  }

  //获取表格数据
  sendData = () => {
    let that = this;
    axios
      .post(Api.umTableDatas, {
        userCode: this.state.number,
        userName: this.state.name,
        currentPage: this.state.current,
      })
      .then(function (resp) {
        const {data} = resp;
        if (data.list.length == 0) {
          notification.destroy();
          notification['warning']({
            message: '暂无数据',
          });
        }
        let tmp = [];
        for (let i = 0, len = data.list.length; i < len; i++) {
          tmp.push({
            key: i + 1,
            userName: data.list[i].userName,
            userId: data.list[i].userId,
            areaName: data.list[i].areaName ? data.list[i].areaName : '-',
            userState: data.list[i].userState == 1 ? '已冻结' : '未冻结',
            phoneNumber: data.list[i].phoneNumber ? data.list[i].phoneNumber : '-',
          });
        }
        that.setState({
          tableData: tmp,
          pageSize: data.pageSize,
          current: data.currentPage,
          totalSize: data.totalSize,
        });
      });
  };

  changeNumber = e => {
    this.setState({ number: e.target.value });
  };

  changeName = e => {
    this.setState({ name: e.target.value });
  };

  setModalVisible(modalVisible) {
    //this.setState({ modalVisible });
    this.setState({
      modalVisible: modalVisible,
    });
  }
  setmodifyVisible = modifyVisible => {
    //this.setState({ modifyVisible });
    this.setState({
      modifyVisible: modifyVisible,
    });
  };
  changeUser = id => {
    let aaa = {
      pathname: '/fe/systemManagement/givePermission',
      query: {
        number: this.state.number,
        name: this.state.name,
        id: id,
      },
    };
    router.push(aaa);
    this.setState({ modifyId: id });
  };

  frozenUser = id => {
    let aaa = {
      pathname: '/fe/systemManagement/giveRole',
      query: {
        number: this.state.number,
        name: this.state.name,
        id: id,
      },
    };
    router.push(aaa);
    this.setState({ modifyId: id });
  };

  addNumber = e => {
    this.setState({ newNumber: e.target.value });
  };

  addName = e => {
    this.setState({ newName: e.target.value });
  };

  addPassword = e => {
    this.setState({ newPassword: e.target.value });
  };

  addSamePassword = e => {
    this.setState({ samePassword: e.target.value });
  };

  addPhone = e => {
    this.setState({ newPhone: e.target.value });
  };

  addDepartment = value => {
    this.setState({ newDepartment: value });
  };

  addArea = value => {
    this.setState({ newArea: value });
  };

  pageChange = (page, temp, _pricetype) => {
    this.setState({ current: page }, () => {
      this.sendData();
    });
  };

  render() {
    let that = this;
    const {
      currentUserLoading,
    } = this.props;
    //表头
    const columns = [
      {
        title: '工号',
        dataIndex: 'userId',
        key: 'userId',
      },
      {
        title: '姓名',
        dataIndex: 'userName',
        key: 'userName',
      },
      {
        title: '所属区域',
        dataIndex: 'areaName',
        key: 'areaName',
      },
      {
        title: '状态',
        dataIndex: 'userState',
        key: 'userState',
      },
      {
        title: '联系电话',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <div>
            <a href="javascript:;"
              onClick={() => {
                that.changeUser(item.userId);
              }}
            >
              赋予权限
            </a>
              &emsp;
            <a href="javascript:;"
              onClick={() => {
                that.frozenUser(item.userId);
              }}
            >
              赋予角色
            </a>
          </div>
        ),
      },
    ];

    const treeData = [
      {
        title: 'Node1',
        value: '0-0',
        key: '0-0',
        children: [
          {
            title: 'Child Node1',
            value: '0-0-1',
            key: '0-0-1',
          },
          {
            title: 'Child Node2',
            value: '0-0-2',
            key: '0-0-2',
          },
        ],
      },
      {
        title: 'Node2',
        value: '0-1',
        key: '0-1',
      },
    ];

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>用户赋权</p>
              <p>USER EMPOWERMENT</p>
            </div>
          </div>
          <div className='mrb15'>
            <Form layout="inline" align='center'>
              <FormItem>
                <span className="label">工号：</span>
                <Input
                  value={this.state.number}
                  data-inp="zdbh"
                  onChange={this.changeNumber.bind(this)}
                  placeholder="请输入工号"
                />
              </FormItem>

              <FormItem>
                  <span className="label">姓名：</span>
                  <Input
                    value={this.state.name}
                    onChange={this.changeName.bind(this)}
                    data-inp="zldz"
                    placeholder="请输入姓名"
                  />
              </FormItem>
              <Row gutter={24} style={{ textAlign: 'center'}}>
                  <Button icon='search' type="primary" className="evfilterbtn" onClick={this.sendData.bind(this)}>
                    查询
                  </Button>
                  &emsp;
                  <Button
                    onClick={this.resetForm.bind(this)}
                    type="primary"
                    htmlType="reset"
                    className="evfilterbtn" icon="reload"
                  >
                    重置
                </Button>
              </Row>
          </Form>
          </div>
          <Row className="choicedBox">
            <Table
              bordered={true}
              align={'center'}
              columns={columns}
              dataSource={this.state.tableData}
              pagination={{
                pageSize: that.state.pageSize,
                current: that.state.current,
                onChange: that.pageChange,
                total: that.state.totalSize,
                showTotal: total => `共 ${total} 项`
              }}
            />
          </Row>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default userEmpowerment;
