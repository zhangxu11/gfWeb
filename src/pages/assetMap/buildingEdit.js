//楼栋编辑
import * as React from 'react';
import {
  Form,
  Select,
  Input,
  Button,
  Col,
  Row,
  Checkbox,
  Tabs,
  DatePicker,
  Modal,
  Upload,
  Spin,
  Icon,notification,message
} from 'antd';
import axios from 'axios';
import Api from '../../services/apis';
import moment from 'moment';
import IframeComm from '../../components/IframeComm';
import Link from 'umi/link';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './style.less';
import positionImg from '../../assets/ico_pin2.png'
const InputGroup = Input.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const confirm = Modal.confirm;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))

class BuildingEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: '1',
      info: {},
      showMap: false,
      fileList: [],
      previewVisible: false,
      previewImage: '',
      loading: false,
    };
  }

  
  componentWillMount() {
    
    const buildingCode = this.props.location.query.buildingCode;
    this.setState({
      buildingCode: buildingCode
    }, () => {
      this.getData();
    })
  }
  
  getData() {
    const that = this;
    // that.setState({ loading: true });
    axios.get(Api.amQueryBuilding, {
      params: {
        buildingCode: that.state.buildingCode
      }
    }).then(function (res) {
      const { data } = res;
      const images = [];
      data.images.forEach((item, i) => {
        const splits = item.filePath.split('/');
        const name = splits[splits.length - 1];
        const img = {
          name: name,
          url: 'http://gisams-hitech.cindata.cn/res_file' + item.filePath,
          status: 'done',
          uid: i,
          imageId: item.imageId
        };
        images.push(img);
      })
      that.setState({
        info: data,
        fileList: images
      })

      that.props.form.setFieldsValue({
        location: data.location,
        communityName: data.communityName,
        buildingNum: data.buildingNum,
        buildingName2: data.buildingName2,
        buildingArea: data.buildingArea,
        useArea: data.useArea,
        basicBuildDate: data.basicBuildDate ? moment(data.basicBuildDate) : null,
        buildingStructure: data.buildingStructure,
        basicBuildTerm: data.basicBuildTerm,
        upFloor: data.upFloor,
        downFloor: data.downFloor,
        basicBuildUnitCnt: data.basicBuildUnitCnt,
        basicBuildHouseCnt: data.basicBuildHouseCnt,
        basicBuildProperty: data.basicBuildProperty=='1'?true:false,
        decoration: data.decoration,
        floorHigh: data.floorHigh,
        spaceDistribution: data.spaceDistribution,
        buildingfunction: data.buildingfunction,
        maintenanceStatus: data.maintenanceStatus,
        ownershipInfo: data.ownershipInfo,
        buildingToward: data.buildingToward,
        buildPurO: data.buildPurO,
        buildPurRealO: data.buildPurRealO,
        stateLive: data.stateLive,
        stateStoreLease: data.stateStoreLease,
        qualityLevel: data.qualityLevel,
        qualityIdentify: data.qualityIdentify,
        qualityIdentifyDate:  data.qualityIdentifyDate ? moment(data.basicBuildDate) : null,
        qualityIsFixed: data.qualityIsFixed=='1'?true:false,
        qualitySeismicLev: data.qualitySeismicLev,
        qualityIsSaving: data.qualityIsSaving=='1'?true:false,
        buildProperty: data.buildProperty,
        qualityRepairDate: data.qualityRepairDate ? moment(data.qualityRepairDate) : null,
        qualitySeismicDate: data.qualitySeismicDate ? moment(data.qualitySeismicDate) : null,
        qualitySaveData: data.qualitySaveData ? moment(data.qualitySaveData) : null,
        buildIsRenovation: data.buildIsRenovation,
        equipIsWater: data.equipIsWater=='1'?true:false, 
        equipIsHeating: data.equipIsHeating=='1'?true:false, 
        equipIsNon: data.equipIsNon=='1'?true:false, 
        equipIsDrainage: data.equipIsDrainage=='1'?true:false, 
        equipIsPower: data.equipIsPower=='1'?true:false, 
        equipIsGas: data.equipIsGas=='1'?true:false, 
        equipIsHygiene: data.equipIsHygiene=='1'?true:false, 
        equipIsShower: data.equipIsShower=='1'?true:false, 
        equipFire: data.equipFire=='1'?true:false, 
        equipEleveator: data.equipEleveator=='1'?true:false, 
        equipAircool: data.equipAircool=='1'?true:false, 
        remarks: data.remarks
      })
      
      that.setState({ loading: false });
    })
  }
  
  save() {
    const that = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        /* //处理图片
        const { fileList } = this.state;
        let formData = new FormData();
        
        console.log(formData)
        fileList.forEach((file,index) => {
          console.log(file)
          formData.append('file', file);
          // console.log(JSON.stringify(formData.getAll('file')))
          console.log(formData.getAll('file'))
        }); */

        that.setState({ loading: true });
        axios.post(Api.amModifyBuilding, {
          buildingCode: that.state.buildingCode,
          location: values.location,
          buildingNum: values.buildingNum,
          buildingName2: values.buildingName2,
          buildingArea: values.buildingArea,
          useArea: values.useArea,
          basicBuildDate: values.basicBuildDate ? moment(values.basicBuildDate).format('YYYY-MM-DD') : '',
          buildingStructure: values.buildingStructure,
          basicBuildTerm: values.basicBuildTerm,
          upFloor: values.upFloor,
          downFloor: values.downFloor,
          basicBuildUnitCnt: values.basicBuildUnitCnt,
          basicBuildHouseCnt: values.basicBuildHouseCnt,
          basicBuildProperty: values.basicBuildProperty?'1':'0',
          facilities: values.facilities,
          decoration: values.decoration,
          floorHigh: values.floorHigh,
          spaceDistribution: values.spaceDistribution,
          buildingfunction: values.buildingfunction,
          maintenanceStatus: values.maintenanceStatus,
          ownershipInfo: values.ownershipInfo,
          buildingToward: values.buildingToward,
          buildPurO: values.buildPurO,
          buildPurRealO: values.buildPurRealO,
          stateLive: values.stateLive,
          stateStoreLease: values.stateStoreLease,
          qualityLevel: values.qualityLevel,
          qualityIdentify: values.qualityIdentify,
          qualityIdentifyDate: values.qualityIdentifyDate ? moment(values.qualityIdentifyDate).format('YYYY-MM-DD') : '',
          qualityIsFixed: values.qualityIsFixed?'1':'0',
          qualitySeismicLev: values.qualitySeismicLev,
          qualityIsSaving: values.qualityIsSaving?'1':'0',
          buildProperty: values.buildProperty,
          qualityRepairDate: values.qualityRepairDate ? moment(values.qualityRepairDate).format('YYYY-MM-DD') : '',
          qualitySeismicDate: values.qualitySeismicDate ? moment(values.qualitySeismicDate).format('YYYY-MM-DD') : '',
          qualitySaveData: values.qualitySaveData ? moment(values.qualitySaveData).format('YYYY-MM-DD') : '',
          buildIsRenovation: values.buildIsRenovation,
          equipIsWater: values.equipIsWater?'1':'0',
          equipIsHeating: values.equipIsHeating?'1':'0',
          equipIsNon: values.equipIsNon?'1':'0',
          equipIsDrainage: values.equipIsDrainage?'1':'0',
          equipIsPower: values.equipIsPower?'1':'0',
          equipIsGas: values.equipIsGas?'1':'0',
          equipIsHygiene: values.equipIsHygiene?'1':'0',
          equipIsShower: values.equipIsShower?'1':'0',
          equipFire: values.equipFire?'1':'0',
          equipEleveator: values.equipEleveator?'1':'0',
          equipAircool: values.equipAircool?'1':'0',
          remarks: values.remarks
        }).then(function (res) {
          const { message } = res.data;
          notification.destroy();
          notification['success']({ message: message });
          that.setState({ loading: false });

          if (message == '保存成功') {
            window.close();
          }
        })
      }
    })
  }

  changeStep(stepType) {
    let step = parseInt(this.state.step);
    if (stepType == 'pre') {
      step = step - 1;
    } else {
      step = step + 1;
    }
    this.setState({
      step: String(step)
    });
  }
  cancelEdit(){
    confirm({
      title: '确定取消编辑？',
      content: '信息将不会保存。',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        window.close();
      }
    });
  }
  showMap = (flag) => {
    this.setState({showMap: flag})
  }
  showPop = (msg) => {
    
    if(msg.data.position) {
      this.props.form.setFieldsValue({s_name: msg.data.position})
    }
  }
  tabClick(step) {
    this.setState({
      step: String(step)
    });
  }

  handleCancel = () => this.setState({ previewVisible: false })

  handlePreview = (file) => {
    console.log(file)
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  handleChange(info) {
    const { fileList } = info;
    this.setState({ fileList, fileIds: fileList });
    if (fileList[fileList.length-1].status === 'done') {
      message.success(`${fileList[fileList.length-1].name} 图片上传成功！`);
    } else if (fileList[fileList.length-1].status === 'error') {
      message.error(`${fileList[fileList.length-1].name} 图片上传失败！`);
    }
  }
  
  removePic(info) {
    const that = this;
    let p = new Promise((resp, rej) => {
      
      if (!info.imageId) {
        message.error('新上传的图片没有imageId，请刷新后再删除！');
        rej('error');
        return;
      }
      axios.get(Api.amDeletePic, {
        params: {
          imageId : info.imageId
        }
      }).then((res) => {
        message.success(res.data.message);
        if (res.data.message === '删除成功！') {
          const index = that.state.fileList.indexOf(info);
          const newFileList = that.state.fileList.slice();
          newFileList.splice(index, 1);
          that.setState({fileList:newFileList})
          resp('success');
        } else {
          rej('error');
        };
      });
    });
    return p;
  }

  beforeUpload = (file) => {
    this.setState(state => ({
      fileList: [...state.fileList, file],
    }));
    return false;
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      currentUserLoading,
    } = this.props;
    const plainOptions = ['Apple', 'Pear', 'Orange'];
    
    const data = this.state.info;
    const _attributes = {
      src: '/gis/editMap.html',
      width: 560,
      height: 400,
      position: 'fixed'
    };
    const { previewVisible, previewImage, fileList } = this.state;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <Form layout='inline' align='left' onSubmit={this.query}>
            <Spin spinning={this.state.loading}>
            <Tabs
              defaultActiveKey="1"
              activeKey={this.state.step}
              onTabClick={this.tabClick.bind(this)}
            >
              <TabPane tab="第一步 基本信息" key="1">
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>基本信息</p>
                    <p>BASIC INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="所属单位：">
                      {data && data.transferUnit ? data.transferUnit  : '-'}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="详细地址：">
                      {
                        getFieldDecorator('location', {
                          rules:[{required:true,messeage:'请输入详细地址！'}]
                        })(
                          <Input placeholder="请输入详细地址" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="栋号：">
                      {
                        getFieldDecorator('buildingNum', {
                          rules:[{required:true,messeage:'请输入栋号！'}]
                        })(
                          <Input placeholder="请输入栋号" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="楼栋名称：">
                      {
                        getFieldDecorator('buildingName2', {
                          rules:[{required:true,messeage:'请输入楼栋名称！'}]
                        })(
                          <Input placeholder="请输入楼栋名称" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑面积（㎡）：">
                      {
                        getFieldDecorator('buildingArea')(
                          <Input placeholder="请输入建筑面积" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="使用面积（㎡）：">
                      {
                        getFieldDecorator('useArea')(
                          <Input placeholder="请输入使用面积" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建造年月：">
                      {
                        getFieldDecorator('basicBuildDate')(
                          <DatePicker placeholder="请选择建造年月"/>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑结构：">
                      {getFieldDecorator('buildingStructure')(
                      <Select placeholder="请选择建筑结构">
                        <Option value="01">钢结构</Option>
                        <Option value="02">钢混结构</Option>
                        <Option value="03">砖混结构</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="设计使用年限：">
                      {getFieldDecorator('basicBuildTerm')(
                      <Select placeholder="请输入设计使用年限">
                        <Option value="01">5年</Option>
                        <Option value="02">30年</Option>
                        <Option value="03">40年</Option>
                        <Option value="04">50年</Option>
                        <Option value="05">70年</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="地上层数：">
                      {
                        getFieldDecorator('upFloor')(
                          <Input placeholder="请输入地上层数" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="地下层数：">
                      {
                        getFieldDecorator('downFloor')(
                          <Input placeholder="请输入地下层数" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="单元数：">
                      {
                        getFieldDecorator('basicBuildUnitCnt')(
                          <Input placeholder="请输入单元数" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="住房套数：">
                      {
                        getFieldDecorator('basicBuildHouseCnt')(
                          <Input placeholder="请输入住房套数" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="是否物业管理：">
                      {
                        getFieldDecorator('basicBuildProperty', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="设施设备：">
                      {
                        getFieldDecorator('facilities')(
                          <Input placeholder="请输入设施设备" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="装饰装修：">
                      {
                        getFieldDecorator('decoration')(
                          <Input placeholder="请输入装饰装修" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="层高：">
                      {
                        getFieldDecorator('floorHigh')(
                          <Input placeholder="请输入层高" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="空间分布：">
                      {
                        getFieldDecorator('spaceDistribution')(
                          <Input placeholder="请输入空间分布" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="建筑功能：">
                      {
                        getFieldDecorator('buildingfunction')(
                          <Input placeholder="请输入建筑功能" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="维修状态：">
                      {
                        getFieldDecorator('maintenanceStatus')(
                          <Input placeholder="请输入维修状态" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="权属信息：">
                      {
                        getFieldDecorator('ownershipInfo')(
                          <Input placeholder="请输入权属信息" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="楼栋朝向：">
                      {
                        getFieldDecorator('buildingToward')(
                          <Input placeholder="请输入楼栋朝向" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>房产用途</p>
                    <p>HOUSING PURPOSES</p>
                  </div>
                </div>
                <Row>
                <Col span={8}>
                  <FormItem label="证载用途：">
                    {getFieldDecorator('buildPurO')(
                    <Select placeholder="请选择证载用途">
                      <Option value="01">办公用房</Option>
                      <Option value="02">工业厂房</Option>
                      <Option value="03">文体用房</Option>
                      <Option value="04">公寓住房</Option>
                      <Option value="05">招待用房</Option>
                      <Option value="06">公共生活用房</Option>
                      <Option value="07">专业用房</Option>
                      <Option value="08">库房</Option>
                      <Option value="09">车炮库房</Option>
                      <Option value="10">住宅</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="实际用途：">
                    {getFieldDecorator('buildPurRealO')(
                    <Select placeholder="请选择实际用途">
                      <Option value="01">办公用房</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                  <Col span={8}>
                    <FormItem label="自用总面积：">
                      {
                        getFieldDecorator('stateLive')(
                          <Input placeholder="请输入自用总面积" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="出租总面积：">
                      {
                        getFieldDecorator('stateStoreLease')(
                          <Input placeholder="请输入出租总面积" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>地理位置信息</p>
                    <p>GEOGRAPHICAL LOCATION INFORMATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={24}>
                    <FormItem label="地理坐标信息：">
                      <img onClick={this.showMap} src={positionImg} className={styles.contentImg} />
                      {
                        getFieldDecorator('s_name')(
                          
                          <Input placeholder="请输入地理坐标信息" style={{width:'956px'}}/>
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>

              <TabPane tab="第二步 质量与使用" key='2'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>质量状况</p>
                    <p>QUALITY CONDITION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="质量等级：">
                      {getFieldDecorator('qualityLevel')(
                      <Select placeholder="请选择质量等级">
                        <Option value="01">完好</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="鉴定机构：">
                      {
                        getFieldDecorator('qualityIdentify')(
                          <Input placeholder="请输入鉴定机构" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="鉴定时间：">
                      {
                        getFieldDecorator('qualityIdentifyDate')(
                          <DatePicker placeholder="请选择鉴定时间" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="是否抗震加固：">
                      {
                        getFieldDecorator('qualityIsFixed', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="抗震等级：">
                      {getFieldDecorator('qualitySeismicLev')(
                      <Select placeholder="请选择抗震等级">
                        <Option value="01">一级</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="是否节能建筑：">
                      {
                        getFieldDecorator('qualityIsSaving', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="住房性质：">
                      {getFieldDecorator('buildProperty')(
                      <Select placeholder="请选择住房性质">
                        <Option value="01">公寓房</Option>
                        <Option value="02">安居房</Option>
                        <Option value="03">集资房</Option>
                        <Option value="04">经济适用房</Option>
                        <Option value="05">现有住房</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>

              <TabPane tab="第三步 维修改造" key='3'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>维修改造</p>
                    <p>MAINTENANCE AND RECONSTRUCTION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="大修时间：">
                      {
                        getFieldDecorator('qualityRepairDate')(
                          <DatePicker placeholder="请选择大修时间" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="抗震加固时间：">
                      {
                        getFieldDecorator('qualitySeismicDate')(
                          <DatePicker placeholder="请选择抗震加固时间" />
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem label="节能改造时间：">
                      {
                        getFieldDecorator('qualitySaveData')(
                          <DatePicker placeholder="请选择节能改造时间" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>房屋整治</p>
                    <p>HOUSING RENOVATION</p>
                  </div>
                </div>
                <Row>
                  <Col span={8}>
                    <FormItem label="房屋整治建议：">
                      {getFieldDecorator('buildIsRenovation')(
                      <Select placeholder="请选择房屋整治建议">
                        <Option value="01">翻建</Option>
                      </Select>)}
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>
              
              <TabPane tab="第四步 设备设施" key='4'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>设施设备情况</p>
                    <p>FACILITY INSTRUMENT</p>
                  </div>
                </div>
                <Row>
                  <Col span={4}>
                    <FormItem label="供水：">
                      {
                        getFieldDecorator('equipIsWater', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="供热：">
                      {
                        getFieldDecorator('equipIsHeating', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="中水：">
                      {
                        getFieldDecorator('equipIsNon', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="排水：">
                      {
                        getFieldDecorator('equipIsDrainage', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="供电：">
                      {
                        getFieldDecorator('equipIsPower', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="供气：">
                      {
                        getFieldDecorator('equipIsGas', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="卫生设施：">
                      {
                        getFieldDecorator('equipIsHygiene', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="淋浴设施：">
                      {
                        getFieldDecorator('equipIsShower', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="消防设施：">
                      {
                        getFieldDecorator('equipFire', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="电梯设施：">
                      {
                        getFieldDecorator('equipEleveator', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="中央空调：">
                      {
                        getFieldDecorator('equipAircool', {
                          valuePropName: 'checked',
                        })(
                          <Checkbox></Checkbox>
                        )
                      }
                    </FormItem>
                  </Col>
                  <Col span={24}>
                    <FormItem label="备注：">
                      {
                        getFieldDecorator('remarks')(
                          <Input placeholder="请输入备注" />
                        )
                      }
                    </FormItem>
                  </Col>
                </Row>
              </TabPane>

              <TabPane tab="第五步 照片" key='5'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>照片</p>
                    <p></p>
                  </div>
                </div>
                <Row>
                  <div className="clearfix">
                    <Upload
                      action={`${Api.amUploadPic}?addressId=${this.state.buildingCode}`}
                      listType="picture-card"
                      fileList={this.state.fileList}
                      onPreview={this.handlePreview.bind(this)}
                      onChange={this.handleChange.bind(this)}
                      onRemove={this.removePic.bind(this)}
                    >
                      <div>
                        <Icon type="plus" />
                        <div className="ant-upload-text">上传</div>
                      </div>
                    </Upload>
                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                      <img alt="example" style={{ width: '100%' }} src={previewImage} />
                    </Modal>
                  </div>
                </Row>
              </TabPane>
            </Tabs>
            </Spin>
            <div className={styles.energyEditFooter}>
              <Button type='primary' disabled={this.state.step === '1' ? true : false} onClick={()=>this.changeStep('pre')}>上一步</Button>
              &emsp;
              <Button type='primary' disabled={this.state.step !== '1' && this.state.step === '5' ? true : false} onClick={()=>this.changeStep('next')}>下一步</Button>
              &emsp;
              <Button type='primary' onClick={this.save.bind(this)}>保存</Button>
              &emsp;
              <Button type='primary' onClick={this.cancelEdit.bind(this)}>取消</Button>
            </div>
          </Form>
        </div>
        <Modal
          title="地理位置标注"
          visible={this.state.showMap}
          onOk={() => this.showMap(false)}
          onCancel={() => this.showMap(false)}
          width={600}
          height={'auto'}>
          <IframeComm
            attributes={_attributes}
            postMessageData={this.state.info.buildingId}
            handleReceiveMessage={this.showPop}
          />
        </Modal>
      </PageHeaderWrapper >
    );
  }
}

const WrappedApp = Form.create()(BuildingEdit);

export default WrappedApp;
