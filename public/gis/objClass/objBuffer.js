//**********************************
//基础功能类-缓冲区
//Power by han 2018.07.04
//**********************************
var objBuffer = {
    pUrl: "", //存储生成缓冲区服务地址
    pWkid:0,//存储坐标系代码
    pDistance: 0, //存储缓冲区生成半径
    pGeometry: null, //存储待生成缓冲区的几何对象
    init:function (u,w){ //初始化类对象
        this.pUrl = u;
        this.pWkid = w;
    },
    init2:function (u,w,d,g){ //初始化类对象2
        this.pUrl = u;
        this.pWkid = w;
        this.pDistance = d;
        this.pGeometry = g;
    },
    setWkid: function (e) { //设置坐标系代码
        this.pWkid = e;
    },
    getWkid: function () { //获取坐标系代码
        return this.pWkid;
    },
    setUrl: function (e) { //设置生成缓冲区服务地址
        this.pUrl = e;
    },
    getUrl: function () { //获取生成缓冲区服务地址
        return this.pUrl;
    },
    setDistance: function (e) { //设置缓冲区生成半径
        this.pDistance = e;
    },
    getDistance: function () { //获取缓冲区生成半径
        return this.pDistance;
    },
    setGeometry: function (e) { //设置待生成缓冲区的几何对象
        this.pGeometry = e;
    },
    getGeometry: function () { //获取待生成缓冲区的几何对象
        return this.pGeometry;
    },
    paramTest: function (){ //参数验证
        if (this.pUrl == "") {
            alert("objBuffer error 'url'");
            console.log("缓冲区服务地址未设置");
            return false;
        }
        if (this.pWkid == 0) {
            alert("objBuffer error 'wkid'");
            console.log("坐标系代码未设置");
            return false;
        }
        if (this.pDistance == 0) {
            alert("objBuffer error 'distance'");
            console.log("缓冲区查询半径未设置");
            return false;
        }
        if (this.pGeometry == null) {
            alert("objBuffer error 'geometry'");
            console.log("待生成缓冲区的几何对象未设置");
            return false;
        }
        return true;
    },
    execute: function (callback) { //执行缓冲区查询方法
        if (this.paramTest() == false) return;
        var geometryService = new esri.tasks.GeometryService(this.pUrl);
        var bufferParams = new esri.tasks.BufferParameters();
        bufferParams.geometries = [this.pGeometry];
        bufferParams.distances = [this.pDistance];
        bufferParams.unit = esri.tasks.GeometryService.UNIT_METER;
        bufferParams.bufferSpatialReference = new esri.SpatialReference({"wkid": this.pWkid});
        bufferParams.outSpatialReference = new esri.SpatialReference({"wkid": this.pWkid});
        geometryService.buffer(bufferParams, callback);
    }
}

