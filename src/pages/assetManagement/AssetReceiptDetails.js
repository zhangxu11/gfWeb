//项目管理
import * as React from 'react';
import {
  Row,
  Col,
  Steps,
  Upload,
  message,
  Button,
  Icon,
  Input,
  Select,
  DatePicker,
  Spin
} from 'antd';

import router from 'umi/router';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from "./style.less";

const Step = Steps.Step;
const Option = Select.Option;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))

class AssetReceiptDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectCode: '',
      loading: true,
      data: {},
      operateDate: '0',
      tradeId: '',
      node: '',
      realNode: '0',
      step: [
        {
          operateDate: '',
          userCode: '',
          title: '房屋核实'
        },
        {
          operateDate: '',
          userCode: '',
          title: '上传核实报告'
        },
        {
          operateDate: '',
          userCode: '',
          title: '上报划转请示'
        },
        {
          operateDate: '',
          userCode: '',
          title: '划转批复'
        },
        {
          operateDate: '',
          userCode: '',
          title: '签署协议'
        },
        {
          operateDate: '',
          userCode: '',
          title: '接管'
        },
        {
          operateDate: '',
          userCode: '',
          title: '运营中'
        },
      ]
    };
  }
  handleChange(value) {
    console.log(`selected ${value}`);
  }
  handleBlur() {
    console.log('blur');
  }

  handleFocus() {
    console.log('focus');
  }
  changeTime(date, operateDate) {
    this.setState({
      operateDate
    });
  }
  componentWillMount() {
    this.setState({
      projectCode: this.props.location.state.projectCode,
      canEdit: this.props.location.state.canEdit
    });
    axios.get(Api.receiveDetails, {
      params: {
        projectCode: this.props.location.state.projectCode
      }
    }).then((res) => {
      let step = this.state.step;
      for (let i = 0, len = res.data.items.length; i < len; i++) {
        step[i].operateDate = res.data.items[i].operateDate;
        step[i].userCode = res.data.items[i].userCode;
      };

      this.setState({
        data: res.data,
        node: res.data.node,
        tradeId: res.data.tradeId,
        step,
        realNode: res.data.items.length
      });
      this.toggleLoading(false);
    }).catch((err) => {
      this.toggleLoading(false);
    });
  }

  toggleLoading(loading) {
    this.setState({
      loading
    });
  }
  submitDate() {
    this.toggleLoading(true);
    axios.get(Api.dealReceiveProcess, {
      params: {
        operateDate: this.state.operateDate,
        projectCode: this.state.projectCode,
        node: this.state.node,
        tradeId: this.state.tradeId
      }
    }).then((res) => {
      this.setState({
        data: res.data
      });
      message.success('提交成功！');
      this.toggleLoading(false);
    }).catch((err) => {
      message.error(`提交失败，请重试！错误信息${err}`);
      this.toggleLoading(false);
    });
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>资产接收{this.state.canEdit ? '编辑' : '详情'}</p>
              <p>ASSET RECEIPT DETAILS</p>
            </div>
          </div>
          <Spin spinning={this.state.loading}>
            <div className={styles.mrb30}>
              <Steps current={Number(this.state.realNode)}>
                {this.state.step.map((item, index) => (
                  <Step key={index} title={item.title} description={<div><p>{item.userCode}</p><p>{item.operateDate}</p></div>} />
                ))}
              </Steps>
            </div>

            <div className={`${styles.darBox} clearfix`}>
              <div className={`${styles.darUpload} pull-left`}>
                <div className={styles.darUploadList}>
                  <p>文件</p>
                  <Upload
                    accept='xls,xlsx'
                    name='file'
                    multiple={false}
                    action={`${Api.uploadFile}?projectCode=${this.state.projectCode}`}
                    onChange={(info, list) => {
                      const len = info.fileList.length;
                      if (len === 0) {
                        this.setState({
                          canJump: false
                        });
                      };
                      const status = info.file.status;
                      if (status === 'done') {
                        message.success(`${info.file.name} 上传成功。`);
                        if (!this.state.canJump) {
                          this.setState({
                            canJump: true
                          });
                        };
                      } else if (status === 'error') {
                        message.error(`${info.file.name} 上传失败。`);
                      }
                    }}
                    onRemove={(file) => {
                      console.log(file);

                      //return false;
                    }}>
                    <Button type='primary'>
                      <Icon type="upload" /> 上传文件
                    </Button>
                  </Upload>
                </div>
              </div>
              <div className={`${styles.darDetails} pull-left`}>
                <Row>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">项目名称：</span>
                    {this.state.data.projectName}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">项目编号：</span>
                    {this.state.data.projectCode}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">划转单位：</span>
                    {this.state.data.transferUnit}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">房源数量：</span>
                    {this.state.data.houseCount}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">楼栋数量：</span>
                    {this.state.data.buildingCount}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">签署协议：</span>
                    {this.state.data.signStatus}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">省份：</span>
                    {this.state.data.provinceStr}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">市：</span>
                    {this.state.data.cityStr}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">区：</span>
                    {this.state.data.areaStr}
                  </Col>
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">资产状态：</span>
                    {this.state.data.assetStatus}
                </Col>
                  {this.state.canEdit ?
                    <Col className={`pull-left form_item ${styles.darItem}`}>
                      <span className="label">操作时间：</span>
                      <DatePicker disabled={!this.state.canEdit} onChange={this.changeTime.bind(this)} />
                    </Col> : null}
                </Row>

              </div>
            </div>
            {this.state.canEdit ?
              <div className={styles.buttonBox}>
                <Button onClick={this.submitDate.bind(this)} icon='cloud-upload' type='primary'>确定</Button>
              </div> : null}
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default AssetReceiptDetails;
