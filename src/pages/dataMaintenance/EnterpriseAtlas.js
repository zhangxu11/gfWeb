/*
 * @Author: cjx 
 * @Date: 2019-04-30 09:57:44 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-07-02 13:15:01
 * @file 运营管理/企业图谱
 */
import React, { Component } from 'react';
import { Card, Col, Row, Avatar, Pagination, Spin } from 'antd';
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Ellipsis from '@/components/Ellipsis';
import styles from './style.less'
const { Meta } = Card;
@connect(({ user, project, activities, chart, loading }) => ({
	currentUser: user.currentUser,
	currentUserLoading: loading.effects['user/fetchCurrent']
}))
export default class EnterpriseAtlas extends Component {
	constructor(props) {
		super(props);
		this.userCode = sessionStorage.getItem('currentUser');
		this.state = {
			enterpriseData: {},
			loading: false
		};
	}
	getData = (page) => {
		axios
			.get(Api.getRenterInfo, {
				params: {
					offset: page
				}
			})
			.then(res => {
				this.setState({
					loading: false
				})
				if (res.data === null) {
					message.warn('暂无数据！');
				} else {
					this.setState({
						enterpriseData: res.data
					})
				}
			});
	}
	componentDidMount() {
		this.setState({
			loading: true
		})
		this.getData(1)
	}
	onChange = (page) => {
		this.setState({
			loading: true
		})
		this.getData(page)
	}
	render() {
		const { currentUserLoading } = this.props;
		const { enterpriseData } = this.state;
		let list = enterpriseData.list ? enterpriseData.list : [];
		let renderData = [];
		for (let i = 0; i < list.length; i++) {
			const item = list[i];
			renderData.push(
				<Col span={8} key={i}>
					<Link to={`/fe/dataMaintenance/renterDetails?id=${item.id}`} target="_blank">
						<Card className={styles.cardStyle}>
							<Meta
								avatar={item.pic_url && item.pic_url.length > 0
									?
									<Avatar shape="square" size={110} src={`http://gisams-hitech.cindata.cn/res_file${item.pic_url}`} />
									:
									<div className={styles.noData}>暂无</div>}
								description={
									<div className={styles.enterpriseCardDetails}>
										<div className={styles.enterpriseCardContent}>企业名称:
											<Ellipsis length='6' tooltip className={styles.ellipsis}>
												{item.renter_name}
											</Ellipsis>
										</div>
										<div className={styles.enterpriseCardContent}>行业类型:
											<Ellipsis length='6' tooltip className={styles.ellipsis}>
												{item.name}
											</Ellipsis>
										</div>
										<div className={styles.enterpriseCardContent}>企业规模:
											<Ellipsis length='6' tooltip className={styles.ellipsis}>
												{item.people_num}
											</Ellipsis>
										</div>
										<div className={styles.enterpriseCardContent}>企业地址:
											<Ellipsis length='6' tooltip className={styles.ellipsis}>
												{item.registered_address && item.registered_address.length > 0 ? item.registered_address : ' -'}
											</Ellipsis>
										</div>
									</div>
								}
							/>
						</Card>
					</Link>
				</Col>
			)
		}

		if (enterpriseData.length) { }
		return (
			<PageHeaderWrapper
				loading={currentUserLoading}
			>
				<div className="mainContainer">
					<div className='contentHeader'>
						<div className="contentHeaderTitle">
							<p>企业图谱</p>
							<p>ENTERPRISE ATLAS</p>
						</div>
					</div>
					<Row gutter={24} className={styles.enterprise}>
						<Spin spinning={this.state.loading}>
							{renderData}
						</Spin>
					</Row>
					<Pagination
						style={{ textAlign: 'right' }}
						total={enterpriseData.count}
						defaultCurrent={1}
						onChange={this.onChange}
					/>
				</div>
			</PageHeaderWrapper>
		);
	}
}
