//**********************************
//基础功能类-几何样式
//Power by han 2018.07.16
//**********************************
var objGeoStyle = {
    enumStyleType: {
        SOLID: function () {
            return esri.symbol.SimpleLineSymbol.STYLE_SOLID;
        },
        DASHDOT: function () {
            return esri.symbol.SimpleLineSymbol.STYLE_DASHDOT;
        }
    },
    enumStyleColor: {
        Black: function () {
            return new dojo.Color([0, 0, 0]);
        },
        Black2: function () {
            return new dojo.Color([0, 0, 0, 0.25]);
        },
        White: function () {
            return new dojo.Color([255, 255, 255]);
        },
        White2: function () {
            return new dojo.Color([255, 255, 255, 0.25]);
        },
        Blue: function () {
            return new dojo.Color([0, 0, 255]);
        },
        Blue2: function () {
            return new dojo.Color([0, 0, 255, 0.25]);
        },
        Red: function () {
            return new dojo.Color([255, 0, 0]);
        },
        Red2: function (e) {
            var display = 0.25;
            if (e && e != null) {
                display = e;
            }
            return new dojo.Color([255, 0, 0, display]);
        }
    },
    getStylePolygon: function (lineColor,polygonColor) {
        var lColor = new dojo.Color([255, 0, 0, 0.55]);
        var rColor = new dojo.Color([255, 255, 0, 0.25]);
        if (lineColor && lineColor != null) {
            lColor=lineColor;
        }
        if (polygonColor && polygonColor != null) {
            rColor=polygonColor;
        }

        var PolygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DASHDOT,
                lColor, 2), rColor
        );
        return PolygonSymbol;
    },
    getStylePolyline: function (type, color) {
        var e = this.enumStyleType.SOLID();
        if (type && type != null) {
            e = type;
        }
        var c = this.enumStyleColor.Red();
        if (color && color != null) {
            c = color;
        }
        var PolylineSymbol = new esri.symbol.SimpleLineSymbol(e, c, 2);
        return PolylineSymbol;
    }
}

