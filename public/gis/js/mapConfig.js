var config = {
    urlAPI: "http://taxation-qdgray.cindata.cn/arcgis_js_api_314/library/3.14/3.14",//API地址
    urlBaseLayer: "http://gissvc4an.cindata.cn/arcgis/rest/services/comm/ChinaGray/MapServer", //底图地址
    urlImgLayer: "http://gissvc4an.cindata.cn/arcgis/rest/services/comm/googleMap/MapServer",//影像图地址
  //http://taxation-gisserver.cindata.cn/arcgis/rest/services/dalianguofang/ADDR/MapServer
    urlAddr: "http://taxation-gisserver.cindata.cn/arcgis/rest/services/dalianguofang/ADDR_new/MapServer",//数据服务地址
	  urlPOI:"http://taxation-gisserver.cindata.cn/arcgis/rest/services/dalianguofang/POI/MapServer",//POI查询地址
//    urlIndoor:"http://taxation-gisserver.cindata.cn/arcgis/rest/services/dalianguofang/indoor_addr/MapServer",    //大连国房
    urlIndoor:"http://taxation-gisserver.cindata.cn/arcgis/rest/services/dalianguofang/indoor_addr_kj/MapServer",   //科技创新城
    urlGeometryServer: "http://taxation-gisserver.cindata.cn/arcgis/rest/services/Utilities/Geometry/GeometryServer",//几何服务地址

    xmin_BJ: 12911360.56,//北京
    ymin_BJ: 4835524.88,
    xmax_BJ: 13001451.37,
    ymax_BJ: 4867870.25,
    cityData_BJ: "http://gissvc4an.cindata.cn/arcgis/rest/services/beijing/address/MapServer",
    xmin_DL: 13519267.65,//大连市
    ymin_DL: 4699383.25,
    xmax_DL: 13564313.05,
    ymax_DL: 4716316.62,
    cityData_DL: "http://regissvc4al.cindata.cn/arcgis/rest/services/dalian/address/MapServer",
    xmin_DD: 13782899.80,//丹东市
    ymin_DD: 4842059.31,
    xmax_DD: 13909635.47,
    ymax_DD: 4896232.85,
    cityData_DD: "http://regissvc4al.cindata.cn/arcgis/rest/services/dandongshi/address/MapServer",
    xmin_ZH: 14599372.5310,//珠海市
    ymin_ZH: 5884420.5077,
    xmax_ZH: 14599372.5310,
    ymax_ZH: 5884420.5077,
    cityData_ZH: "http://regissvc4mz.cindata.cn/arcgis/rest/services/zhuhai/address/MapServer",
    xmin_HRB: 14069874.0762,//哈尔滨
    ymin_HRB: 5723732.9038,
    xmax_HRB: 14131654.4081,
    ymax_HRB: 5757136.6164,
    cityData_HRB: "",
    xmin_GZ: 12528625.23,//广州
    ymin_GZ: 2616030.74,
    xmax_GZ: 12709071.43,
    ymax_GZ: 2694612.15,
    cityData_GZ: "http://regissvc4al.cindata.cn/arcgis/rest/services/guangzhou/address/MapServer",
    xmin_SZ: 12672756.75,//深圳
    ymin_SZ: 2568800.74,
    xmax_SZ: 12717868.30,
    ymax_SZ: 2588446.09,
    cityData_SZ: "http://regissvc4mz.cindata.cn/arcgis/rest/services/shenzhen/address/MapServer",
    xmin_SH: 13470453.18,//上海
    ymin_SH: 3625542.00,
    xmax_SH: 13560676.28,
    ymax_SH: 3664832.70,
    cityData_SH: "http://regissvc4mz.cindata.cn/arcgis/rest/services/shanghai/address/MapServer",
    xmin_CD: 1.2952273962634303E7,
    ymin_CD: 4866192.442633594,
    xmax_CD: 1.2952739907154199E7,
    ymax_CD: 4866495.213506709,
    cityData_CD: "http://regissvc4al.cindata.cn/arcgis/rest/services/chengdu/address/MapServer",
    xmin_NJ: 1.321979007700778E7,
    ymin_NJ: 3771798.369026309,
    xmax_NJ: 1.322710317496735E7,
    ymax_NJ: 3777555.7138743643,
    cityData_NJ: "http://regissvc4mz.cindata.cn/arcgis/rest/services/nanjing/address/MapServer",
    wkid: 3785,
    minZoom: 4,//地图最小缩放比例
    initZoom: 5,//地图初始化级别
    initZoom2: 10,
    xmin: 8175445.558847766,
    ymin: 2056264.7553386763,
    xmax: 1.503780826042661E7,
    ymax: 7087593.985293306,
    urlCommunity: "/1",
    urlBuilding: "/2",
    urlNational: "/0",
    urlProvince: "/1",
    urlDistrict: "/2",
    urlPOI_p: "/0",
    urlIndoor_unit:"/0",
    urlIndoor_build:"/1",
    urlIndoor_room:"/2",
    urlIndoor_land:"/3",
    urlIndoor_roomLand:"/4",
    showBufferLevel: 18,//生成buffer后定位的级别
    showProvinceLevel: 7,    // 省级显示级别
    showCityLevelBegin: 8,   // 市显示起始级别
    showCityLevelEnd: 10,    // 市显示结束级别
    showDistrictLevelBegin: 11,  // 区显示起始级别
    showDistrictLevelEnd: 14,    // 区显示结束级别
    zoomCity: 8,       // 点击缩放到市级
    zoomDistrict: 11,    //点击缩放到区级
    zoomXQ: 15,
    zoomBuilding: 16,     // 楼栋级别
    showMapGJXQ: 16,      // 地图挂接缩放级别

    imageURL:"http://gisams-hitech.cindata.cn/res_file"
}

//地图类型枚举
var configShowLayerType = {
    Vector: 0,
    Img: 1
}

var configColor = {
    Black:0,
    White:1,
    Blue:2,
    Yellow:3
}

var configImagePath = {
	img_POI_eduction: "img/point_5.png",
	img_POI_park: "img/point_3.png",
	img_POI_dinner: "img/point_2.png",
	img_POI_ciname: "img/point_6.png",
	img_POI_bus: "img/point_1.png",
	img_POI_medical: "img/point_4.png",
	img_POI_shooping: "img/point_8.png",
  img_POI_business: "img/point_7.png"
}

// POI类型
var POIType = {
	Education:   0,       // 学校
	Park:        1,       // 公园
	Dinner:      2,       // 餐饮
	Cinema:      3,       // 电影
	BusStation:  4,       // 公交站
	Hospital:    5,       // 医院
  Shopping:    6        // 购物
}

var prefix = '';
if (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1'||window.location.hostname === '10.200.15.211' ||window.location.hostname === '10.200.15.247'||window.location.hostname === '10.200.15.226') {
  prefix = '/api'
};
//接口访问
var ajaxUrl = {
  //根据楼栋ID查询楼栋详细信息
  queryLDDetail: prefix + '/asset/getBuildBubble',
  queryHUDetail: prefix + '/asset/getHouseBubble',
  queryHosueInfo: prefix + '/contract/queryHosueInfo'
}
/*图片符号对应：
8——卫生间
45——销售公寓
46——前台接待
47——开水间
48——门斗
49——走廊
*/




