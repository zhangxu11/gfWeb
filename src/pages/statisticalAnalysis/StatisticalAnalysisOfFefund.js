import React, { Component } from 'react';
import { connect } from 'dva';
import { Row } from 'antd';
import moment from 'moment';
import axios from 'axios';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Api from '@/services/apis';
import TimeSelectorWithMonthYear from '@/components/TimeSelector';
import ReactSwiper from '@/components/ReactSwiper';
import RentReasonCard from '@/components/RentReasonCard';
import ReactEcharts from 'echarts-for-react';

/**
 * page of /fe/statisticalAnalysis/statisticalAnalysisOfFefund
 *
 * @class StatisticalAnalysisOfFefund
 * @extends {Component}
 */
@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class StatisticalAnalysisOfFefund extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment().startOf('year'), // 开始时间
      endDate: moment().endOf('year'), // 结束时间
      xAxisData: [], // x轴坐标
      seriesData: [], // x轴数据
      reasonList: [],
    };
    this.swiperOptions = {
      loop: true,
      slidesPerView: 5,
      loopFillGroupWithBlank: true,
    };
  }

  componentDidMount() {
    const { startDate, endDate } = this.state;
    this.getOutcontracttrend(startDate, endDate);
    this.getOutcontract();
  }

  getOutcontracttrend = (startDate, endDate) => {
    const startDateYear = moment().startOf('year');
    const endDateYear = moment().endOf('year');
    let checkYear = false;
    if (startDate.isSame(startDateYear, 'day') && endDate.isSame(endDateYear, 'day')) checkYear = true;
    axios.post(Api.outcontracttrend, {
      startDate: startDate.format("YYYY-MM-DD"),
      endDate: endDate.format("YYYY-MM-DD"),
      byDay: !checkYear,
    }).then((res) => {
      let xAxisData = [];
      const seriesData = res.data.result.incrementStatisticsList.map((list, index) => {
        if (index === 0) xAxisData = this.buildXAxisData(list.incrementStatisticsModelList);
        const data = this.buildSeriesData(list.incrementStatisticsModelList);
        return {
          name: list.name,
          smooth: true,
          type: 'line',
          data,
        };
      });
      this.setState({
        xAxisData,
        seriesData,
      });
    }).catch(e => {
      console.error('get outcontracttrend error: ', e);
    })
  }

  getOutcontract = () => {
    axios.post(Api.outcontract).then(res => {
      this.setState({ reasonList: res.data.result });
    }).catch(e => {
      console.error('get reasonList error: ', e);
    })
  }

  dateOnChange = (startDate, endDate) => {
    this.getOutcontracttrend(startDate, endDate);
    this.setState({
      startDate,
      endDate,
    });
  }

  buildXAxisData = (data) => data.map(d => d.dateAlias)

  buildSeriesData = (data) => data.map(d => ({
    name: d.dateAlias,
    value: d.renterNum
  }))

  buildChartOption = () => {
    const { xAxisData, seriesData } = this.state;
    return {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        }
      },
      title: {
        show: true,
        text: '退租走势',
        textStyle: {
          color: 'rgba(0, 0, 0, 0.427450980392157)',
          fontSize: 14,
          fontWeight: 400,
          fontStyle: 'normal',
        },
      },
      xAxis: [
        {
          type: 'category',
          data: xAxisData,
          axisPointer: {
            type: 'shadow'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '数量',
          // min: 0,
          // max: 250,
          // interval: 50,
          axisLabel: {
            formatter: '{value} 家'
          }
        },
      ],
      series: seriesData
    }
  }

  render() {
    const { currentUserLoading } = this.props;
    const {
      startDate,
      endDate,
      reasonList,
    } = this.state;
    const option = this.buildChartOption();
    return (
      <PageHeaderWrapper loading={currentUserLoading} style={{ padding: '25px 25px 35px' }}>
        <Row>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>退租原因统计</p>
                <p>STATISTICAL ANALYSIS OF REFUND</p>
              </div>
            </div>
          </div>
        </Row>
        <Row />
        <Row>
          <ReactSwiper options={this.swiperOptions} needNavigation needPagination>
            {reasonList.map(({ reason, percent }) => (<RentReasonCard key={reason} reason={reason} value={percent} />))}
          </ReactSwiper>
        </Row>
        <Row style={{ marginTop: 25 }}>
          <ReactEcharts option={option} notMerge lazyUpdate style={{ height: 295 }} />
          <div style={{ position: 'absolute', top: 0, right: 32 }}>
            <TimeSelectorWithMonthYear startDate={startDate} endDate={endDate} onChange={this.dateOnChange} />
          </div>
        </Row>
      </PageHeaderWrapper>
    )
  }
}

export default StatisticalAnalysisOfFefund;
