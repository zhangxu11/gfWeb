import React, { Component } from 'react';
import { Row, Col, Spin, Modal } from 'antd';
import BannerAnim, { Element } from 'rc-banner-anim';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';

const BgElement = Element.BgElement;
@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class EnergyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      data: {
        address: '-',
        completionTime: '-',
        constructionBudget: '-',
        constructionSettlement: '-',
        constructionType: '-',
        constructionTypeStr: '-',
        createTime: '-',
        createUser: '-',
        currentStep: '-',
        currentStepStr: '-',
        dataStatus: '-',
        fileId: '-',
        fileList: [],
        investmentAmount: '-',
        projectCode: '-',
        projectName: '-',
        startTime: '-',
        transferUnit: '-',
      },
      loading: true
    };
  }
  componentWillMount() {
    const projectCode = this.props.location.query.projectCode;
    this.setState({ projectCode });
    this.getFeeMaintainById(projectCode);
  }
  getFeeMaintainById(projectCode) {
    axios.get(Api.detailProject, {
      params: { projectCode }
    }).then(res => {
      this.setState({
        data: res.data,
        loading: false
      });
    });
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div style={{
          marginBottom: 50
        }}>
          <Spin spinning={this.state.loading}>
            <Row className={styles.w1120}>
              <div className={`pull-left ${styles.w1120}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className="contentHeaderTitle">
                      <p>基本信息</p>
                      <p>BASIC INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div>
                  <BannerAnim prefixCls='buildingDetailsBannerItem'>
                    {
                      this.state.data.fileList.map((item, index) => {
                        return (
                          <Element
                            key={index}
                          >
                            <BgElement
                              key="bg"
                              style={{
                                backgroundImage: `url("http://gisams-hitech.cindata.cn/res_file/${item.filePath}")`,
                                backgroundPosition: 'center',
                                height: 260
                              }}
                            />
                          </Element>
                        );
                      })
                    }
                  </BannerAnim>
                </div>
                <div className={styles.br1}>
                  <div>
                    <span>项目名称：{this.state.data.projectName}</span>
                    &emsp;
               <span>项目编号：{this.state.data.projectCode}</span>
                    &emsp;
               <span>坐落地址：{this.state.data.address}</span>
                  </div>
                </div>
              </div>
              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>期限信息</p>
                      <p>DATE INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1} style={{
                  height: 'auto'
                }}>
                  <span>开工时间：{this.state.data.startTime}</span>
                  &emsp;
                  <span>竣工时间：{this.state.data.completionTime}</span>
                  &emsp;
                  <span>当前阶段：{this.state.data.currentStepStr}</span>
                </div>
              </div>
              <div className={`pull-left ${styles.w560}`}>
                <div className='mainContainer'>
                  <div className="contentHeader">
                    <div className={`contentHeaderTitle ${styles.small}`}>
                      <p>投资信息</p>
                      <p>MONEY INFORMATION</p>
                    </div>
                  </div>
                </div>
                <div className={styles.br1} style={{
                  height: 'auto'
                }}>
                  <span>工程预算：{this.state.data.constructionBudget}</span>
                  &emsp;
                  <span>投资额度：{this.state.data.investmentAmount}</span>
                  &emsp;
                  <span>工程结算：{this.state.data.constructionSettlement}</span>
                </div>
              </div>
            </Row>
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default EnergyDetails;
