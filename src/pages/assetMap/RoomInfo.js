import React, { Component } from 'react';
import axios from 'axios';
import Api from '../../services/apis';
import moment from 'moment';

import styles from "./style.less";

class RoomInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      contractData: {},
      // 合同金额
      contractSum: 0,
      // 装修状况
      renovationCondition: '',
      houseInfo: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== undefined) {
      this.setState({
        data: nextProps.data
      });
    };
    if (nextProps.data !== this.props.data) {
      this.getContractId(nextProps.data.house_id);
      this.getHouseInfo(nextProps.data.house_id);
    }
  }

  componentWillMount() {
    if (this.props.data !== undefined) {
      this.setState({
        data: this.props.data
      });
      this.getHouseInfo(this.props.data.house_id);
    };
  }

  componentDidMount() {
    this.getContractId(this.props.data.house_id)
  }
  // 获取合同id
  getContractId = (houseCode) => {
    let contractCount = 0;
    axios
      .post(Api.getContractId, {
        houseCode
      })
      .then(res => {
        if (res.data === null) {
          message.warn('暂无数据！');
        } else {
          if (res.data.end_date && res.data.start_date && res.data.rent_area && res.data.rent_price) {
            const time = moment(res.data.end_date).diff(moment(res.data.start_date), 'days');
            contractCount = time * Number(res.data.rent_area) * Number(res.data.rent_price)
          }
          this.setState({
            contractData: res.data,
            contractSum: contractCount
          });
        }
      });
    axios
      .get(Api.getHouseMessage, {
        params: {
          houseCode
        }
      })
      .then(res => {
        if (res.data === null) {
          message.warn('暂无数据！');
        } else {
          this.setState({
            renovationCondition: res.data.renovationCondition ? res.data.renovationCondition : '-'
          })
        }
      });

  }

  handleMenuClick = (url) => {
    var win = window.open(url, '_blank');
    win.focus();
  }
  getHouseInfo = (houseId) => {
    if (!houseId) return;
    axios.get(Api.getHouseMessage, {
      params: {
        houseCode: houseId
      }
    }).then((res) => {
      this.setState({ houseInfo: res.data })
    }).catch(() => {
      notification.destroy();
      notification.warning({ message: '获取房间信息失败！' });
    });
  }

  render() {
    const { houseInfo } = this.state;
    return (
      <div className={styles.roomInfo}>
        <div className={styles.filterItem} style={{ fontSize: '14px', paddingTop: '20px' }}>
          <b style={{ fontWeight: 'bold' }}>{houseInfo.houseName2 ? houseInfo.houseName2 : '-'}</b> &emsp;{houseInfo.buildingArea ? houseInfo.buildingArea : 0}㎡
        </div>
        <div className={styles.filterItem}>
          <div className={styles.filterHeader}>摘要
            <a className={styles.option} href="javascrip:;" onClick={this.handleMenuClick.bind(this, `/fe/assetMap/houseDetails?houseCode=${this.props.data.house_id}`)}>详情</a>
          </div>
          <div className={styles.filterInner}>
            <span title=''>租赁状况：{this.state.data.isRent}</span>
            <span title=''>装修状况：{this.state.renovationCondition}</span>
          </div>
        </div>
        <div className={styles.filterItem}>
          <div className={styles.filterHeader}>租赁合同
            <a className={styles.option} href="javascrip:;" onClick={this.handleMenuClick.bind(this, `/fe/dataMaintenance/operationDetails?contractId=${this.state.contractData.contract_id ? this.state.contractData.contract_id : null}`)}>详情</a>
          </div>
          <div className={styles.filterInner}>
            <span title={this.state.contractData.renter_name}>承租方：{this.state.contractData.renter_name}</span>
            <span title={this.state.contractData.rent_area}>租赁面积：{this.state.contractData.rent_area}㎡</span>
            <span title={this.state.contractData.start_date}>开始日期：{this.state.contractData.start_date}</span>
            <span title={this.state.contractData.end_date}>结束日期：{this.state.contractData.end_date}</span>
            <span title={this.state.contractSum}>合同金额：{this.state.contractSum}元</span>
            <span title={this.state.contractData.rent_price}>租金单价：{this.state.contractData.rent_price}/元㎡日</span>
          </div>
        </div>
      </div>
    );
  }
}

export default RoomInfo;
