/*
* @Author: 程家兴
* @Date: 2019-05-08 16:12:16
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-13 22:07:52
* @File: 招商分析-柱图组件
*/
import React, { Component } from 'react';
import { Bar } from '@/components/Charts'
import ReactEcharts from 'echarts-for-react';
import moment from 'moment';
import TimeSelectorWithMonthYear from '@/components/TimeSelector';
import styles from "./style.less";
import {
    Card
} from 'antd';
import axios from 'axios';
import Api from '../../../services/apis';
// const { RangePicker } = DatePicker;
// const { Search } = Input;

export default class InvestmentAnalysisBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noTitleKey: 'tab1',
            startDate: moment().startOf('year'), // 开始时间
            endDate: moment().endOf('year'), // 结束时间
            xAxisData: [], // x轴坐标
            seriesData: [], // x轴数据
            type: "潜在企业"
        }

    }
    componentDidMount() {
        this.getData(this.state.startDate, this.state.endDate, '潜在企业');
    }
    buildXAxisData = (data) => {
        if (data && data.length > 0) {
            return data.map(d => d.substr);
        }
    }

    buildSeriesData = (data) => {
        if (data && data.length > 0) {
            return data.map(d => {
                return {
                    name: d.substr,
                    value: d.num
                };
            })
        }
    }
    getData = (startDate, endDate, type) => {
        const startDateYear = moment().startOf('year');
        const endDateYear = moment().endOf('year');
        let checkYear = false;
        if (startDate.isSame(startDateYear, 'day') && endDate.isSame(endDateYear, 'day')) checkYear = true;
        axios.get(Api.getRenterBar, {
            params: {
                sDate: startDate.format("YYYY-MM-DD"),
                eDate: endDate.format("YYYY-MM-DD"),
                level: "period",
                enterpriseType: type
            }
        }).then((res) => {
            if (res.status === 200) {
                const data = res.data.list;
                const xAxisData = this.buildXAxisData(data);
                const seriesData = this.buildSeriesData(data);
                this.setState({
                    xAxisData,
                    seriesData,
                    noTitleKey: type,
                    type: type
                });
            }
        })
    }
    onTabChange = (key, type) => {
        this.getData(this.state.startDate, this.state.endDate, key);
    }
    dateOnChange = (startDate, endDate) => {
        this.getData(startDate, endDate, this.state.type);
        this.setState({
            startDate,
            endDate,
        });
    }
    render() {
        const tabList2 = [{
            key: '潜在企业',
            tab: '潜在企业'
        }, {
            key: '意向企业',
            tab: '意向企业'
        }, {
            key: '在租企业',
            tab: '在租企业'
        }, {
            key: '流失企业',
            tab: '流失企业'
        }]
        const { xAxisData, seriesData } = this.state;
        const option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            title: {
                show: true,
                text: '客户走势图',
                textStyle: {
                    color: 'rgba(0, 0, 0, 0.427450980392157)',
                    fontSize: 14,
                    fontWeight: 400,
                    fontStyle: 'normal',
                },
            },
            xAxis: [
                {
                    type: 'category',
                    data: xAxisData,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                },
            ],
            series: [
                {
                    name: '增长量',
                    type: 'bar',
                    barWidth: 18,
                    itemStyle: {
                        color: '#FF7476'
                    },
                    data: seriesData
                },
            ]
        };
        const reactEcharts = <ReactEcharts option={option} notMerge={true} lazyUpdate={true} style={{ height: 295 }} />
        const contentList = {
            '潜在企业': reactEcharts,
            '意向企业': reactEcharts,
            '在租企业': reactEcharts,
            '流失企业': reactEcharts,
        }
        return (
            <Card style={{ width: '100%', marginBottom: 20, height: 200 }}
                bordered={false}
                tabList={tabList2}
                activeTabKey={this.state.noTitleKey}
                onTabChange={(key) => {
                    this.onTabChange(key, 'noTitleKey');
                }}>
                {
                    seriesData && seriesData.length > 0 ? contentList[this.state.noTitleKey] : <div className={styles.noData}>暂无数据</div>
                }
                <div style={{ position: 'absolute', top: 10, right: 32 }}>
                    <TimeSelectorWithMonthYear startDate={this.state.startDate} endDate={this.state.endDate} onChange={this.dateOnChange} />
                </div>
            </Card>
        );
    }

}