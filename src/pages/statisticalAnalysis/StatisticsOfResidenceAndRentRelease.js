/*
 * @Author: 程家兴
 * @Date: 2019-05-06 13:50:01 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-14 08:56:53
 * @file 进驻退租统计
 */
import React, { Component } from 'react';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import styles from './style.less'
import { Row, Col } from 'antd';
import Rank from './StatisticsOfResidenceAndRentReleasePackage/StatisticsOfResidenceAndRentReleaseRank';
import Line from './StatisticsOfResidenceAndRentReleasePackage/StatisticsOfResidenceAndRentReleaseLine';
import Bar from './StatisticsOfResidenceAndRentReleasePackage/StatisticsOfResidenceAndRentReleaseBar';
import Trend from './StatisticsOfResidenceAndRentReleasePackage/StatisticsOfResidenceAndRentReleaseTrend';

@connect(({ user, loading }) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))
export default class StatisticsOfResidenceAndRentRelease extends Component {
    constructor(props) {
        super(props);
        this.userCode = sessionStorage.getItem('currentUser');
        this.state = {
            line1Data: [],
            line2Data: [],
            bar1Data: [],
            bar2Data: [],
            rank1Data: [],
            rank2Data: [],
            rank3Data: [],
            trendData: []
        };
    }

    componentDidMount() {
        Promise.all([
            axios.post(Api.queryInCom),
            axios.post(Api.queryInCom1),
            axios.post(Api.queryOutCom),
            axios.post(Api.queryOutCom1),
            axios.post(Api.RankingCompanyRz),
            axios.post(Api.RankingCompanyLs),
            axios.post(Api.RankingCompanyYx),
            axios.post(Api.InOrOutCompanyFx)
        ]).then(resList => {
            this.setState({
                line1Data: resList[0].data.list,
                line2Data: resList[1].data.list,
                bar1Data: resList[2].data.list,
                bar2Data: resList[3].data.list,
                rank1Data: resList[4].data.list,
                rank2Data: resList[5].data.list,
                rank3Data: resList[6].data.list,
                trendData: resList[7].data.list,
            });
        })
    }
    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }
    render() {
        return (
            <div className={styles.StatisticsOfResidenceAndRentReleaseContent}>
                <div className="mainContainer">
                    <div className='contentHeader'></div>
                    <Row style={{ marginTop: '50px' }}>
                        <Col span={12} style={{ textAlign: 'center' }}>
                            <span style={{ padding: '12px 29px', borderBottom: '1px solid #E9E9E9' }}>入驻企业</span>
                        </Col>
                        <Col span={12} style={{ textAlign: 'center' }}>
                            <span style={{ padding: '12px 29px', borderBottom: '1px solid #E9E9E9' }}>迁出企业</span>
                        </Col>
                    </Row>
                    <Row gutter={24} style={{ marginTop: '10px' }}>
                        <Col span={6}>
                            <Line data={this.state.line1Data} title='增长' />
                        </Col>
                        <Col span={6}>
                            <Bar data={this.state.bar1Data} />
                        </Col>
                        <Col span={6}>
                            <Line data={this.state.line2Data} title='退租' />
                        </Col>
                        <Col span={6}>
                            <Bar data={this.state.bar2Data} />
                        </Col>
                    </Row>
                    <Row type="flex" justify="space-around" style={{ marginTop: '20px' }}>
                        <Col span={7}>
                            <Rank title="入驻行业排名" data={this.state.rank1Data} />
                        </Col>
                        <Col span={7}>
                            <Rank title="流失行业排名" data={this.state.rank2Data} />
                        </Col>
                        <Col span={7}>
                            <Rank title="意向行业排名" data={this.state.rank3Data} />
                        </Col>
                    </Row>
                    <Row type="flex" justify="space-around" style={{ marginTop: '60px' }}>
                        <Col span={23}>
                            <Trend data={this.state.trendData} />
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}