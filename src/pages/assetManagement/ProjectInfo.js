import * as React from 'react'
import { Table, Card, Row, Col, Form, Icon, Progress, Spin } from 'antd'
import axios from 'axios'
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { func } from 'prop-types';
import styles from "./style.less";
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import router from 'umi/router';

const FormItem = Form.Item;
@connect(({ user, project, activities, chart, loading }) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))

class ProjectInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectName: '',
            projectCode: '',
            coveredArea: 0,
            landArea: 0,
            buildCount: 0,
            houseCount: 0,
            plotRatio: 0,
            transferUnit: '',
            ownershipStatus: '',
            assetStatus: '',
            pageSize: 5,
            current: 1,
            postMsg: {},
            clickType: 'BusStation',
            poiData: [],
            headerData: [
                {
                tit: '交通',
                icon: 'environment',
                choiced: true,
                type: 'BusStation'
                },
                {
                tit: '学校',
                icon: 'team',
                choiced: false,
                type: 'Education'
                },
                {
                tit: '购物',
                icon: 'shopping-cart',
                choiced: false,
                type: 'Shopping'
                },
                {
                tit: '医院',
                icon: 'schedule',
                choiced: false,
                type: 'Hospital'
                },
                {
                tit: '公园',
                icon: 'bank',
                choiced: false,
                type: 'Park'
                },
                {
                tit: '餐饮',
                icon: 'cloud',
                choiced: false,
                type: 'Dinner'
                },
                {
                tit: '电影院',
                icon: 'thunderbolt',
                choiced: false,
                type: 'Cinema'
                },
            ],
            choicedIndex: 0,
            listData: [
                {
                title: '',
                distance: '',
                details: ''
                }
            ],
            tableData: [
                {
                    key: '',
                    name: '',
                    company: '',
                    progress: '',
                    jgdate: '',
                    startdate: '',
                    enddate: '',
                    people: '',
                }
            ],
            modalVisible: false

        }
    }

    componentWillMount() {
        const projectCode = this.props.location.query.projectCode;
        this.sendData(projectCode);

        axios.post(Api.getCommunityList, {
            projectCode: projectCode
        }).then((data) => {
            this.setState({ 
                totalData: data,
                postMsg:{
                    type: 'itemclickPOI',
                    data,
                    poiType:'BusStation'
                }
             });
        });
    }

    sendPOI(data) {
      this.setState({
        postMsg: {
          type: 'clickPOI',
          data
        }
      });
    }

    sendData = (projectCode) => {
        const that = this;
        this.setState({ loading: true })
        axios.post(Api.amGetProInfo, {
            projectCode: projectCode
        }).then(function (resp) {
            const { data } = resp;
            if (data.buildList && data.buildList.length !== 0) {
                data.buildList.forEach((item, index) => {
                    item.key = index;
                })
            }
            that.setState({
                projectName: data.projectName,
                projectCode: data.projectCode,
                coveredArea: data.coveredArea,
                buildCount: data.buildCount,
                houseCount: data.houseCount,
                plotRatio: data.plotRatio,
                transferUnit: data.transferUnit,
                assetStatus: data.assetStatus,
                buildList: data.buildList,
            })
            that.setState({ loading: false })
        })
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    changeList(index) {
        let headerData = this.state.headerData;
        headerData[this.state.choicedIndex].choiced = false;
        headerData[index].choiced = true;
        this.setState(prevState=>
            ({
                headerData,
                choicedIndex: index,
                clickType: headerData[index].type,
                postMsg: {
                    type: 'itemclickPOI',
                    poiType: headerData[index].type,
                    data: prevState.totalData
                }
            })
        );
    }
    
    onReceiveMessage(data) {
        this.setState({
            poiData: data.data
        });
    }

    handleMenuClick = (building) => {
        var win = window.open(`/fe/assetMap/buildingDetails?buildingCode=${building.buildingCode}&buildingId=${building.buildingId}`, '_blank');
        win.focus();
    }

    render() {
        const that = this;
        const { currentUserLoading } = this.props;

        const columns = [
            {
                title: '楼栋名称',
                dataIndex: 'buildingName2',
                key: 'buildingName2',
                /* render: (text, record) => (
                    <span>
                        <a href="/WarrantManagement">{text}</a>
                    </span>
                ), */
            },
            {
                title: '楼栋号',
                dataIndex: 'buildingCode',
                key: 'buildingCode',
            },
            {
                title: '总层数',
                dataIndex: 'totalFloor',
                key: 'totalFloor',
            },
            {
                title: '总面积',
                dataIndex: 'buildingArea',
                key: 'buildingArea',
            },
            {
                title: '房产数',
                key: 'houseCount',
                dataIndex: 'houseCount',
            },
            {
                title: '容积率',
                key: 'plotRatio',
                dataIndex: 'plotRatio',
            },
            {
                title: '操作',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a href="javascript:;" onClick={this.handleMenuClick.bind(this, record)}>
                            详情
                        </a>
                    </span>
                ),
            },
        ];

        return (
            <PageHeaderWrapper
                loading={currentUserLoading}
            >
                <Spin spinning={this.state.loading}>
                    <div className={styles.cdMain}>
                        <div className='mainContainer'>
                            <div className='contentHeader'>
                                <div className="contentHeaderTitle">
                                    <p>{this.state.projectName}</p>
                                    <p>PROJECT NAME</p>
                                </div>
                            </div>
                        </div>
                        <div className={styles.w980}>
                            <Card>
                                <Form layout='inline'>
                                    <Row>
                                        <Col span={8}>
                                            <FormItem label="项目编号：">{this.state.projectCode}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="总建面：">{this.state.coveredArea}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="楼栋数量：">{this.state.buildCount}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="房产数量：">{this.state.houseCount}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="容积率：">{this.state.plotRatio}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="划转单位：">{this.state.transferUnit}</FormItem>
                                        </Col>
                                        <Col span={8}>
                                            <FormItem label="资产状态：">{this.state.assetStatus}</FormItem>
                                        </Col>
                                    </Row>
                                </Form>
                            </Card>
                        </div>
                        <div className='mainContainer'>
                            <div className='contentHeader'>
                                <div className="contentHeaderTitle">
                                    <p>楼栋列表</p>
                                    <p>BUILDING LIST</p>
                                </div>
                            </div>
                        </div>
                        <div className={styles.w980}>
                            <Table
                                bordered={true}
                                columns={columns}
                                dataSource={this.state.buildList}
                                pagination={{
                                    pageSize: 5,
                                }}
                            />
                        </div>
                        {Number(sessionStorage.getItem('isMap')) === 1 ?
                        <div>
                        <div className='mainContainer'>
                            <div className='contentHeader'>
                                <div className="contentHeaderTitle">
                                    <p>周边配套</p>
                                    <p>PERIPHERAL PACKAGE</p>
                                </div>
                            </div>
                        </div>
                            <div className={`${styles.w980} ${styles.h500}`}>
                                <IframeComm
                                attributes={{
                                    src: "/gis/mapPOI.html",
                                    style: {
                                        width: '100%',
                                        height: '100%',
                                        top: '0px',
                                        left: '0px',
                                        position: "absolute"
                                    }
                                }}
                                postMessageData={this.state.postMsg}
                                handleReceiveMessage={this.onReceiveMessage.bind(this)}
                                />
                                <div className={styles.mapList}>
                                <div className={styles.mapListHeader}>
                                    <Row className={styles.mapListHeaderRow}>
                                    {this.state.headerData.map((item, index) => {
                                        return (
                                        <Col span={6} className={item.choiced ? styles.checked : ''} onClick={this.changeList.bind(this, index)} key={index}>
                                            <Icon type={item.icon} />
                                            <br />
                                            {item.tit}
                                        </Col>
                                        );
                                    })}
                                    </Row>
                                </div>
                                <div className={styles.mapListContent}>
                                    <QueueAnim>
                                    {this.state.poiData !== undefined ? this.state.poiData.map((item, index) => {
                                        return (
                                        <div className={styles.mapListItem} key={index} onClick={this.sendPOI.bind(this,item.popName)}>
                                            <div className='clearfix'>
                                            <div className="pull-left">
                                                {item.popName.poiName}
                                            </div>
                                            <div className="pull-right">
                                                <Icon type='environment' />
                                                &nbsp;
                                                {item.popName.distance}米
                                            </div>
                                            </div>
                                            <p>
                                            {item.address}
                                            </p>
                                        </div>
                                        );
                                    }) : []}
                                    </QueueAnim>
                                </div>
                                </div>
                            </div>
                            </div>
                            :
                            null
                        }
                    </div>
                </Spin>
            </PageHeaderWrapper>

        )
    }
}

export default ProjectInfo;
