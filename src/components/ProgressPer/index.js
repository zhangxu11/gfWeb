import React, { Component } from 'react';
import classNames from 'classnames';
import { Progress } from 'antd';
import styles from './style.less';
import router from 'umi/router';

export class ProgressPer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onRentChartClick = (params) => {
    if (this.props.type) {
      router.push({
        pathname: '/fe/assetManagement/houseManagement',
        state: {
          isRent: params
        }
      });
    }
  }
  render(){
    const {data, bodyStyle, width = 120, titleSize, countSize, type} = this.props;
    const percent = data.count ? Number(data.count.replace('%', '')) : 0;
    return (
      <div className={classNames(styles.progress, bodyStyle || null)} onClick={() => this.onRentChartClick(type)}>
        <Progress type="circle"
          percent={percent}
          width={width}
          strokeColor="#ff6c74"
          strokeWidth={12}
          format={(percent) => (
            <div>
              <p className={styles.progressCount} style={{ fontSize: `${countSize}px` }}>
                {data.count}
              </p>
              <p className={styles.progressTitle} style={{ fontSize: `${titleSize}px` }}>
                {data.name}
              </p>
            </div>
          )} />
      </div>
    )
  }
}