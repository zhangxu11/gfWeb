import { routerRedux } from 'dva/router';
import { stringify } from 'qs';
import { fakeAccountLogin, getFakeCaptcha } from '@/services/api';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import Api from '../services/apis';
import axios from 'axios';
import router from 'umi/router';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Spin,
  Select
} from 'antd';
export default {
  namespace: 'login',

  state: {
    status: undefined,
    canCommit: false,
    canAapproval: false,
  },

  effects: {
    *login({ payload }, { call, put }) {

      let _payload = {
        userCode: payload.userName,
        userPassword: payload.password,
        type: payload.type
      }
      const response = yield call(fakeAccountLogin, _payload);

      yield put({
        type: 'changeLoginStatus',
        payload: response,
      });
      // let that = this;
      // axios.post(Api.login, {
      //   userCode: payload.userName,
      //   userPassword: payload.password
      // })
      // .then(function (resp) {
      //   if(resp.data) {
      //     
      //     if(resp.data.code == 1) {
      //       reloadAuthorized();
      //       let arr = [];

      //       setAuthority(arr.push(resp.data.userInfo.userCode).toString());
      //       routerRedux.replace('/homePage')
      //       notification.destroy();
      //       notification['success']({message:'登录成功'});
      //       // router.push({
      //       //   pathname: '/homePage',
      //       //   state: ''
      //       // })
      //     }else {
      //       notification.destroy();
      //       notification['error']({message:'登录失败'});
      //     }

      //   }



      // });


      // Login successfully

      if (response.code == 1) {
        reloadAuthorized();
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        let { redirect } = params;
        sessionStorage.setItem('currentUser', response.userInfo.userCode);
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = redirect;
            return;
          }
        }
        yield put(routerRedux.replace(redirect || '/'));
      } else {
        notification.destroy();
        notification['error']({ message: '登录失败' });
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    *logout(_, { put }) {
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          currentAuthority: 'guest',
        },
      });
      reloadAuthorized();
      sessionStorage.setItem('currentUser', '');

      yield put(
        routerRedux.push({
          pathname: '/fe/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        })
      );
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      sessionStorage.setItem('isMap', payload.isMap);
      let dangan = false;
      let key = false;
      let canCommit = false;
      let canApproval = 0;
      payload.funs.map((item, index)=> {
        if(item.functionName == 'approvalOne') {
          canApproval = 1;
        }
        if(item.functionName == 'approvalTwo') {
          canApproval = 2;
        }
        if(item.functionName == 'approvalThree') {
          canApproval = 3;
        }
        if(item.functionName == 'approvalFour') {
          canApproval = 4;
        }
        if(item.functionCode == '1104') {
          canCommit = true;
        }
        if(item.functionCode == '1001') {
          dangan = true;
        }
        if(item.functionCode == '1002') {
          key = true;
        }
      })
      sessionStorage.setItem('dangan', dangan);
      sessionStorage.setItem('key', key);
      if (canCommit !== false) sessionStorage.setItem('canCommit', canCommit);
      if (canApproval !== false) sessionStorage.setItem('canApproval', canApproval);
      if (Number(payload.isMap) === 1) {
        payload.currentAuthority = 'admin';
      } else {
        payload.currentAuthority = 'user';
      };
      sessionStorage.setItem('currentAuthority', payload.currentAuthority);
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
