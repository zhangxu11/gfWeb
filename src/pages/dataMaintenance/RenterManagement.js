import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Icon,
  Input,
  Table,
  Button,
  Form,
  Tabs,
  Spin,
  Select,
  Upload
} from 'antd';

import Link from 'umi/link';
import axios from 'axios';
import router from 'umi/router';
import Api from '@/services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { formatTreeData } from '@/components/commonAips';
import styles from './style.less'

const { TextArea } = Input;
const { confirm } = Modal;
const { Option } = Select;
const FormItem = Form.Item;
const { MonthPicker } = DatePicker;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class RenterManagement extends React.Component {
  constructor(props) {
    super(props);
    // const token = sessionStorage.getItem('tokenData') ? sessionStorage.getItem('tokenData') : '';
    this.state = {
      pageSize: 2,
      current: 1,
      tableData: [],
      modalVisible: false,
      selectdIds: [],
      ownerStatus: [],
      ownerType: [],
      signStatus: [],
      assetStatus: [],
      submitFlag: '',
      showDownload: false,
      downloadName: '',
      downloadLink: '',
      startDownload: false,
      orgTreeData: [],
      fileIds: [],
      fileList: [],
      enterprisePic: [],
      uploadState: 0,
      // 企业登记机关机构数据
      registrationAuthorityAreaData: [],
      searchParams: {
        renterName: '',
        startDate: '',
        endDate: '',
        tranUnits: '',
        code: ''
      },
    };
  }

  componentWillMount() {
    this.setState({
      searchParams: {
        renterName: '',
        startDate: '',
        endDate: '',
        tranUnits: '',
        code: this.props.location.state && this.props.location.state.code ? this.props.location.state.code : ''
      }
    }, () => {
      this.sendData();
    })
    const that = this;

    // 获取系统参数
    // 获取部门树

    axios.get(Api.getIndustryTree, {}).then((resp) => {
      const { data } = resp;
      if (data) {
        const treeData = new Array(data)[0];
        formatTreeData(treeData, 'name', 'orgLevelFormat', 'industryId', 'industryList');
        console.log('yyyyyyyyyy', treeData)
        that.setState({ orgTreeData: treeData });
      }
    });
    axios.post(Api.getOptionList, {
      formField: "registrationAuthorityArea",
    })
      .then((function (resp) {
        const data = resp.data;
        if (data.length === 0) {
          notification.destroy();
          notification['warning']({ message: '暂无数据' })
        }
        that.setState({
          registrationAuthorityAreaData: data.list,
        })
      }))
  }

  // 获取表格数据
  sendData = () => {
    const params = {
      ...this.state.searchParams,
      currentPage: this.state.current
    };
    this.setState({ loading: true });
    axios.post(Api.getRenterList, params).then((resp) => {
      const { data } = resp;
      if (data.list.length === 0) {
        notification.destroy();
        notification.warning({
          message: '暂无数据',
        });
      }
      data.list.forEach((d, i) => {
        d.key = i;
        d.receivedDate = moment(d.receivedDate).format("YYYY-MM-DD HH:mm:ss")
      })
      this.setState({
        tableData: data.list,
        pageSize: data.pageSize,
        current: data.currentPage,
        total: data.totalSize
      })
      this.setState({ loading: false })
    })
  };



  // 查询
  query = () => {
    this.setState({ current: 1 }, () => {
      this.sendData();
    });
  };

  // 获取项目
  getProject = (id) => {
    this.setState({ loading: true })
    this.setState({ renterId: id })
    const that = this;
    axios.post(Api.getRenterDetailInfoById, { renterId: id })
      .then((resp) => {
        const { data } = resp;
        that.props.form.setFieldsValue({
          renterName: data.renterName,
          legalPerson: data.legalPerson,
          linkman: data.linkman,
          cardNum: data.cardNum,
          hyType: data.hy_type,
          contactWay: data.r_contactWay,
          mailbox: data.mailbox,
          mainBusiness: data.mainBusiness,
          investmentAmount: data.investmentAmount,
          peopleNum: data.peopleNum,
          renterSituation: data.renterSituation,
          high: data.high,
          establishedTime: moment(data.establishedTime),
          fullIncome: data.fullIncome,
          income: data.income,
          zcType: data.zcType,
          registrationAuthority: data.registrationAuthority,
          registrationAuthorityArea: data.registrationAuthorityArea,
          registeredCapital: data.registeredCapital,
          registeredAddress: data.registeredAddress,
          intro: data.intro,
          remark: data.remark,
          manager: data.manager,

          openingBank: data.openingBank,
          accountNumber: data.accountNumber,
          i_contactWay: data.i_contactWay,
          i_nsrsbh: data.i_nsrsbh,
          address: data.address,
          i_updateDate: moment(data.i_updateDate),

          shxydm: data.shxydm,
          b_nsrsbh: data.b_nsrsbh,
          registrationNumber: data.registrationNumber,
          orgNum: data.orgNum,
          b_updateTime: moment(data.b_updateTime),
          businessRegistAddress: data.businessRegistAddress,

          bachelorNum: data.bachelorNum,
          masterNumber: data.masterNumber,
          doctorNumber: data.doctorNumber,
          abroadNumber: data.abroadNumber,
          personnelNumber: data.personnelNumber,
          partyMember: data.partyMember,
          tutor: data.tutor,
          totalNumber: data.totalNumber,
          enterpriseType: data.enterpriseType,
          // enterprisePic: data.enterprisePic
        });

        // const files = [];
        const { fileList } = data;
        const files = fileList && fileList.length > 0 && fileList.map(item => ({
          uid: item.fileId,
          id: item.fileId,
          url: `http://gisams-hitech.cindata.cn/res_file${item.filePath}`,
          name: item.uploadName,
          status: 'done'
        }))
        const pics = [{
          uid: 1,
          id: 1,
          url: data.picurl ? `http://gisams-hitech.cindata.cn/res_file${data.picurl}` : '',
          name: data.renterName,
          status: 'done'
        }]
        that.setModalVisible(true, 'edit', id);
        that.setState({
          loading: false,
          fileList: files,
          enterprisePic: pics
        });
      })
  };

  // 删除项目
  deleteProject = (id) => {
    const that = this;
    confirm({
      title: "删除确认",
      content: '确认删除该项目？',
      onOk() {
        that.setState({ loading: true })
        axios.post(Api.deleteRenter, { renterId: id })
          .then((resp) => {
            const { data } = resp;
            if (data.flag == 1) {
              notification.destroy();
              notification.success({ message: data.msg });
              that.sendData();
            } else {
              notification.destroy();
              notification.warning({ message: data.msg });
            }
            that.setState({ loading: false })
          })
      }
    })
  }

  // 编辑,新建项目(点弹框确定)
  editProject = () => {
    const that = this;
    this.setState({
      loading: true,
    })
    const { fileIds } = this.state;
    this.state.fileList && this.state.fileList.length > 0 && this.state.fileList.forEach(item => {
      if (item.id) {
        fileIds.push(item.id)
      }
    })
    if (this.state.submitFlag == 'add') {
      this.setState({
        enterprisePic: []
      })
      this.props.form.validateFields((err, values) => {
        if (!err) {
          axios.post(Api.createRenter, {
            renterName: values.renterName,
            legalPerson: values.legalPerson,
            linkman: values.linkman,
            cardNum: values.cardNum,
            hyType: values.hyType,
            r_contactWay: values.contactWay,
            mailbox: values.mailbox,
            mainBusiness: values.mainBusiness,
            investmentAmount: values.investmentAmount,
            peopleNum: values.peopleNum,
            renterSituation: values.renterSituation,
            high: values.high,
            establishedTime: values.establishedTime,
            fullIncome: values.fullIncome,
            income: values.income,
            zcType: values.zcType,
            registrationAuthority: values.registrationAuthority,
            registrationAuthorityArea: values.registrationAuthorityArea,
            registeredCapital: values.registeredCapital,
            registeredAddress: values.registeredAddress,
            intro: values.intro,
            remark: values.remark,
            manager: values.manager,

            openingBank: values.openingBank,
            accountNumber: values.accountNumber,
            i_contactWay: values.i_contactWay,
            i_nsrsbh: values.i_nsrsbh,
            address: values.address,
            i_updateDate: values.i_updateDate,
            shxydm: values.shxydm,
            b_nsrsbh: values.b_nsrsbh,
            registrationNumber: values.registrationNumber,
            orgNum: values.orgNum,
            b_updateTime: values.b_updateTime,
            businessRegistAddress: values.businessRegistAddress,
            bachelorNum: values.bachelorNum,
            masterNumber: values.masterNumber,
            doctorNumber: values.doctorNumber,
            abroadNumber: values.abroadNumber,
            personnelNumber: values.personnelNumber,
            partyMember: values.partyMember,
            tutor: values.tutor,
            totalNumber: values.totalNumber,
            enterpriseType: values.enterpriseType,
            fileIds: fileIds.join(','),
          })
            .then((resp) => {
              const { data } = resp;
              if (data.flag == 1) {
                notification.destroy();
                notification.success({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else if (data.flag == 0) {
                notification.destroy();
                notification.error({ message: data.msg });
              }
              that.setState({ loading: false })
            })
        }
      })
    } else {
      this.props.form.validateFields((err, values) => {
        console.log(values.hyType)
        if (!err) {
          axios.post(Api.updateRenter, {
            id: this.state.renterId,
            renterName: values.renterName,
            legalPerson: values.legalPerson,
            linkman: values.linkman,
            cardNum: values.cardNum,
            hyType: values.hyType,
            r_contactWay: values.contactWay,
            mailbox: values.mailbox,
            mainBusiness: values.mainBusiness,
            investmentAmount: values.investmentAmount,
            peopleNum: values.peopleNum,
            renterSituation: values.renterSituation,
            high: values.high,
            establishedTime: values.establishedTime,
            fullIncome: values.fullIncome ? values.fullIncome : '',
            income: values.income,
            zcType: values.zcType,
            registrationAuthority: values.registrationAuthority,
            registrationAuthorityArea: values.registrationAuthorityArea,
            registeredCapital: values.registeredCapital,
            registeredAddress: values.registeredAddress,
            intro: values.intro,
            remark: values.remark,
            manager: values.manager,
            openingBank: values.openingBank,
            accountNumber: values.accountNumber,
            i_contactWay: values.i_contactWay,
            i_nsrsbh: values.i_nsrsbh,
            address: values.address,
            i_updateDate: values.i_updateDate,

            shxydm: values.shxydm,
            b_nsrsbh: values.b_nsrsbh,
            registrationNumber: values.registrationNumber,
            orgNum: values.orgNum,
            b_updateTime: values.b_updateTime,
            businessRegistAddress: values.businessRegistAddress,

            bachelorNum: values.bachelorNum,
            masterNumber: values.masterNumber,
            doctorNumber: values.doctorNumber,
            abroadNumber: values.abroadNumber,
            personnelNumber: values.personnelNumber,
            partyMember: values.partyMember,
            tutor: values.tutor,
            totalNumber: values.totalNumber,
            enterpriseType: values.enterpriseType,
            fileIds: fileIds.join(','),
          })
            .then((resp) => {
              const { data } = resp;
              if (data.flag == 1) {
                notification.destroy();
                notification.success({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else if (data.flag == 0) {
                notification.destroy();
                notification.error({ message: data.msg });
              }
              that.setState({ loading: false })
            })
        }
      })
    }
    this.setState({ renterId: 0 })
  };

  cancel = () => {
    this.setModalVisible(false)
    this.setState({ loading: false })
  }
  // 导出
  export = () => {
    if (this.state.selectdIds.length != 0) {
      const param = this.state.selectdIds.join(',');
      this.setState({
        startDownload: false
      });
      axios.get(Api.amExportPro, {
        params: {
          proCodes: param,
        },
        responseType: 'arraybuffer'
      }).then((res) => {
        notification.destroy();
        notification.success({ message: '开始下载！' })

        const blob = new Blob([res.data], { type: "application/vnd.ms-excel" });
        const [dLink, name] = [
          window.URL.createObjectURL(blob),
          `项目列表${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.xls`
        ];
        const link = document.createElement('a');
        link.href = dLink;
        link.download = name;
        link.click();
        this.setState({
          startDownload: true,
          downloadLink: dLink,
          downloadName: name,
          showDownload: true
        });
      }).catch((err) => {
        notification.destroy();
        notification.warning({ message: `下载失败！错误信息：${err}` })
      });
    } else {
      notification.destroy();
      notification.warning({ message: '请选择表格中的项后进行导出！' })
    }
  };

  showDownload(showDownload) {
    this.setState({ showDownload });
  }

  changeState = (key, value) => {
    const searchParams = { ...this.state.searchParams };
    searchParams[key] = value;
    this.setState({ searchParams });
  }

  pageChange = page => {
    this.setState(
      {
        current: page,
      },
      () => {
        this.sendData();
      }
    );
  };

  setModalVisible = (visible, type) => {
    const that = this;
    this.setState({
      modalVisible: visible,
      submitFlag: type,
      enterprisePic: []
    });
    if (type !== 'add' && visible) {
      this.setState({
        uploadState: 0
      })
    } else {
      this.setState({
        uploadState: 1
      })
      that.props.form.setFieldsValue({
        renterName: '',
        legalPerson: '',
        linkman: '',
        cardNum: '',
        hyType: '',
        contactWay: '',
        mailbox: '',
        mainBusiness: '',
        investmentAmount: '',
        peopleNum: '',
        renterSituation: '',
        high: '',
        establishedTime: '',
        fullIncome: '',
        income: '',
        zcType: '',
        registrationAuthority: '',
        registrationAuthorityArea: '',
        registeredCapital: '',
        registeredAddress: '',
        intro: '',
        remark: '',
        manager: '',
        openingBank: '',
        accountNumber: '',
        i_contactWay: '',
        i_nsrsbh: '',
        address: '',
        i_updateDate: '',
        shxydm: '',
        b_nsrsbh: '',
        registrationNumber: '',
        orgNum: '',
        b_updateTime: '',
        businessRegistAddress: '',
        bachelorNum: '',
        masterNumber: '',
        doctorNumber: '',
        abroadNumber: '',
        personnelNumber: '',
        partyMember: '',
        tutor: '',
        totalNumber: '',
        enterpriseType: ''
      })
    }
    if (!visible) {
      that.props.form.setFieldsValue({
        renterName: '',
        legalPerson: '',
        linkman: '',
        cardNum: '',
        hyType: '',
        contactWay: '',
        mailbox: '',
        mainBusiness: '',
        investmentAmount: '',
        peopleNum: '',
        renterSituation: '',
        high: '',
        establishedTime: '',
        fullIncome: '',
        income: '',
        zcType: '',
        registrationAuthority: '',
        registrationAuthorityArea: '',
        registeredCapital: '',
        registeredAddress: '',
        intro: '',
        remark: '',
        manager: '',
        openingBank: '',
        accountNumber: '',
        i_contactWay: '',
        nsrsbh: '',
        address: '',
        i_updateDate: '',
        shxydm: '',
        nsrsbh: '',
        registrationNumber: '',
        orgNum: '',
        b_updateTime: '',
        businessRegistAddress: '',
        bachelorNum: '',
        masterNumber: '',
        doctorNumber: '',
        abroadNumber: '',
        personnelNumber: '',
        partyMember: '',
        tutor: '',
        totalNumber: '',
        enterpriseType: ''
      })
      this.setState({ renterId: null });
    }

    this.setState({ fileIds: [], fileList: [] })
  };

  changeSelected = (selectedRowKeys, selectedRows) => {
    const selectdIds = [];
    selectedRows &&
      selectedRows.forEach((d, i, arr) => {
        selectdIds.push(d.projectCode);
      });

    this.setState({ selectedArr: selectedRowKeys, selectdIds });
  };


  handleMenuClick = (project) => {
    const win = window.open(`/fe/assetManagement/projectInfo?projectCode=${project.projectCode}`, '_blank');
    win.focus();
  }

  resetForm = () => {
    this.setState({
      searchParams: {
        renterName: '',
        startDate: '',
        endDate: '',
        tranUnits: '',
        code: ''
      },
      pageSize: 5,
      current: 1,
    });
  }


  handleChange = (info) => {
    this.setState({ fileList: info.fileList })
    if (info.file.status === 'done') {
      notification.destroy();
      notification.success({ message: '上传成功！' });

      const { fileIds } = this.state;
      fileIds.push(info.file.response);
      this.setState({ fileIds });

    } else if (info.file.status === 'error') {
      notification.destroy();
      notification.error({ message: '上传失败！' });
    }
  }
  picBeforeUpload = (file) => {
    const isJPEG = file.type === 'image/jpeg';
    const isJPG = file.type === 'image/jpg';
    const isPNG = file.type === 'image/png';
    if (!(isJPEG || isJPG || isPNG)) {
      Modal.error({
        title: '只能上传JPG 、JPEG 、 PNG格式的图片~'
      })
    }

    return (isJPEG || isJPG || isPNG)
  }
  handleChange2 = (info) => {
    const { fileList } = info;
    const length = fileList.length;
    if (this.picBeforeUpload(fileList[length - 1])) {
      this.setState({
        enterprisePic: [fileList[length - 1]],
      });
    }
  }

  picClick = (info) => {
    if (info.url) {
      var win = window.open(info.url, '_blank');
      win.focus();
    } else {
      notification.warning({ message: '暂未上传企业图片，请先上传企业图片！' })
    }
  }
  handleRemove = (info) => {
    const that = this;
    const p = new Promise((resp, rej) => {
      axios.get(Api.renterdeleteFile, {
        params: {
          fileId: info.id
        }
      }).then((res) => {
        if (Number(res.data.code) === 1 || Number(info.id) === 0) {
          notification.destroy();
          notification.success({ message: res.data.msg });
          const { fileIds } = that.state;
          const index = fileIds.indexOf(info.id);
          fileIds.splice(index, 1);
          that.setState({ fileIds });
          resp('success');
        } else {
          notification.destroy();
          notification.error({ message: res.data.msg });
          rej('error');
        };
      });
    });
    return p;
  }
  render() {
    const that = this;
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;

    // 表格表头
    const columns = [
      {
        title: '企业名称',
        dataIndex: 'renter_name',
        key: 'renter_name',
        render: (text, item, index) => (
          <Link
            target='_blank'
            to={{
              pathname: '/fe/dataMaintenance/renterDetails',
              search: `?id=${item.id}`
            }}
          >
            {text}
          </Link>
        ),
      },
      {
        title: '联系人',
        dataIndex: 'linkman',
        key: 'linkman',
      },
      {
        title: '联系方式',
        dataIndex: 'contact_way',
        key: 'contact_way',
      },
      {
        title: '证件类型',
        dataIndex: 'transferUnit',
        key: 'transferUnit',
        render: (text, item, index) => (
          <span>
            {'身份证'}
          </span>
        )
      },
      ,
      {
        title: '证件号码',
        dataIndex: 'card_num',
        key: 'card_num',
      },
      ,
      {
        title: '邮箱',
        dataIndex: 'mailbox',
        key: 'mailbox',
      },
      {
        title: '行业类型',
        dataIndex: 'industry_type',
        key: 'industry_type',
      },
      {
        title: '企业情况',
        dataIndex: 'renter_situation',
        key: 'renter_situation',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <span>
            <a href="javascript:;" onClick={() => this.getProject(item.id)}>编辑</a>&emsp;
            <a href="javascript:;" onClick={() => this.deleteProject(item.id)}>删除</a>
          </span>
        ),
      },
    ];

    return (
      < PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>企业列表</p>
              <p>RENTER MANAGEMENT</p>
            </div>
          </div>
          <Form layout="inline" align='center'>
            {
              this.props.location.state && this.props.location.state.code ?
                ''
                :
                <FormItem>
                  <span className="label">企业名称</span>
                  <Input
                    value={this.state.searchParams.renterName}
                    data-inp="zdbh"
                    onChange={(e) => this.changeState('renterName', e.target.value)}
                    placeholder="请输入企业名称"
                  />

                  <span className="label">行业分类</span>
                  <Input
                    value={this.state.searchParams.tranUnits}
                    data-inp="zdbh"
                    onChange={(e) => this.changeState('tranUnits', e.target.value)}
                    placeholder="请输入行业分类"
                  />
                </FormItem>

            }
            {
              this.props.location.state && this.props.location.state.code ?
                ''
                :
                <Row>
                  <Col span={24} style={{ textAlign: 'center', marginBottom: '10px' }}>
                    <Button
                      type="primary"
                      onClick={this.setModalVisible.bind(this, true, 'add')}
                      htmlType="reset"
                      icon="plus-circle"
                    >
                      新增
                </Button>
                    &emsp;
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>
                      搜索
                </Button>
                    &emsp;
                <Button
                      onClick={this.resetForm.bind(this)}
                      type="primary"
                      htmlType="reset"
                      icon="reload"
                    >
                      重置
                </Button>
                  </Col>
                </Row>
            }
          </Form>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.tableData}
                rowKey={record => record.id}
                pagination={{
                  pageSize: that.state.pageSize,
                  current: that.state.current,
                  onChange: that.pageChange,
                  total: that.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>


            <Modal
              title={this.state.submitFlag === 'add' ? "新建企业" : "编辑企业"}
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.modalVisible}
              onOk={this.editProject}
              onCancel={this.cancel}
              width={830}
            >
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>企业基本信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="企业名称：">
                    {getFieldDecorator('renterName', {
                      rules: [{ required: true, whitespace: true, message: '请输入企业名称！' },
                      { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入企业名称" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="法人：">
                    {getFieldDecorator('legalPerson', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入法人" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="联系人：">
                    {getFieldDecorator('linkman', {
                      rules: [
                        { required: true, whitespace: true, message: '请输入联系人！' },
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入联系人" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
              <Col span={8}>
                  <FormItem label="证件号码：">
                    {getFieldDecorator('cardNum', {
                      rules: [
                        { pattern: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: '请按照规则输入！' },
                      ]
                    })(<Input placeholder="请输入证件号码" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="行业分类：">
                    {getFieldDecorator('hyType', {
                      rules: [
                        { required: true, whitespace: true, message: '请选择行业分类！' }]
                    })(
                      <TreeSelect
                        treeData={this.state.orgTreeData}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto', maxWidth: 300, top: 80 }}
                        allowClear
                        showSearch
                        treeDefaultExpandedKeys={['1']}
                        filterTreeNode={(inputValue, option) => {
                          if (option.props.name.indexOf(inputValue) > -1) {
                            return true;
                          }
                        }}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="联系方式：">
                    {getFieldDecorator('contactWay', {
                      rules: [
                        { required: true, whitespace: true, message: '请输入联系方式！' },
                        { max: 50, message: '文字内容超过50字！' }
                      ]
                    })(
                      <Input placeholder="请输入联系方式" />)}
                  </FormItem>
                </Col>
                
              </Row>
              <Row gutter={24}>
              <Col span={8}>
                  <FormItem label="邮箱：">
                    {getFieldDecorator('mailbox', {
                      rules: [
                        { pattern: /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/, message: '请输入正确的邮箱格式！' },
                        { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入邮箱" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="主营业务：">
                    {getFieldDecorator('mainBusiness', {
                      rules: [
                        { required: true, whitespace: true, message: '请输入主营业务！' },
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入主营业务" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="投资额：">
                    {getFieldDecorator('investmentAmount', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入投资额" />)}
                  </FormItem>
                </Col>
                <Col span={8} id="selectPosition">
                  <FormItem label="人员规模：">
                    {getFieldDecorator('peopleNum')(
                      <Select
                        placeholder="请选择人员规模"
                        style={{ width: "100%" }}
                        getPopupContainer={() => document.getElementById('selectPosition')}>
                        <Option value='100人以下'>100人以下</Option>
                        <Option value='100-400人'>100-400人</Option>
                        <Option value='400人以上'>400人以上</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8} id="selectPosition">
                  <FormItem label="企业情况：">
                    {getFieldDecorator('renterSituation')(
                      <Select
                        placeholder="请选择企业情况"
                        style={{ width: "100%" }}
                        getPopupContainer={() => document.getElementById('selectPosition')}>
                        <Option value='上市'>上市</Option>
                        <Option value='三板'>三板</Option>
                        <Option value='区域挂牌'>区域挂牌</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8} id="selectPosition">
                  <FormItem label="高企：">
                    {getFieldDecorator('high')(
                      <Select
                        placeholder="请选择是否高企"
                        style={{ width: "100%" }}
                        getPopupContainer={() => document.getElementById('selectPosition')}>
                        <Option value='是'>是</Option>
                        <Option value='否'>否</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="成立时间：">
                    {getFieldDecorator('establishedTime')(<DatePicker style={{ width: "100%" }} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="当年全口径税收：">
                    {getFieldDecorator('fullIncome', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入当年全口径税收" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="当年收入：">
                    {getFieldDecorator('income', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入当年收入" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="注册类型：">
                    {getFieldDecorator('zcType', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(<Input placeholder="请输入注册类型" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="登记机关：">
                    {getFieldDecorator('registrationAuthority', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(<Input placeholder="请输入登记机关" />)}
                  </FormItem>
                </Col>
                <Col span={8} id="selectPosition">
                  <FormItem label="企业登记机关机构：">
                    {getFieldDecorator('registrationAuthorityArea', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(
                      <Select
                        placeholder="请选择企业登记机关机构"
                        style={{ width: "100%" }}
                        getPopupContainer={() => document.getElementById('selectPosition')}>
                        {
                          this.state.registrationAuthorityAreaData.map((item) => (
                            <Option value={item.selectCode} key={item.id}>{item.selectName}</Option>
                          ))
                        }
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="注册资金：">
                    {getFieldDecorator('registeredCapital', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入注册资金" />)}
                  </FormItem>
                </Col><Col span={8}>
                  <FormItem label="注册地址：">
                    {getFieldDecorator('registeredAddress', {
                      rules: [{
                        max: 200, message: '输入内容超过200字'
                      }]
                    })(<Input placeholder="请输入注册地址" />)}
                  </FormItem>
                </Col>
                <Col span={8} id="selectPosition">
                  <FormItem label="企业类型：">
                    {getFieldDecorator('enterpriseType')(
                      <Select
                        placeholder="请选择企业类型"
                        style={{ width: "100%" }}
                        getPopupContainer={() => document.getElementById('selectPosition')}>
                        <Option value='意向企业'>意向企业</Option>
                        <Option value='在租企业'>在租企业</Option>
                        <Option value='潜在企业'>潜在企业</Option>
                        <Option value='流失企业'>流失企业</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="经办人：">
                    {getFieldDecorator('manager', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(<Input rows={4} placeholder="请输入经办人" />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="附件上传：">
                    {getFieldDecorator('fujian')(
                      <Upload
                        action={Api.renteruploadFile}
                        fileList={this.state.fileList}
                        onChange={this.handleChange}
                        onRemove={this.handleRemove}
                        multiple
                        data={{ renterId: this.state.renterId || 0 }}
                      >
                        <Button>
                          <Icon type="upload" /> 上传文件
                        </Button>
                        <p className="ant-upload-text">支持扩展名：.rar .zip .doc .docx .pdf .jpg...</p>
                      </Upload>)}
                  </FormItem>
                </Col>
                {this.state.uploadState === 0 ?
                  <Col span={12}>
                    <FormItem label="企业图片上传：">
                      {getFieldDecorator('enterprisePic')(
                        <Upload
                          action={Api.uploadBuildingPic}
                          fileList={this.state.enterprisePic}
                          onChange={this.handleChange2}
                          onPreview={this.picClick}
                          // beforeUpload={this.picBeforeUpload}
                          onRemove={() => {
                            this.setState({
                              enterprisePic: []
                            });
                          }}
                          listType='text'
                          data={{ renterId: this.state.renterId || 0 }}
                        >
                          <Button>
                            <Icon type="upload" /> 上传图片
                        </Button>
                          <p className="ant-upload-text">支持扩展名：.png .jpeg .jpg</p>
                        </Upload>)}
                    </FormItem>
                  </Col> : null
                }

                <Col span={12}>
                  <FormItem label="企业简介：">
                    {getFieldDecorator('intro', {
                      rules: [{
                        max: 500, message: '输入内容超过500字'
                      }]
                    })(<TextArea rows={4} placeholder="请输入企业简介" />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="备注：">
                    {getFieldDecorator('remark', {
                      rules: [{
                        max: 200, message: '输入内容超过200字'
                      }]
                    })(<TextArea rows={4} placeholder="请输入备注" />)}
                  </FormItem>
                </Col>

              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>开票信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="开户行：">
                    {getFieldDecorator('openingBank', {
                      rules: [{
                        max: 100, message: '输入内容超过100字'
                      }]
                    })(<Input placeholder="请输入开户行" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="账号（银行卡卡号）：">
                    {getFieldDecorator('accountNumber', {
                      rules: [{ pattern: /^([1-9]{1})(\d{14}|\d{18})$/, message: '请输入正确的银行卡账号' }]
                    })(<Input placeholder="请输入账号（银行卡卡号）" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="联系方式：">
                    {getFieldDecorator('i_contactWay', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(<Input placeholder="请输入联系方式" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="纳税人识别号：">
                    {getFieldDecorator('i_nsrsbh', {
                      rules: [
                        { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                      ]
                    })(<Input placeholder="请输入纳税人识别号" />)}
                  </FormItem>
                </Col><Col span={8}>
                  <FormItem label="开票地址：">
                    {getFieldDecorator('address', {
                      rules: [{
                        max: 100, message: '输入内容超过100字'
                      }]
                    })(<Input placeholder="请输入开票地址" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="更新日期：">
                    {getFieldDecorator('i_updateDate')(<DatePicker style={{ width: "100%" }} />)}
                  </FormItem>
                </Col>
              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>工商信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="统一社会信用代码：">
                    {getFieldDecorator('shxydm', {
                      rules: [
                        { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                      ]
                    })(<Input placeholder="请输入统一社会信用代码" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="纳税人识别号：">
                    {getFieldDecorator('b_nsrsbh', {
                      rules: [
                        { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                      ]
                    })(<Input placeholder="请输入纳税人识别号" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="注册号：">
                    {getFieldDecorator('registrationNumber', {
                      rules: [
                        { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                      ]
                    })(<Input placeholder="请输入注册号" />)}
                  </FormItem>
                </Col><Col span={8}>
                  <FormItem label="组织机构代码：">
                    {getFieldDecorator('orgNum', {
                      rules: [
                        { pattern: /^[A-Za-z0-9]+$/, message: '请输入正确的格式！' }
                      ]
                    })(<Input placeholder="请输入组织机构代码" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="更新日期：">
                    {getFieldDecorator('b_updateTime')(<DatePicker style={{ width: "100%" }} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="企业工商注册地址：">
                    {getFieldDecorator('businessRegistAddress', {
                      rules: [{
                        max: 100, message: '输入内容超过100字'
                      }]
                    })(<Input placeholder="请输入企业工商注册地址" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>企业人员信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }} /></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="学士：">
                    {getFieldDecorator('bachelorNum', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入学士人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="硕士：">
                    {getFieldDecorator('masterNumber', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入硕士人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="博士：">
                    {getFieldDecorator('doctorNumber', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入博士人数" />)}
                  </FormItem>
                </Col><Col span={8}>
                  <FormItem label="留学人员：">
                    {getFieldDecorator('abroadNumber', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入留学人员人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="外籍人员：">
                    {getFieldDecorator('personnelNumber', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入外籍人员人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="党员：">
                    {getFieldDecorator('partyMember', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入党员人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="导师：">
                    {getFieldDecorator('tutor', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入导师人数" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="企业总人数：">
                    {getFieldDecorator('totalNumber', {
                      rules: [{
                        pattern: /^\d+$|^\d+[.]?\d+$/, message: '请输入数字'
                      }]
                    })(<Input placeholder="请输入企业总人数" />)}
                  </FormItem>
                </Col>
              </Row>
            </Modal>
          </Spin>
        </div>
      </PageHeaderWrapper >
    );
  }
}

const WrappedApp = Form.create()(RenterManagement);
export default WrappedApp;