import React, { Component } from 'react';

import { Row, Col, Checkbox, Modal, Spin } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Button,
  Util
} from "bizcharts";
import ReactEcharts from 'echarts-for-react';
import moment from 'moment';

const BgElement = Element.BgElement;
const CheckboxGroup = Checkbox.Group;

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class OperationDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contractId: '',
      community_name: '-',
      building_name: '-',
      house_name: '-',
      renter_name: '-',
      use_area: '-',
      renovation_condition: '-',
      principal: '-',
      legal_person: '-',
      linkman: '-',
      contact_way: '-',
      location: '-',
      hy_type_str: '-',
      main_business: '-',
      investment_amount: '-',
      people_num: '-',
      lessee: '-',
      rentArea: '-',
      heatArea: '-',
      mapArea: '-',
      useArea: '-',
      buildArea: '-',
      rentPrice: '-',
      rentTotal: '-',
      startDate: '-',
      endDate: '-',
      expectedBond: '-',
      actualBond: '-',
      acceptUser: '-',
      acceptDate: '-',
      registered_address: '-',
      loading: true,
      chartLoading: false,
      fileList: [],
      list: ['附件1', '附件1', '附件1', '附件1', '附件20.jpg', '附件20.jpg', '附件20.jpg', '附件20.jpg', '附件20.jpg'],
      barData: {
        color: ['#f35245'],
        tooltip: {
          trigger: 'axis',
          axisPointer: {            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
          }
        },
        xAxis: [
          {
            type: 'category',
            data: ['2014', '2015', '2016', '2017', '2018', '', '', ''],
          }
        ],
        yAxis: [
          {
            type: 'value',
            min: 0,
          }
        ],
        series: [
          {
            name: '数量',
            type: 'bar',
            data: [15, 56, 9, 88, 120]
          }
        ]
      },
      // 费用信息
      water: '-',
      electricity: '-',
      heating: '-',
      property: '-',
      elevator: '-',
      total_money: '-'
    };
  }
  componentWillMount() {
    const contractId = this.props.location.query.contractId;
    this.setState({ contractId });
    this.getFeeMaintainById(contractId);
  }
  getFeeMaintainById(contractId) {
    axios.post(Api.queryOne, { contractId }).then(res => {
      this.setState({
        ...res.data,
        loading: false
      });

    });
  }
  printWord = () => {
    axios.get(Api.contractPrint, {
      params: {
        contractId: this.state.contractId
      }
    }).then((res) => {
      const { data } = res;
      let wind = window.open("", 'print', 'toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');
      wind.document.body.innerHTML = data.content;
      wind.print();
    })
  }
  render() {
    const {
      currentUserLoading,
    } = this.props;

    const optionsWithDisabled = [
      { label: 'Apple', value: 'Apple' },
      { label: 'Pear', value: 'Pear' },
      { label: 'Orange', value: 'Orange', disabled: false },
    ];
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div style={{
          marginBottom: 50
        }}>
          <Row className={styles.w1120}>
            <div className={`pull-left ${styles.w1120}`}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>房屋信息</p>
                    <p>HOUSE INFORMATION</p>
                  </div>
                </div>
              </div>
              <div className={styles.print} onClick={this.printWord}>打印</div>
              <div style={{ height: '136px' }} className={styles.br2}>
                <Row>

                  <Col className={styles.col8Overflow} title={this.state.houseName2} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>房间名称：</span>{this.state.houseName2}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.principal} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>楼宇包保负责人：</span>{this.state.principal}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.build_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>房屋面积：</span>{this.state.build_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.renovation_condition} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '140px' }} className='label'>装修状况：</span>{this.state.renovation_condition}
                  </Col>

                </Row>
              </div>
            </div>
            <div className={`pull-left ${styles.w1120}`} style={{ marginTop: 20 }}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>企业信息</p>
                    <p>RENTER INFORMATION</p>
                  </div>
                </div>
              </div>
              <div style={{ height: '202px' }} className={styles.br2}>
                <Row>

                  <Col className={styles.col8Overflow} title={this.state.renter_name} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>企业名称：</span>{this.state.renter_name}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.linkman} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>联系人：</span>{this.state.linkman}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.contact_way} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>联系方式：</span>{this.state.contact_way}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.registered_address} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>详细地址：</span>{this.state.registered_address}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.hy_type_str} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>行业分类：</span>{this.state.hy_type_str}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.main_business} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>主营业务：</span>{this.state.main_business}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.hy_type_str} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>公司类型：</span>{this.state.hy_type_str}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.investment_amount} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>注册资金：</span>{this.state.investment_amount}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.people_num} style={{ textAlign: 'left' }} span={8}>
                    <span className='label'>人员规模：</span>{this.state.people_num}
                  </Col>
                </Row>
              </div>
            </div>
            <div className={`pull-left ${styles.w1120}`} style={{ marginTop: 20 }}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>租赁合同</p>
                    <p>LEASE AGREEMENT</p>
                  </div>
                </div>
              </div>
              <div style={{ height: '330px' }} className={styles.br2}>
                <Row>
                  <Col className={styles.col8Overflow} title={this.state.renter_name} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>承租方：</span>{this.state.renter_name}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.rent_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>租赁面积：</span>{this.state.rent_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.heat_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>供暖面积：</span>{this.state.heat_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.map_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>测绘面积：</span>{this.state.map_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.use_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>使用面积：</span>{this.state.use_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.build_area} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>建筑面积：</span>{this.state.build_area}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.rent_price} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>租金单价：</span>{this.state.rent_price}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.rent_total} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>租金总价：</span>{this.state.rent_total}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.start_date} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>开始日期：</span>{this.state.start_date}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.end_date} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>结束日期：</span>{this.state.end_date}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.accept_user} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>受理人：</span>{this.state.accept_user}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.accept_date} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>受理日期：</span>{moment(this.state.accept_date).format('YYYY-MM-DD')}
                  </Col>
                </Row>
              </div>
            </div>
            <div className={`pull-left ${styles.w1120}`} style={{ marginTop: 20 }}>
              <div className='mainContainer'>
                <div className="contentHeader">
                  <div className="renter_contentHeaderTitle">
                    <p>费用信息</p>
                    <p>COST INFORMATION</p>
                  </div>
                </div>
              </div>
              <div style={{ height: '330px' }} className={styles.br2}>
                <Row>
                  <Col className={styles.col8Overflow} title={this.state.expected_bond} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} style={{ width: '120px' }} className='label'>应缴纳保障金：</span>{this.state.expected_bond}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.actual_bond} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>实收保障金：</span>{this.state.actual_bond}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.water} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>水费：</span>{this.state.water}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.electricity} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>电费：</span>{this.state.electricity}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.heating} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>供暖费：</span>{this.state.heating}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.property} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>物业费：</span>{this.state.property}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.elevator} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>电梯费：</span>{this.state.elevator}
                  </Col>
                  <Col className={styles.col8Overflow} title={this.state.total_money} style={{ textAlign: 'left' }} span={8}>
                    <span style={{ width: '120px' }} className='label'>总金额：</span>{this.state.total_money}
                  </Col>

                </Row>
              </div>
            </div>

          </Row>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default OperationDetails;
