import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Icon,
  Input,
  Table,
  Button,
  Form,
  Tabs,
  Spin,
  Select,
  Upload
} from 'antd';

import Link from 'umi/link';
import axios from 'axios';
import Api from '@/services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { formatTreeData } from '@/components/commonAips';
import styles from './style.less'

const { TextArea } = Input;
const { confirm } = Modal;
const { Option } = Select;
const FormItem = Form.Item;
const { MonthPicker } = DatePicker;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class RenterManagement extends React.Component {
  constructor(props) {
    super(props);
    // const token = sessionStorage.getItem('tokenData') ? sessionStorage.getItem('tokenData') : '';
    this.state = {
      name: '',
      tranUnits: '',
      pageSize: 2,
      current: 1,
      tableData: [],
      orgTreeData: [],
    };
  }

  componentWillMount() {
    const that = this;
    that.sendData();

    // 获取系统参数
    // 获取部门树

    axios.get(Api.getIndustryTree, {}).then((resp) => {
      const { data } = resp;
      if (data) {
        const treeData = new Array(data)[0];
        formatTreeData(treeData, 'name', 'orgLevelFormat', 'industryId', 'industryList');
        that.setState({ orgTreeData: treeData });
      }
    });
    axios.post(Api.getOptionList, {
      formField: "registrationAuthorityArea",
    })
      .then((function (resp) {
        const data = resp.data;
        if (data.length === 0) {
          notification.destroy();
          notification['warning']({ message: '暂无数据' })
        }
        that.setState({
          registrationAuthorityAreaData: data.list,
        })
      }))
  }

  // 获取表格数据
  sendData = () => {
    const { current, name, tranUnits, startDate, endDate } = this.state;
    this.setState({ loading: true });
    this.props.form.validateFields((err, values) => {
      axios.post(Api.getRenterListByMonth, {
        currentPage: current,
        renterName: name,
        tranUnits,
        month: this.props.location.state.month
      }).then((resp) => {
        const { data } = resp;
        if (data.list.length === 0) {
          notification.destroy();
          notification.warning({
            message: '暂无数据',
          });
        }
        data.list.forEach((d, i) => {
          d.key = i;
          d.receivedDate = moment(d.receivedDate).format("YYYY-MM-DD HH:mm:ss")
        })
        this.setState({
          tableData: data.list,
          pageSize: data.pageSize,
          current: data.currentPage,
          total: data.totalSize
        })
        this.setState({ loading: false })
      })
    })
  };



  // 查询
  query = () => {
    this.setState({ current: 1 }, () => {
      this.sendData();
    });
  };


  changeName = e => {
    this.setState({ name: e.target.value });
  };

  changeTranUnits = e => {
    this.setState({ tranUnits: e.target.value });
  };

  pageChange = page => {
    this.setState(
      {
        current: page,
      },
      () => {
        this.sendData();
      }
    );
  };


  resetForm = () => {
    this.props.form.setFieldsValue({
      s_date: null,
      e_date: null
    });
    this.setState({ name: '', tranUnits: '' })
  }


  render() {
    const that = this;
    const { currentUserLoading } = this.props;

    // 表格表头
    const columns = [
      {
        title: '企业名称',
        dataIndex: 'renter_name',
        key: 'renter_name',
        render: (text, item, index) => (
          <Link
            target='_blank'
            to={{
              pathname: '/fe/dataMaintenance/renterDetails',
              search: `?id=${item.id}`
            }}
          >
            {text}
          </Link>
        ),
      },
      {
        title: '法人',
        dataIndex: 'linkman',
        key: 'linkman',
      },
      {
        title: '联系方式',
        dataIndex: 'contact_way',
        key: 'contact_way',
      },
      {
        title: '证件类型',
        dataIndex: 'transferUnit',
        key: 'transferUnit',
        render: (text, item, index) => (
          <span>
            {'身份证'}
          </span>
        )
      },
      ,
      {
        title: '证件号码',
        dataIndex: 'card_num',
        key: 'card_num',
      },
      ,
      {
        title: '邮箱',
        dataIndex: 'mailbox',
        key: 'mailbox',
      },
      {
        title: '行业类型',
        dataIndex: 'industry_type',
        key: 'industry_type',
      },
      {
        title: '企业情况',
        dataIndex: 'renter_situation',
        key: 'renter_situation',
      }
    ];

    return (
      < PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>{this.props.location.state.month}企业列表</p>
              <p>RENTER MANAGEMENT</p>
            </div>
          </div>
          <Form layout="inline" align='center'>
            <FormItem>
              <span className="label">企业名称</span>
              <Input
                value={this.state.name}
                data-inp="zdbh"
                onChange={this.changeName}
                placeholder="请输入企业名称"
              />
            </FormItem>

            <FormItem>
              <span className="label">行业分类</span>
              <Input
                value={this.state.tranUnits}
                data-inp="zdbh"
                onChange={this.changeTranUnits}
                placeholder="请输入行业分类"
              />
            </FormItem>
            <Row>
              <Col span={24} style={{ textAlign: 'center', marginBottom: '10px' }}>
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>
                  搜索
                </Button>
                &emsp;
                <Button
                  onClick={this.resetForm.bind(this)}
                  type="primary"
                  htmlType="reset"
                  icon="reload"
                >
                  重置
                </Button>
              </Col>
            </Row>
          </Form>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.tableData}
                rowKey={record => record.projectCode}
                pagination={{
                  pageSize: that.state.pageSize,
                  current: that.state.current,
                  onChange: that.pageChange,
                  total: that.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>
          </Spin>
        </div>
      </PageHeaderWrapper >
    );
  }
}

const WrappedApp = Form.create()(RenterManagement);
export default WrappedApp;