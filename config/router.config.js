export default [
  // user
  {
    path: '/fe/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/fe/user', redirect: '/fe/user/login' },
      { path: '/fe/user/login', component: './User/Login' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      // 重定向
      { path: '/', redirect: '/fe/homePage' },
      // 首页
      {
        path: '/fe/homePage',
        name: 'homePage',
        component: './homePage/HomePage',
      },
      // 资产地图
      {
        path: '/fe/assetMap',
        name: 'assetMap',
        authority: ['admin'],
        routes: [
          {
            path: '/fe/assetMap/buildingEdit',
            name: '',
            component: './assetMap/BuildingEdit'
          },
          {
            path: '/fe/assetMap/houseEdit',
            name: '',
            component: './assetMap/HouseEdit'
          },
          {
            path: '/fe/assetMap/assetMap',
            name: 'assetMap',
            component: './assetMap/AssetMap'
          },
          {
            path: '/fe/assetMap/communityDetails',
            name: '',
            component: './assetMap/CommunityDetails'
          },
          {
            path: '/fe/assetMap/buildingDetails',
            name: '',
            component: './assetMap/BuildingDetails'
          },
          {
            path: '/fe/assetMap/houseDetails',
            name: '',
            component: './assetMap/HouseDetails'
          },
          // {
          //   path: '/fe/assetMap/imageContrast',
          //   name: 'imageContrast',
          //   component: './assetMap/ImageContrast'
          // },
        ],
      },
      {
        path: '/fe/assetManagement',
        name: 'assetManagement',
        routes: [
          {
            path: '/fe/assetManagement/projectManagement',
            // name: 'projectManagement',
            component: './assetManagement/ProjectManagement'
          },
          {
            path: '/fe/assetManagement/buildingManagement',
            name: 'buildingManagement',
            component: './assetManagement/BuildingManagement',
          },
          {
            path: '/fe/assetManagement/houseManagement',
            name: 'houseManagement',
            component: './assetManagement/HouseManagement',
          },
          {
            path: '/fe/assetManagement/assetsReceived',
            // name: 'assetsReceived',
            component: './assetManagement/AssetsReceived'
          },
          {
            path: '/fe/assetManagement/dataCorrection',
            name: '',
            component: './assetManagement/DataCorrection',
            authority: ['admin'],
          },
          {
            path: '/fe/assetManagement/assetReceiptDetails',
            name: '',
            component: './assetManagement/AssetReceiptDetails'
          },
          {
            path: '/fe/assetManagement/projectInfo',
            name: '',
            component: './assetManagement/ProjectInfo'
          },
          {
            path: '/fe/assetManagement/mountMap',
            name: '',
            component: './assetManagement/MountMap'
          },
          {
            path: '/fe/assetManagement/newHouse',
            name: '',
            component: './assetManagement/NewHouse',
          },
          {
            path: '/fe/assetManagement/editHouse',
            name: '',
            component: './assetManagement/EditHouse',
          },
        ],
      },
      // 数据维护
      {
        path: '/fe/dataMaintenance',
        name: 'dataMaintenance',
        routes: [
          // {
          //   path: '/fe/dataMaintenance/rentalMaintenance',
          //   name: 'rentalMaintenance',
          //   component: './dataMaintenance/RentalMaintenance'
          // },
          {
            path: '/fe/dataMaintenance/energyCostMaintenance',
            name: '',
            component: './dataMaintenance/EnergyCostMaintenance'
          },
          {
            path: '/fe/dataMaintenance/projectManagement',
            name: '',
            component: './dataMaintenance/ProjectManagement'
          },
          {
            path: '/fe/dataMaintenance/projectDetails',
            name: '',
            component: './dataMaintenance/ProjectDetails'
          },
          {
            path: '/fe/dataMaintenance/rentDetails',
            name: '',
            component: './dataMaintenance/RentDetails'
          },
          {
            path: '/fe/dataMaintenance/energyEdit',
            name: '',
            component: './dataMaintenance/EnergyEdit'
          },
          {
            path: '/fe/dataMaintenance/energyDetails',
            name: '',
            component: './dataMaintenance/EnergyDetails'
          }
          ,
          {
            path: '/fe/dataMaintenance/renterManagement',
            name: 'renterManagement',
            component: './dataMaintenance/RenterManagement'
          },
          // 用于首页月份查询
          {
            path: '/fe/dataMaintenance/renterManagementForMonth',
            name: '',
            component: './dataMaintenance/RenterManagementForMonth'
          },
          {
            path: '/fe/dataMaintenance/renterDetails',
            name: '',
            component: './dataMaintenance/RenterDetails'
          }
          ,
          {
            path: '/fe/dataMaintenance/operationManagement',
            name: 'operationManagement',
            component: './dataMaintenance/OperationManagement'
          },
          {
            path: '/fe/dataMaintenance/operationDetails',
            name: '',
            component: './dataMaintenance/OperationDetails'
          },
          {
            path: '/fe/dataMaintenance/serviceTrajectory',
            name: 'serviceTrajectory',
            component: './dataMaintenance/ServiceTrajectory'
          },
          {
            path: '/fe/dataMaintenance/protocolManagement',
            name: 'protocolManagement',
            component: './dataMaintenance/protocolManagement'
          },
          {
            path: '/fe/dataMaintenance/protocolDetails',
            name: '',
            component: './dataMaintenance/ProtocolDetails'
          },
          {
            path: '/fe/dataMaintenance/EnterpriseAtlas',
            name: 'EnterpriseAtlas',
            component: './dataMaintenance/EnterpriseAtlas'
          }
        ],
      },
      {
        path: '/fe/ownershipManagement',
        name: '',
        routes: [
          {
            path: '/fe/ownershipManagement/projectOwnershipManagement',
            name: 'projectOwnershipManagement',
            component: './ownershipManagement/ProjectOwnershipManagement'
          },
          {
            path: '/fe/ownershipManagement/warrantManagement',
            name: 'warrantManagement',
            component: './ownershipManagement/WarrantManagement'
          },
          {
            path: '/fe/ownershipManagement/ownershipStatus',
            name: '',
            component: './ownershipManagement/OwnershipStatus'
          },
          {
            path: '/fe/ownershipManagement/ownershipStatusHouse',
            name: '',
            component: './ownershipManagement/OwnershipStatusHouse'
          },
        ],
      },
      // 档案管理
      // {
      //   path: '/fe/recordManagement',
      //   name: 'recordManagement',
      //   routes: [
      //     {
      //       path: '/fe/recordManagement/recordManagement',
      //       name: 'recordManagement',
      //       component: './recordManagement/RecordManagement'
      //     },
      //     {
      //       path: '/fe/recordManagement/recordBorrow',
      //       name: 'recordBorrow',
      //       component: './recordManagement/RecordBorrow'
      //     },
      //     {
      //       path: '/fe/recordManagement/borrowApproval',
      //       name: 'borrowApproval',
      //       component: './recordManagement/BorrowApproval'
      //     },
      //   ]
      // },
      // {
      //   path: '/fe/KeyManagement',
      //   name: 'keyManagement',
      //   routes: [
      //     {
      //       path: '/fe/keyManagement/keyManagement',
      //       name: 'keyManagement',
      //       component: './keyManagement/KeyManagement'
      //     },
      //     {
      //       path: '/fe/keyManagement/keyBorrow',
      //       name: 'keyBorrow',
      //       component: './keyManagement/KeyBorrow'
      //     },
      //     {
      //       path: '/fe/keyManagement/borrowApproval',
      //       name: 'borrowApproval',
      //       component: './keyManagement/BorrowApproval'
      //     },
      //   ],
      // },
      {
        path: '/fe/statisticalAnalysis',
        name: 'statisticalAnalysis',
        routes: [
          {
            path: '/fe/statisticalAnalysis/statisticalAnalysisOfHousingUse',
            // name: 'statisticalAnalysisOfHousingUse',
            component: './statisticalAnalysis/StatisticalAnalysisOfHousingUse'
          },
          // {
          //   path: '/fe/statisticalAnalysis/xx1',
          //   name: 'xx1',
          //   component: './statisticalAnalysis/StatisticalAnalysisOfHousingUse'
          // },
          // {
          //   path: '/fe/statisticalAnalysis/xx2',
          //   name: 'xx2',
          //   component: './statisticalAnalysis/StatisticalAnalysisOfHousingUse'
          // },
          // {
          //   path: '/fe/statisticalAnalysis/xx3',
          //   name: 'xx3',
          //   component: './statisticalAnalysis/StatisticalAnalysisOfHousingUse'
          // },
          {
            path: '/fe/statisticalAnalysis/statisticsOfResidenceAndRentRelease',
            name: 'statisticsOfResidenceAndRentRelease',
            component: './statisticalAnalysis/StatisticsOfResidenceAndRentRelease'
          },
          {
            path: '/fe/statisticalAnalysis/statisticalAnalysisOfFefund',
            name: 'statisticalAnalysisOfFefund',
            component: './statisticalAnalysis/StatisticalAnalysisOfFefund'
          }
        ],
      },
      {
        path: '/fe/investmentManagement',
        name: 'investmentManagement',
        routes: [
          {
            path: '/fe/investmentManagement/investmentAnalysis',
            name: 'investmentAnalysis',
            component: './investmentManagement/InvestmentAnalysis'
          }
        ],
      },
      {
        path: '/fe/systemManagement',
        name: 'systemManagement',
        routes: [
          {
            path: '/fe/systemManagement/userManagement',
            name: 'userManagement',
            component: './systemManagement/UserManagement'
          },
          {
            path: '/fe/systemManagement/modifyPassword',
            name: 'modifyPassword',
            component: './systemManagement/ModifyPassword'
          },
          {
            path: '/fe/systemManagement/departmentManagement',
            name: 'departmentManagement',
            component: './systemManagement/DepartmentManagement'
          },
          {
            path: '/fe/systemManagement/permissionPassword',
            name: 'permissionPassword',
            component: './systemManagement/PermissionPassword',
          },
          {
            path: '/fe/systemManagement/userEmpowerment',
            name: 'userEmpowerment',
            component: './systemManagement/UserEmpowerment'
          },
          {
            path: '/fe/systemManagement/givePermission',
            name: '',
            component: './systemManagement/GivePermission',
          },
          {
            path: '/fe/systemManagement/giveRole',
            name: '',
            component: './systemManagement/GiveRole',
          },
          {
            path: '/fe/systemManagement/roleManagement',
            name: 'roleManagement',
            component: './systemManagement/RoleManagement'
          },
          {
            path: '/fe/systemManagement/registrationAuthorityArea',
            name: 'registrationAuthorityArea',
            component: './systemManagement/RegistrationAuthorityArea'
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
