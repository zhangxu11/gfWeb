/*
* @Author: 程家兴
* @Date: 2019-05-08 16:12:16
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-14 08:56:55
* @File: 招商分析-饼图组件
*/
import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';
import styles from './style.less'

export default class InvestmentAnalysisPie extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    createColor = () => {
        let r = Math.floor(Math.random() * 256);
        let g = Math.floor(Math.random() * 256);
        let b = Math.floor(Math.random() * 256);
        return "rgb(" + r + ',' + g + ',' + b + ")";
    }
    render() {
        const data = [];
        const color = ["#FF7476", "#36CBCB", "#4DCB73", "#FAD337"];
        this.props.data && this.props.data.length > 0 && this.props.data.map((item, index) => {
            data.push({
                value: item.num,
                name: item.enterprise_type || item.name,
                percent: item.bfb
            })
            if (index > 4) {
                color.push(this.createColor())
            }
        });
        let formatterData = data.slice(0, 5);

        const pieOption = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                show: true,
                orient: 'vertical',
                right: '0px',
                top: 'middle',
                itemGap: 50,
                itemWidth: 50,
                itemHeight: 10,
                align: 'left',
                selectedMode: false,
                formatter: (name) => {
                    const ind = formatterData.find((d) => d.name === name);
                    return `  {title|${ind.name && ind.name.length > 5 ? ind.name.slice(0, 5) + '...' : ind.name}}  |  {pre|${ind.percent}}  {count|${ind.value}}家`;
                },
                textStyle: {
                    rich: {
                        title: {
                            fontWeight: 400,
                            fontStyle: 'normal',
                            width: 80,
                            color: 'rgba(0, 0, 0, 0.647058823529412)',
                        },
                        pre: {
                            fontWeight: 400,
                            fontStyle: 'normal',
                            color: 'rgba(0, 0, 0, 0.427450980392157)',
                        },
                        count: {
                            fontWeight: 400,
                            fontStyle: 'normal',
                            color: 'rgba(0, 0, 0, 0.647058823529412)',
                        }
                    }
                },
                data: formatterData.map((d, i) => {
                    return { name: d.name, icon: 'circle', };
                })
            },
            title: {
                show: true,
                text: this.props.centerTitle,
                textStyle: {
                    color: 'black',
                    fontSize: 24,
                    fontWeight: 400,
                    fontStyle: 'normal',
                },
                x: '20%',
                y: 'center'
            },
            series: [
                {
                    name: '行业分布',
                    title: '行业分布',
                    type: 'pie',
                    radius: ['57%', '77%'],
                    center: ['30%', '50%'],
                    color: color,
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            // formatter: '{d}%'
                        },
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    // itemStyle: {
                    //     borderWidth: 5,
                    //     borderColor: '#fff'

                    // },
                    data: data,
                }
            ]
        };

        return (
            <div>
                <div className={styles.moduleTitle}>{this.props.title}</div>
                {this.props.data && this.props.data.length > 0 ?
                    <ReactEcharts option={pieOption} notMerge={true} lazyUpdate={true} style={{ height: 300 }} /> :
                    <div className={styles.noData}>暂无数据</div>
                }
            </div>
        );
    }
}