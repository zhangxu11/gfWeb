import React, { Component } from 'react';

import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import {
  Table,
  Col,
  Card,
  Row,
  Icon,
  Input,
  Spin,
  Button,
  Modal,
  message,
  DatePicker,
  Tag
} from 'antd';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import ReactEcharts from 'echarts-for-react';
import axios from 'axios';
import classNames from 'classnames';
import moment from 'moment';
import Api from '../../services/apis';
import TimeSelectorWithMonthYear from '@/components/TimeSelector';

import styles from "./style.less";

const { RangePicker } = DatePicker;
const { Search } = Input;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
export default class Part4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartLoading: false,
      startDate: moment().startOf('year'),
      endDate: moment().endOf('year'),
      incrementOfYear: null, // 全年合同增量
      percentOfYear: null, // 年环比
      incrementOfMonth: null, // 本月合同增量
      percentOfMonth: null, // 月环比
      xAxisData: [],
      seriesData: [],
    };
  }

  componentDidMount() {
    this.getData(this.state.startDate, this.state.endDate);
  }

  getData = (startDate, endDate) => {
    const startDateYear = moment().startOf('year');
    const endDateYear = moment().endOf('year');
    let checkYear = false;
    if (startDate.isSame(startDateYear, 'day') && endDate.isSame(endDateYear, 'day')) checkYear = true;
    axios.post(Api.getContractStatistics, {
      startDate: startDate.format("YYYY-MM-DD"),
      endDate: endDate.format("YYYY-MM-DD"),
      byDay: !checkYear,
    }).then((res) => {
      if (res.data.code === '200') {
        const data = res.data.result;
        const xAxisData = this.buildXAxisData(data.incrementStatisticsList);
        const seriesData = this.buildSeriesData(data.incrementStatisticsList);
        this.setState({
          xAxisData,
          seriesData,
          incrementOfYear: data.incrementOfYear, // 全年合同增量
          percentOfYear: data.percentOfYear, // 年环比
          incrementOfMonth: data.incrementOfMonth, // 本月合同增量
          percentOfMonth: data.percentOfMonth, // 月环比
        });
      }
    })
  }

  buildXAxisData = (data) => {
    return data.map(d => d.dateAlias);
  }

  buildSeriesData = (data) => {
    let a = 0;
    return data.map(d => {
      return {
        name: d.dateAlias,
        value: d.renterNum
      };
    })
  }

  dateOnChange = (startDate, endDate) => {
    this.getData(startDate, endDate);
    this.setState({
      startDate,
      endDate,
    });
  }

  formatPrecent(percent) {
    if (!percent) return { percent, down: false };
    if (percent.substr(0, 1) === '-') return { percent: `${percent.substring(1)}`, down: true, };
    else return { percent: `${percent.substring(1)}`, down: false };
  }

  render() {
    const { xAxisData, seriesData, incrementOfMonth, incrementOfYear, percentOfMonth, percentOfYear } = this.state;

    const option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        }
      },
      title: {
        show: true,
        text: '合同增长情况',
        textStyle: {
          color: 'rgba(0, 0, 0, 0.427450980392157)',
          fontSize: 14,
          fontWeight: 400,
          fontStyle: 'normal',
        },
      },
      xAxis: [
        {
          type: 'category',
          data: xAxisData,
          axisPointer: {
            type: 'shadow'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '合同增量',
          // min: 0,
          // max: 250,
          // interval: 50,
          axisLabel: {
            formatter: '{value} 家'
          }
        },
      ],
      series: [
        {
          name: '增长量',
          smooth: true,
          type: 'line',
          itemStyle: {
            color: '#E22534'
          },
          areaStyle: {
            color: '#FCEAEB',
          },
          data: seriesData
        },
      ]
    };

    const percentYear = this.formatPrecent(percentOfYear);
    const percentMonth = this.formatPrecent(percentOfMonth);
    // console.log(percentYear, percentMonth)
    return (
      <Spin spinning={this.state.chartLoading}>
        <Row>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>房产运营概况</p>
                <p>OPRATION PROFILE</p>
              </div>
            </div>
          </div>
        </Row>
        <Row className={classNames(styles.part4Content)}>
          <div style={{ float: 'left', width: 230, height: 295 }}>
            <div style={{ marginBottom: 40, padding: '20px', width: '238px', height: '118px', display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignitems: 'center' }} className={styles.partBorder}>
              <p className={styles.subTitle}>全年合同累计增长</p>
              <p><span className={styles.count}>{incrementOfYear}</span><span>环比</span><span className={styles.precent} style={{ color: percentYear.down ? 'rgba(0, 102, 0, 0.647058823529412)' : '#E22534' }}>{percentOfYear}<img src={percentYear.down ? require('../../assets/icon-down.png') : require('../../assets/icon-up.png')} /></span></p>
            </div>
            <div style={{ padding: '20px', width: '238px', height: '118px', display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignitems: 'center' }} className={styles.partBorder}>
              <p className={styles.subTitle}>本月合同累计增长</p>
              <p><span className={styles.count}>{incrementOfMonth}</span><span>环比</span><span className={styles.precent} style={{ color: percentYear.down ? 'rgba(0, 102, 0, 0.647058823529412)' : '#E22534' }}>{percentOfMonth}<img src={percentYear.down ? require('../../assets/icon-down.png') : require('../../assets/icon-up.png')} /></span></p>
            </div>
          </div>
          <ReactEcharts option={option} notMerge={true} lazyUpdate={true} style={{ height: 295, width: 800, float: 'right' }} />
          <div style={{ position: 'absolute', top: 0, right: 32 }}>
            <TimeSelectorWithMonthYear startDate={this.state.startDate} endDate={this.state.endDate} onChange={this.dateOnChange} />
          </div>
        </Row>
      </Spin>
    );
  }
}