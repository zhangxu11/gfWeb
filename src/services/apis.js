let prefix = '';

// if (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1'||window.location.hostname === '10.200.15.211' ||window.location.hostname === '10.200.15.247'||window.location.hostname === '10.200.15.226' || window.location.hostname === '10.200.17.237') {
//   prefix = '/api'
// };
// 为了方式开发人员换ip，现在匹配内网ip为 10.200.1 几的都加上代理
if (window.location.hostname === 'localhost' || /^10\.200\.1.+/.test(window.location.hostname)) {
  prefix = '/api'
};
const Api = {
  getAssetStatistics: `${prefix}/home/asset/statistics`,
  getRenterStatistics: `${prefix}/home/renter/statistics`,
  getEnterpriseStatistics: `${prefix}/home/enterprise/statistics`,
  getContractStatistics: `${prefix}/home/contract/statistics`,
  getIndustryOfRenterIncome: `${prefix}/home/industry/rank/renter/income`,
  getRenterReason: `${prefix}/home/renter/statistics/reason`,
  outcontract: `${prefix}/statistics/outcontract`, // 退租统计原因接口
  outcontracttrend: `${prefix}/statistics/outcontracttrend`, // 退租统计原因折线图接口

  // 合同与协议的接口
  queryProtocol: `${prefix}/contract/queryProtocol`,
  getApproval: `${prefix}/contract/checkApproval`,
  commitApproval: `${prefix}/contract/commitInfo`,
  delApproval: `${prefix}/contract/del`,
  disorderApproval: `${prefix}/contract/disorderApproval`,
  renewal: `${prefix}/contract/renewal`,
  updateContractInfo: `${prefix}/contract/updateContractInfo`,
  moveOut: `${prefix}/contract/moveOut`,
  addApproval: `${prefix}/contract/approval`,
  getMoveReason: `${prefix}/contract/getMoveReason`,
  getHouseRenter: `${prefix}/renter/getHouseRenter`,
  getRentUse: `${prefix}/contract/getRentUse`,


  getNodeInfo: `${prefix}/os/getNodeInfo`,
  addCard: `${prefix}/os/addCard`,
  executePlan: `${prefix}/os/executePlan`,
  getHListByBuildCode: `${prefix}/asset/getHListByBuildCode`,
  getBListByProCode: `${prefix}/asset/getBListByProCode`,
  getproListByName: `${prefix}/asset/getproListByName`,
  warrant: `${prefix}/os/getOsCardList`,
  ownership: `${prefix}/os/getProOs`,
  omGetBuildingList: `${prefix}/os/getBuildingList`,
  omTagBuildingHaveCard: `${prefix}/os/tagHaveCard`,
  omGetDataList: `${prefix}/os/getDataList`,
  omEditOwnerInfo: `${prefix}/os/editOwnerInfo`,
  omTagDataHaveCard: `${prefix}/os/tagDataHaveCard`,
  getHouseLeaseInfo: `${prefix}/houseLease/queryAll`,


  login: `${prefix}/manager/login`,
  umTableDatas: `${prefix}/manager/getUserList`,
  umGetOrganizationTree: `${prefix}/manager/getOrganizationTree`,
  umGetOrganizationList: `${prefix}/manager/getOrganizationList`,
  umGetAreaTree: `${prefix}/manager/getAreaTree`,
  umAddUser: `${prefix}/manager/addUser`,
  umGetUserByCode: `${prefix}/manager/getUserByCode`,
  umUpdateUser: `${prefix}/manager/updateUser`,
  umUpdateUserState: `${prefix}/manager/updateUserState`,
  umChangePassword: `${prefix}/manager/changePassword`,
  umAddOrganization: `${prefix}/manager/addOrganization`,
  umGetOrgById: `${prefix}/manager/getOrgById`,
  umUpdateOrganization: `${prefix}/manager/updateOrganization`,
  umGetMenuTreeByFun: `${prefix}/manager/getMenuTreeByFun`,
  umGetDicById: `${prefix}/manager/getDicById`,
  umGetDicList: `${prefix}/manager/getDicList`,
  umGetFunctionList: `${prefix}/manager/getFunctionList`,
  umAddFunction: `${prefix}/manager/addFunction`,
  umUpdateFunction: `${prefix}/manager/updateFunction`,
  umUpdateFunState: `${prefix}/manager/updateFunState`,
  umGetFunListByUser: `${prefix}/manager/getFunListByUser`,
  umSetUserFunction: `${prefix}/manager/setUserFunction`,
  umGetRolesByUser: `${prefix}/manager/getRolesByUser`,
  umGetRoleList: `${prefix}/manager/getRoleList`,
  umAddRole: `${prefix}/manager/addRole`,
  umUpdateRoleState: `${prefix}/manager/updateRoleState`,
  umGetFunListByRole: `${prefix}/manager/getFunListByRole`,

  amGetProList: `${prefix}/asset/getProList`,
  amExportPro: `${prefix}/asset/exportPro`,// 项目生成报表
  amGetProInfo: `${prefix}/asset/getProInfo`,// 获取项目详情
  amAddProjectInfo: `${prefix}/asset/addProjectInfo`, // 新增项目
  amGetSysDic: `${prefix}/asset/getSysDic`, // 获取系统参数
  amUpdateProInfo: `${prefix}/asset/updateProInfo`, // 修改项目信息
  amDelProjectInfo: `${prefix}/asset/delProjectInfo`, // 删除项目信息
  amGetAreaList: `${prefix}/asset/getAreaList`, // 获取省市区
  amExportBuild: `${prefix}/asset/exportBuild`,// 项目生成报表
  amGetBuildList: `${prefix}/asset/getBuildList`, // 获取楼栋列表
  amGetBuildInfo: `${prefix}/asset/getBuildInfo`, // 获取楼栋详情
  amGetHookBuildData: `${prefix}/asset/getHookBuildData`, // 获取掛接楼栋列表
  amUpdateBuildInfo: `${prefix}/asset/updateBuildInfo`, // 修改楼栋信息
  amDelBuildInfo: `${prefix}/asset/delBuildInfo`, // 删除楼栋
  amAddBuildInfo: `${prefix}/asset/addBuildInfo`, // 新增楼栋
  amExportHouse: `${prefix}/asset/exportHouse`,// 项目生成报表
  amGetHouseList: `${prefix}/asset/getHouseList`, // 获取户列表
  amGetHouseInfo: `${prefix}/asset/getHouseInfo`, // 获取户详情
  amUpdateHouseInfo: `${prefix}/asset/updateHouseInfo`, // 修改户信息
  amDelHouseInfo: `${prefix}/asset/delHouseInfo`, // 删除户信息
  amAddHouseInfo: `${prefix}/asset/addHouseInfo`, // 新增户信息
  amStatisticMoney: `${prefix}/fee/statisticMoney`, // 获取能耗统计
  amStatisticDayMoney: `${prefix}//fee/statisticDayMoney`, // 获取能耗统计
  amQueryBuilding: `${prefix}/asset/queryBuilding`,// 地图楼编辑
  amQueryHouse: `${prefix}/asset/queryHouse`,// 地图户编辑
  amModifyBuilding: `${prefix}/asset/modifyBuilding`,// 楼栋编辑
  amModifyHouse: `${prefix}/asset/modifyHouse`,// 房屋编辑
  amUploadPic: `${prefix}/asset/uploadPic`,// 上传图片
  amDeletePic: `${prefix}/asset/deletePic`,// 删除图片
  uploadBuildingPic: `${prefix}/asset/uploadBuildingPic`,// 上传楼栋图片




  rmGetPositionList: `${prefix}/doc/getPositionList`, // 获取所在房间下拉
  rmGetPositionListByParentList: `${prefix}/doc/getPositionListByParentList`, // 获取柜子下拉
  rmGetPackageList: `${prefix}/doc/getPackageList`, // 获取档案袋下拉
  rmGetDocList: `${prefix}/doc/getDocList`, // 获取档案列表
  rmAddDoc: `${prefix}/doc/addDoc`, // 新增档案信息
  rmUpdateDoc: `${prefix}/doc/updateDoc`, // 更新档案信息
  rmGetDocById: `${prefix}/doc/getDocById`, // 根据ID查询档案信息
  rmGetRecord: `${prefix}/doc/getRecord`, // 获取借取信息列表
  rmBorrow: `${prefix}/doc/borrow`, // 借取
  rmApproval: `${prefix}/doc/approval`, // 审批
  rmApprovalOne: `${prefix}/doc/approvalOne`, // /获取审批记录
  rmReject: `${prefix}/doc/reject`, // /驳回
  rmBack: `${prefix}/doc/back `, // /归档

  kmGetKeySpecies: `${prefix}/key/getKeySpecies`, // 获取燃气卡种类
  kmGetKeyList: `${prefix}/key/getKeyList`, // 钥匙列表
  kmAddKey: `${prefix}/key/addKey`, // 新增钥匙
  kmUpdateKey: `${prefix}/key/updateKey`, // 更新钥匙信息
  kmGetKeyById: `${prefix}/key/getKeyById`, // 查询一条钥匙信息
  kmGetRecordList: `${prefix}/key/getRecordList`, // 获取借取信息列表
  kmApprovalOne: `${prefix}/key/approvalOne`, // /获取审批记录
  kmBorrow: `${prefix}/key/borrow`, // 借取
  kmApproval: `${prefix}/key/approval`, // 审批
  kmReject: `${prefix}/key/reject`, // /驳回
  kmBack: `${prefix}/key/back `, // /归档

  // upload: `${prefix}/receive/upload`,//上传
  upload: `${prefix}/receive/import`,// 上传
  uploadFile: `${prefix}/receive/uploadFile`,// 资产接收详情上传
  queryCommunity: `${prefix}/receive/queryCommunity`,// 获取楼盘
  queryHouse: `${prefix}/receive/queryHouse`,// 上传结果查询所有户信息接口
  queryBuilding: `${prefix}/receive/queryBuilding`,// 上传结果查询所有楼栋信息接口
  queryProject: `${prefix}/receive/queryProject`,// 查询资产接收列表接口
  receiveDetails: `${prefix}/receive/receiveDetails`,// 查询资产接收列表编辑
  dealReceiveProcess: `${prefix}/receive/dealReceiveProcess`,// 接收时间
  exportPro: `${prefix}/receive/exportPro`,// 接收时间
  mountBuilding: `${prefix}/receive/mountBuilding`,// 挂接楼盘
  mountHouse: `${prefix}/receive/mountHouse`,// 挂接楼盘
  queryAsset: `${prefix}/assetMap/queryAsset`,// 搜索引擎
  searchTransferUnit: `${prefix}/assetMap/searchBuildingName`,// 划转单位
  // searchTransferUnit: `${prefix}/assetMap/searchTransferUnit`,//划转单位
  searchProjectName: `${prefix}/assetMap/searchProjectName`,// 项目名称
  searchCommunityName: `${prefix}/assetMap/searchCommunityName`,// 地址
  mountCommunity: `${prefix}/receive/mountCommunity`,// 地图挂接小区
  queryHouseCount: `${prefix}/assetMap/queryHouseCount`,// 资产地图查询
  getCommunityInfo: `${prefix}/asset/getCommunityInfo`,// 小区详情页
  getBuildingMessage: `${prefix}/asset/getBuildingMessage`,// 楼栋详情页
  getBuildHouseInfo: `${prefix}/asset/getBuildHouseInfo`,// 楼栋详情页
  getHouseMessage: `${prefix}/asset/getHouseMessage`,// 楼栋详情页
  houseCount: `${prefix}/statistics/houseCount`,// 统计echart接口
  houseCountByUnit: `${prefix}/statistics/houseCountByUnit`,// 统计表格接口
  exportExcel: `${prefix}/statistics/exportExcel`,// 统计表格导出excel接口
  getBuildingOwnership: `${prefix}/asset/getBuildingOwnership`,// 统计表格接口
  getHouseOwnership: `${prefix}/asset/getHouseOwnership`,// 统计表格接口
  queryAreaByParams: `${prefix}/assetMap/queryAreaByParams`,// 资产地图地图接口
  screenSelect: `${prefix}/assetMap/screenSelect`,// 资产地图地图接口
  getAllCount: `${prefix}/receive/getAllCount`,// 首页接口
  searchCommunityId: `${prefix}/assetMap/searchCommunityId`,// 资产地图点击地图显示
  getCommunityList: `${prefix}/asset/getCommunityList`,// 资产地图点击地图显示
  getAllTransferUnit: `${prefix}/fee/getAllTransferUnit`,// 查询所有划转单位
  getCommunitiesByTrUnit: `${prefix}/fee/getCommunitiesByTrUnit`,// 查询所有划转单位
  getBuildsByProjCode: `${prefix}/fee/getBuildsByProjCode`,// 查询所有划转单位
  getAllHousesByBuildingCode: `${prefix}/fee/getAllHousesByBuildingCode`,// 查询所有划转单位
  getAllFeeType: `${prefix}/fee/getAllFeeType`,// 查询所有划转单位
  getFeeMaintainList: `${prefix}/fee/getFeeMaintainList`,// 查询所有划转单位
  addOrUpdateFeeMaintain: `${prefix}/fee/addOrUpdateFeeMaintain`,// 查询所有划转单位
  getAllStatisticType: `${prefix}/fee/getAllStatisticType`,// 查询所有划转单位
  deleteFeeMaintain: `${prefix}/fee/deleteFeeMaintain`,// 查询所有划转单位
  getFeeMaintainById: `${prefix}/fee/getFeeMaintainById`,// 查询所有划转单位
  icBuildingCount: `${prefix}/imageContrast/buildingCount`,// 影像对比查全部
  icQueryBuilding: `${prefix}/imageContrast/queryBuilding`,// 影像对比查询
  queryAll: `${prefix}/houseLease/queryAll`,// 租赁列表
  queryAllUnit: `${prefix}/houseLease/queryAllUnit`,// 查询划转单位列表
  del: `${prefix}/houseLease/del`,// 查询划转单位列表
  queryAllCommunity: `${prefix}/houseLease/queryAllCommunity`,// 查询小区列表
  queryBuildingByCommunity: `${prefix}/houseLease/queryBuildingByCommunity`,// 查询小区列表
  queryHouseByBuilding: `${prefix}/houseLease/queryHouseByBuilding`,// 查询小区列表
  queryOne: `${prefix}/houseLease/queryOne`,// 查询小区列表
  add: `${prefix}/houseLease/add`,// 查询小区列表
  update: `${prefix}/houseLease/update`,// 查询小区列表
  searchList: `${prefix}/projectManagement/searchList`,// 工程项目管理列表
  addProject: `${prefix}/projectManagement/addProject`,// 工程项目管理新增
  projectManagementUploadFile: `${prefix}/projectManagement/uploadFile`,// 工程项目管理新增
  deleteProject: `${prefix}/projectManagement/deleteProject`,// 工程项目管理删除
  updateProject: `${prefix}/projectManagement/updateProject`,// 工程项目管理编辑
  detailProject: `${prefix}/projectManagement/detailProject`,// 工程项目管理详情
  deleteFile: `${prefix}/projectManagement/deleteFile`,// 工程项目管理删除

  hlUploadFile: `${prefix}/houseLease/uploadFile`,// 房地产租赁管理上传


  queryContract: `${prefix}/contract/queryContract`,
  queryBuildingList: `${prefix}/contract/queryBuildingList`,
  queryFloorByBuilding: `${prefix}/contract/queryFloorByBuilding`,
  queryOne: `${prefix}/contract/queryOne`,
  queryHouseByFloor: `${prefix}/contract/queryHouseByFloor`,
  queryRenter: `${prefix}/contract/queryRenter`,
  contractadd: `${prefix}/contract/add`,
  contractupdate: `${prefix}/contract/update`,
  queryHosueInfo: `${prefix}/contract/queryHosueInfo`,
  queryRenterInfo: `${prefix}/contract/queryRenterInfo`,
  delete: `${prefix}/contract/del`,
  queryContractByBuilding: `${prefix}/contract/queryContractByBuilding`,

  getRenterList: `${prefix}/renter/getRenterList`,
  getRenterDetailInfoById: `${prefix}/renter/getRenterDetailInfoById`,
  getRenterDetailInfoById2: `${prefix}/renter/getRenterDetailInfoById2`,
  createRenter: `${prefix}/renter/createRenter`,
  updateRenter: `${prefix}/renter/updateRenter`,
  getIndustryTree: `${prefix}/renter/getIndustryTree`,
  deleteRenter: `${prefix}/renter/deleteRenter`,


  getServiceLog: `${prefix}/renter/getServiceLog`,
  getServiceLogList: `${prefix}/renter/getServiceLogList`,
  getDetailServiceLog: `${prefix}/renter/getDetailServiceLog`,
  updateServiceLog: `${prefix}/renter/updateServiceLog`,
  createServiceLog: `${prefix}/renter/createServiceLog`,
  queryBuildingByParams: `${prefix}/assetMap/queryBuildingByParams`,
  queryHouseByParams: `${prefix}/assetMap/queryHouseByParams`,
  renteruploadFile: `${prefix}/renter/uploadFile`,
  renterdeleteFile: `${prefix}/renter/deleteFile`,
  // queryRenter: `${prefix}/renter/queryRenter`,
  // renteradd: `${prefix}/renter/add`,
  // renterupdate: `${prefix}/renter/update`,
  // queryOne: `${prefix}/renter/queryOne`,
  // queryHosueInfo: `${prefix}/renter/queryHosueInfo`, 
  // queryRenterInfo: `${prefix}/renter/queryRenterInfo`,   


  // 获取合同id
  getContractId: `${prefix}/contract/getContractId`,
  // 获取合同详情
  getcontract: `${prefix}/contract/queryOne`,
  // 合同导出
  exportContract: `${prefix}/contract/exportContract`,
  // 企业图谱
  getRenterInfo: `${prefix}/receive/getRenterInfo`,


  // 进驻退租统计
  queryInCom: `${prefix}/companystatistics/queryInCom`,
  queryInCom1: `${prefix}/companystatistics/queryInCom1`,
  queryOutCom: `${prefix}/companystatistics/queryOutCom`,
  queryOutCom1: `${prefix}/companystatistics/queryOutCom1`,
  RankingCompanyRz: `${prefix}/companystatistics/RankingCompanyRz`,
  RankingCompanyLs: `${prefix}/companystatistics/RankingCompanyLs`,
  RankingCompanyYx: `${prefix}/companystatistics/RankingCompanyYx`,
  InOrOutCompanyFx: `${prefix}/companystatistics/InOrOutCompanyFx`,

  // 招商分析
  // 招商分析－招商客户分析
  getEnterprise: `${prefix}/receive/getEnterprise`,
  // 招商分析－招商行业分析
  getHyType: `${prefix}/receive/getHyType`,
  // 招商分析－-客户走势图
  getRenterBar: `${prefix}/receive/getRenterBar`,

  // 企业图片logo
  uploadBuildingPic: `${prefix}/renter/uploadBuildingPic`,

  // 协议管理生成报表接口
  exportProtocol: `${prefix}/contract/exportProtocol`,

  // 企业登记机关区划
  addOption: `${prefix}/select/addOption`, // 新增
  getOptionList: `${prefix}/select/getOptionList`, // 列表（分页）
  getOptionCodeList: `${prefix}/select/getOptionCodeList`, // 选项列表
  editOption: `${prefix}/select/editOption`, // 编辑

  // 根据用户名获取权限
  getFunListByUserCode: `${prefix}/manager/getFunListByUserCode`, // 编辑

  // 按月查询企业列表
  getRenterListByMonth: `${prefix}/renter/getRenterListByMonth`,

  // 地图选房信息新增接口
  addHouse: `${prefix}/contract/addHouse`,

  // 地图选房信息删除接口
  releaseHouse: `${prefix}/contract/releaseHouse`,

  // 服务轨迹问题分类下拉数据
  getReasonType: `${prefix}/contract/getReasonType`,

  // 合同详情下载接口
  contractPrint: `${prefix}/contract/contractPrint`,

  // 房屋管理附件上传接口
  uploadAttachments: `${prefix}/asset/uploadAttachments`,

  // 房屋管理附件删除接口
  deleteAttachments: `${prefix}/asset/deleteAttachments`,

  // 待转合同转为在租合同和回退的接口
  agreementApproval: `${prefix}/contract/agreementApproval`,

};

export default Api;
