import React from 'react';
import { Card, Row } from 'antd';
import classNames from 'classnames';
import { ProgressPer } from '@/components/ProgressPer';
import axios from 'axios';
import Api from '../../services/apis'
import router from 'umi/router';

import styles from './style.less';

export default class Part5 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      reasons: [{
        name: '租金过高',
        count: '0%',
      }, {
        name: '物业服务不满意',
        count: '0%',
      }, {
        name: '企业自身原因',
        count: '0%',
      }],
      rankings: [],
    };
  }

  componentDidMount() {
    Promise.all([
      axios.post(Api.getIndustryOfRenterIncome, {count: 5}),
      axios.post(Api.getRenterReason),
    ]).then(resList => {
      let rankings = [];
      let reasons = [];
      if (resList[0].data.code === '200') rankings = resList[0].data.result;
      if (resList[1].data.code === '200') {
        if (resList[1].data.result && resList[1].data.result.length > 0) {
          reasons = resList[1].data.result.map(res => {
            return {
              name: res.reason,
              count: res.percent,
            };
          });
        } else {
          reasons = this.state.reasons;
        }
      }
      this.setState({ rankings, reasons });
    }).catch(e => console.error(444444, e));
  }

  jumpToRenter = (params) => {
    router.push({
      pathname: '/fe/dataMaintenance/renterManagement',
      state: {
        hyType: params
      }
    });
  }

  render() {
    const rankings = this.state.rankings.map((ranking, index) => {
      let paiming;
      if (index === 0) paiming = <img style={{marginRight: '20px', height: '29px', width: '29px'}} src={require('../../assets/indus-list-0.png')} />;
      else if (index === 1) paiming = <img style={{marginRight: '20px', height: '29px', width: '29px'}} src={require('../../assets/indus-list-1.png')} />;
      else if (index === 2) paiming = <img style={{marginRight: '20px', height: '29px', width: '29px'}} src={require('../../assets/indus-list-2.png')} />;
      else paiming = <span className={styles.part5ContentLeftIndex}>{index +1}</span>;
      return (
        <p key={ranking.name} style={{ display: 'flex' }}>
          {paiming}
          <span className={styles.part5ContentLeftName} onClick={() => this.jumpToRenter(ranking.name)}>{ranking.name}</span>
          <span className={styles.part5ContentLeftCount}><a>{ranking.totalIncome}</a></span>
        </p>
      );
    });

    const progressPers = this.state.reasons.map((reason, index) => <ProgressPer data={reason} key={index} titleSize={'13'} countSize={'20'}/>);
    
    return (
      <div className={styles.part5Content}>
        <Row className={classNames(styles.part5ContentLeft)}>
          <div className='mainContainer' style={{ overflowX: 'hidden', height: '100px' }}>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>行业全口径税排名</p>
                <p>INDUSTRY LIST</p>
              </div>
            </div>
          </div>
          <div className={styles.partBorder} style={ { display: 'flex', justifyContent: 'space-around', alignItems: 'center', flexWrap: 'wrap', height: '217px' } }>
            {rankings}
          </div>
        </Row>
        <Row className={classNames(styles.part5ContentRight)}>
          <div className='mainContainer' style={{ height: '100px' }}>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>退租分析</p>
                <p>REFUND ANALYSIS</p>
              </div>
            </div>
          </div>
          <div className={styles.partBorder} style={ { display: 'flex', justifyContent: 'center', flexWrap: 'wrap', height: '217px' } }>
            {progressPers}
          </div>
        </Row>
      </div>
    )
  }
}