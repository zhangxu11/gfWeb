import React, { Component } from 'react';
import {
  Input,
  Col,
  Select,
  Button,
  Tooltip,
  Tag,
  Pagination,
  notification,
  Checkbox,
  Radio,
  Icon,
  Row,
  Badge,
  message,
} from 'antd';

import Link from 'umi/link';
import axios from 'axios';
import QueueAnim from 'rc-queue-anim';
import Api from '../../services/apis';
import IframeComm from '../../components/IframeComm';

import Info from './Info';
import Chart from './Chart';
import RoomInfo from './RoomInfo';
import RoomInfo2 from './RoomInfo2';
import LDInfo from './LDInfo';
import ZoneInfo from './ZoneInfo';

import styles from './style.less';

const InputGroup = Input.Group;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const RadioGroup = Radio.Group;

const noPic =
  'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0NSA3OS4xNjM0OTksIDIwMTgvMDgvMTMtMTY6NDA6MjIgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjkxQkRDRjc5RTdCNjExRTg4NjFCQTZBMDM3QUZDMUNCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjkxQkRDRjdBRTdCNjExRTg4NjFCQTZBMDM3QUZDMUNCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTFCRENGNzdFN0I2MTFFODg2MUJBNkEwMzdBRkMxQ0IiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCRENGNzhFN0I2MTFFODg2MUJBNkEwMzdBRkMxQ0IiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCAC0ALQDAREAAhEBAxEB/8QAeAABAAMBAQEBAAAAAAAAAAAAAAMEBQIGAQgBAQAAAAAAAAAAAAAAAAAAAAAQAAECBgECBQEGBAQHAQAAABEBAgAxQRIiAwQhBVFhgRMjgnGhscHRMpFCoiRishQ08HLSQ3NUFTURAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP0S5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwC35Db9R8vCAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwHO97devbsc0tahULMQGbt75qK+3pVyO/crlC9PsMB2zvPGfiut97wiyB/jAF73w1u+N/UeHUesB9Z3fRt2prZqcq7FRCqon6wFrl8jVx9av2NVWuVG9KqD+UBWXvPCW7FykUn98AXvPCW7FykUn98AXvPCW7FykUn98BbTYx+ldyMVWuajk6hVQGAor3vird8T+o8KesAXvfFW74n9R4U9YAve+Kt3xP6jwp6wHeru3H3bU1t1Ou2KidVT9YC7uc3Xr2bHNLWoVCzRICtp7lxuRtXXra5XO8ehHrAS8zenH0bNq67kx6EHqICPhc1OY3avtWq0FLifuSAtOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcBztaWbERlyqnRFrAec3cblakXZt1q0rNenVfsgJuHuejVYrGOa5VL3/uTpRVWAg43vXPdrYj7Gq59yIqI1Jr1gLfE5PN3PVNWpioipfa1E6QGryW8V5ZvRq9UcjXOBoZwFXfx+1t07Va1lzWqrQ/qqjwMBl8Fmp/KYm1EXX1KKoSXjAa68XtXUN1r4Zz++AnVNP+n2N1IjmNaEtcZJ+UBg8PYzXtVztfuojVDPFYD7qdu3bVtcxrlUo1wRPQoID6m/ku2+02x7nKEtYwKvl0gOtDU/8AqBE6e45EbKqwG1zk/tORjNkzPpAYHD3t0b02ObciIuKdJpAW+b3XVyND9bdatVypkq+CwEXb+fr4zNjXsV94kolAbHE5LOXqfsbrVEutVFWYRF/OAnc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcBxyNK7dW3WiBXoLv8AjwgMDl8XRx3u1e6rtrR0tCdUPisBzrTiJofe53v/APbH7R59ICzo38HTxN2pFV23a0K9UVEI6eiQEvZWod6IqP6N6IUqviiQFnm9r/1O5dtytVERESZgKPN7WzjaXbF2qqlEYgmq+sBR1JqVV9xytQdLUPWA1P8A4SKiqm1VkOidfvgLWjhJxeNuYi3o7qZUgMfgLvTcvssR+y1Q1ZfikBHuc5+967QxxW8J0NZQE3DTc3cqcaza9UkvTpPpdbAOPcvckuTJdilsupWA2+cn9pyMZsmZ9IDz3G269W1H7NabGISxYC6vcuB1/sm9fs/SA7083g7drdacJiK9UQ9P0gNVunWxrm69SNaoUNCIv8IDpzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgPP92//AENv0/5UgJ/b5n/oauv+Ff8AqgIuS3kppcr+Jr1N6F7Wqip1p1WAn7GhXegPRvSVVgNTft1amPfsCN6dVWcBh8ndu7hybdTFt/lb+awEHL4/+n3u0m5WohXzVEWA3eRytWnjLtVEei2hEd+6AauQnJ4mzamu0oqAzEBj9ubsfvc3X+9WOtp1gJ9nD7qhVzG7DNQxfx6wHHHds1crX7nGRjnORpDm/u6eIgONIXunRLkXY4efVYDY7grW8PerkBaEUzVekBkdp0+7yurL2taqqiy8IDbXjaMvgash0TrAef4qf32tAc06esB6RzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAZ/M7Q7kcjZuTZbcA0GSInjARr2Pb1G9V8MZ/1QHx3Ytin51Xwxn/VAXOHwGcXW9qF6uBcvQj8ID5ze3pytjXOVUayiTcR4ygJtXG06WOZq1hvSvVYCvze1s5OxdiKrH9EUBUWArr2FOvyqol0Tr98Be18Rmrju0a29ACq9VVawGevYHdRuIljP+qAL2B3UbiJYz/qgC9gd1G4iWM/6oCfi9oZo2Lsc5drm/sT9qdfVYCTuHA2cpzLVta03eZA6eUBLx+Hq4+t2vWwkFVXq6Amc03Ykis4DO19mTXvTberrXI5GhEP3wGi5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwFXmc/j8ZXNe1XPcFtRfx8ICuzvfHc5UfqVqOHUn+MBo9HI5UbcjgqKizgKnL7nxdL3sVFe/oUavh4rAQN73x3Kt+pzbgVRTKA0WP17WK/WiPa4BUWf6QEHM5vH4xTYhe4BqL1X9ICs3vfHc5UfqVqOHUn+MBd379bND99t+sIpRf3JAV+P3PRydq6manXP8VSnrAWeRt1aWPftCN6VnAUH9749zrdLnIs1VQR/GAscbuPG5KuaiK3Y4YOUEeCwFtzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwHmeY9z+Xuc5St6/cogO+dxF4u5Nd1xajj9vT8oC6zku19mR387lsa4+a/giQFHicTZytqsZ0CFzlgLXM7O/Tqds1uvazq5FmKqkBx2je5u9dIuZt/ll1TqYCDnvc/m7lWj1T0aoSA55OrVqejWPvUJf0AXwgL3HVy9l3lOiOCL5FF/OAr9qQ81qA9F6SpAO6b128x6fy61tRPs6LAT8fsuzZp9x7rXORFY1EM/GAqbeHydW5zEY5zmL0c1FX1SA3+M7Zs46P2a1TY5EuReiqqdJUgJXNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgPMcn/c7f8And+MBc75/vG9P5E/FYD77Ln9lRWp+x1y+aIqp9xgIe3cxnH2OTYh17ERF8hKAvc7ufFXRsZqW92xAeoHrAUe16XbOW1Ubc1hc6nkkBDzP93v/wDI7/MsBfXs27a9dl6IxwVPHrAWuXx2aO2bdbGhqInU9V6p1gMztSHmtQHovSVICPn6na+ZtaqTcrk+x3VIDU4/deIugbcXoiI5OvUdOggM7mc/bt5D36nvZrUIjUVU6J9kBtcNm1OI33UV+xyIrlVeqnr90BO5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcBj7uy8t+7Y9HMDnKqdVqp8ICfuXbeRyeR7mu1ERqNyVSqlfKAs8Liv0cX2djUctQvRSprAUuV2Ryvc7jqiNox35LAQM7JzFXK1qeJMBrcbh6uNqcxjbiC5V6u/SAzuR2bl7N+3Y1WBzlchVSFU+EBqtYqMtVpVEai9ZiA45mh+7j7dbERHPRAqr0ULAUOF2rk6OQmx9jmoi9EVSpT7IC3zu36+UiqLdiftelftSAzHdk5qKqJa5KKi/rAWOL2VWOv3h9v7WNXov2rAajmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4Bb8ht+o+XhAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwC35Db9R8vCAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgFvyG36j5eEAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnALfkNv1Hy8IA5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAW/IbfqPl4QBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAc03Ykis4A5puxJFZwBzTdiSKzgDmm7EkVnAHNN2JIrOAOabsSRWcAt+Q2/UfLwgDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gDvbyPldP0gHx+5/j9fCA//2Q==';

class AssetMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showList: false,
      page: 1,
      resultDatas: [],
      resultDatasCount: 0,
      showFilter: false,
      mj: [
        { label: '小于100㎡', value: 1 },
        { label: '100㎡~200㎡', value: 2 },
        { label: '200㎡~300㎡', value: 3 },
        { label: '300㎡以上', value: 4 }
      ],
      zjdj: [
        { label: '100元/月㎡以下', value: 1 },
        { label: '100元/月㎡~200元/月㎡', value: 2 },
        { label: '200元/月㎡以上', value: 3 }
      ],
      zjzj: [
        { label: '1,00,000元以下', value: 1 },
        { label: '1,00,000元~2,00,000元', value: 2 },
        { label: '2,00,000元以上', value: 3 }
      ],
      cx: [
        { label: '东', value: 1 },
        { label: '西', value: 2 },
        { label: '南', value: 3 },
        { label: '北', value: 4 }
      ],
      rentState: [
        { label: '待租赁', value: '待租赁' },
        { label: '租赁中', value: '租赁中' }
      ],
      mjValue: [],
      zjdjValue: [],
      zjzjValue: [],
      cxValue: [],
      isRentValue: [],
      right: 0,
      roomright: -380,
      ldright: -380,
      zoneLeft: -380,
      nowType: 'info',
      sendMapData: {
        type: 'all',
        data: [],
      },
      searchType: 1,
      timer: '',
      res: [],
      searchVal: '',
      showLoading: false,
      communityVal: [],
      arrVal: {
        props: {
          children: '',
        },
      },
      cityData: {},
      postData: {
        buildingArea: [],
        rentPrice: [],
        rentTotal: [],
        isRent: []
      },
      infoData: {
        buildingArea: 0,
        buildingCount: 0,
        houseCount: 0,
        landArea: 0,
        projectCount: 0,
        rentingArea: 0,
        waitRentArea: 0,
        xArr: [],
        xArrFee: [],
        yArr: [],
        yArrFee: [],
      },
      clickXQ: {
        communityName: '',
        communityId: '',
        address: '',
        buildCount: '',
        houseCount: '',
        xCoord: '',
        yCoord: '',
        cityCode: '',
        city: '',
      },
      showMapDiv: false,
      roomData: {},
      ldData: {},
      buildId: ''
    };
  }

  searchVal(val) {
    const resultDatas = [];
    switch (Number(this.state.searchType)) {
      case 1:
        for (let i = 0, outLen = val.length; i < outLen; i++) {
          resultDatas.push({
            badge: (i + 1).toString(),
            projectName: val[i].projectName,
            projectCode: val[i].projectCode,
            totalBuildingArea: val[i].totalBuildingArea,
            totalLandArea: val[i].totalLandArea,
            buildCount: val[i].buildCount,
            houseCount: val[i].houseCount,
            receivedDate: val[i].receivedDate,
            transferUnit: val[i].transferUnit,
            isTitle: true,
          });
          for (let j = 0, inLen = val[i].communityList.length; j < inLen; j++) {
            resultDatas.push({
              title: val[i].communityList[j].communityName,
              tags: ['未知'],
              position: val[i].communityList[j].address,
              ld: val[i].communityList[j].buildCount,
              fw: val[i].communityList[j].houseCount,
              projectCode: val[i].projectCode,
              projectName: val[i].projectName,
              communityId: val[i].communityList[j].communityId,
              city: val[i].communityList[j].city,
              cityCode: val[i].communityList[j].cityCode,
              yCoord: val[i].communityList[j].yCoord,
              xCoord: val[i].communityList[j].xCoord,
              isTitle: false,
              url: val[i].communityList[j].url,
            });
          }
        }
        break;
      case 2:
        resultDatas.push({
          badge: '1',
          projectName: val.projectName,
          projectCode: val.projectCode,
          totalBuildingArea: val.totalBuildingArea,
          totalLandArea: val.totalLandArea,
          buildCount: val.buildCount,
          houseCount: val.houseCount,
          receivedDate: val.receivedDate,
          transferUnit: val.transferUnit,
          isTitle: true,
        });
        for (let i = 0, outLen = val.communityPage.list.length; i < outLen; i++) {
          resultDatas.push({
            title: val.communityPage.list[i].communityName,
            tags: ['未知'],
            position: val.communityPage.list[i].address,
            ld: val.communityPage.list[i].buildCount,
            fw: val.communityPage.list[i].houseCount,
            projectCode: val.communityPage.list[i].projectCode,
            projectName: val.communityPage.list[i].projectName,
            communityId: val.communityPage.list[i].communityId,
            city: val.communityPage.list[i].city,
            cityCode: val.communityPage.list[i].cityCode,
            yCoord: val.communityPage.list[i].yCoord,
            xCoord: val.communityPage.list[i].xCoord,
            isTitle: false,
            url: val.communityPage.list[i].url,
          });
        }

        break;
      default:
        resultDatas.push({
          title: val.communityName,
          tags: ['未知'],
          position: val.address,
          ld: val.buildCount,
          fw: val.houseCount,
          communityId: val.communityId,
          communityName: val.communityName,
          city: val.city,
          cityCode: val.cityCode,
          yCoord: val.yCoord,
          xCoord: val.xCoord,
          isTitle: false,
          url: val.url,
        });
        break;
    }

    this.setState({
      resultDatas,
      showList: true,
      showMapDiv: false,
      showFilter: false,
    });
  }

  showTotal(total) {
    return (
      <span>
        共<span style={{ color: '#e22534' }}>{total}</span>条结果
      </span>
    );
  }

  filter() {
    this.setState({
      showList: false,
      showFilter: !this.state.showFilter,
      showMapDiv: false,
    });
  }

  changeFilter(type, checkedValues) {
    const res = {
      type,
    };
    this.setState({
      right: -380,
    });
    const { postData } = this.state;
    postData.buildingArea = this.state.mjValue; // 面积 
    postData.rentPrice = this.state.zjdjValue; // 租金单价
    postData.rentTotal = this.state.zjzjValue; // 租金总价
    postData.isRent = this.state.isRentValue; // 租赁状态
    switch (type) {
      case 'mj':
        this.setState({
          mjValue: checkedValues,
        });
        res.checkedValues = checkedValues;
        postData.buildingArea = checkedValues;
        break;
      case 'zjdj':
        this.setState({
          zjdjValue: checkedValues,
        });
        res.checkedValues = checkedValues;
        postData.rentPrice = checkedValues;
        break;
      case 'zjzj':
        this.setState({
          zjzjValue: checkedValues,
        });
        res.checkedValues = checkedValues;
        postData.rentTotal = checkedValues;
        break;
      case 'rentState':
        this.setState({
          isRentValue: checkedValues,
        });
        res.checkedValues = checkedValues;
        postData.isRent = checkedValues;
        break;
    }
    this.setState({ postData });
    if (this.state.mapType === 'clickedXQ') {
      this.getCommunityByFilter(this.state.communityId);
    } else if (this.state.mapType === 'clickedLD') {
      this.getHouseByParams(this.state.buildingId);
    } else {
      this.getMapData(this.formateData(postData));
    }
  }

  closeInfo(name) {
    this.setState({
      [name]: -380,
    });
  }

  // changeTools(nowType) {
  //   this.setState({
  //     nowType
  //   });
  // }
  closeZoneInfo = () => {
    this.setState({
      zoneLeft: -400
    })
  }
  onReceiveMessage(data) {
    let right = 0;
    const postData = {
      buildingArea: this.state.mjValue, // 面积 
      rentPrice: this.state.zjdjValue, // 租金单价
      rentTotal: this.state.zjzjValue, // 租金总价
      isRent: this.state.isRentValue, // 租金总价
      province: data.data.province,
      city: data.data.city,
      area: data.data.district,
    };
    this.setState({ mapType: data.data.type });

    // 点击园区空白处
    if (data.data.type === 'clickedLand') {
      // this.setState({
      //   zoneLeft: 200
      // })
    }
    // 点击楼栋
    if (data.data.type === 'clickedXQ') {
      this.setState({ mapType: 'clickedXQ', communityId: data.data.communityId });

      // right = -380;
      this.setState({
        ldright: -380,
        right,
      });
      axios
        .get(Api.searchCommunityId, {
          params: {
            communityId: data.data.communityId,
          },
        })
        .then(res => {
          if (res.data.entiy === null) {
            message.warn('暂无数据！');
          } else {
            this.setState({
              clickXQ: res.data.entiy,
              showMapDiv: true,
              showList: true,
              searchVal: '',
              res: [],
              resultDatas: [],
              page: 1,
            });
          }
        });


      // 查询小区
      this.getCommunityByFilter(data.data.communityId);
    } else {
      // this.receivedMapData(this.formateData(postData));
    }
    // 点击楼栋
    if (data.data.type === 'clickedLD') {
      const buildingId = data.data.build_id;
      // 查询楼栋
      this.getHouseByParams(buildingId);
    }

    // 点击房间
    if (data.data.type === 'clickedLDR') {
      let showInfoOne;
      let usedFlag = 0;
      if (data.data.isRent === '待租赁') showInfoOne = false;
      else if (data.data.isRent === '租赁中') showInfoOne = true;
      if (data.data.isRent === '占用') {
        notification['warning']({
          message: '该房间已占用',
        });
      }
      if (data.data.isRent && data.data.isRent !== '占用') {
        this.setState({
          ldright: -380,
          roomright: 0,
          showInfoOne,
          houseId: data.data.house_id,
          roomData: { ...data.data }
        });
      }

      // 查询房间
      // this.getRoomData();
    }
    // 点击楼栋点
    if (data.data.type === 'clickedLDPoint') {
      const buildingId = data.data.build_id;
      const currentFloor = data.data.build_floor;
      this.setState({
        right: -380,
        roomright: -380,
        ldright: 0,
        buildingId, currentFloor,
        ldData: { ...data.data },
        buildId: data.data.buildId
      });
      // 查询楼栋
      this.getHouseByParams(buildingId);
    }
    if (data.data.type !== 'clickedLD' && data.data.type !== 'clickedLDR' && data.data.type !== 'clickedXQ') {
      this.getMapData(this.formateData(postData));
    }

    this.setState({
      right: -380,
      cityData: data,
    });
  }

  // 查询房间
  getRoomData = () => {
    const that = this;
    const { houseId } = this.state;

    axios
      .get(Api.xxxx, {
        params: {
          houseId,
        },
      })
      .then(res => {
        if (res.data === null) {
          message.warn('暂无数据！');
        } else {
          that.setState({ roomData: { ...res.data, houseId } });
        }
      });
  }

  // 查询楼栋数据
  getHouseByParams = (buildingId) => {
    if (buildingId) {
      const filterData = this.formateData(this.state.postData);
      axios
        .get(Api.queryHouseByParams, {
          params: {
            buildingId,
            ...filterData
          },
        })
        .then(res => {
          if (res.data === null) {
            message.warn('暂无数据！');
          } else {
            const data = res.data.houses && res.data.houses.map(item => ({ houseCode: item.houseCode, isRent: item.isRent }))
            this.setState({
              sendMapData: {
                type: 'clickedLD',
                data,
                buildingId,
                floors: res.data.floors
              }
            });
          }
        });
    }
  }


  // 查询小区数据
  getCommunityByFilter = (communityId) => {
    const filterData = this.formateData(this.state.postData);
    // if (!communityId) {
    axios
      .get(Api.queryBuildingByParams, {
        params: {
          communityId,
          ...filterData
        },
      })
      .then(res => {
        if (res.data === null) {
          message.warn('暂无数据！');
        } else {
          const data = res.data && res.data.map(item => item.buildingCode)
          this.setState({
            sendMapData: {
              type: 'clickedXQ',
              data
            }
          });
        }
      });
    // }
  }

  searchTransferUnit(searchVal, arr, page, sType) {
    // 搜索
    const that = this;

    this.setState({
      searchVal,
      arrVal: arr,
      right: -380,
    });
    let url = '';
    let params = {
      ...this.formateData({
        buildingArea: this.state.mjValue, // 面积
        rentPrice: this.state.zjdjValue, // 租金单价
        rentTotal: this.state.zjzjValue, // 租金总价
        isRent: this.state.isRentValue, // 租赁状态
      }),
    };

    params.currentPage = page;
    const type = sType || Number(this.state.searchType);

    switch (type) {
      case 1:
        url = Api.searchTransferUnit;
        params = { name: arr.props.children };
        break;
      case 2:
        url = Api.searchProjectName;
        params.name = arr.props.children;
        break;
      case 3:
        url = Api.searchCommunityName;
        params.address = arr.props.children;
        break;
    }

    axios
      .get(url, {
        params: {
          ...params,
        },
      })
      .then(res => {
        switch (type) {
          case 1:
            this.setState({
              // resultDatasCount: res.data.totalSize,
              showList: true,
              sendMapData: {
                type: 'search',
                searchType: Number(this.state.searchType),
                buildingCode: res.data.buildingCode,
              },
            });
            // this.searchVal(res.data.list);
            break;
          case 2:
            this.setState({
              showList: true,
              resultDatasCount: res.data.entiy.communityPage.totalSize,
              sendMapData: {
                type: 'search',
                searchType: Number(this.state.searchType),
                buildingCode: res.data.buildingCode,
              },
            });
            // this.searchVal(res.data.entiy);
            break;
          case 3:
            /* if (res.data.entiy === null) {
              message.warn('暂无数据');
              this.setState({
                showList: false,
              });
            } else { */
            this.setState({
              showList: true,
              communityVal: res.data.entiy,
              resultDatasCount: res.data.totalSize,
              sendMapData: {
                type: 'search',
                searchType: Number(this.state.searchType),
                buildingCode: res.data.buildingCode,
              },
            });
            // this.searchVal(res.data.entiy);
            /* } */

            break;
        }
      });
  }

  handleChange(searchVal, arr) {
    this.searchTransferUnit(searchVal, arr, this.state.page);
  }

  changeLeft(searchType) {
    this.setState({
      searchType,
      searchVal: '',
      res: [],
      resultDatas: [],
      showList: false,
      showMapDiv: false,
      page: 1,
    });
  }

  handleSearch(searchContent) {
    this.setState({
      right: -380,
    });
    if (this.state.timer !== '') {
      clearTimeout(this.state.timer);
    }
    if (searchContent.length > 0) {
      this.setState({
        showLoading: true,
      });
    }
    const timer = setTimeout(() => {
      const that = this;
      axios
        .get(Api.queryAsset, {
          params: {
            searchType: 3,// that.state.searchType,
            searchContent,
          },
        })
        .then(res => {
          this.setState({
            res: res.data,
          });
          if (res.data.length === 0) {
            this.setState({
              showLoading: false,
            });
          }
        });
    }, 600);
    this.setState({
      timer,
    });
  }

  formateData(postData) {
    const data = JSON.parse(JSON.stringify(postData));
    data.buildingArea = data.buildingArea.join();
    data.rentPrice = data.rentPrice.join();
    data.rentTotal = data.rentTotal.join();
    data.isRent = data.isRent.join();
    return data;
  }
  componentWillUnmount() {

  }
  componentWillMount() {
    const postData = {
      buildingArea: this.state.mjValue, // 面积
      rentPrice: this.state.zjdjValue, // 租金单价
      rentTotal: this.state.zjzjValue, // 租金总价
      isRent: this.state.isRentValue, // 租金总价
      province: '',
      city: '',
      area: '',
    };
    this.getMapData(this.formateData(postData));
    // this.receivedMapData(this.formateData(postData));
  }

  receivedMapData(postData) {
    axios
      .get(Api.queryHouseCount, {
        params: {
          ...postData,
        },
      })
      .then(res => {
        this.setState({
          right: 0,
          infoData: res.data,
        });
      });
  }

  getMapData(postData) {
    function screenSelect() {
      return axios.get(Api.screenSelect, {
        params: {
          ...postData,
        },
      });
    }

    function queryAreaByParams() {
      return axios.get(Api.queryAreaByParams, {
        params: {
          ...postData,
        },
      });
    }

    axios.all([screenSelect(), queryAreaByParams()]).then(
      axios.spread((screenSelect, queryAreaByParams) => {
        // 两个请求现在都执行完成
        this.setState({
          sendMapData: {
            type: 'filter',
            data: {
              screenSelect: screenSelect.data,
              queryAreaByParams: queryAreaByParams.data,
            },
          },
        });
      })
    );
  }

  changePage(current, size) {
    searchTransferUnit(this.state.searchVal, this.state.arrVal, current);
  }
  /* 
    filterOne = () => {
      return (
        <div className={styles.filter} key="filter">
          <div className={styles.filterItem}>
            <div className={styles.filterHeader}>房产用途</div>
            <div>
              <CheckboxGroup
                options={this.state.qsxz}
                onChange={this.changeFilter.bind(this, 'qsxz')}
                value={this.state.qsValue}
              />
            </div>
          </div>
          <div className={styles.filterItem}>
            <div className={styles.filterHeader}>资产状态</div>
            <div>
              <CheckboxGroup
                options={this.state.zczt}
                onChange={this.changeFilter.bind(this, 'zczt')}
                value={this.state.zcValue}
              />
            </div>
          </div>
          <div className={styles.filterItem}>
            <div className={styles.filterHeader}>租赁状态</div>
            <div>
              <CheckboxGroup
                options={this.state.zlzt}
                onChange={this.changeFilter.bind(this, 'zlzt')}
                value={this.state.zlValue}
              />
            </div>
          </div>
        </div>
      );
    }; */

  showfilter = () => (
    <div className={styles.filter} key="filter">
      <div className={styles.filterItem2}>
        <div className={styles.filterHeader}>面积</div>
        <div>
          <CheckboxGroup
            options={this.state.mj}
            onChange={this.changeFilter.bind(this, 'mj')}
            value={this.state.mjValue}
          />
        </div>
      </div>
      <div className={styles.filterItem2}>
        <div className={styles.filterHeader}>租金单价</div>
        <div>
          <CheckboxGroup
            options={this.state.zjdj}
            onChange={this.changeFilter.bind(this, 'zjdj')}
            value={this.state.zjdjValue}
          />
        </div>
      </div>
      <div className={styles.filterItem2}>
        <div className={styles.filterHeader}>租金总价</div>
        <div>
          <CheckboxGroup
            options={this.state.zjzj}
            onChange={this.changeFilter.bind(this, 'zjzj')}
            value={this.state.zjzjValue}
          />
        </div>
      </div>
      <div className={styles.filterItem2}>
        <div className={styles.filterHeader}>租赁状态</div>
        <div>
          <CheckboxGroup
            options={this.state.rentState}
            onChange={this.changeFilter.bind(this, 'rentState')}
            value={this.state.isRentValue}
          />
        </div>
      </div>
      {/* <div className={styles.filterItem2}>
          <div className={styles.filterHeader}>朝向</div>
          <div>
            <CheckboxGroup
              options={this.state.cx}
              onChange={this.changeFilter.bind(this, 'cx')}
              value={this.state.cxValue}
            />
          </div>
        </div> */}
    </div>
  )

  render() {
    const { infoData } = this.state;
    return (
      <div className={styles.mapBox}>
        <div className={styles.searchBox}>
          <InputGroup compact>
            <Select
              className={styles.select}
              defaultValue="3"
              onChange={this.changeLeft.bind(this)}
            >
              {/* <Option value="1">划转单位</Option>
              <Option value="2">项目名称</Option> */}
              <Option value="3">地址</Option>
            </Select>
            <Select
              showSearch
              placeholder="请输入内容"
              className={styles.search}
              defaultActiveFirstOption={false}
              showArrow
              filterOption={false}
              onSearch={this.handleSearch.bind(this)}
              onChange={this.handleChange.bind(this)}
              notFoundContent={
                this.state.showLoading ? (
                  <span>
                    加载中
                    <Icon type="loading" />
                  </span>
                ) : (
                    <span>暂无数据</span>
                  )
              }
              onBlur={() => {
                this.setState({ showLoading: false });
              }}
              value={this.state.searchVal}
            >
              {this.state.res.map((item, index) => (
                <Option key={index} value={item + index} title={item}>
                  {item}
                </Option>
              ))}
            </Select>
            <Tooltip placement="bottom" title="搜索">
              <Button
                type="primary"
                icon="search"
                onClick={this.handleChange.bind(this, this.state.searchVal, this.state.arrVal)}
              />
            </Tooltip>
            &emsp;
            <Tooltip placement="bottom" title="地图筛选条件">
              <Button type="primary" icon="filter" onClick={this.filter.bind(this)}>
                筛选条件
              </Button>
            </Tooltip>
          </InputGroup>
          {/* <div
            className={styles.searchResult}
            style={{
              opacity: this.state.showList ? '1' : '0',
            }}
          >
            <QueueAnim component="ul" className={styles.mandatory}>
              {this.state.showList && this.state.resultDatas.length !== 0
                ? this.state.resultDatas.map((item, index) => {
                    if (item.isTitle) {
                      return (
                        <li
                          key={index}
                          className={styles.searchResultItem}
                          style={{
                            position: 'sticky',
                            top: 0,
                            zIndex: 99,
                          }}
                        >
                          <div className={`clearfix ${styles.itemTitle}`}>
                            <Badge count={item.badge} style={{ backgroundColor: '#52c41a' }} />
                            &emsp;
                            {item.projectName}
                          </div>
                          <Row>
                            <Col className="mrb5" span={11} title={item.projectCode}>
                              <span className="label">项目编号：</span>
                              {item.projectCode}
                            </Col>
                            <Col className="mrb5" span={11} offset={1}>
                              <span className="label" title={item.totalBuildingArea}>
                                总建面：
                              </span>
                              {item.totalBuildingArea}
                            </Col>
                            <Col className="mrb5" span={11} title={item.totalLandArea}>
                              <span className="label">土地面积：</span>
                              {item.totalLandArea}
                            </Col>
                            <Col className="mrb5" span={11} offset={1} title={item.buildCount}>
                              <span className="label">楼栋数量：</span>
                              {item.buildCount}
                            </Col>
                            <Col className="mrb5" span={11} title={item.houseCount}>
                              <span className="label">房产数量：</span>
                              {item.houseCount}
                            </Col>
                            <Col className="mrb5" span={11} offset={1} title={item.transferUnit}>
                              <span className="label">划转单位：</span>
                              {item.transferUnit}
                            </Col>
                            <Col className="mrb5" span={11} title={item.receivedDate}>
                              <span className="label">接管时间：</span>
                              {item.receivedDate}
                            </Col>
                          </Row>
                        </li>
                      );
                    } else {
                      return (
                        <li key={index} className={styles.searchResultItem}>
                          <div className={`clearfix ${styles.itemTitle}`}>
                            <span className={styles.redC}>
                              <Link
                                target="_blank"
                                to={{
                                  pathname: '/fe/assetMap/communityDetails',
                                  state: {
                                    communityId: item.communityId,
                                  },
                                  search: `?communityId=${item.communityId}`,
                                }}
                              >
                                {item.title}
                              </Link>
                            </span>
                          </div>
                          <div className={`clearfix ${styles.itemContent}`}>
                            <div className={`pull-left ${styles.w240}`}>
                              <p title={this.state.clickXQ.position}>{item.position}</p>
                              <p>资产数量：</p>
                              <div className="clearfix">
                                <p className={`pull-left ${styles.w50}`}>楼栋：{item.ld}</p>
                                <p className={`pull-left ${styles.w50}`}>房屋：{item.fw}</p>
                              </div>
                            </div>
                            <div className={`pull-left ${styles.w100}`}>
                              <img
                                src={
                                  item.url === '-' || !item.url
                                    ? noPic
                                    : `http://gisams-hitech.cindata.cn/res_file${item.url}`
                                }
                              />
                            </div>
                          </div>
                          <div />
                        </li>
                      );
                    }
                  })
                : null}
              {this.state.showMapDiv ? (
                <li key="showMapDiv" className={styles.searchResultItem}>
                  <div className={`clearfix ${styles.itemTitle}`}>
                    <span className={styles.redC}>
                      <Link
                        target="_blank"
                        to={{
                          pathname: '/fe/assetMap/communityDetails',
                          state: {
                            communityId: this.state.clickXQ.communityId,
                          },
                          search: `?communityId=${this.state.clickXQ.communityId}`,
                        }}
                      >
                        {this.state.clickXQ.communityName}
                      </Link>
                    </span>
                  </div>
                  <div className={`clearfix ${styles.itemContent}`}>
                    <div className={`pull-left ${styles.w240}`}>
                      <p title={this.state.clickXQ.address}>{this.state.clickXQ.address}</p>
                      <p>资产数量：</p>
                      <div className="clearfix">
                        <p className={`pull-left ${styles.w50}`}>
                          楼栋：{this.state.clickXQ.buildCount}
                        </p>
                        <p className={`pull-left ${styles.w50}`}>
                          房屋：{this.state.clickXQ.houseCount}
                        </p>
                      </div>
                    </div>
                    <div className={`pull-left ${styles.w100}`}>
                      <img
                        src={
                          this.state.clickXQ.url === '-' || !this.state.clickXQ.url
                            ? noPic
                            : `http://gisams-hitech.cindata.cn/res_file${this.state.clickXQ.url}`
                        }
                      />
                    </div>
                  </div>
                  <div />
                </li>
              ) : null}
            </QueueAnim>
            {this.state.showMapDiv ? null : (
              <Pagination
                size="small"
                total={this.state.resultDatasCount}
                showTotal={this.showTotal.bind(this)}
                className="pd10"
                style={{
                  padding: 10,
                }}
                onChange={this.changePage.bind(this)}
              />
            )}
          </div>
           */}<QueueAnim>
            {this.state.showFilter ?
              this.showfilter()
              : null}
          </QueueAnim>
        </div>
        {Number(sessionStorage.getItem('isMap')) === 1 ? (
          <div>
            <IframeComm
              attributes={{
                src: '/gis/map.html',
                style: {
                  width: '100%',
                  height: '100%',
                  top: '0px',
                  left: '0px',
                  position: 'absolute',
                },
              }}
              postMessageData={this.state.sendMapData}
              handleReceiveMessage={this.onReceiveMessage.bind(this)}
            />
          </div>
        ) : null}

        {/* <div
          className={styles.dataInformation}
          style={{
            right: this.state.right,
          }}
        >
          <div className={styles.toolBox}>
            <div onClick={this.closeInfo.bind(this,'right')}>
              <Tooltip placement="left" title="关闭">
                <Icon type="arrow-right" />
              </Tooltip>
            </div>
          </div>
          <div className={styles.infoMain}>
            <Info data={infoData} cityData={this.state.cityData} />
            <Chart data={infoData} />
          </div>
        </div> */}

        <div
          className={styles.dataInformation}
          style={{
            right: this.state.roomright,
            height: 'auto'
          }}
        >
          <div className={styles.toolBox}>
            <div onClick={this.closeInfo.bind(this, 'roomright')}>
              <Tooltip placement="left" title="关闭">
                <Icon type="arrow-right" />
              </Tooltip>
            </div>
          </div>

          <div className={styles.infoMain}>
            {
              this.state.showInfoOne ?
                <RoomInfo data={this.state.roomData} />
                :
                <RoomInfo2 data={this.state.roomData} />
            }
          </div>
        </div>
        <div
          className={styles.dataInformation}
          style={{
            right: this.state.ldright,
            height: 'auto'
          }}
        >
          <div className={styles.toolBox}>
            <div onClick={this.closeInfo.bind(this, 'ldright')}>
              <Tooltip placement="left" title="关闭">
                <Icon type="arrow-right" />
              </Tooltip>
            </div>
          </div>
          <div className={styles.infoMain}>
            <LDInfo data={this.state.ldData} buildId={this.state.buildId} />
          </div>
        </div>
        <div className={styles.zoneInfo}
          style={{
            left: this.state.zoneLeft,
            height: 'auto'
          }}>
          <div className={styles.toolBox}>
            <div onClick={this.closeZoneInfo}>
              <Tooltip placement="right" title="关闭">
                <Icon type="arrow-left" />
              </Tooltip>
            </div>
          </div>
          <div className={styles.infoMain}>
            <ZoneInfo />
          </div>
        </div>
      </div>
    );
  }
}

export default AssetMap;
