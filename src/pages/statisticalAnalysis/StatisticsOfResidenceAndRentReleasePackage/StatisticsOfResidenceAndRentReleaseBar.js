/*
 * @Author: 程家兴
 * @Date: 2019-05-06 13:50:01 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-29 15:11:30
 * @file 进驻退租统计-柱图组件
 */
import { ChartCard, MiniBar } from '@/components/Charts';
import { Row, Col } from 'antd';
import moment from 'moment';
import React, { Component } from 'react';
import styles from './../style.less';


export default class Bar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visitData: []
        }
    }
    componentDidMount() {
        let data = [];
        const beginDay = new Date().getTime();
        for (let i = 0; i < 20; i += 1) {
            data.push({
                x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
                y: Math.floor(Math.random() * 100) + 10,
            });
        }
        this.setState({
            visitData: data
        })
    }
    render() {
        let industry = this.props.data && this.props.data.length > 0 && this.props.data[0].companyType;
        let growthRate = this.props.data && this.props.data.length > 0 && this.props.data[0].bl;
        let companyCnt = this.props.data && this.props.data.length > 0 && this.props.data[0].companyCnt;
        return (
            <Row>
                <Col span={24} style={{ marginTop: 24 }}>
                    <ChartCard
                        title={industry}
                        total={<span style={{ fontSize: 21 }}><span style={{ color: 'red', fontSize: 40 }}>{companyCnt ? companyCnt : '-'}</span>家</span>}
                        footer={<span>增长<span style={{ color: 'red', paddingLeft: 40 }}>{growthRate ? growthRate : '-'}</span></span>}
                        contentHeight={76}
                    >
                        <MiniBar height={46} data={this.state.visitData} tooltip={false} />
                    </ChartCard>
                </Col>
            </Row>
        )
    }
}