//**********************************
//基础功能类-气泡
//Power by han 2018.07.18
//**********************************
var objBubble = {
    createBubble: function (layer, geometry, imgSrc, imgHeight, imgWidth, attributes, content, fontSize, fontOffsetX, fontOffsetY, fontColor) {
        if (!geometry) {
            this.alertError(".createBubble Error!");
            console.log("无法获取传入参数{geometry}");
            return;
        }
        if (!geometry.type) {
            this.alertError(".createBubble Error!");
            console.log("无法获取传入参数{geometry}的type属性");
            return;
        }
        var pt;
        switch (geometry.type) {
            case "point":
                pt = geometry;
                break;
            case "polygon":
                pt = geometry.getExtent().getCenter();
                break;
            default:
                pt = geometry.getExtent().getCenter();
        }
        if (!pt) {
            this.alertError(".createBubble Error!");
            console.log("无法获取传入参数{geometry}的点");
            return;
        }
        if (imgSrc != "") {
            var symbol = new esri.symbol.PictureMarkerSymbol({
                "url": imgSrc,
                "height": imgHeight,
                "width": imgWidth,
                "type": "esriPMS"
            });
            var graf = new esri.Graphic(pt, symbol, attributes);
            layer.add(graf);
        }
        if (content != "") {
            var fColor = new dojo.Color([0, 0, 0]);
            if (fontColor && fontColor != null) {
                fColor = fontColor;
            }
            var textSymbol = new esri.symbol.TextSymbol(content).setColor(fColor).setOffset(fontOffsetX, fontOffsetY).setAlign(esri.symbol.TextSymbol.ALIGN_START);
            var font = new esri.symbol.Font(esri.symbol.Font.WEIGHT_BOLD);
            font.setSize(fontSize);
            textSymbol.setFont(font);
            layer.add(new esri.Graphic(pt, textSymbol, attributes));
        }
    },

    alertError: function (e) {
        alert("objBubble.js" + e);
    }

}

