//评估记录
import * as React from 'react';
import {
  TreeSelect,
  notification,
  Modal,
  Row,
  Col,
  DatePicker,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  message,
  InputNumber,
} from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import axios from 'axios';
import Api from '../../services/apis';

import style from './style.less';
@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class modifyPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      name: '',
      newPassword: '',
      checkPassword: '',
      userData: {},
    };
  }

  componentWillMount() {
    let temp = JSON.parse(sessionStorage.getItem('userkey'));
    this.setState({ userData: temp });
  }

  changeNumber = e => {
    this.setState({ oldPassword: e.target.value });
  };

  changeName = e => {
    this.setState({ name: e.target.value });
  };

  changenewPassword = e => {
    this.setState({ newPassword: e.target.value });
  };

  checkPassword = e => {
    this.setState({ checkPassword: e.target.value });

    // let val = e.target.value;
    // let passWord = this.state.newPassword;
    // if(val == passWord) {
    //     this.setState({showBtn: true})
    // }
  };

  savePassword = () => {
    let that = this;
    let checkPassword = this.state.checkPassword;
    let passWord = this.state.newPassword;
    if (checkPassword != passWord) {
      notification.destroy();
      notification['warning']({
        message: '新密码与确认密码不一致',
      });
      return;
    }
    axios
      .post(Api.umChangePassword, {
        userCode: this.state.name,
        userPassword: this.state.oldPassword,
        newUserPassword: this.state.newPassword,
      })
      .then(function (resp) {
        const {data} = resp;
        notification.destroy();
        notification['warning']({
          message: data.msg,
        });
      });
  };

  render() {
    let that = this;
    const {
      currentUserLoading,
    } = this.props;
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>修改密码</p>
              <p>MODIFY PASSWORD</p>
            </div>
          </div>
          <Form>
            <Row className={style.choicedBox}>
              <Row className="">
                <Col className={style.form_item}>
                  <span className="label">
                    <span style={{ color: 'red' }}>*</span>工号
                </span>
                  <Input
                    value={this.state.name}
                    onChange={this.changeName}
                    data-inp="zldz"
                    placeholder=""
                  />
                </Col>
                <Col className={style.form_item}>
                  <span className="label">
                    <span style={{ color: 'red' }}>*</span>原密码
                </span>
                  <Input
                    value={this.state.oldPassword}
                    data-inp="zdbh"
                    onChange={this.changeNumber}
                    placeholder=""
                  />
                </Col>
                <Col className={style.form_item}>
                  <span className="label">
                    <span style={{ color: 'red' }}>*</span>新密码
                </span>
                  <Input
                    value={this.state.newPassword}
                    data-inp="zdbh"
                    onChange={this.changenewPassword}
                    placeholder=""
                  />
                </Col>
                <Col className={style.form_item}>
                  <span className="label">
                    <span style={{ color: 'red' }}>*</span>确认密码
                </span>
                  <Input
                    value={this.state.checkPassword}
                    data-inp="zdbh"
                    onChange={this.checkPassword}
                    placeholder=""
                  />
                </Col>
              </Row>
              <Row className="evbtnbox">
                <Button type="primary" onClick={this.savePassword} className="evfilterbtn">
                  保存
              </Button>
              </Row>
            </Row>
          </Form>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default modifyPassword;
