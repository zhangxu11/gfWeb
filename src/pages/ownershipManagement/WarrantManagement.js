//项目权属管理
import * as React from 'react';
import {
  Table, Form, Menu, Select, Dropdown, Input, Button, Col, Row, Modal, Icon, Popover, Steps, notification
} from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import axios from 'axios'
import Api from '../../services/apis'
import styles from './style.less';

const FormItem = Form.Item;
const Option = Select.Option;
const Step = Steps.Step;


@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class WarrantManagement extends React.Component {
  constructor(props) {
    super(props);
    let currentUser = sessionStorage.getItem('currentUser');
    let date = new Date()
    date = date.toLocaleDateString();
    this.state = {
      modalVisible: false,
      _modalVisible: false,
      loading: true,
      canEdit: true,
      isAdd: false,
      tableData: [],
      nodeStatus:'办理中',
      pageSize: 5,
      current: 1,
      currentData: {},
      newCardStatus: '未过户',
      cardBelong: '',
      newDocType: '',
      newCardName: '',
      newCardBelong: '',
      cardBelongCode: '',
      newtransfer_unit: '',
      newPrincipal:'',
      doneDate: '',
      currentStep: 0,
      newBuildings: '',
      newRooms: '',
      selectData: [
      ],
      currentStepData: [],
      newstepData: [
        {
          title: '无证',
          description: currentUser,
          time: date
        },
        {
          title: '办理中',
          description: '',
          time: ''
        },
        {
          title: '办理完成',
          description: '',
          time: ''
        },
        {
          title: '过户中',
          description: '',
          time: ''
        },
        {
          title: '有证',
          description: '',
          time: ''
        }
      ],
      stepData: [
        {
          title: '无证',
          description: 'xxx',
          time: ' 2018年10月11日',
        },
        {
          title: '办理中',
          description: 'xxx',
          time: ' 2018年10月11日',
        },
        {
          title: '办理完成',
          description: 'xxx',
          time: ' 2018年10月11日',
        },
        {
          title: '过户中',
          description: 'xxx',
          time: ' 2018年10月11日',
        },
        {
          title: '有证',
          description: 'xxx',
          time: '',
        },
      ],
      buildingData: [],
      roomData: []
    };
  }

  setModalVisible(modalVisible, type, item) {
    let that = this;
    if (type === 'add') {
      if(!modalVisible) {
        axios.post(Api.addCard,{
          docName:that.state.newCardName,      //证件名称
          projectCode: that.state.cardBelongCode,  //所属项目
          transferUnit:that.state.newtransfer_unit,  //划转单位
          docType:that.state.newDocType,        //权证类型
          cardStatus:that.state.newCardStatus,  //过户状态
          principal:that.state.newPrincipal,    //负责人
          refCode: that.state.cardNature == 6 ? that.state.newBuildings : that.state.newRooms,
          docLevel: that.state.cardNature,
          cardBelong: that.state.cardBelong,
          buildingCode: that.state.newBuildings
        })
        .then((function(resp){
            const data = resp.data;
            notification.destroy();
            notification['warning']({message:data.msg})
            that.setState({
              currentStep: 0,  //当前步数
              newCardName:'',      //证件名称
              cardBelongCode: '',  //所属项目
              newCardBelong: '',
              cardBelong: '',
              newtransfer_unit:'',  //划转单位
              newDocType:'',        //权证类型
              newCardStatus:'',   //过户状态
              cardNature: '',
              newBuildings: '',
              newRooms: '',
              newPrincipal: '',

            })
            that.sendData();
        }))
        
      }
      
      this.setState({ modalVisible,isAdd: true });
    }else {
      if(modalVisible) {
        axios.post(Api.getNodeInfo,{
          docId: item.doc_id
        })
        .then((function(resp){
          
            let data = resp.data;
            let _data = [];
            data.map((option,index) => {
              let temp = {
                title: option.dic_name?option.dic_name:'',
                description: option.handle_user?option.handle_user:'',
                time: option.handle_date?option.handle_date.substring(0, 10):''
              }
              _data.push(temp);
            })
            that.setState({ 
              modalVisible,isAdd: false,currentData: item ? item : {},currentStepData: _data,currentStep:parseInt(item.plan)
            })
        }))
      }else {
        this.setState({modalVisible,isAdd: false})
      }
      

    }
  }
  closeAdd = () => {
      this.setState({ 
        currentStep: 0,  //当前步数
        newCardName:'',      //证件名称
        cardBelongCode: '',  //所属项目
        newCardBelong: '',
        cardBelong: '',
        newtransfer_unit:'',  //划转单位
        newDocType:'',        //权证类型
        newCardStatus:'',   //过户状态
        cardNature: '',
        newBuildings: '',
        newRooms: '',
        newPrincipal: '',
        modalVisible:false,
        isAdd: false
      });
  }
  _closeAdd = () => {
      this.setState({ _modalVisible:false});
  }
  _setModalVisible(modalVisible, item) {
    let that = this;
    if(modalVisible) {
      axios.post(Api.getNodeInfo,{
        docId: item.doc_id
      })
      .then((function(resp){
        
          let data = resp.data;
          let _data = [];
          data.map((option,index) => {
            let temp = {
              title: option.dic_name?option.dic_name:'',
              description: option.handle_user?option.handle_user:'',
              time: option.handle_date?option.handle_date.substring(0, 10):''
            }
            _data.push(temp);
          })
          that.setState({ 
            _modalVisible:modalVisible
            ,isAdd: false
            ,currentData: item ? item : {} 
            ,currentStep: item.plan ? parseInt(item.plan) : 0
            ,cardStatus_edit: item.plan_status
            ,cardBelong: item.card_belong
            ,editCardnodeStatus: item.card_status
            ,doneDate: item.plan_done_date
            ,currentStepData: _data
          })
      }))
      
    }else {
      if(parseInt(this.state.currentData.plan) == this.state.currentStep) {
        notification.destroy();
        notification['error']({message:'请更改流程'})
        return;
      }else {
        axios.post(Api.executePlan,{
          docId: that.state.currentData.doc_id,
          principal: that.state.currentData.principal,
          transferUnit: that.state.currentData.transfer_unit,
          cardBelong: that.state.cardBelong,
          cardStatus: that.state.currentData.card_status,
          projectCode: that.state.currentData.project_code
        })
        .then((function(resp){
            const data = resp.data;
            notification.destroy();
            notification['warning']({message:data.msg})
            that.sendData();
            that.setState({_modalVisible:modalVisible})
        }))
      }
      
    }
    
  }

  componentWillMount(){
    // this.sendData();
  }

  componentDidMount() {
    if(this.props.location.query && this.props.location.query.name) {
      this.props.form.setFieldsValue({
        projectName: this.props.location.query.name
      })
    }
    this.sendData();
  }

  sendData = () => {
      const that = this;
      const searchValues = this.props.form.getFieldsValue();
      axios.post(Api.warrant,{
          docName: searchValues.name,
          transferUnit: searchValues.company,
          projectName: searchValues.projectName,
          currentPage: this.state.current
      })
      .then((function(resp){
          const data = resp.data;
    
          if(data.list.length === 0){
              notification.destroy();
              notification['warning']({message:'暂无数据'})
          }else{
              data.list.forEach((d,i) => {
                  d.key = i;
              });
          }

          that.setState({
              tableData: data.list,
              pageSize: data.pageSize,
              current: data.currentPage,
              totalSize: data.totalSize
          })
      }))
  }

  pageChange = (page) => {
      this.setState({current:page},()=>{
          this.sendData();
      })
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({current:1},()=>{
        this.sendData();
    })
  };

  onEdit() {
    this.setState({
      canEdit: !this.state.canEdit,
    });
  }

  handleChange(value) {
    console.log(`selected ${value}`);
  }
  handleBlur() {
    console.log('blur');
  }

  handleFocus() {
    console.log('focus');
  }
  changeTime(date, dateString) {
    console.log(date, dateString);
  }
  changeCardNature = (value) => {
    this.setState({cardNature: value});
  }
  changeCardBelong = (e) => {
    this.setState({cardBelong: e.target.value});
  }
  editnodehandleChange = (value) => {
    this.setState({editCardnodeStatus: value})
  }
  
  edithandleChange = (value) => {
    this.setState({cardStatus_edit: value})
  }
  changeNodeStatus = (value) => {
    this.setState({nodeStatus: value})
  }
  
  newCardName = (e) => {
    this.setState({newCardName: e.target.value})
  }
  newCardBelong = (value,option) => {
    // axios.post(Api.getproListByName,{
    //     projectName: value
    // })
    // .then((function(resp){
    let cardBelongCode = ''; 
    let cardBelong = ''; 

    this.state.selectData.map((item,index) => {
      if(item.projectName == option.props.children){
        cardBelongCode = item.projectCode;
        cardBelong = item.transferUnit
      }
    })
    let that = this;
    axios.post(Api.getBListByProCode,{
        projectCode: cardBelongCode
    })
    .then((function(resp){
        const data = resp.data;
        that.setState({buildingData: data});
    }))
    this.setState({newCardBelong: value,cardBelong:cardBelong,newtransfer_unit: cardBelong,cardBelongCode:cardBelongCode});
    // }))
  }
  newtransfer_unit = (e) => {
    this.setState({newtransfer_unit: e.target.value})
  }
  changeNewPrincipal = (e) => {
    this.setState({newPrincipal: e.target.value})
  }
  newCardStatus = (value) => {
    this.setState({newCardStatus: value})
  }
  newBuildings = (value) => {
    let that = this;
    axios.post(Api.getHListByBuildCode,{
        buildingCode: value
    })
    .then((function(resp){
        const data = resp.data;
        that.setState({roomData: data,newBuildings: value});
    }))
  }
  newRooms = (value) => {
    this.setState({newRooms: value})
  }
  newDocType = (value) => {
    this.setState({newDocType: value})
  }
  changeSteps = (e,data,index) => {
    if(parseInt(this.state.currentData.plan) +1 != index ) {
      return
    }else {
      let stepData = this.state.stepData;
      let currentUser = sessionStorage.getItem('currentUser');
      let date = new Date()
      date = date.toLocaleDateString();
      stepData.map((item,idx) => {
        if(idx == index){
          item.description = currentUser;
          item.time = date;
        }
      })
      if(index == 1 ) {
        this.setState({stepData: stepData,currentStep: index,cardStatus_edit: '办理中',editCardnodeStatus: '未过户'})
      }else if(index == 2) {
        this.setState({stepData: stepData,currentStep: index,cardStatus_edit: '办理完成',editCardnodeStatus: '未过户'})
      }else if(index == 3) {
        this.setState({stepData: stepData,currentStep: index,cardStatus_edit: '办理完成',editCardnodeStatus: '未过户'})
      }else if(index == 4) {
        this.setState({stepData: stepData,currentStep: index,cardStatus_edit: '办理完成',editCardnodeStatus: '已过户',cardBelong:'国防经营公司',doneDate: date})
      }
        
      this.setState({stepData: stepData,currentStep: index})
    }
    
  }
  handleSearch = (value) => {
    let that = this;
    axios.post(Api.getproListByName,{
        projectName: value
    })
    .then((function(resp){
        const data = resp.data;
        that.setState({selectData: data});
    }))


  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      currentUserLoading,
    } = this.props;
    const columns = [
      {
        title: '证件名称',
        dataIndex: 'doc_name',
        key: 'doc_name',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '所属项目',
        dataIndex: 'project_name',
        key: 'project_name',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '划转单位',
        dataIndex: 'transfer_unit',
        key: 'transfer_unit',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '权证状态',
        dataIndex: 'plan_status',
        key: 'plan_status',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '权证类型',
        dataIndex: 'doc_type',
        key: 'doc_type',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '权证性质',
        dataIndex: 'card_type',
        key: 'card_type',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '负责人/公司',
        key: 'principal',
        dataIndex: 'principal',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '权证归属',
        key: 'card_belong',
        dataIndex: 'card_belong',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '过户状态',
        key: 'card_status',
        dataIndex: 'card_status',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '开始时间',
        key: 'start_time',
        dataIndex: 'start_time',
        render: (text, record) => (
          <span>{text ? text.substring(0, 10) : '-'}</span>
        )
      },
      {
        title: '结束时间',
        key: 'end_time',
        dataIndex: 'end_time',
        render: (text, record) => (
          <span>{text ? text.substring(0, 10) : '-'}</span>
        )
      },
      {
        title: '操作人',
        key: 'handle_user',
        dataIndex: 'handle_user',
        render: (text, record) => (
          <span>{text ? text : '-'}</span>
        )
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => {
          return(
            <Dropdown.Button onClick={this.setModalVisible.bind(this, true, 'edit',record)} overlay={
              <Menu>
                <Menu.Item key="details" onClick={this._setModalVisible.bind(this, true,record)}>编辑</Menu.Item>
              </Menu>}>
              详情
            </Dropdown.Button>
        )},
      },
    ];
    const options = this.state.selectData.map(d => <Option value={d.projectCode} key={d.projectName}>{d.projectName}</Option>);
    const options_building = this.state.buildingData.map(d => <Option value={d.buildingCode} key={d.buildingName2}>{d.buildingName2}</Option>);
    const options_room = this.state.roomData.map(d => <Option value={d.houseCode} key={d.houseName2}>{d.houseName2}</Option>);
    
    let stepData = this.state.isAdd ? this.state.newstepData : this.state.currentStepData;
    let _currentStep = this.state.isAdd ? 0 : this.state.currentStep;

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className='mainContainer'>
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>权证管理</p>
              <p>WARRANT MANAGEMENT</p>
            </div>
          </div>
          <div className="pom">
            <div className='mrb15'>
              <Form layout="inline" align='center' onSubmit={this.handleSubmit}>
                  <FormItem label="证件名称:">
                    {getFieldDecorator('name', {
                      rules: [{ required: false, message: '请输入证件名称！' }],
                    })(<Input placeholder="请输入证件名称" />)}
                  </FormItem>
                  <FormItem label="项目名称:">
                    {getFieldDecorator('projectName', {
                      rules: [{ required: false, message: '请输入项目名称！' }],
                    })(<Input placeholder="请输入项目名称" />)}
                  </FormItem>
                  <FormItem label="划转单位:">
                    {getFieldDecorator('company', {
                      rules: [{ required: false, message: '请选择划转单位！' }],
                    })(
                      <Input placeholder="请输入划转单位" />
                    )}
                  </FormItem>
                  <Row gutter={24} style={{ textAlign: 'center'}}>
                      <Button icon="search" type="primary" htmlType="submit">
                        搜索
                    </Button>
                      &emsp;
                    <Button
                        icon="plus-circle"
                        type="primary"
                        htmlType="button"
                        onClick={this.setModalVisible.bind(this, true, 'add')}
                      >
                        新增
                    </Button>
                </Row>
              </Form>
            </div>
            <Table
              bordered={true}
              scroll={{ x: true }}
              columns={columns}
              dataSource={this.state.tableData}
              pagination={{
                  pageSize:this.state.pageSize,
                  current:this.state.current,
                  total:this.state.totalSize,
                  onChange:this.pageChange,
                  showTotal: total => `共 ${total} 项`
              }} />
          </div>
          <Modal
            title="权证详情"
            centered
            visible={this.state.modalVisible}
            onOk={() => this.setModalVisible(false,this.state.isAdd ?'add':'edit')}
            onCancel={() => this.closeAdd(false)}
            width={1200}
            className="oaomodal">
            <div>
              <Steps current={_currentStep}>
                {stepData.map((item, index) => {
                  return (
                    <Step
                      description={[
                        <div key={index}>
                          <p>{item.description}</p>
                          <p>{item.time}</p>
                        </div>,
                      ]}
                      
                      title={item.title}
                      key={index}
                    />
                  );
                })}
              </Steps>
            </div>
            <Row className={styles.pd20}>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">证件名称:</span>
                {this.state.isAdd ? <Input value={this.state.newCardName} onChange={this.newCardName} placeholder='请输入证件名称'></Input> : this.state.currentData.doc_name}
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">所属项目:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch={true}
                    value={this.state.newCardBelong}
                    defaultActiveFirstOption={false}
                    showArrow={false}
                    filterOption={false}
                    onSearch={this.handleSearch}
                    onSelect={this.newCardBelong}
                    notFoundContent={null}
                    className={styles.darSelect}>
                    {options}
                  </Select>
                  : this.state.currentData.project_name}
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">划转单位:</span>
                {this.state.isAdd ? 
                  
                  <Input value={this.state.newtransfer_unit} onChange={this.newtransfer_unit} placeholder='请输入划转单位'></Input>
                   : this.state.currentData.transfer_unit}
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证类型:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch
                    placeholder="请选择权证类型"
                    optionFilterProp="children"
                    value={this.state.newDocType}
                    onChange={this.newDocType.bind(this)}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}>
                    <Option value="使用权证">使用权证</Option>
                    <Option value="产权证">产权证</Option>
                  </Select>
                  :
                  this.state.currentData.doc_type
                }

              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证状态:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch
                    placeholder="请选择权证状态"
                    optionFilterProp="children"
                    value={this.state.nodeStatus }
                    disabled={true}
                    onChange={this.changeNodeStatus.bind(this)}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}
                  >
                    <Option value="办理中">办理中</Option>
                    <Option value="办理完成">办理完成</Option>
                  </Select>
                  :
                  this.state.currentData.plan_status
                }

              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证性质:</span>
                {this.state.isAdd ? 
                  <Select
                    showSearch
                    placeholder="请选择权证性质"
                    optionFilterProp="children"
                    onChange={this.changeCardNature } 
                    value={this.state.cardNature }
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}
                  >
                    <Option value={6}>楼栋</Option>
                    <Option value={9}>房屋</Option>
                  </Select>
                   : this.state.currentData.doc_type}
              </Col>

              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证归属:</span>
                {this.state.isAdd ? <Input onChange={this.changeCardBelong} value={this.state.cardBelong} placeholder='请输入权证归属'></Input> : this.state.currentData.card_belong}
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">过户状态:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch
                    placeholder="请选择过户状态"
                    disabled={true}
                    optionFilterProp="children"
                    onChange={this.newCardStatus } 
                    value={this.state.newCardStatus }
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}
                  >
                    <Option value="未过户">未过户</Option>
                    <Option value="已过户">已过户</Option>
                  </Select>
                  :
                  this.state.currentData.card_status 
                }
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">负责人/单位:</span>
                {this.state.isAdd ? <Input onChange={this.changeNewPrincipal} value={this.state.newPrincipal} placeholder='请输入负责人/单位'></Input> : this.state.currentData.principal}
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">楼栋:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch
                    placeholder="请选择楼栋"
                    optionFilterProp="children"
                    value={this.state.newBuildings}
                    onChange={this.newBuildings.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}>
                    {options_building}
                  </Select>
                  :
                  this.state.currentData.building_name2
                }

              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">户:</span>
                {this.state.isAdd ?
                  <Select
                    showSearch={true}
                    disabled={this.state.cardNature == 6 ? true : false }
                    placeholder="请选择户"
                    optionFilterProp="children"
                    value={this.state.newRooms}
                    onChange={this.newRooms.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}>
                    {options_room}
                  </Select>
                  :
                  this.state.currentData.house_name2
                }

              </Col>
              {
                !this.state.isAdd ?
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">开始时间:</span>
                    <span>{this.state.currentData.start_time ? this.state.currentData.start_time.substring(0, 10) : ''}</span>
                  </Col>
                  :
                  ''
              }

              {
                !this.state.isAdd ?
                  <Col className={`pull-left form_item ${styles.darItem}`}>
                    <span className="label">结束时间:</span>
                    <span>{this.state.currentData.end_time ? this.state.currentData.end_time.substring(0, 10) : ''}</span>
                  </Col>
                  :
                  ''
              }
            </Row>
          </Modal>

          <Modal
            title="编辑"
            centered
            visible={this.state._modalVisible}
            onOk={() => this._setModalVisible(false)}
            onCancel={() => this._closeAdd(false)}
            width={1200}
            className="oaomodal">
            <div>
              <Steps current={this.state.currentStep}>
                {this.state.currentStepData.map((item, index) => {
                  return (
                    <Step
                      description={[
                        <div key={index}>
                          <p>{item.description}</p>
                          <p>{item.time}</p>
                        </div>,
                      ]}
                      onClick={(e) => this.changeSteps(e,item,index)}
                      title={item.title}
                      key={index}
                    />
                  );
                })}
              </Steps>
            </div>
            <Row className={styles.pd20}>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">证件名称:</span>
                <Input value={this.state.currentData.doc_name} disabled={true} />
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">所属项目:</span>
                <Input value={this.state.currentData.card_belong} disabled={true} />
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">划转单位:</span>
                <Input value={this.state.currentData.transfer_unit} disabled={true} />
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证类型:</span>
                <Input value={this.state.currentData.doc_type} disabled={true} />
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证状态:</span>
                  <Select
                    showSearch
                    placeholder="请选择权证状态"
                    disabled={true}
                    value={this.state.cardStatus_edit}
                    optionFilterProp="children"
                    onChange={this.edithandleChange.bind(this)}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    className={styles.darSelect}
                  >
                    <Option value="办理中">办理中</Option>
                    <Option value="办理完成">办理完成</Option>
                  </Select>
                  

              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证性质:</span>
                <Input value={this.state.currentData.card_type} disabled={true} />
              </Col>

              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">权证归属:</span>
                <Input disabled={true} value={this.state.cardBelong}></Input>
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">过户状态:</span>
                  <Select
                    showSearch
                    placeholder="请选择过户状态"
                    value = {this.state.editCardnodeStatus}
                    optionFilterProp="children"
                    disabled={true}
                    onChange={this.editnodehandleChange.bind(this)}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    className={styles.darSelect}
                  >
                    <Option value="未过户">未过户</Option>
                    <Option value="已过户">已过户</Option>
                  </Select>
                  
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">负责人/单位:</span>
                <Input value={this.state.currentData.principal} disabled={true} />
              </Col>
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">开始时间:</span>
                <Input value={this.state.currentData.start_time ? this.state.currentData.start_time.substring(0, 10) : ''} disabled={true} />
              </Col>
            
              <Col className={`pull-left form_item ${styles.darItem}`}>
                <span className="label">结束时间:</span>
                <Input value={this.state.currentData.end_time ? this.state.currentData.end_time.substring(0, 10) : ''} disabled={true} />
              </Col>
            </Row>
          </Modal>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(WarrantManagement);

export default WrappedApp;
