import React, { Component } from 'react';
import Link from 'umi/link';
import TweenOne from 'rc-tween-one';
import Children from 'rc-tween-one/lib/plugin/ChildrenPlugin';
import ReactEcharts from 'echarts-for-react';
import { Card, Row } from 'antd';
import classNames from 'classnames';
import { ProgressPer } from '@/components/ProgressPer';
import { formatNumberWithComma } from '@/utils/utils';
import router from 'umi/router';

import styles from "./style.less";

TweenOne.plugins.push(Children);

export default class Part2 extends Component {
  // if (!leftData || !rightData.industryList) return null;

  constructor(props) {
    super(props);
    this.state = {
      codeArr: '',
      industryList: []
    };
  }
  componentDidMount() {
    const { rightData, leftData } = this.props;
    const industryList = rightData.industryList.map(industry => {
      return {
        value: industry.count,
        name: industry.name,
        percent: industry.percent,
        code: industry.code
      }
    });
    const codeArr = industryList && industryList.map(item => item.code);
    this.setState({
      industryList,
      codeArr: codeArr.filter(item => item !== null).join(',')
    })
  }
  onRentChartClick(param) {
    router.push({
      pathname: '/fe/dataMaintenance/renterManagement',
      state: {
        isRent: param
      }
    });
  }

  onChartClick(param) {
    router.push({
      pathname: '/fe/dataMaintenance/renterManagement',
      state: {
        code: param.data.code ? param.data.code : this.state.codeArr
      }
    });
  }
  render() {
    const { industryList } = this.state;
    const { rightData, leftData } = this.props;
    const rentState = ['租赁中', '待租赁'];

    let onEvents = {
      'click': this.onChartClick.bind(this)
    }

    const pieOption = {
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
      },
      legend: {
        orient: 'vertical',
        right: '0',
        top: 'middle',
        itemGap: 20,
        itemWidth: 50,
        itemHeight: 12,
        align: 'left',
        selectedMode: false,
        // formatter: (name) => {
        //   const ind = industryList.find((industry) => industry.name === name);
        //   return `{title|${ind.name}}   {pre|${ind.percent}}  |  {count|${ind.value}}`;
        // },
        textStyle: {
          rich: {
            title: {
              fontWeight: 400,
              fontStyle: 'normal',
              color: 'rgba(0, 0, 0, 0.647058823529412)',
            },
            pre: {
              fontWeight: 400,
              fontStyle: 'normal',
              color: 'rgba(0, 0, 0, 0.427450980392157)',
            },
            count: {
              fontWeight: 400,
              fontStyle: 'normal',
              color: 'rgba(0, 0, 0, 0.647058823529412)',
            }
          }
        },
        data: industryList && industryList.map((industry) => { return { name: industry.name, icon: 'circle' } }),
      },
      title: {
        show: true,
        text: '租户总量',
        textStyle: {
          color: 'rgba(0, 0, 0, 0.427450980392157)',
          fontSize: 14,
          fontWeight: 400,
          fontStyle: 'normal',
        },
        subtext: `{red|${rightData.totalRenter}}{common|户}`,
        subtextStyle: {
          rich: {
            red: {
              color: '#E22534',
              fontSize: 24,
              fontWeight: 400,
              fontStyle: 'normal',
            },
            common: {
              fontSize: 16,
              color: 'rgba(0, 0, 0, 0.847058823529412)',
            }
          }
        },
        // x:'12%',
        x: '25%',
        y: 'center'
      },
      series: [
        {
          name: '行业分布',
          title: '行业分布',
          type: 'pie',
          radius: ['50%', '70%'],
          // center : ['18%', '50%'],
          center: ['30%', '50%'],
          color: ["#3AA0FF", "#36CBCB", "#4DCB73", "#FAD337", "#F2637B", '#FF0000', "black"],
          avoidLabelOverlap: false,
          label: {
            normal: {
              show: false,
              // formatter: '{d}%'
            },
          },
          labelLine: {
            normal: {
              show: false
            }
          },
          data: industryList,
        }
      ]
    };

    const reasons = [
      {
        name: '在租面积',
        count: leftData.percentageOfRentalArea || '0%',
      },
      {
        name: '空置面积',
        count: leftData.percentageOfVacantArea || '0%',
      },
      {
        name: '管理面积',
        count: null,
      },
      {
        name: '可招商面积',
        count: null,
      },
    ];
    const progressPers = reasons.map((reason, index) => {
      if (!reason.count) return;
      return <ProgressPer data={reason} key={index} width={180} titleSize={'16'} countSize={'28'} type={rentState[index]} />;
    });

    return (
      <div className={styles.part2Content}>
        <Row className={classNames(styles.part2ContentLeft)}>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className={classNames(styles.contentHeaderTitle, 'contentHeaderTitle')}>
                <p>房产租赁情况</p>
                <p>RENT INFORMATION</p>
              </div>
            </div>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-around', flexWrap: 'wrap', alignItems: 'center', height: 290, cursor: 'pointer' }}>
            {progressPers}
          </div>
        </Row>
        <Row className={classNames(styles.part2ContentRight)}>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className={classNames(styles.contentHeaderTitle, 'contentHeaderTitle')}>
                <p>行业分布</p>
                <p>INDUSTRY DISTRIBUTION</p>
              </div>
            </div>
          </div>
          <div>
            <ReactEcharts option={pieOption} notMerge={true} lazyUpdate={true} style={{ height: 300 }} onEvents={onEvents} />
          </div>
        </Row>
      </div>
    );
  }
}
