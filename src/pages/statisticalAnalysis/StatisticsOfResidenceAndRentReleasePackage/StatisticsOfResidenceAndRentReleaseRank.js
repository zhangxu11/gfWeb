/*
 * @Author: 程家兴
 * @Date: 2019-05-06 13:50:01 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-13 16:15:32
 * @file 进驻退租统计-排行组件
 */
import numeral from 'numeral';
import React, { Component } from 'react';
import styles from './../style.less';


export default class Rank extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const data = [{
            "companyType": '——',
            "companyCnt": 0
        }, {
            "companyType": '——',
            "companyCnt": 0
        }, {
            "companyType": '——',
            "companyCnt": 0
        }, {
            "companyType": '——',
            "companyCnt": 0
        }, {
            "companyType": '——',
            "companyCnt": 0
        }]
        return (
            <div>
                <span style={{ fontWeight: 'bold', color: 'black' }}>{this.props.title}</span>
                <ul className={styles.rankingList}>
                    {this.props.data && this.props.data.length > 0 ? this.props.data.map((item, i) => (
                        <li key={item.companyType}>
                            {i < 3
                                ? <span>
                                    <img style={{ marginRight: '20px', height: '29px', width: '29px' }} src={require(`../../../assets/indus-list-${i}.png`)} />
                                </span>
                                : <span
                                    className={styles.rankingItemNumber}
                                >
                                    {i + 1}
                                </span>}
                            <span className={styles.rankingItemTitle} title={item.title}>
                                {item.companyType}
                            </span>
                            <span className={styles.rankingItemValue}>
                                {numeral(item.companyCnt).format('0,0')}
                            </span>
                        </li>
                    )) : <div className={styles.noData}>暂无数据</div>
                    }
                </ul>
            </div>
        )
    }
}