/*
 * @Author: 程家兴
 * @Date: 2019-05-06 13:50:01 
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-05-29 15:16:20
 * @file 进驻退租统计-条形图组件
 */
import { ChartCard } from '@/components/Charts';
import { Row, Col } from 'antd';
import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';


export default class Line extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
    }
    render() {
        const option = {
            xAxis: {
                show: false,
                type: 'category'
            },
            yAxis: {
                show: false,
                type: 'value'
            },
            series: [{
                data: [20, 80, 50, 70, 90, 40, 60, 70, 130, 60],
                type: 'line',
                symbol: 'none',
                lineStyle: [{
                    width: 1
                }]
            }]
        }
        let industry = this.props.data && this.props.data.length > 0 && this.props.data[0].companyType;
        let growthRate = this.props.data && this.props.data.length > 0 && this.props.data[0].bl;
        let companyCnt = this.props.data && this.props.data.length > 0 && this.props.data[0].companyCnt;
        return (
            <Row>
                <Col span={24} style={{ marginTop: 24 }}>
                    <ChartCard
                        title={industry}
                        total={<span style={{ fontSize: 21 }}>增长率<span style={{ color: 'red' }}>{growthRate ? growthRate : '-'}</span></span>}
                        footer={<span>{this.props.title}<span style={{ color: 'red' }}>{companyCnt ? companyCnt : '-'}</span>家</span>}
                        contentHeight={76}
                    >
                        <ReactEcharts option={option} notMerge={true} lazyUpdate={true} style={{ height: 150, top: 50, width: 272, left: -25 }} />
                    </ChartCard>
                </Col>
            </Row>
        )
    }
}