//项目管理
import * as React from 'react';
import {
  TreeSelect,
  Modal,
  Row,
  Col,
  DatePicker,
  notification,
  Menu,
  Steps,
  Dropdown,
  Icon,
  Input,
  Table,
  Button,
  Form,
  InputNumber,
  Tabs,
  Spin,
  Tooltip,
  Select,
  Popover,
} from 'antd';

import router from 'umi/router';
import Link from 'umi/link';
import { formatTreeData } from '../../components/commonAips';
import axios from 'axios';
import Api from '../../services/apis';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from 'moment';
import 'moment/locale/zh-cn';
import styles from './style.less'
import IframeComm from '../../components/IframeComm';
import ico_map from "../../assets/ico_map.png";
import ico_nodata from "../../assets/ico_nodata.png";
const { TextArea } = Input;
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
const { RangePicker } = DatePicker;
const Step = Steps.Step;
const Option = Select.Option;
const FormItem = Form.Item;

import { ApprovalContent } from '@/components/ApprovalContent';

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class OperationManagement extends React.Component {
  constructor(props) {
    super(props);
    this.userCode = sessionStorage.getItem('currentUser');
    this.state = {
      addHouseArr: [],
      delHouseArr: [],
      name: '',
      tranUnits: '',
      pageSize: 5,
      current: 1,
      tableData: [],
      modalVisible: false,
      // selectdIds: [],
      selectedArr: [],
      ownerStatus: [],
      ownerType: [],
      signStatus: [],
      assetStatus: [],
      submitFlag: '',
      showDownload: false,
      downloadName: '',
      downloadLink: '',
      startDownload: false,
      buildingList: [],
      floorList: [],
      houseList: [],
      mapModal: false,
      sendMapData: {},
      approvalActiceStep: 0,
      approvalList: [{}, {}, {}, {}],
      currentContractId: null,
      moveOutModalVisible: false,
      reasonList: [],
      startYearRent: null,
      endYearRent: null,
      // 用户权限信息
      userInfo: [],
      // 判断用户是否包含财务权限
      isFinance: false,
      contractType: '',
      contractId: null,
      showCreateModel: false,
      searchParams: {
        rentName: null, // 楼栋名称
        startDate: null, // 楼栋编号
        endDate: null, // 在租面积起
        contractType: null, // 在租面积止
        startYearRent: null, // 建筑面积起
        endYearRent: null, // 建筑面积止
      },
      isApproval: false,
      // 控制操作中的显示按钮，1代表迁出，2代表审批，
      processState: '1'
    };
  }
  //获取表格数据
  sendData = () => {
    this.setState({ loading: true })
    const params = {
      ...this.state.searchParams,
      currentPage: this.state.current
    };
    axios.post(Api.queryContract, params).then((resp) => {
      const data = resp.data;
      if (data.list.length == 0) {
        notification.destroy();
        notification['warning']({
          message: '暂无数据',
        });
      }
      let time = moment(moment(new Date()).format('YYYY-MM-DD'));
      data.list.map((d, i) => {
        d.key = i;
        d.receivedDate = moment(d.receivedDate).format("YYYY-MM-DD HH:mm:ss");
        if (time.diff(moment(d.endDate).format('YYYY-MM-DD'), 'day') > 0 && d.contractType != '1'
          && d.contractType != '3' && d.contractType != '4' && d.contractType != '5') {
          d.contractType = '6'
        }
      })
      this.setState({
        tableData: data.list,
        pageSize: data.pageSize,
        current: data.currentPage,
        total: data.totalSize
      })
      this.setState({ loading: false })
    })
  };
  componentWillMount() {
    this.setState({
      searchParams: {
        contractType: '2'
      }
    });
  }

  componentDidMount() {
    const userName = sessionStorage.getItem('currentUser');
    this.sendData();
    Promise.all([
      axios.post(Api.getFunListByUserCode, { userCode: userName }),
      axios.post(Api.queryBuildingList, {}),
      axios.post(Api.queryRenter, {}),
      axios.post(Api.getMoveReason),
      axios.get(Api.getRentUse),
    ]).then(resList => {
      const userInfo = resList[0].data;
      const buildingList = resList[1].data;
      const renterList = resList[2].data;
      const reasonList = resList[3].data;
      const rentUse = resList[4].data;
      let isFinance = false;
      let isApproval = false;
      let isCommit = false;
      userInfo.map(item => {
        if (item.functionId == 110009) {
          isFinance = true;
        }
        if (item.functionId == 110005) {
          isApproval = true
        }

        if (item.functionId == 110004) {
          isCommit = true
        }

      })
      this.setState({
        isFinance,
        userInfo,
        buildingList,
        renterList,
        reasonList,
        rentUse,
        isApproval,
        isCommit
      });
    })
  }



  //查询
  query = () => {
    if (this.state.searchParams.startYearRent && this.state.searchParams.endYearRent) {
      if (this.state.searchParams.endYearRent > this.state.searchParams.startYearRent) {
        this.setState({ current: 1 }, () => {
          this.sendData();
        });
      } else {
        notification['warning']({ message: '请输入正确的租金范围' });
      }
    } else {
      this.setState({ current: 1 }, () => {
        this.sendData();
      });
    }

  };
  agreementApproval = (id, status, type) => {
    let that = this;
    confirm({
      okText: '确定',
      cancelText: '取消',
      title: status === '0' ? '回退提示' : '同意提示',
      content: status === '0' ? '是否回退到待转合同？' : '是否确认无误并转成待租合同？',
      onOk() {
        axios.post(Api.agreementApproval, {
          contractId: id,
          editStatus: status,
          contractType: type
        })
          .then(function (resp) {
            that.setState({ current: 1 }, () => {
              that.sendData();
            });
          })
      }
    })

  }
  // 欠费详情
  arrears = () => {
    let { unitPrice, totalArea, lengthOfArrears } = this.state;
    this.setState({
      totalArrears: Math.round(unitPrice * totalArea * (lengthOfArrears/365).toFixed(2) * 100) / 100
    })
  }
  //获取项目
  getProject = (id, type, contractType) => {
    this.setState({ loading: true })
    this.setState({
      contractId: id,
      contractType
    })
    const that = this;
    axios.post(Api.queryOne, { contractId: id })
      .then(function (resp) {
        const { data, data: { dataList } } = resp;
        that.props.form.setFieldsValue({
          c_person: data.update_user,

          renterName: data.id,
          person: data.linkman,
          cardNumber: data.card_num,
          industry: data.hy_type_str,
          contact: data.contact_way,
          mail: data.mailbox,
          mainContent: data.main_business,
          investNumber: data.investment_amount,
          personSize: data.people_num,
          renterStatus: data.renter_situation,
          hConpermany: data.high,
          foundDate: data.established_time,
          rentIncome: data.full_income,
          income: data.income,
          rType: data.zc_type,
          rAuthority: data.rAuthority,
          company: data.intro,

          leaseholder: data.renter_name,
          zlArea: data.rent_area ? Math.round(data.rent_area * 100) / 100 : 0,
          chArea: data.map_area ? Math.round(data.map_area * 100) / 100 : 0,
          useArea: data.use_area ? Math.round(data.use_area * 100) / 100 : 0,
          gnArea: data.heat_area ? Math.round(data.heat_area * 100) / 100 : 0,
          jzArea: data.build_area ? Math.round(data.build_area * 100) / 100 : 0,
          rentMoney: data.rent_price,
          rentTotalMoney: data.rent_total,
          rentUse: data.rent_use,
          bzPrice: data.expected_bond,
          realPrice: data.actual_bond,
          s_fontDate: data.start_date ? moment(data.start_date) : null,
          s_eDate: data.end_date ? moment(data.end_date) : null,
          sDate: data.accept_date ? data.accept_date.substr(0, 10) : '',
          sPerson: data.accept_user,
          allMoney: data.total_money,
          payStatus: data.pay_status,
          water: data.water,
          electricity: data.electricity,
          heating: data.heating,
          property: data.property,
          elevator: data.elevator,
        });

        let houseList = [];
        dataList && dataList.map(item => {
          houseList.push({
            ...item,
            rentPrice: data.rent_price
          })
        })
        if (type == 'arrears') {
          that.setState({
            arrearsModalVisible: true
          })
        } else if (type === 'rerent') {
          that.setModalVisible(true, 'rerent', id, contractType)
        } else {
          that.setModalVisible(true, 'edit', id, contractType)
        }

        // 计算欠费详情中的欠费时长
        let m1 = moment();
        let m2 = moment(data.end_date);
        let day = m1.diff(m2, 'day');

        that.setState({
          renterName: data.renter_name,
          loading: false,
          houseList: dataList || [],
          renterId: data.id,
          lengthOfArrears: day ? day : 0,
          totalArea: data.rent_area ? Math.round(data.rent_area * 100) / 100 : 0
        });
      })
  };

  //删除项目
  deleteProject = (id) => {
    const that = this;
    confirm({
      title: "删除确认",
      content: '确认删除该项目？',
      onOk() {
        that.setState({ loading: true })
        axios.post(Api.delete, { contractId: id })
          .then(function (resp) {
            const { data } = resp;
            if (data.code == 1) {
              notification.destroy();
              notification['success']({ message: data.msg });
              that.sendData();
            } else {
              notification.destroy();
              notification['warning']({ message: data.msg });
            }
            that.setState({ loading: false })
          })
      }
    })
  }
  cancel = () => {
    let that = this;
    const { addHouseArr, delHouseArr, contractId } = that.state;
    addHouseArr && addHouseArr.length > 0 && addHouseArr.map((item, index) => {
      axios.post(Api.releaseHouse, {
        contractId,
        houseCode: item
      })
        .then(function (resp) {
          that.setState({
            addHouseArr: []
          })
        })
    })
    delHouseArr && delHouseArr.length > 0 && delHouseArr.map((item, index) => {
      axios.post(Api.addHouse, {
        contractId,
        houseCode: item
      })
        .then(function (resp) {
          that.setState({
            delHouseArr: []
          })
        })
    })
    this.setModalVisible(false)
    this.setState({ loading: false })
  }
  //编辑项目(点弹框确定)
  editProject = () => {
    let that = this;
    this.setState({
      loading: true
    })
    const houseCodes = this.state.houseList && this.state.houseList.map(item => item.houseCode);
    let fieldNames = ['rentName', 'c_person', 'person', 'cardNumber', 'industry', 'contact',
      'mail', 'mainContent', 'investNumber', 'personSize', 'renterStatus', 'hConpermany', 'foundDate',
      'rentIncome', 'income', 'rType', 'rAuthority', 'company', 'leaseholder', 'zlArea', 'chArea', 'useArea', 'gnArea',
      'jzArea', 'rentMoney', 'rentTotalMoney', 'rentUse', 'bzPrice', 'realPrice', 's_fontDate', 's_eDate', 'sDate',
      'sPerson', 'allMoney', 'payStatus', 'arriveAccount', 'water', 'electricity', 'heating', 'property', 'elevator'
    ];
    if (this.state.submitFlag == 'edit') {
      this.props.form.validateFields(fieldNames, (err, values) => {
        if (!err) {
          axios.post(Api.updateContractInfo, {
            contractId: that.state.contractId,
            contractType: that.state.contractType,
            houseCodeList: houseCodes.join(','),
            update_user: values.c_person,

            renterId: that.state.renterId,
            renterName: values.rentName,
            linkman: values.person,
            card_num: values.cardNumber,
            hy_type: values.industry,
            contact_way: values.contact,
            mailbox: values.mail,
            main_business: values.mainContent,
            investment_amount: values.investNumber,
            people_num: values.personSize,
            renter_situation: values.renterStatus,
            high: values.hConpermany,
            established_time: values.foundDate,
            full_income: values.rentIncome,
            zc_type: values.income,
            income: values.rType,
            rAuthority: values.rAuthority,
            intro: values.company,

            lessee: values.leaseholder,
            rentArea: Number(values.zlArea),
            mapArea: Number(values.chArea),
            useArea: Number(values.useArea),
            heatArea: Number(values.gnArea),
            buildArea: Number(values.jzArea),
            rentPrice: Number(values.rentMoney),
            rentTotal: Number(values.rentTotalMoney),
            expectedBond: Number(values.bzPrice),
            actualBond: Number(values.realPrice),
            startDate: values.s_fontDate ? moment(values.s_fontDate).format("YYYY-MM-DD") : '',
            endDate: values.s_eDate ? moment(values.s_eDate).format("YYYY-MM-DD") : '',
            acceptDate: values.sDate,
            acceptUser: values.sPerson,
            totalMoney: Number(values.allMoney),
            payStatus: values.payStatus,
            arriveAccount: values.arriveAccount,
            rentUse: values.rentUse,
            water: Number(values.water),
            electricity: Number(values.electricity),
            heating: Number(values.heating),
            property: Number(values.property),
            elevator: Number(values.elevator),
          })
            .then(function (resp) {
              const { data } = resp;
              if (data.status == 'success') {
                notification.destroy();
                notification['success']({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else {
                notification.destroy();
                notification['error']({ message: data.msg });
              }
              that.setState({
                loading: false,
                addHouseArr: [],
                delHouseArr: []
              })
            })
        }
      })
    } else {
      this.props.form.validateFields(fieldNames, (err, values) => {
        if (!err) {
          axios.post(Api.renewal, {

            houseCodeList: houseCodes.join(','),
            contractId: that.state.contractId,
            update_user: values.c_person,

            renterId: that.state.renterId,
            renterName: values.rentName,
            linkman: values.person,
            card_num: values.cardNumber,
            hy_type: values.industry,
            contact_way: values.contact,
            mailbox: values.mail,
            main_business: values.mainContent,
            investment_amount: values.investNumber,
            people_num: values.personSize,
            renter_situation: values.renterStatus,
            high: values.hConpermany,
            established_time: values.foundDate,
            full_income: values.rentIncome,
            zc_type: values.income,
            income: values.rType,
            rAuthority: values.rAuthority,
            intro: values.company,

            lessee: values.leaseholder,
            rentArea: Number(values.zlArea),
            mapArea: Number(values.chArea),
            useArea: Number(values.useArea),
            heatArea: Number(values.gnArea),
            buildArea: Number(values.jzArea),
            rentPrice: Number(values.rentMoney),
            rentTotal: Number(values.rentTotalMoney),
            expectedBond: Number(values.bzPrice),
            actualBond: Number(values.realPrice),
            startDate: values.s_fontDate,
            endDate: values.s_eDate,
            acceptDate: values.sDate,
            acceptUser: values.sPerson,
            totalMoney: Number(values.allMoney),
            payStatus: values.payStatus,
            arriveAccount: values.arriveAccount,
            rentUse: values.rentUse,
            water: Number(values.water),
            electricity: Number(values.electricity),
            heating: Number(values.heating),
            property: Number(values.property),
            elevator: Number(values.elevator),
          })
            .then(function (resp) {
              const { data } = resp;
              if (data.code == 1) {
                notification.destroy();
                notification['success']({ message: data.msg });
                that.sendData();
                that.setModalVisible(false);
              } else if (data.code == 0) {
                notification.destroy();
                notification['error']({ message: data.msg });
              }
              that.setState({ loading: false })
            })
        }
      })
    }

  };
  // 确认提交
  showConfirm = () => {
    let that = this;
    if (that.state.contractType === '5') {
      confirm({
        title: '确定提交此待转合同?',
        // content: 'Some descriptions',
        okText: '确定',
        okType: 'danger',
        cancelText: '取消',
        onOk() {
          that.editProject()
        },
        onCancel() {
          console.log('Cancel');
        },
      });
    } else {
      that.editProject()
    }
  }



  showDownload(showDownload) {
    this.setState({ showDownload });
  }

  pageChange = page => {
    this.setState(
      {
        current: page,
      },
      () => {
        this.sendData();
      }
    );
  };

  setModalVisible = (visible, type, id, contractType) => {
    let that = this;
    var myDate = new Date();
    let date = myDate.toLocaleDateString().replace(/\//g, '-');
    let currentUser = sessionStorage.getItem('currentUser');
    this.setState({ modalVisible: visible, submitFlag: type });

  };

  // changeSelected = (selectedRowKeys, selectedRows) => {
  //   let selectdIds = [];
  //   selectedRows &&
  //     selectedRows.forEach((d, i, arr) => {
  //       selectdIds.push(d.projectCode);
  //     });

  //   this.setState({ selectedArr: selectedRowKeys, selectdIds: selectdIds });
  // };


  handleMenuClick = (project) => {
    var win = window.open('/fe/assetManagement/projectInfo?projectCode=' + project.projectCode, '_blank');
    win.focus();
  }

  resetForm = () => {
    this.setState({
      searchParams: {
        rentName: null, // 楼栋名称
        startDate: null, // 楼栋编号
        endDate: null, // 在租面积起
        contractType: null, // 在租面积止
        startYearRent: null, // 建筑面积起
        endYearRent: null, // 建筑面积止
      },
      pageSize: 5,
      current: 1,
    });
  }

  renterChange = (value, option) => {
    let that = this;
    //获取房屋信息
    axios.post(Api.queryRenterInfo, {
      renterId: value
    })
      .then(function (resp) {
        const data = resp.data;
        that.props.form.setFieldsValue({
          person: data.linkman ? data.linkman : '-',
          cardNumber: data.card_num ? data.card_num : '-',
          industry: data.hy_type ? data.hy_type : '-',
          contact: data.contact_way ? data.contact_way : '-',
          mail: data.mailbox ? data.mailbox : '-',
          mainContent: data.main_business ? data.main_business : '-',
          investNumber: data.investment_amount ? data.investment_amount : '-',
          personSize: data.people_num ? data.people_num : '-',
          renterStatus: data.renter_situation ? data.renter_situation : '-',
          hConpermany: data.high ? data.high : '-',
          foundDate: data.established_time ? data.established_time.substr(0, 10) : '-',
          rentIncome: data.full_income ? data.full_income : '-',
          rType: data.zc_type ? data.zc_type : '-',
          income: data.income ? data.income : '-',
          company: data.intro ? data.intro : '-',
          leaseholder: data.renter_name
        });
        that.setState({
          renterData: data
        })
      })
  }


  //查询楼栋数据
  getHouseByParams = (buildingId) => {
    if (buildingId) {
      axios
        .get(Api.queryHouseByParams, {
          params: {
            buildingId
          },
        })
        .then(res => {
          if (res.data === null) {
            notification.destroy();
            notification['warning']({ message: '暂无数据！' });
          } else {
            let data = res.data.houses && res.data.houses.map(item => { return { houseCode: item.houseCode, isRent: item.isRent } })
            this.setState({
              sendMapData: {
                type: 'clickedLD',
                data,
                buildingId,
                floors: res.data.floors
              }
            });
          }
        });
    }
  }

  showMapModal = (visible) => {
    this.setState({ mapModal: visible });
  }

  onReceiveMessage = (data) => {
    if (data.data.type === 'clickedLD') {
      this.getHouseByParams(data.data.build_id);
    }

    let flag = false;
    if (data.data.type === 'clickedLDR') {
      const info = data.data.attribute;
      this.state.houseList && this.state.houseList.map(item => {
        if (item.houseCode === data.data.house_id) {
          flag = true;
        }
      })
      if (flag) {
        notification.destroy();
        notification.warning({ message: '该房间已选择，请重新选择！' })
        this.setState({ mapHouse: null });
        return;
      }
      const { addHouseArr } = this.state
      let addHouseArr2 = Array.from(addHouseArr);
      addHouseArr2.push(data.data.house_id)
      const par = {
        houseCode: data.data.house_id,
        houseName2: info.house_name2,
        buildingName2: info.building_name2,
        useArea: 1, // info.use_area,
        prerent_price: info.prerent_price, // info.itemprerent_price,
        renovationCondition: info.renovation_condition,
        buildingArea: info.building_area, // 建筑面积
        heatingArea: info.heating_area, // 供热面积
        useArea: info.use_area, // 使用面积
      };
      this.setState({
        mapHouse: par,
        addHouseArr: addHouseArr2
      }, () => {
        notification.destroy();
        notification.success({ message: `已选择房间${par.houseName2}` });
      });
    }
  }

  setHouseToModal = () => {
    if (this.state.mapHouse && this.state.houseList) {
      const { houseList } = this.state;
      houseList.push(this.state.mapHouse);
      this.setState({ houseList }, () => {
        this.computRent();
      });
    }
    this.showMapModal(false)
  }

  //计算单价、总价并赋值
  computRent = () => {
    let rentMoney = 0;
    let rentTotalMoney = 0;
    const zlArea = 0;
    const chArea = 0;
    let useArea = 0;
    let gnArea = 0;
    let jzArea = 0;
    this.state.houseList.forEach(item => {
      if (item.rentPrice) {
        rentMoney += Number(item.rentPrice);
        if (item.useArea) rentTotalMoney += Number(item.useArea) * Number(item.rentPrice);
      }
      if (item.buildingArea) jzArea += Number(item.buildingArea);
      if (item.useArea) useArea += Number(item.useArea);
      if (item.heatingArea) gnArea += Number(item.heatingArea);
    })
    rentMoney = rentMoney ? rentMoney / this.state.houseList.length : '';
    this.props.form.setFieldsValue({
      rentMoney,
      rentTotalMoney: rentTotalMoney || '',
      jzArea,
      gnArea,
      useArea,
      zlArea: jzArea,
      chArea: jzArea,
    });
  }

  delHouse = (item, index, type) => {
    let that = this;
    const { houseList, delHouseArr, contractId } = that.state;
    let houseList2 = Array.from(houseList);

    houseList2.splice(index, 1);
    that.setState({
      houseList: houseList2,
    }, () => {
      that.computRent();
    });
    if (type !== 'add') {
      axios.post(Api.releaseHouse, {
        contractId,
        houseCode: item.houseCode
      })
        .then(function (resp) {
          let delHouseArr2 = Array.from(delHouseArr);
          delHouseArr2.push(item.houseCode)
          that.setState({
            delHouseArr: delHouseArr2
          })
        })
    }
  }

  export = () => {
    this.setState({ startDownload: false });
    axios.post(Api.exportContract, {
      ...this.state.searchParams
    },
      { responseType: 'arraybuffer' }
    ).then((res) => {
      notification.destroy();
      notification['success']({ message: '开始下载！' })

      let blob = new Blob([res.data], { type: "application/vnd.ms-excel" });
      const [dLink, name] = [
        window.URL.createObjectURL(blob),
        `合同列表${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.xls`
      ];
      let link = document.createElement('a');
      link.href = dLink;
      link.download = name;
      link.click();
      this.setState({
        startDownload: true,
        downloadLink: dLink,
        downloadName: name,
        showDownload: true
      });
    }).catch((err) => {
      notification.destroy();
      notification['warning']({ message: `下载失败！错误信息：${err}` })
    });
  };

  checkStatusIsActive = (index) => index <= this.state.approvalActiceStep;

  getApproval = (contractId) => {
    axios.post(Api.getApproval, {
      contractId,
    }).then(res => {
      const approvalList = [
        {}, {}, {}, {}
      ];
      let approvalActiceStep = 0;
      res.data.items.forEach(approval => {
        if (approval.contractStatus === '9') {
          approvalList[Number(approval.contractPerple) - 1] = {
            contractDate: moment(approval.contractDate).format("YYYY-MM-DD")
          };
        } else if (Number(approval.contractStatus) > approvalActiceStep) approvalActiceStep = Number(approval.contractStatus);
      });
      this.setState({
        approvalList,
        approvalActiceStep,
        currentContractId: contractId
      });
    });
  }

  disorderApproval = (approval, canApproval, index) => {
    // if (approval.contractDate || (canApproval !== (index + 1))) return;
    if (this.state.isApproval) {
      axios.post(Api.disorderApproval, {
        contractId: this.state.currentContractId,
        node: '1',
      }).then((resp) => {
        this.getApproval(this.state.currentContractId);
        this.sendData();
      })
    }
  }

  // 迁出
  editMoveOut = () => {
    let fieldNames = ['moveOutReason', 'moveOutDate']
    this.props.form.validateFields(fieldNames, (err, values) => {
      if (!err) {
        axios.post(Api.moveOut, {
          contractId: this.state.currentContractId,
          people: this.userCode,
          date: values.moveOutDate,
          reason: values.moveOutReason,
        }).then((res) => {
          this.setState({
            moveOutModalVisible: false
          });
          this.getApproval(this.state.currentContractId);
          this.sendData();
        })
      }
    })
  }

  addApproval = (e) => {
    if (this.state.isFinance && e == '缴费状态') {
      axios.post(Api.addApproval, {
        contractId: this.state.currentContractId,
      }).then((res) => {
        this.getApproval(this.state.currentContractId);
        this.sendData();
      })
    }

    if (this.state.isApproval && e == '确认迁出') {
      axios.post(Api.addApproval, {
        contractId: this.state.currentContractId,
      }).then((res) => {
        this.getApproval(this.state.currentContractId);
        this.sendData();
      })
    }
  }

  changeState = (key, value) => {
    const searchParams = { ...this.state.searchParams };
    searchParams[key] = value;
    this.setState({ searchParams });
  }
  disabledControl = (key) => {
    if (this.state.submitFlag === 'edit') {
      if (this.state.isFinance) {
        return key === 'payStatus'
          ?
          false
          :
          true
      } else {
        return key === 'payStatus'
          ?
          true
          :
          false
      }
    } else {
      return key === 'sDate' || key === 'allMoney' || key === 'yearMout' || key === 'payStatus' || key === 'sPerson' || key === 'arriveAccount'
        ?
        true
        :
        false
    }
  }

  setAddModalVisible = (show) => {
    this.setState({ showCreateModel: show });
    this.setState({
      houseList: []
    })
    this.props.form.setFieldsValue({
      rentNameForAdd: '',
      person: '',
      cardNumber: '',
      industry: '',
      contact: '',
      mail: '',
      mainContent: '',
      investNumber: '',
      personSize: '',
      renterStatus: '',
      hConpermany: '',
      foundDate: '',
      rentIncome: '',
      rType: '',
      income: '',
      company: '',
      leaseholder: '',
      zlArea: '',
      chArea: '',
      useArea: '',
      gnArea: '',
      jzArea: '',
      rentMoney: '',
      rentTotalMoney: '',
      bzPrice: '',
      realPrice: '',
      s_fontDate: '',
      s_eDate: '',
      s_fontDate: '',
      sDate: '',
      allMoney: '',
      yearMout: '',
      payStatus: '',
      arriveAccount: '',
      sPerson: '',
      rentUse: ''
    })
  }
  totalArrears = (e) => {
    this.setState({
      unitPrice: e
    })
  }
  moveOutBefore = () => {
    if (this.state.isCommit) {
      this.setState({ moveOutModalVisible: true })
    }
  }
  render() {
    let that = this;
    const { form: { getFieldDecorator }, currentUserLoading } = this.props;
    const canApproval = sessionStorage.getItem('canApproval');
    const canCommit = sessionStorage.getItem('canCommit');

    let ApprovalPopover = (<span>审批状态</span>);
    // if (this.state.approvalActiceStep === 1) ApprovalPopover = (<Popover placement="leftTop" trigger="click" title={'审批状态'} content={<ApprovalContent isShow={false} />}><a href="javascript:;" >审批状态</a></Popover>);
    // if (this.state.approvalActiceStep >= 2) ApprovalPopover = (<Popover placement="leftTop" trigger="click" title={'查看审批'} content={<ApprovalContent isShow={true} />}><a href="javascript:;" >审批状态</a></Popover>);

    // 迁出的popover
    const popoverContent = (
      <Steps direction="vertical" size="small" style={{ width: 300 }}>
        <Step status={this.checkStatusIsActive(1) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>迁出申请</span>
            {this.state.approvalActiceStep === 0 ? <a onClick={() => this.moveOutBefore()}>提交</a> : null}
            {this.checkStatusIsActive(1) ? <span>完成</span> : null}
          </div>} />
        <Step status={this.checkStatusIsActive(2) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            {ApprovalPopover}
            {this.state.approvalActiceStep === 1 ? <a onClick={() => this.disorderApproval()}>审批</a> : null}
            {this.checkStatusIsActive(2) ? <span style={{ flex: 'right' }}>完成</span> : null}
          </div>} />
        <Step status={this.checkStatusIsActive(3) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>缴费状态</span>
            {this.state.approvalActiceStep === 2 ? <a onClick={() => this.addApproval("缴费状态")}>确认</a> : null}
            {this.checkStatusIsActive(3) ? <span>完成</span> : null}
          </div>} />
        {/* <Step status={this.checkStatusIsActive(4) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>验房状态</span>
            {this.state.approvalActiceStep === 3 ? <a onClick={() => this.addApproval()}>确认</a> : null}
            {this.checkStatusIsActive(4) ? <span>完成</span> : null}
          </div>} />
        <Step status={this.checkStatusIsActive(5) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>退还保障金</span>
            {this.state.approvalActiceStep === 4 ? <a onClick={() => this.addApproval()}>确认</a> : null}
            {this.checkStatusIsActive(5) ? <span>完成</span> : null}
          </div>} /> */}
        <Step status={this.checkStatusIsActive(6) ? 'process' : 'wait'} description={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>完成迁出</span>
            {this.state.approvalActiceStep === 5 || this.state.approvalActiceStep === 3 ? <a onClick={() => this.addApproval("确认迁出")}>确认</a> : null}
            {this.checkStatusIsActive(6) ? <span>完成</span> : null}
          </div>} />
      </Steps>
    );


    //表格表头
    const columns = [
      {
        title: '企业名称',
        dataIndex: 'renterName',
        key: 'renterName',
        render: (text, item, index) => (
          <Link key={index} target='_blank' to={{
            pathname: '/fe/dataMaintenance/operationDetails',
            search: `?contractId=${item.contractId}`
          }}>
            {text}
          </Link>
        ),
      },
      {
        title: '缴费状态',
        dataIndex: 'payStatus',
        key: 'payStatus',
        render: (text, item, index) => {
          switch (text) {
            case '1':
              return '已缴费';
              break;
            case '2':
              return '未缴费';
              break;
            case '3':
              return '欠费未缴';
              break;
            default:
              break;
          }
        }
        ,
      },
      {
        title: '楼栋名称',
        dataIndex: 'buildingName2',
        key: 'buildingName2',
      },
      ,
      {
        title: '房屋名称',
        dataIndex: 'houseName2',
        key: 'houseName2',
      },
      {
        title: '注册地址',
        dataIndex: 'registeredAddress',
        key: 'registeredAddress',
      },
      {
        title: '出租面积',
        dataIndex: 'rentArea',
        key: 'rentArea',
      },
      {
        title: '开始日期',
        dataIndex: 'startDate',
        key: 'startDate',
      },
      {
        title: '结束日期',
        dataIndex: 'endDate',
        key: 'endDate',
      },
      {
        title: '租金单价(元/㎡年)',
        dataIndex: 'rentPrice',
        key: 'rentPrice',
      },
      {
        title: '总金额',
        dataIndex: 'rentTotal',
        key: 'rentTotal',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, item, index) => (
          <span key={index}>
            {
              item.contractType === '3' || item.contractType === '4' || item.contractType === '5' || item.contractType == '6'
                ?
                ''
                :
                <a href="javascript:;" onClick={() => this.getProject(item.contractId, 'rerent', item.contractType)}>续租 </a>
            }
            {
              item.contractType === '3' || item.contractType === '4' || item.contractType === '5' || item.contractType == '6'
                ?
                ''
                :
                <Popover placement="leftTop" trigger="click" title={'迁出流程'} content={popoverContent} onVisibleChange={(visible) => !visible || this.getApproval(item.contractId)}><a href="javascript:;" >迁出 </a></Popover>
            }
            {
              item.contractType == '6'
                ?
                ''
                :
                <Link key={index} target='_blank' to={{ pathname: '/fe/dataMaintenance/operationDetails', search: `?contractId=${item.contractId}` }}>详情 </Link>
            }

            {

              (this.state.isFinance && item.contractType === '2') || (!this.state.isFinance && item.contractType === '5' && item.editStatus !== '1') || item.contractType == '6'
                ?
                <a href="javascript:;" onClick={() => this.getProject(item.contractId, 'edit', item.contractType)}>编辑 </a>
                :
                null
            }

            {

              item.editStatus === '1' && this.state.isApproval
                ?
                <a href="javascript:;" onClick={() => this.agreementApproval(item.contractId, '0', item.contractType)}>回退 </a>
                :
                null
            }
            {

              item.editStatus === '1' && this.state.isApproval
                ?
                <a href="javascript:;" onClick={() => this.agreementApproval(item.contractId, '1', item.contractType)}>同意</a>
                :
                null
            }
            {
              item.contractType == '6'
                ?
                <a href="javascript:;" onClick={() => this.getProject(item.contractId, 'arrears', item.contractType)}>欠费详情 </a>
                :
                ''
            }
            {/* <a href="javascript:;" onClick={this.getProject.bind(this, item.contractId)}>编辑</a>&emsp;
            <a href="javascript:;" onClick={this.deleteProject.bind(this, item.contractId)}>删除</a> */}
          </span>
        ),
      },
    ];

    // const rowSelection = {
    //   selectedRowKeys: this.state.selectedArr,
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.changeSelected(selectedRowKeys, selectedRows);
    //   },
    // };
    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className="mainContainer">
          <div className='contentHeader'>
            <div className="contentHeaderTitle">
              <p>合同列表</p>
              <p>RENTER MANAGEMENT</p>
            </div>
          </div>
          <Form layout="inline" align='center'>
            {/* <FormItem label="企业名称：">
              {
                getFieldDecorator('s_name')(
                  <Input placeholder="请输入企业名称" />
                )
              }
            </FormItem> */}

            <FormItem>
              <span className="label">企业名称：</span>
              <Input
                value={this.state.searchParams.rentName}
                data-inp="zdbh"
                onChange={(e) => this.changeState('rentName', e.target.value)}
                placeholder="请输入企业名称"
              />
              <span className="label">开始日期：</span>
              <DatePicker
                placeholder="请选择开始日期"
                value={this.state.searchParams.startDate}
                onChange={(value) => this.changeState('startDate', value)} />

              <span className="label">结束日期：</span>
              <DatePicker
                placeholder="请选择结束日期"
                value={this.state.searchParams.endDate}
                onChange={(value) => this.changeState('endDate', value)} />

              <span className="label">合同状态：</span>
              <Select placeholder="请选择合同状态"
                value={this.state.searchParams.contractType}
                onChange={(value) => this.changeState('contractType', value)}>
                <Option value='2'>在租合同</Option>
                <Option value='3'>归档</Option>
                <Option value='4'>退租</Option>
                <Option value='5'>待转合同</Option>
                <Option value='6'>延期未缴费</Option>
              </Select>
              &emsp;
              <span className="label" style={{ width: 'auto' }}>当年租金：</span>
              <Input
                style={{ width: 120, textAlign: 'center' }}
                placeholder="下限租金"
                value={this.state.searchParams.startYearRent}
                onChange={(e) => this.changeState('startYearRent', e.target.value)}
              />
              ~
                <Input
                style={{ width: 120, textAlign: 'center' }}
                placeholder="上限租金"
                value={this.state.searchParams.endYearRent}
                onChange={(e) => this.changeState('endYearRent', e.target.value)}
              />
            </FormItem>

            {/* <FormItem label="开始日期：">

              {
                getFieldDecorator('s_date')(
                  <DatePicker style={{ width: "100%" }} />
                )
              }
            </FormItem>
            <FormItem label='结束日期：'>
              {
                getFieldDecorator('e_date')(
                  <DatePicker style={{ width: "100%" }} />
                )
              }
            </FormItem> */}
            {/* <FormItem label="合同状态：">
              {
                getFieldDecorator('contractType')(
                  <Select placeholder="请选择合同状态">
                    <Option value='1'>协议</Option>
                    <Option value='2'>合同</Option>
                    <Option value='3'>归档</Option>
                    <Option value='4'>退租</Option>
                    <Option value='5'>延期未缴费</Option>
                  </Select>
                )
              }
            </FormItem> */}

            <Row>
              <Col span={24} style={{ textAlign: 'center', marginBottom: '10px' }}>
                {/* <Button
                  type="primary"
                  onClick={() => this.setAddModalVisible(true)}
                  htmlType="reset" icon="plus-circle"
                >
                  新增
                </Button>
                &emsp; */}
                <Button icon="search" type="primary" className="evfilterbtn" onClick={this.query}>
                  搜索
                </Button>
                &emsp;
                <Button
                  onClick={() => this.resetForm()}
                  type="primary"
                  htmlType="reset"
                  icon="reload"
                >
                  重置
                </Button>
                &emsp;
                <Button icon='profile' ghost
                  onClick={this.export}
                  type="primary"
                  htmlType="reset"
                  className="evfilterbtn">
                  生成报表
                </Button>
              </Col>
            </Row>
          </Form>

          <Spin spinning={this.state.loading}>
            <Row>
              <Table
                bordered={true}
                scroll={{ x: true }}
                columns={columns}
                dataSource={this.state.tableData}
                pagination={{
                  pageSize: that.state.pageSize,
                  current: that.state.current,
                  onChange: that.pageChange,
                  total: that.state.total,
                  showTotal: total => `共 ${total} 项`
                }}
              />
            </Row>
            <Modal
              title='地图选房'
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.mapModal}
              onOk={this.setHouseToModal}
              onCancel={() => this.showMapModal(false)}
              width={830}>
              <div>
                <IframeComm
                  attributes={{
                    src: '/gis/mapHouse.html',
                    style: {
                      width: '100%',
                      height: '450px',
                    },
                  }}
                  postMessageData={this.state.sendMapData}
                  handleReceiveMessage={this.onReceiveMessage}
                />
              </div>
            </Modal>

            <Modal
              title={this.state.submitFlag == 'edit' ? "编辑合同" : "续租合同"}
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.modalVisible}
              onOk={this.showConfirm}
              onCancel={this.cancel}
              width={830}
            >
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>房屋基本信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }}></span></Row>
              <Row gutter={24} style={{ marginBottom: '10px' }}>
                <Col span={24} style={{ textAlign: 'center', borderBottom: "1px dashed #eee" }}>
                  <Button type="primary" style={{ margin: '13px 0' }} onClick={() => this.showMapModal(true)}>请选择房屋&nbsp;&nbsp;<img src={ico_map} width={19} style={{ float: 'right' }} /></Button>
                </Col>
                <Col span={24} style={{ maxHeight: '225px', overflowY: "auto" }}>
                  {
                    !this.state.houseList || this.state.houseList.length === 0 ?
                      <div className={styles.noData}><img src={ico_nodata} width={169} /><br />暂无数据</div>
                      :
                      this.state.houseList.map((item, index) => (
                        <Row gutter={24} className={styles.mapHouse} key={item.houseCode}>
                          <span className={styles.dot}></span>
                          <span span={4}>房屋名称：{item.houseName2 ? item.houseName2 : '-'}</span>
                          <span span={4}>楼栋名称：{item.buildingName2 ? item.buildingName2 : '-'}</span>
                          <span span={4}>房屋面积：{item.buildingArea ? item.buildingArea + '㎡' : '-'}</span>
                          <span span={4}>租金单价：{item.prerent_price ? item.prerent_price + '元/㎡/日' : '-'}</span>
                          <span span={4}>装修情况：{item.renovationCondition ? item.renovationCondition : '-'}</span>
                          <span span={4}>
                            <Tooltip title="删除">
                              <a><Icon type="delete" onClick={() => this.delHouse(item, index, 'edit')} theme="twoTone" twoToneColor="#E22534" /></a>
                            </Tooltip>
                          </span>
                        </Row>
                      ))
                  }
                </Col>
              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>企业基本信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }}></span></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="企业名称：">
                    {getFieldDecorator('renterName', {
                      rules: [{
                        required: true, message: '请选择企业名称'
                      }]
                    })(<Select
                      onChange={this.renterChange}
                      placeholder="请选择企业名称"
                      style={{ width: "100%" }}
                      showSearch
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }>
                      {
                        this.state.renterList && this.state.renterList.map((item, index) => {
                          return <Option value={item.id} key={item.id}>{item.renter_name}</Option>
                        })
                      }
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="联系人：">
                    {getFieldDecorator('person', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入联系人" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="证件号码：">
                    {getFieldDecorator('cardNumber', {
                      rules: [
                        { pattern: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: '请按照规则输入！' },
                      ]
                    })(<Input placeholder="请输入证件号码" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="行业分类：">
                    {getFieldDecorator('industry', {
                      rules: [{ message: '请输入行业分类！' }]
                    })(<Select placeholder="请选择行业分类" style={{ width: "100%" }} disabled >
                      <Option value='自管'>自管</Option>
                      <Option value='托管'>托管</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="联系方式：">
                    {getFieldDecorator('contact', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }
                      ]
                    })(
                      <Input placeholder="请输入联系方式" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="邮箱：">
                    {getFieldDecorator('mail', {
                      rules: [
                        { pattern: /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/, message: '请输入正确的邮箱格式！' },
                        { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入邮箱" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="主营业务：">
                    {getFieldDecorator('mainContent', {
                      rules: [
                        { max: 200, message: '文字内容超过200字' }
                      ]
                    })(<Input placeholder="请输入主营业务" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="投资额：">
                    {getFieldDecorator('investNumber', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入投资额" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="人数：">
                    {getFieldDecorator('personSize')(
                      <Select placeholder="请选择人员规模" style={{ width: "100%" }} disabled>
                        <Option value='100人以下'>100人以下</Option>
                        <Option value='100-400人'>100-400人</Option>
                        <Option value='400人以上'>400人以上</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="企业情况：">
                    {getFieldDecorator('renterStatus')(
                      <Select placeholder="请选择企业情况" style={{ width: "100%" }} disabled>
                        <Option value='上市'>上市</Option>
                        <Option value='三板'>三板</Option>
                        <Option value='区域挂牌'>区域挂牌</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="高企：">
                    {getFieldDecorator('hConpermany')(
                      <Select placeholder="请选择是否高企" style={{ width: "100%" }} disabled>
                        <Option value='是'>是</Option>
                        <Option value='否'>否</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="成立时间：">
                    {getFieldDecorator('foundDate')(<Input placeholder="请输入成立时间" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="当年全口径税收：">
                    {getFieldDecorator('rentIncome', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入当年全口径税收" disabled />)}
                  </FormItem>
                </Col>

                <Col span={8}>
                  <FormItem label="注册类型：">
                    {getFieldDecorator('rType', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(<Input placeholder="请输入注册类型" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="当年收入：">
                    {getFieldDecorator('income', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入当年收入" disabled />)}
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="企业简介：">
                    {getFieldDecorator('company', {
                      rules: [{
                        max: 500, message: '输入内容超过500字'
                      }]
                    })(<TextArea rows={4} placeholder="请输入企业简介" disabled />)}
                  </FormItem>
                </Col>
              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>合同信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }}></span></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="承租方：">
                    {getFieldDecorator('leaseholder', {
                      rules: [
                        { max: 50, message: '文字内容超过50字' }]
                    })(<Input placeholder="请输入承租方" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="租赁面积：">
                    {getFieldDecorator('zlArea', {
                      rules: [
                        { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入租赁面积" disabled={this.disabledControl('zlArea')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="测绘面积：">
                    {getFieldDecorator('chArea', {
                      rules: [
                        { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入测绘面积型" disabled={this.disabledControl('chArea')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="使用面积：">
                    {getFieldDecorator('useArea', {
                      rules: [
                        { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入使用面积" disabled={this.disabledControl('useArea')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="供暖面积：">
                    {getFieldDecorator('gnArea', {
                      rules: [
                        { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入供暖面积" disabled={this.disabledControl('gnArea')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="建筑面积：">
                    {getFieldDecorator('jzArea', {
                      rules: [
                        { pattern: /^[0-9]{1,7}([.][0-9]{1,2})?$/, message: '面积应小于1千万，限制小数点后两位' }]
                    })(<Input placeholder="请输入建筑面积型" disabled={this.disabledControl('jzArea')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="租金单价：">
                    {getFieldDecorator('rentMoney', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入租金单价" disabled={this.disabledControl('rentMoney')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="租金总价：">
                  {/* disabled={this.disabledControl('rentTotalMoney')} */}
                    {getFieldDecorator('rentTotalMoney', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入租金总价" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="租金用途：">
                    {getFieldDecorator('rentUse')(<Select
                      disabled={this.disabledControl('rentUse')}
                      placeholder="请选择租金用途"
                      style={{ width: "100%" }}
                    >
                      {
                        this.state.rentUse && this.state.rentUse.map((item, index) => {
                          return <Option value={item.dic_code} key={item.dic_code}>{item.dic_name}</Option>
                        })
                      }
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="应缴纳保障金：">
                    {getFieldDecorator('bzPrice', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入应缴纳保障金型" disabled={this.disabledControl('bzPrice')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="实收保障金：">
                    {getFieldDecorator('realPrice', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入实收保障金" disabled={this.disabledControl('realPrice')} />)}
                  </FormItem>
                </Col>

                <Col span={8}>
                  <FormItem label="开始日期：">
                    {getFieldDecorator('s_fontDate')(<DatePicker style={{ width: "100%" }} disabled={this.disabledControl('s_fontDate')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="结束日期：">
                    {getFieldDecorator('s_eDate')(<DatePicker style={{ width: "100%" }} disabled={this.disabledControl('s_eDate')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="受理日期：">
                    {getFieldDecorator('sDate')(<Input placeholder="请输入受理日期" disabled={this.disabledControl('sDate')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="总金额：">
                    {getFieldDecorator('allMoney', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入总金额" disabled={this.disabledControl('allMoney')} />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="当年租金：">
                  {/* disabled={this.disabledControl('yearMout')} */}
                    {getFieldDecorator('yearMout', {
                      rules: [{ pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入当年租金" disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="缴费状态：">
                    {getFieldDecorator('payStatus')(<Select placeholder="请选择缴费状态" disabled={this.disabledControl('payStatus')} >
                      <Option value="1" key="1">已缴费</Option>
                      <Option value="2" key="2">未缴费</Option>
                      <Option value="3" key="3">欠费未缴</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="到账账户：">
                    {getFieldDecorator('arriveAccount')
                      (<Select placeholder="请选择到账账户：" disabled={this.disabledControl('arriveAccount')}>
                        <Option value="孵化器" key="0">孵化器</Option>
                        <Option value="基础公司" key="1">基础公司</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="受理人：">
                    {getFieldDecorator('sPerson', {
                      rules: [{
                        max: 50, message: '输入内容超过50字'
                      }]
                    })(<Input placeholder="请输入受理人" disabled={this.disabledControl('sPerson')} />)}
                  </FormItem>
                </Col>
              </Row>
              <Row style={{ marginBottom: '4px' }} className={styles.renterTitle} gutter={24}>费用信息</Row>
              <Row style={{ textAlign: 'center' }}><span style={{ height: '1px', width: '124px', textAlign: 'center', borderBottom: '1px solid #E9E9E9', display: 'inline-block' }}></span></Row>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="水费：">
                    {getFieldDecorator('water', {
                      rules: [
                        { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入水费" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="电费：">
                    {getFieldDecorator('electricity', {
                      rules: [
                        { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入电费" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="供暖费：">
                    {getFieldDecorator('heating', {
                      rules: [
                        { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入供暖费" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="物业费：">
                    {getFieldDecorator('property', {
                      rules: [
                        { pattern: /^[0-9]{1,8}([.][0-9]{1,2})?$/, message: '只能输入数字且小数点前8位后两位' }]
                    })(<Input placeholder="请输入物业费" />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="电梯费：">
                    {getFieldDecorator('elevator')(<Input placeholder="请输入电梯费" disabled />)}
                  </FormItem>
                </Col>
              </Row>
            </Modal>
            <Modal
              title={"迁出申请"}
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.moveOutModalVisible}
              onOk={this.editMoveOut}
              onCancel={() => this.setState({ moveOutModalVisible: false })}
              width={830}
            >
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="提交人：">
                    <Input placeholder="请输入提交人" value={this.userCode} disabled />
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="提交日期：">
                    {getFieldDecorator('moveOutDate', {
                      initialValue: moment().format("YYYY-MM-DD"),
                      rules: [{
                        required: true, message: '请选择日期'
                      }]
                    })(<Input style={{ width: "100%" }} disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="退租原因：">
                    {getFieldDecorator('moveOutReason', {
                      rules: [{
                        required: true, message: '请填写退租原因'
                      }]
                    })(
                      <Select placeholder="请选择退租原因" >
                        <Option value="租金过高">租金过高</Option>
                        <Option value="物业服务不满意">物业服务不满意</Option>
                        <Option value="企业自身原因">企业自身原因</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </Row>
            </Modal>
            <Modal
              title={"费用欠缴信息"}
              wrapClassName="vertical-center-modal"
              className='modalAlign'
              visible={this.state.arrearsModalVisible}
              onOk={this.arrears}
              okText="计算"
              onCancel={() => this.setState({ arrearsModalVisible: false })}
              width={830}
            >
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="企业名称：">
                    <Input placeholder="请输入企业名称" value={this.state.renterName} disabled />
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="开始日期：">
                    {getFieldDecorator('s_fontDate')(<DatePicker style={{ width: "100%" }} disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="结束日期：">
                    {getFieldDecorator('s_eDate')(<DatePicker style={{ width: "100%" }} disabled />)}
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="欠费时长：">
                    <Input placeholder="请输入欠费时长" value={this.state.lengthOfArrears} disabled />
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="计费标准：">
                    <Input placeholder="请输入计算标准" onChange={(e) => this.totalArrears(e.target.value)} />
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="租赁总面积：">
                    <Input placeholder="请输入租赁总面积" value={this.state.totalArea} disabled />
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="欠费总额：">
                    <Input placeholder="请输入欠费总额" value={this.state.totalArrears} />
                  </FormItem>
                </Col>
              </Row>
            </Modal>
          </Spin>
        </div>
      </PageHeaderWrapper>
    );
  }
}

const WrappedApp = Form.create()(OperationManagement);
export default WrappedApp;