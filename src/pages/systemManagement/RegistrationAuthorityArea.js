/* * @Author: 程家兴  
* @Date: 2019-06-05 20:12:22  
 * @Last Modified by: 程家兴
 * @Last Modified time: 2019-06-05 15:07:41
* @File: 企业登记机关区划  */
import * as React from 'react';
import { Table, Form, Select, Input, Button, Col, Row, Modal, Icon, Popver, Steps, notification } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import axios from 'axios'
import Api from '../../services/apis'
import { func } from 'prop-types';

const FormItem = Form.Item;
const Option = Select.Option;
const Step = Steps.Step;
const confirm = Modal.confirm;

@connect(({ user, loading }) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent']
}))

class registrationAuthorityArea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            loading: true,
            canEdit: true,
            tableData: [],
            current: 1,
            pageSize: 5,
            m_current: 1,
            m_pageSize: 5,
            submitFlag: '',
            confirmLoading: false,
            formName: '',
            formField: '',
            description: '',
            selectNameQueryInput: ''
        }
    }

    sendData = () => {
        const that = this;
        const searchValues = this.props.form.getFieldsValue();
        axios.post(Api.getOptionList, {
            currentPage: that.state.current,
            formField: "registrationAuthorityArea",
            selectName: that.state.selectNameQueryInput
        })
            .then((function (resp) {
                const data = resp.data;
                if (data.length === 0) {
                    notification.destroy();
                    notification['warning']({ message: '暂无数据' })
                }
                that.setState({
                    tableData: data.list,
                    totalSize: data.totalSize
                })
            }))
    }

    componentWillMount() {
        //请求数据
        this.sendData();

    }

    query = (e) => {
        e.preventDefault();
        this.setState({
            current: 1
        }, () => {
            this.sendData();
        })
    }

    pageChange = (page) => {
        this.setState({ current: page }, () => {
            this.sendData();
        })
    }

    resetForm = () => {
        this.props.form.resetFields();
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible })
    }

    //新建、修改开启模态框
    openModal = (type, id, selectName, selectCode, description, formName, formField) => {
        const that = this;
        this.setState({ submitFlag: type });
        if (type != 'add') {
            axios.post(Api.getOptionCodeList, {
                docId: id
            })
                .then(function (resp) {
                    const data = resp.data;
                    that.setModalVisible(true);
                    setTimeout(() => {
                        that.props.form.setFieldsValue({
                            selectName: selectName,
                            selectCode: selectCode,
                            description: description,
                            formName: formName
                        })
                    }, 0)
                    that.setState({
                        id,
                        selectName,
                        selectCode,
                        description,
                        formName,
                        formField
                    });
                })
        } else {
            that.setModalVisible(true);
            setTimeout(() => {
                this.props.form.setFieldsValue({
                    selectName: "",
                    selectCode: "",
                    description: ""
                })
            }, 0)
        }
    }

    //新建/修改
    modalSubmit = () => {
        const that = this;
        if (that.state.submitFlag == 'show') {
            that.setModalVisible(false);
            return;
        }
        that.setState({ confirmLoading: true })
        that.props.form.validateFields((err, values) => {
            if (!err) {
                let url = ''
                let param = {
                    id: that.state.id,
                    selectName: values.selectName,
                    selectCode: values.selectCode,
                    description: values.description,
                    formName: '企业登记机关区划',
                    formField: 'registrationAuthorityArea'
                }
                if (this.state.submitFlag == "add") {//新增
                    url = Api.addOption;
                } else if (this.state.submitFlag == "edit") {//更新
                    url = Api.editOption;
                }
                axios.post(url, param)
                    .then(function (resp) {
                        that.setState({ confirmLoading: false }, () => { that.sendData() })
                        const data = resp.data;
                        notification.destroy();
                        notification['success']({ message: data.msg });

                        that.setModalVisible(false);
                    })
            }
        })
    }
    getPositionList = (type, value) => {
        this.setState({
            type: value
        })
    }
    selectNameQuery = name => {
        this.setState({
            selectNameQueryInput: name.target.value
        })
    }

    render() {
        const that = this;
        const { form: { getFieldDecorator }, currentUserLoading } = this.props;

        const columns = [
            {
                title: '登记机关',
                key: 'selectName',
                dataIndex: 'selectName',
            },
            {
                title: '码值',
                key: 'selectCode',
                dataIndex: 'selectCode',
            },
            {
                title: '备注',
                key: 'description',
                dataIndex: 'description',
            },
            {
                title: '操作人',
                key: 'userName',
                dataIndex: 'userName',
            },
            {
                title: '操作时间',
                key: 'date',
                dataIndex: 'date',
            },
            {
                title: '操作',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a href="javascript:;" onClick={() => this.openModal("edit", record.id, record.selectName, record.selectCode, record.description, record.formName, record.formField)}>
                            编辑
                    </a>
                    </span>
                ),
            },
        ]



        return (
            <PageHeaderWrapper loading={currentUserLoading}>
                <div className='mainContainer'>
                    <div className='contentHeader'>
                        <div className='contentHeaderTitle'>
                            <p>企业登记机关区划列表</p>
                            <p>REGISTRATION AUTHORITY AREA LIST</p>
                        </div>
                    </div>

                    <div className='pom'>
                        <div className='mrb15'>
                            <Form layout='inline' align='center'>
                                <FormItem label="登记机关：">
                                    {
                                        getFieldDecorator('selectNameQuery')(
                                            <Input placeholder="请输入登记机关" onChange={this.selectNameQuery} />
                                        )
                                    }
                                </FormItem>

                                <Row gutter={24} style={{ textAlign: 'center' }}>
                                    <Button icon='search' type='primary' htmlType='submit' onClick={this.query}>搜索</Button>&emsp;
                                    <Button icon="plus-circle" type="primary" htmlType='button' onClick={() => this.openModal("add")}>新建</Button>&emsp;
                                    <Button icon="reload" type="primary" htmlType='button' onClick={this.resetForm}>重置</Button>
                                </Row>
                            </Form>
                        </div>

                        <Table
                            bordered={true}
                            scroll={{ x: true }}
                            columns={columns}
                            dataSource={this.state.tableData}
                            pagination={{
                                defaultPageSize: that.state.pageSize,
                                current: that.state.current,
                                onChange: that.pageChange,
                                total: that.state.totalSize,
                                showTotal: total => `共 ${total} 项`
                            }} />
                    </div>

                    <Modal
                        title={this.state.submitFlag == 'show' ? '登记机关详情' : (this.state.submitFlag == 'add' ? '新增登记机关' : '编辑登记机关')}
                        centered
                        visible={this.state.modalVisible}
                        onOk={this.modalSubmit}
                        confirmLoading={this.state.submitFlag == 'show' ? false : this.state.confirmLoading}
                        onCancel={() => this.setModalVisible(false)}
                        width={830}
                        className='oaomodal'
                    >
                        <Row gutter={24}>

                            <Col span={8}>
                                <FormItem label='登记机关：'>
                                    {getFieldDecorator('selectName', {
                                        rules: [{ required: true, message: '请输入登记机关！' },
                                    {max: 50, message: '输入内容不超过50字'}]
                                    })(<Input style={{ width: "100%" }} placeholder="请输入登记机关" onChange={(value) => this.getPositionList('selectName', value)} disabled={this.state.submitFlag == 'show' ? true : false}>
                                    </Input>)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem label='码值：'>
                                    {getFieldDecorator('selectCode', {
                                        rules: [{ required: true, message: '请输入码值！' },
                                        {max: 50, message: '输入内容不超过50字'}]
                                    })(<Input style={{ width: "100%" }} placeholder="请输入码值" onChange={(value) => this.getPositionList('selectCode', value)} disabled={this.state.submitFlag == 'edit' ? true : false}>
                                    </Input>)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem label='备注：'>
                                    {getFieldDecorator('description', {
                                        rules: [
                                        {max: 200, message: '输入内容不超过200字'}]
                                    })(<Input style={{ width: "100%" }} placeholder="请输入备注" onChange={(value) => this.getPositionList('description', value)} disabled={this.state.submitFlag == 'show' ? true : false}>
                                    </Input>)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem label='类型：'>
                                    {getFieldDecorator('type', {
                                        initialValue: "登记机关"
                                    })(<Select style={{ width: "100%" }} placeholder="请输入备注" disabled={true}>
                                        
                                    </Select>)}
                                </FormItem>
                            </Col>

                        </Row>

                    </Modal>
                </div>
            </PageHeaderWrapper>
        )
    }
}

const RecordWrappedApp = Form.create()(registrationAuthorityArea);

export default RecordWrappedApp;
