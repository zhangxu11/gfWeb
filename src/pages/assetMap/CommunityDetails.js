import React, { Component } from 'react';

import { Row, Col, Table, Button, Icon, Spin } from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import IframeComm from "../../components/IframeComm";
import 'rc-banner-anim/assets/index.css';
import styles from "./style.less";
import Link from 'umi/link';
import axios from 'axios';
import Api from '../../services/apis';
import QueueAnim from 'rc-queue-anim';

const BgElement = Element.BgElement;

const nopic = require('../../assets/nopic.png');

@connect(({ user, project, activities, chart, loading }) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent']
}))
class CommunityDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clickType: 'BusStation',
      poiData: [],
      headerData: [
        {
          tit: '交通',
          icon: 'environment',
          choiced: true,
          type: 'BusStation'
        },
        {
          tit: '学校',
          icon: 'team',
          choiced: false,
          type: 'Education'
        },
        {
          tit: '购物',
          icon: 'shopping-cart',
          choiced: false,
          type: 'Shopping'
        },
        {
          tit: '医院',
          icon: 'schedule',
          choiced: false,
          type: 'Hospital'
        },
        {
          tit: '公园',
          icon: 'bank',
          choiced: false,
          type: 'Park'
        },
        {
          tit: '餐饮',
          icon: 'cloud',
          choiced: false,
          type: 'Dinner'
        },
        {
          tit: '电影院',
          icon: 'thunderbolt',
          choiced: false,
          type: 'Cinema'
        },
      ],
      choicedIndex: 0,
      listData: [
        {
          title: '',
          distance: '',
          details: ''
        }
      ],
      communityId: '',
      data: {
        communityName: '',//别名
        address: '',//地址
        projectName: '',//项目民称
        projectCode: '',//code
        manageType: '',//管理方式
        obtainWay: '',//取得方式
        receiveDate: '',//接管日期
        waterFee: '',//水费
        electricFee: '',
        gasFee: '',
        propertyFee: '',
        heatingFee: '',
        buildingData: []
      },
      feesData: [
        {
          title: '水费',
          money: '-',
          icon: 'dashboard'
        },
        {
          title: '电费',
          money: '-',
          icon: 'thunderbolt'
        },
        {
          title: '供暖费',
          money: '-',
          icon: 'sliders'
        },
        {
          title: '物业费',
          money: '-',
          icon: 'money-collect'
        },
        {
          title: '燃气费',
          money: '-',
          icon: 'fire'
        },
      ],
      postMsg: {
        poiType: 'BusStation',
        data: ''
      },
      loading: true
    };
  }
  onReceiveMessage(data) {
    this.setState({
      loading: false,
      poiData: data.data
    });
  }

  changeList(index) {
    let headerData = this.state.headerData;
    headerData[this.state.choicedIndex].choiced = false;
    headerData[index].choiced = true;

    let postMsg = JSON.parse(JSON.stringify(this.state.postMsg));
    postMsg.poiType = headerData[index].type;
    postMsg.type = headerData[index].type;
    postMsg.data = this.state.comData;

    this.setState({
      loading: true,
      headerData,
      choicedIndex: index,
      clickType: headerData[index].type,
      postMsg
    });
  }
  componentWillMount() {
    console.log(this.props.location);
    
    this.setState({
      communityId: this.props.location.query.communityId
    });

    axios.get(Api.getCommunityInfo, {
      params: {
        communityId: this.props.location.query.communityId
      }
    }).then((res) => {
      let bulidingList = res.data.bulidingList;
      for (let i = 0, len = bulidingList.length; i < len; i++) {
        bulidingList[i].key = i;
      };
      let data = {
        communityName: res.data.communityName,//别名
        address: res.data.address,//地址
        projectName: res.data.project.projectName,//项目民称
        projectCode: res.data.project.projectCode,//code
        manageType: res.data.project.manageType,//管理方式
        obtainWay: res.data.project.obtainWay,//取得方式
        receiveDate: res.data.project.receiveDate,//接管日期
        transferUnit: res.data.project.transferUnit,//来源
        plotRatio: res.data.project.plotRatio ? (res.data.project.plotRatio).toFixed(2) : '',//容积率
        waterFee: res.data.fee.waterFee,//水费
        electricFee: res.data.fee.electricFee,
        gasFee: res.data.fee.gasFee,
        propertyFee: res.data.fee.propertyFee,
        heatingFee: res.data.fee.heatingFee,
        bulidingList,
        urlList: res.data.urlList && Array.isArray(res.data.urlList) ? res.data.urlList : []
      };
      let feesData = this.state.feesData;
      feesData[0].money = res.data.fee.waterFee;
      feesData[1].money = res.data.fee.electricFee;
      feesData[2].money = res.data.fee.heatingFee;
      feesData[3].money = res.data.fee.propertyFee;
      feesData[4].money = res.data.fee.gasFee;

      this.setState({
        data,
        feesData,
        comData: res.data,
        postMsg: {
          poiType: 'BusStation',
          data: res.data
        },
      });
    })
  }

  sendPOI(data) {
    this.setState({
      postMsg: {
        type: 'clickPOI',
        data
      }
    });
  }

  render() {
    const {
      currentUserLoading,
    } = this.props;

    const columns = [
      {
        title: '楼栋名称',
        dataIndex: 'buildingName2',
        key: 'buildingName2',
      },
      {
        title: '总层数',
        dataIndex: 'totalFloor',
        key: 'totalFloor',
        render: (item, record) => (Number(item) === 0 ? '-' : item)
      },
      {
        title: '总面积',
        dataIndex: 'buildingArea',
        key: 'buildingArea',
        render: (item, record) => (Number(item) === 0 ? '-' : item)
      },
      {
        title: '房产数',
        dataIndex: 'houseCount',
        key: 'houseCount',
        render: (item, record) => (Number(item) === 0 ? '-' : item)
      },
      {
        title: '容积率',
        dataIndex: 'plotRatio',
        key: 'plotRatio',
        render: (item, record) => (Number(item) === 0 ? '-' : item.toFixed(2))
      },
      {
        title: '操作',
        key: 'action',
        render: (item, record) => (

          <Link to={{
            pathname: '/fe/assetMap/buildingDetails',
            state: {
              projectCode: this.state.data.projectCode,
              buildingCode: item.buildingCode,
              buildingId: item.buildingId
            }
          }}>
            <Button type='primary'>
              详情
            </Button>
          </Link>
        ),
      },
    ];

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
      >
        <div className={styles.cdMain}>
          <div className={styles.cdHeader}>
            {this.state.data.communityName}
          </div>
          <Row>
            <div className={`${styles.cdHeaderLeft} pull-left`}>
              <BannerAnim prefixCls='buildingDetailsBannerItem' autoPlay>
              {
                  this.state.data.urlList && this.state.data.urlList.map((item, index) =>
                      <Element
                        key={index}
                      >
                        <BgElement
                          key="bg"
                          className={styles.bannerItemPic}
                          style={{
                            backgroundImage: `url(http://gisams-hitech.cindata.cn/res_file${item})`
                          }}
                        />
                      </Element>
                    )
                }
              </BannerAnim>
            </div>
            <div className={`${styles.cdHeaderRight} pull-left`}>
              <div className={styles.cdHeaderTop}>
                <p>别名：{this.state.data.communityName}</p>
                <p>地址：{this.state.data.address}</p>
              </div>
              <Row>
                <Col span={6} title={this.state.data.projectName}>
                  <span className='label'>项目名称：</span>{this.state.data.projectName}
                </Col>
                <Col span={6} title={this.state.data.projectCode}>
                  <span className='label'>项目编号：</span>{this.state.data.projectCode}
                </Col>
                <Col span={6} title={this.state.data.manageType}>
                  <span className='label'>管理方式：</span>{this.state.data.manageType}
                </Col>
                <Col span={6} title={this.state.data.obtainWay}>
                  <span className='label'>取得方式：</span>{this.state.data.obtainWay}
                </Col>
                <Col span={6} title={this.state.data.receiveDate}>
                  <span className='label'>接管日期：</span>{this.state.data.receiveDate}
                </Col>
                <Col span={6} title={this.state.data.transferUnit}>
                  <span className='label'>项目来源：</span>{this.state.data.transferUnit}
                </Col>
                <Col span={6} title='2001'>
                  <span className='label'>建成年份：</span>2001
                </Col>
                <Col span={6} title={this.state.data.plotRatio}>
                  <span className='label'>容积率：</span>{this.state.data.plotRatio}
                </Col>
              </Row>
            </div>
          </Row>
          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>楼栋列表</p>
                <p>BUILDING LIST</p>
              </div>
            </div>
          </div>
          <div className={styles.w980}>
            <Table
              bordered={true}
              scroll={{ x: true }}
              columns={columns}
              dataSource={this.state.data.bulidingList} />
          </div>
          {Number(sessionStorage.getItem('isMap')) === 1 ?
            <div>
              <div className='mainContainer'>
                <div className='contentHeader'>
                  <div className="contentHeaderTitle">
                    <p>周边配套</p>
                    <p>PERIPHERAL PACKAGE</p>
                  </div>
                </div>
              </div>
              <div className={`${styles.w980} ${styles.h500}`}>
                <IframeComm
                  attributes={{
                    src: "/gis/mapPOI.html",
                    style: {
                      width: 610,
                      height: '100%',
                      top: '0px',
                      left: '0px',
                      position: "absolute"
                    }
                  }}
                  postMessageData={this.state.postMsg}
                  handleReceiveMessage={this.onReceiveMessage.bind(this)}
                />
                <div className={styles.mapList}>
                  <div className={styles.mapListHeader}>
                    <Row className={styles.mapListHeaderRow}>
                      {this.state.headerData.map((item, index) => {
                        return (
                          <Col span={6} className={item.choiced ? styles.checked : ''} onClick={this.changeList.bind(this, index)} key={index}>
                            <Icon type={item.icon} />
                            <br />
                            {item.tit}
                          </Col>
                        );
                      })}
                    </Row>
                  </div>
                  <Spin spinning={this.state.loading}>
                    <div className={styles.mapListContent}>
                      <QueueAnim>
                        {this.state.poiData.map((item, index) => {
                          return (
                            <div className={styles.mapListItem} key={index} onClick={this.sendPOI.bind(this, item)}>
                              <div className='clearfix'>
                                <div className="pull-left">
                                  {item.popName.poiName}
                                </div>
                                <div className="pull-right">
                                  <Icon type='environment' />
                                  &nbsp;
                          {item.popName.distance}米
                        </div>
                              </div>
                              <p>
                                {item.popName.address}
                              </p>
                            </div>
                          );
                        })}
                      </QueueAnim>

                    </div>
                  </Spin>
                </div>
              </div>
            </div>
            :
            null
          }

          <div className='mainContainer'>
            <div className='contentHeader'>
              <div className="contentHeaderTitle">
                <p>欠费情况</p>
                <p>ARREARS</p>
              </div>
            </div>
          </div>
          <div className={styles.fees}>

            {this.state.feesData.map((item, index) => {
              return (
                <div key={index} className={styles.feesItem}>
                  <p>
                    <Icon type={item.icon} theme="filled"></Icon>
                  </p>
                  <p>
                    {item.money}元
                  </p>
                  <p>
                    {item.title}
                  </p>
                </div>
              );
            })}

          </div>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default CommunityDetails;
