//格式化树需要的数据
export const formatTreeData = (data,title,key,value,children) => {
	data.forEach((d, i, arr) => {
	        d.title = d[title] ? d[title] : null;
	        d.key = d[key] ? d[key] : i
	        d.value = d[value] ? d[value]+"" : ""
	        d.children = d[children] ? d[children] :null;
	        if(d.key.length < 7){
	        	d.selectable = false;
	        }
	        
	        if(d[children] && d[children].length!=0){
	            formatTreeData(d[children],title,key,value,children);
	        }
	    })

}